//
//  UITableViewCell+Extension.swift
//  City+
//
//  Created by Ivan Grab on 5/31/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    static var cellIdentifier: String {
        return String(describing: self.classForCoder())
    }
    
}

extension UICollectionViewCell {
    
    static var cellIdentifier: String {
        return String(describing: self.classForCoder())
    }
    
}
