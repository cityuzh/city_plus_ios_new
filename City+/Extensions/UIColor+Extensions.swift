//
//  UIColor+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 6/22/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

extension UIColor {
    
    // MARK: - Public Methods
    
    convenience init(r: Int, g: Int, b: Int) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: 1.0)
    }
    
    convenience init(r: Int, g: Int, b: Int, alpha: CGFloat) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: alpha)
    }
    
    static var newAppTintColor: UIColor {
        return UIColor(hex: 0x3B4DC4)
    }
    
    static var newLightBlueColor: UIColor {
        return UIColor(hex: 0xB0B8E8)
    }
    
    static var newMintTextColor: UIColor {
        return UIColor(hex: 0x2F8E9D)
    }
    
    static var appTintColor: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 184.0 / 255.0, blue: 186.0 / 255.0, alpha: 1.0)
    }
    
    static var jobTitleTintColor: UIColor {
        return UIColor(hex: 0x3B4DC4)
    }
    
    static var readedJobTitleTintColor: UIColor {
        return UIColor(hex: 0x899EDC)
    }
    
    
    static var startGradientColor: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 211.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    }
    
    static var startMenuGradientColor: UIColor {
        return UIColor(red: 126.0 / 255.0, green: 87.0 / 255.0, blue: 194.0 / 255.0, alpha: 1.0)
    }
    
    static var centerMenuGradientColor: UIColor {
        return UIColor(red: 77.0 / 255.0, green: 208.0 / 255.0, blue: 225.0 / 255.0, alpha: 1.0)
    }
    
    static var endMenuGradientColor: UIColor {
        return UIColor(red: 224 / 255.0, green: 247 / 255.0, blue: 250 / 255.0, alpha: 1.0)
    }
    
    static var endGradientColor: UIColor {
        return UIColor(red: 57.0 / 255.0, green: 73.0 / 255.0, blue: 133.0 / 255.0, alpha: 1.0)
    }
    
    static var lightAppTintColor: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 198.0 / 255.0, blue: 131.0 / 255.0, alpha: 1.0)
    }
    
}

// MARK: - UIColor+Hex

extension UIColor {
    
    convenience init(hexString: String, alpha: CGFloat = 1) {
        var hex: UInt32 = 0
        let scanner = Scanner(string: hexString)
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        scanner.scanHexInt32(&hex)
        self.init(hex: hex)
    }
    
    convenience init(hex: UInt32, alpha: CGFloat = 1) {
        self.init(red: CGFloat((hex >> 16) & 0xFF) / 255,
                  green: CGFloat((hex >> 8) & 0xFF) / 255,
                  blue: CGFloat(hex & 0xFF) / 255,
                  alpha: alpha)
    }
    
    convenience init(hexWithAlpha: UInt64) {
        self.init(red: CGFloat((hexWithAlpha >> 24) & 0xFF) / 255,
                  green: CGFloat((hexWithAlpha >> 16) & 0xFF) / 255,
                  blue: CGFloat((hexWithAlpha >> 8) & 0xFF) / 255,
                  alpha: CGFloat(hexWithAlpha & 0xFF) / 255)
    }
    
}
