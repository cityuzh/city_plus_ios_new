//
//  TimeInterval+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 7/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    var minutesPerSec: Int {
        return Int(self / 60.0)
    }
    
    var minutesPefMSec: Int {
        return Int(self / 60.0 / 100.0)
    }
    
}

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
