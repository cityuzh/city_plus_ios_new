//
//  UIImageView+ImageDownloading.swift
//  PAMP
//
//  Created by Ivan Grab on 6/23/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Nuke
import UIKit

extension UIImageView {
    
    typealias PlaceholderSettings = (placeholderContentMode: UIView.ContentMode?, placeholderTintColor: UIColor?)
    
    // MARK: - Public
    
    func setImage(with avatar: Avatar?, placeholder: UIImage? = nil, isThumb: Bool = false, using activityIndicatorStyle: UIActivityIndicatorView.Style? = .gray, placeholderSettings: PlaceholderSettings? = nil, completion: ((_ success: Bool) -> Void)? = nil) {
        let options = ImageLoadingOptions(placeholder: placeholder, transition: .fadeIn(duration: 0.3), failureImage: placeholder, failureImageTransition: .fadeIn(duration: 0.3), contentModes: .init(success: contentMode, failure: contentMode, placeholder: placeholderSettings?.placeholderContentMode ?? contentMode))
        if let image = avatar?.image {
            DispatchQueue.main.async {
                self.image = image
                completion?(true)
            }
        } else if let thumb = avatar?.thumbURL, isThumb {
            Nuke.loadImage(with: thumb, options: options, into: self, progress: nil) { (imageResponse, error) in
                completion?(error == nil)
            }
        } else if let imagePath = avatar?.urlPath {
            var url = URL(string: imagePath)
            if imagePath.contains("http://") || imagePath.contains("https://") {
                url = URL(string: imagePath)
            } else {
                url =  Configuration.restAPIURL.appendingPathComponent(imagePath)
            }
            guard let unwrappedUrl = url else {
                completion?(false)
                return
            }
            Nuke.loadImage(with: unwrappedUrl, options: options, into: self, progress: nil) { (response, error) in
                completion?(error == nil)
            }
        } else if let imagePath = avatar?.imageURL {
            Nuke.loadImage(with: imagePath, options: options, into: self, progress: nil) { (imageResponse, error) in
                completion?(error == nil)
            }
        } else if let placeholder = placeholder {
            if let placeholderContentMode = placeholderSettings?.placeholderContentMode {
                contentMode = placeholderContentMode
            }
            if let placeholderTintColor = placeholderSettings?.placeholderTintColor {
                tintColor = placeholderTintColor
            }
            DispatchQueue.main.async {
                self.image = placeholder
                completion?(false)
            }
        } else {
            DispatchQueue.main.async {
                self.image = placeholder
                completion?(false)
            }
        }
    }
    
    func setImage(with path: String?, placeholder: UIImage? = nil, using activityIndicatorStyle: UIActivityIndicatorView.Style? = .gray, placeholderSettings: PlaceholderSettings? = nil, completion: ((_ success: Bool) -> Void)? = nil) {
        
        var url: URL?
        if let path = path {
            if path.contains("http://") || path.contains("https://") {
                url = URL(string: path)
            } else {
                url =  Configuration.restAPIURL.appendingPathComponent(path)
            }
        }
        
        setImage(with: url, placeholder: placeholder, using: activityIndicatorStyle, completion: completion)
    }
    
    func setImage(with url: URL?, placeholder: UIImage? = nil, using activityIndicatorStyle: UIActivityIndicatorView.Style? = .gray, placeholderSettings: PlaceholderSettings? = nil, completion: ((_ success: Bool) -> Void)? = nil) {
        
        let originalContentMode = contentMode
        let originalTintColor = tintColor
        
        if placeholder != nil {
            if let placeholderContentMode = placeholderSettings?.placeholderContentMode {
                contentMode = placeholderContentMode
            }
            if let placeholderTintColor = placeholderSettings?.placeholderTintColor {
                tintColor = placeholderTintColor
            }
            DispatchQueue.main.async {
                self.image = placeholder
            }
        }
        
        let performCompletion: (_ success: Bool) -> Void = { [weak self] (success: Bool) in
            if success, placeholder != nil, let placeholderSettings = placeholderSettings {
                if placeholderSettings.placeholderContentMode != nil {
                    self?.contentMode = originalContentMode
                }
                if placeholderSettings.placeholderTintColor != nil {
                    self?.tintColor = originalTintColor
                }
            }
            if activityIndicatorStyle != nil {
                self?.removeActivityIndicator()
            }
            completion?(success)
        }
        
        guard let url = url else {
            performCompletion(false)
            return
        }
        
        if let activityIndicatorStyle = activityIndicatorStyle {
            let activityIndicator = UIActivityIndicatorView(style: activityIndicatorStyle)
            activityIndicator.center = CGPoint(x: bounds.midX, y: bounds.midY)
            activityIndicator.startAnimating()
            addSubview(activityIndicator)
        }
        
//        self.sd_setImage(with: url, placeholderImage: placeholder, options: [.retryFailed, .allowInvalidSSLCertificates, .delayPlaceholder]) { (image, error, cacheType, url) in
//            DispatchQueue.main.async {
//                self.image = image
//                performCompletion(image != nil)
//            }
//        }
        
//        UIImage.load(url: url) { [weak self] (image) in
//            guard let image = image else {
//                DispatchQueue.main.async {
//
//                }
//                return
//            }
//            DispatchQueue.main.async {
//                self?.image = image
//                performCompletion(true)
//            }
//        }
        
    }
    
    // MARK: - Private
    
    private func removeActivityIndicator() {
        for view in subviews {
            if view is UIActivityIndicatorView {
                view.removeFromSuperview()
            }
        }
    }
    
}

