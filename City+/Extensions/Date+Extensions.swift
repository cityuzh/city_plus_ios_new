//
//  Date+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 7/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation

extension Date {
    
    static var shortDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateStyle = .medium
        return formater
    }
    
    static var monthNameDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "MMM"
        return formater
    }
    
    static var monthDayNameDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "MMM. dd"
        return formater
    }
    
    static var timeDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "HH:mm"
        return formater
    }
    
    static var dayShortDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "EEE. dd MMM."
        return formater
    }
    
    var shortLocaleDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "dd MMM YYYY"
        formater.locale = NSLocale.current
        return formater
    }
    
    static var shortDayDateTimeFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "EEE. dd MMM. HH:mm"
        formater.locale = NSLocale.current
        return formater
    }
    
    static var longDayDateTimeFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "dd MMM. YYYY. HH:mm"
        formater.locale = NSLocale.current
        return formater
    }
    
    var cvvFormat: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "MM/YYY"
        return formater
    }
    
    var birthdayFormat: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "dd MMM YYY"
        formater.locale = NSLocale.current
        return formater
    }
    
    static func timeDateFormaterWith(format: FormatStrings) -> DateFormatter {
        let dateFormatter = timeDateFormater
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter
    }
    
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "рік тому" :
                "\(year)" + " " + "років тому"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "місяць тому" :
                "\(month)" + " " + "місяців тому"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "день тому" :
                "\(day)" + " " + "днів тому"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour) годину тому" : "\(hour) годин тому"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute) хвилину тому" : "\(minute) хвилин тому"
        } else {
            return "щойно"
        }
        
    }
    
    func getRemainingDays() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: Date(), to: self)
        var days = 0
        var hours = 0
        if let year = interval.year, year > 0 {
            for _ in 1...year {
                days += Calendar.current.range(of: .day, in: .year, for: self)?.count ?? 0
            }
        }
        if let month = interval.month, month > 0 {
            days += Calendar.current.range(of: .day, in: .month, for: self)?.count ?? 0
        }
        if let day = interval.day, day > 0 {
            days += day
        }
        if let hour = interval.hour, hour > 0 {
            hours += hour
        } else if let minute = interval.minute, minute > 0 {
            return "\(minute) " + NSLocalizedString("m", comment: "")
        } else {
            return NSLocalizedString("Moins de 1 m.", comment: "")
        }
        return "\(days)" + NSLocalizedString("j. ", comment: "") + NSLocalizedString("et ", comment: "") + "\(hours)" + NSLocalizedString("h", comment: "")
    }
    
    func formatedPubDate() -> String? {
        let dateFormatter = DateFormatter()
        if Calendar.current.isDateInToday(self) {
            dateFormatter.dateFormat = FormatStrings.hoursMinutes24.rawValue
        } else {
            dateFormatter.dateFormat = FormatStrings.monthShortDayYear.rawValue
        }
        
        return dateFormatter.string(from: self)
    }
    
}

extension Date {
    
    enum FormatStrings: String {
        case hoursMinutes24 = "HH:mm"
        case hoursMinutes12 = "hh:mm a"
        case monthDayYear = "MMMM d, yyyy"
        case monthShortDayYear = "MMM d, yyyy"
        case monthDayYearHoursMinutes12 = "MMMM d, yyyy hh:mm a"
        case hoursMinutesMonthDayYear12 = "hh:mm a MMMM d, yyyy"
        case hoursMinutesMonthDayYear24 = "HH:mm MMMM d, yyyy"
        case yearMonthDayHoursMinutesSeconds24 = "yyyy-MM-dd HH:mm:ss"
    }
    
}
