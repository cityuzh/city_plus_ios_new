//
//  Map+Data.swift
//  PAMP
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation
import ObjectMapper

extension Map {
    
    convenience init?(data: Data) {
        guard let jsonData = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any], let json = jsonData["data"] as? [String : Any]  else { return nil }
        self.init(mappingType: .fromJSON, JSON: json)
    }
    
    convenience init?(withoutdataKeyData: Data) {
        guard let json = (try? JSONSerialization.jsonObject(with: withoutdataKeyData, options: .allowFragments)) as? [String: Any] else { return nil }
        self.init(mappingType: .fromJSON, JSON: json)
    }
    
}
