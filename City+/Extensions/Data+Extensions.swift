//
//  Data+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation

extension Data {
    
    var prettyJSONString: String {
        if let json = try? JSONSerialization.jsonObject(with: self, options: .allowFragments), JSONSerialization.isValidJSONObject(json), let formatted = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), let string = String(data: formatted, encoding: .utf8) {
            return string
        }
        return String(data: self, encoding: .utf8) ?? ""
    }
    
}
