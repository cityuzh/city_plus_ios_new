//
//  UIFont+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 6/22/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func appRobotoRegularFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: ofSize)!
    }
    
    static func appRobotoBoldFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Bold", size: ofSize)!
    }
    
    static func appRobotoLightFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Light", size: ofSize)!
    }
    
    static func appRobotoMediumFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: ofSize)!
    }
    
    static func monteserratRegularFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Regular", size: ofSize)!
    }
    
    static func monteserratBoldFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Bold", size: ofSize)!
    }
    
    static func monteserratSemiBoldFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-SemiBold", size: ofSize)!
    }
    
    static func monteserratMediumFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Medium", size: ofSize)!
    }
    
}
