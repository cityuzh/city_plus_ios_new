//
//  Notification+Extension.swift
//  PAMP
//
//  Created by Ivan Grab on 7/12/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation

// MARK: - Notification

extension Notification.Name {
    
    static let LocalStoredUserUpdated = Notification.Name(rawValue: "LocalStoredUserUpdated")
    
}
