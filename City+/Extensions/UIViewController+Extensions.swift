//
//  UIViewController+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

extension UIViewController {
    
    var isVisible: Bool {
        return isViewLoaded && view.window != nil
    }
    
    func setupClearNavigationBar() {
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
    func setupGradientedNavigationBar() {
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .newAppTintColor
    }
}

// MARK: - Error handling

extension UIViewController {
    
    var noInternetConnectionHandler: (_ request: URLRequest) -> Void {
        return { [weak self] request in
            self?.showNoInternetConnectionAlert()
            self?.addNoInternetConnectionPlaceholder()
        }
    }
    
    var requestFailureHandler: (_ error: RESTClient.ErrorType, _ data: Data?, _ request: URLRequest) -> Void {
        return { [weak self] (error, data, request) in
            self?.handleFailedRequest(error: error, data: data)
        }
    }
    
    func showNoInternetConnectionAlert() {
        let message = NSLocalizedString("Підключення до Інтернету відсутнє. Перевірте і повторіть спробу", comment: "")
        let noInternetError = RESTClient.ErrorType.serverError(errorJSON: ["message" : message as AnyObject])
        guard presentedViewController == nil else {
            if let activity = presentedViewController as? ActivityViewController {
                activity.dismiss(animated: true, completion: {
                    ShowAlert.showErrorAlert(in: self, error: noInternetError)
                })
            } else {
                ShowAlert.showErrorAlert(in: self, error: noInternetError)
            }
            return
        }
        ShowAlert.showErrorAlert(in: self, error: noInternetError)
    }
    
    func addNoInternetConnectionPlaceholder() {
        guard let selTableViewController = self as? BaseTableViewController else { return }
        selTableViewController.placeholderMode = .noInternetConnection
        selTableViewController.tableView.backgroundView = selTableViewController.placeholderView
    }
    
    func handleFailedRequest(error: RESTClient.ErrorType, data: Data?) {
        guard presentedViewController == nil else {
            if let activity = presentedViewController as? ActivityViewController {
                activity.dismiss(animated: true, completion: {
                    ShowAlert.showErrorAlert(in: self, error: error)
                })
            }
            return
        }
        ShowAlert.showErrorAlert(in: self, error: error)
    }
    
    func errorMessageFrom(data: Data?) -> String {
        guard let data = data, data.count > 0, let errorJSON = Map(withoutdataKeyData: data)?.JSON as [String: AnyObject]? else {
            return NSLocalizedString("Щось пішло не так!", comment:"")
        }
        
        var message = ""
        if let serverErrorMessage = errorJSON["message"] as? String {
            message = serverErrorMessage + "\n"
            if let statusCode = errorJSON["status_code"] as? Int {
                message.append("Status Code: \(statusCode)\n")
            }
        } else if let dictionary = errorJSON as? [String: [String]] {
            for (_, value) in dictionary {
                for (_, shortMessage) in value.enumerated() {
                    message.append(shortMessage + "\n")
                }
            }
        } else if let remainingTime = errorJSON["remaining_time"] as? Int {
            let minutes = Int(remainingTime)
            message = NSLocalizedString("You are blocked for ", comment: "") + String(minutes) + NSLocalizedString(" minutes.", comment: "") + "\n"
        }
        let cuttedMessage = String(message.characters.dropLast())
        message = cuttedMessage.characters.count > 0 ? cuttedMessage : NSLocalizedString("Щось пішло не так!", comment:"")
        
        return message
    }

}
