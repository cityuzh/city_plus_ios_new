//
//  NSPersistentContainer.swift
//  PAMP
//
//  Created by Ivan Grab on 10/8/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import CoreData

extension NSPersistentContainer {
    // MARK: - Class Methods
    
    class func `default`() -> NSPersistentContainer {
        let persistentContainer = NSPersistentContainer(name: "CityPlus")
        
        persistentContainer.loadPersistentStores { (description, error) in
            if let error = error {
                print("> NSPersistentContainer: could not initialize a DB due to \(error.localizedDescription).")
            } else {
                print("> NSPersistentContainer: a DB directory has been set to \(NSPersistentContainer.defaultDirectoryURL().path).")
            }
        }
        
        return persistentContainer
    }
}
