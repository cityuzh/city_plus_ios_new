//
//  UIWindow+ReplaceStoryboard.swift
//  Ensura
//
//  Created by Ivan Grab on 8/28/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit


class BasicAnimationDelegate: NSObject, CAAnimationDelegate {
    
    var end: (() -> Void)?
    var begin: (() -> Void)?
    
    func animationDidStart(_ anim: CAAnimation) {
        if let beginBlock = begin {
            beginBlock()
        }
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if let endblock = end {
            endblock()
        }
    }
}

class ContainerViewController: UIViewController {
    
    var imageProvider: ImageProvider?
    var sessionManager: SessionManager?
    var appDelegate: AppDelegate?
    
    var statusBarHidden: Bool = false
    var statusBarStyle: UIStatusBarStyle = .default
    
    convenience init(statusBarStyle style: UIStatusBarStyle, hidden: Bool) {
        self.init()
        statusBarHidden = hidden
        statusBarStyle = style
        view.backgroundColor = UIColor.clear
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        setNeedsStatusBarAppearanceUpdate()
    }
    
    internal override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    internal override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    internal override var prefersStatusBarHidden: Bool {
        return statusBarHidden
    }
    
    func passDependenciesToChildViewController() {
        
    }
    
}

extension UIWindow {
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    static func rootViewController() -> UIViewController? {
        var result: UIViewController? = nil
        let windows = UIApplication.shared.windows.filter { (window: AnyObject) -> Bool in
            print(type(of: window) == UIWindow.self)
            return type(of: window) == UIWindow.self
        }
        for window in windows {
            if let controller = window.rootViewController {
                if controller.view.window != nil {
                    result = controller
                }
            }
        }
        return result
    }
    
    static func replaceStoryboard(name: UIStoryboard.Storyboard, completion: ((_ rootVC: UIViewController) -> Void)? = nil) {
        let storyboard = UIStoryboard.named(name.rawValue)
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        guard let newController = storyboard.instantiateInitialViewController() else {
            print("Cannot find instantiate Initial View Controller")
            return
        }
        window.setNewRootViewController(controller: newController, completion: completion)
    }
    

    private func setNewRootViewController(controller: UIViewController, completion: ((_ rootVC: UIViewController) -> Void)? = nil) {
        
        backgroundColor = UIColor.clear
        
        let animationDuration: CFTimeInterval = 0.7;
        
        guard let oldViewController = rootViewController else {
            return
        }
        
        var statusBarStyle = controller.preferredStatusBarStyle
        var statusBarHidden = controller.prefersStatusBarHidden
        
        if controller.isKind(of: UINavigationController.self) {
            if let rootController = controller.children.first {
                statusBarStyle = rootController.preferredStatusBarStyle
                statusBarHidden = rootController.prefersStatusBarHidden
            }
        } else if controller.isKind(of: UITabBarController.self) {
            let tabBarController = controller as! UITabBarController
            if let firstController = tabBarController.viewControllers?.first {
                statusBarStyle = firstController.preferredStatusBarStyle
                statusBarHidden = firstController.prefersStatusBarHidden
            }
        }
        
        
        let tempViewController = ContainerViewController(statusBarStyle: statusBarStyle, hidden: statusBarHidden)
        
        rootViewController = tempViewController
        oldViewController.removeFromParent()
        oldViewController.view.removeFromSuperview()
        
        
        tempViewController.addChild(controller)
        tempViewController.addChild(oldViewController)
        
        tempViewController.view.addSubview(controller.view)
        tempViewController.view.addSubview(oldViewController.view)
        
        let animationGroup = CAAnimationGroup()
        animationGroup.duration = animationDuration
        
        
        
        let positionAnimation = CABasicAnimation(keyPath: "position.y")
        positionAnimation.fromValue = oldViewController.view.layer.position.y
        positionAnimation.toValue = oldViewController.view.layer.bounds.height*1.5
        positionAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 1
        scaleAnimation.toValue = 1
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = 1
        opacityAnimation.toValue = 1
        opacityAnimation.duration = animationGroup.duration
        
        animationGroup.animations = [positionAnimation, scaleAnimation, opacityAnimation]
        
        let newViewControllerScaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        newViewControllerScaleAnimation.fromValue = 0.8
        newViewControllerScaleAnimation.toValue = 1.0
        newViewControllerScaleAnimation.duration = animationGroup.duration
        newViewControllerScaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        
        let newViewControllerOpacityAnimation = CABasicAnimation(keyPath: "opacity")
        newViewControllerOpacityAnimation.fromValue = 0.8
        newViewControllerOpacityAnimation.toValue = 1
        newViewControllerOpacityAnimation.duration = animationGroup.duration
        newViewControllerOpacityAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        
        
        let newViewControllerAnimationGroup = CAAnimationGroup()
        newViewControllerAnimationGroup.duration = animationGroup.duration
        
        
        let animationDelegate = BasicAnimationDelegate()
        
        
        animationDelegate.end = {
            () -> Void in
            oldViewController.view.removeFromSuperview()
//            controller.view.removeFromSuperview()
            
            oldViewController.removeFromParent()
//            controller.removeFromParentViewController()
            
            self.rootViewController = tempViewController
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                tempViewController.appDelegate = delegate
                tempViewController.passDependenciesToChildViewController()
            }
            completion?(controller)
        }
        
        oldViewController.view.layer.opacity = 0
        
        animationGroup.delegate = animationDelegate
        
        newViewControllerAnimationGroup.animations = [newViewControllerOpacityAnimation, newViewControllerScaleAnimation]
        
        controller.view.layer.add(newViewControllerAnimationGroup, forKey: "scale")
        oldViewController.view.layer.add(animationGroup, forKey: "transform")
        
        
    }
}

extension UIStoryboard {
    static func named(_ name: String) -> UIStoryboard {
        return UIStoryboard(name: name, bundle:nil)
    }
}

