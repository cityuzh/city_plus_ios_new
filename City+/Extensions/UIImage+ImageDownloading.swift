//
//  UIImage+ImageDownloading.swift
//  PAMP
//
//  Created by Ivan Grab on 6/23/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

typealias ImageCompletion = (_ image: UIImage?) -> Void

extension UIImage {
    
    private static let cachePath = "UIImage+ImageDownloading"
    private static let secondLevelCache = URLCache(memoryCapacity: 4 * 1024 * 1024, diskCapacity: 500 * 1024 * 1024, diskPath: cachePath)
    private static let firstLevelCache = NSCache<AnyObject, AnyObject>()
    
    private static var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = secondLevelCache
        return URLSession(configuration: configuration)
    }()
    
    // MARK: - Public
    
    func resizeTo(newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth/self.size.width
        let newHeight = self.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func fixedOrientation() -> UIImage {
        if imageOrientation == UIImage.Orientation.up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
            break
        case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi / 2))
            break
        case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
            break
        }
        
        switch imageOrientation {
        case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil,
                                       width: Int(size.width),
                                       height: Int(size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent,
                                       bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
    
    // MARK: - Static func
    
    class func load(url: URL, completion: @escaping ImageCompletion) {
        var request = URLRequest(url: url)
        request.cachePolicy = .returnCacheDataElseLoad
        
        if let image = UIImage.firstLevelCache.object(forKey: request as AnyObject) as? UIImage {
            completion(image)
        } else {
            let task = UIImage.session.dataTask(with: request) { (data, response, error) -> Void in
                guard let data = data, data.count > 0 else { return }
                
                let image = UIImage(data: data)
                if let image = image {
                    UIImage.firstLevelCache.setObject(image, forKey: request as AnyObject)
                }
                completion(image)
            }
            task.resume()
        }
    }
    
}

