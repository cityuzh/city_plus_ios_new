//
//  UIStoryboard+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    enum Storyboard: String {
        case Job
        case Main
        case Chats
        case Forum
        case Place
        case Events
        case Profile
        case Discount
        case CityOnline
        case Authentication
        case TouristPlaces
    }
    
    // MARK: - Convenience Initializers
    
    convenience init(_ storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    // MARK: - Public Methods
    
    func instantiateViewController<T: UIViewController>() -> T {
        guard let viewController = self.instantiateViewController(withIdentifier: String(describing: T.self)) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(String(describing: T.self)) ")
        }
        return viewController
    }
    
}
