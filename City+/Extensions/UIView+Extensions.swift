//
//  UIView+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 7/6/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

// MARK: - UIView+ActivityIndicator

extension UIView {
    
    // MARK: - Properties
    
    var isActive: Bool {
        get {
            return isUserInteractionEnabled
        }
        set {
            isUserInteractionEnabled = newValue
        }
    }
    
    private static var activityIndicatorAssociatedKey = "activityIndicatorAssociatedKey"
    
    private var activityIndicator: UIActivityIndicatorView? {
        get {
            return objc_getAssociatedObject(self, &UIView.activityIndicatorAssociatedKey) as? UIActivityIndicatorView
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &UIView.activityIndicatorAssociatedKey, newValue as UIActivityIndicatorView?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    private var newActivityIndicator: UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = UIColor.black
        return activityIndicator
    }
    
    // MARK: - Public
    
    func showwActivityIndicator() {
        DispatchQueue.main.async {
            self.isActive = false
            self.activityIndicator = self.newActivityIndicator
            self.activityIndicator!.startAnimating()
            self.activityIndicator!.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(self.activityIndicator!)
            
            let centerY = NSLayoutConstraint(item: self.activityIndicator!,
                                             attribute: NSLayoutConstraint.Attribute.centerY,
                                             relatedBy: NSLayoutConstraint.Relation.equal,
                                             toItem: self,
                                             attribute: NSLayoutConstraint.Attribute.centerY,
                                             multiplier: 1,
                                             constant: 0)
            self.superview?.addConstraint(centerY)
            
            let centerX = NSLayoutConstraint(item: self.activityIndicator!,
                                             attribute: NSLayoutConstraint.Attribute.centerX,
                                             relatedBy: NSLayoutConstraint.Relation.equal,
                                             toItem: self,
                                             attribute: NSLayoutConstraint.Attribute.centerX,
                                             multiplier: 1,
                                             constant: 0)
            self.superview?.addConstraint(centerX)
        }
    }
    
    func addShadow(with radius: CGFloat = 3, opacity: Float = 0.5, color: UIColor? = .black, shadowOfset: CGSize = .zero) {
        layer.shadowColor = color?.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = shadowOfset
        layer.shadowRadius = radius
        layer.masksToBounds = false
    }
    
    func roundedCorners(on corners: UIRectCorner, with radius: CGFloat) {
        let rect = self.bounds
        let maskPath = UIBezierPath.init(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = rect
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer;
    }
    
    func hideeActivityIndicator(andActivate activate: Bool = true) {
        DispatchQueue.main.async {
            self.isActive = activate
            guard let activityIndicator = self.activityIndicator else { return }
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
}

// Designable

extension UIView {
    
    @IBInspectable var layerMasksToBounds: Bool {
        set { layer.masksToBounds = newValue }
        get { return layer.masksToBounds }
    }
    
    @IBInspectable var isRoundedSides: Bool {
        set { layer.cornerRadius = newValue ? min(layer.bounds.width, layer.bounds.height) / 2.0 : 0 }
        get { return layer.cornerRadius == min(layer.bounds.width, layer.bounds.height) / 2.0 }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get { return self.layer.cornerRadius }
        set { self.layer.cornerRadius = newValue }
    }
    
    // MARK: - Border
    
    @IBInspectable var borderWidth: CGFloat {
        set { layer.borderWidth = newValue }
        get { return layer.borderWidth }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set { layer.borderColor = newValue?.cgColor }
        get { return UIColor(cgColor: layer.borderColor!) }
    }
    
    // MARK: - Shadow
    
    @IBInspectable var shadowOffset: CGSize {
        set { layer.shadowOffset = newValue }
        get { return layer.shadowOffset }
    }
    
    @IBInspectable var shadowOpacity: Float {
        set { layer.shadowOpacity = newValue }
        get { return layer.shadowOpacity }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        set { layer.shadowRadius = newValue }
        get { return layer.shadowRadius }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        set { layer.shadowColor = newValue?.cgColor }
        get { return UIColor(cgColor: layer.shadowColor!) }
    }
    
    @IBInspectable var isShadowPath: Bool {
        set { layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath }
        get { return layer.shadowPath != nil }
    }
    
}

extension UIView {
    
    // MARK: - Gradient
    
    func add(_ gradientLayer: CAGradientLayer, _ colors: (start: UIColor, end: UIColor), isHorizontal: Bool) {
        gradientLayer.opacity = 1.0
        gradientLayer.colors = [colors.start.cgColor, colors.end.cgColor]
        
        if isHorizontal {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        } else {
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        }
        
        gradientLayer.frame = bounds
        if let button: UIButton = self as? UIButton, let imageViewLayer: CALayer = button.imageView?.layer {
            button.layer.insertSublayer(gradientLayer, below: imageViewLayer)
        } else {
            layer.insertSublayer(gradientLayer, at: 0)
        }
    }
    
    func addGradient(top: UIColor,
                     bottom: UIColor){
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [top.cgColor,
                                bottom.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0,
                                           y: 0)
        gradientLayer.endPoint = CGPoint(x: 0,
                                         y: 1)
        self.layer.insertSublayer(gradientLayer,
                                  at: 0)
    }
}
