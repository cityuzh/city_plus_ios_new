//
//  String+Extensions.swift
//  PAMP
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

// MARK: - Validation

extension String {
    
    // MARK: - Properties
    
    var isValidEmail: Bool {
        return predicateWithRegularExpression(string: RegularExpression.email).evaluate(with: self)
    }
    
    var isValidPassword: Bool {
        return predicateWithRegularExpression(string: RegularExpression.password).evaluate(with: self)
    }
    
    var toNumber: NSNumber? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.number(from: self)
    }
    
    var toCurrency: NSNumber? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        return numberFormatter.number(from: self)
    }
    
    // MARK: - Public Methods
    
    func validateWithRegularExpression(string: String) -> Bool {
        return predicateWithRegularExpression(string: string).evaluate(with: self)
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)        
        return boundingBox.width
    }
}

// MARK: - Private Methods

private extension String {
    
    func predicateWithRegularExpression(string: String) -> NSPredicate {
        return NSPredicate(format: "SELF MATCHES %@", string)
    }
    
}

// MARK: - Constants

private extension String {
    
    struct RegularExpression {
        static let email = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        static let password = "^(?=.*[A-Z])(?=.*[!?@#$%^&+=.,\\\\-_*])([a-zA-Z0-9!?@#$%^&+=*.,\\\\-_]){6,24}$"
    }
    
}

// MARK: - Equal

extension String {
    
    static func areEqual(_ first: String?, _ second: String?) -> Bool {
        return first == second || first?.isEmpty == true && second == nil || first == nil && second?.isEmpty == true
    }
    
}
