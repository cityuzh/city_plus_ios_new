//
//  SizeTransform.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/27/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

open class SizeTransform: TransformType {

    open func transformFromJSON(_ value: Any?) -> CGSize? {
        let numberFormatter = NumberFormatter()
        numberFormatter.decimalSeparator = "."
        numberFormatter.allowsFloats = true
        
        if let valueJSON = value as? [String : Any], let width = valueJSON["width"] as? Double, let height = valueJSON["height"] as? Double {
            return CGSize(width: width, height: height)
        }
        return nil
    }
    
    open func transformToJSON(_ value: CGSize?) -> Any? {
        if let size = value {
            return [ "width" : size.width, "height" : size.height ]
        }
        return nil
    }
    
}
