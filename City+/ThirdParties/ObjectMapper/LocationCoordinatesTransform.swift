//
//  LocationCoordinatesTransform.swift
//  City+
//
//  Created by Ivan Grab on 3/8/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreLocation
import ObjectMapper

class LocationCoordinatesTransform: TransformType {

    open func transformFromJSON(_ value: Any?) -> CLLocationCoordinate2D? {
        let numberFormatter = NumberFormatter()
        numberFormatter.decimalSeparator = "."
        numberFormatter.allowsFloats = true
        
        if let valueString = value as? String {
            let coordinatesArray = valueString.components(separatedBy: ";")
            return CLLocationCoordinate2D(latitude: (coordinatesArray.first! as NSString).doubleValue, longitude: (coordinatesArray.last! as NSString).doubleValue)
        }
        return nil
    }
    
    open func transformToJSON(_ value: CLLocationCoordinate2D?) -> String? {
        if let coord = value {
            return "\(coord.latitude);\(coord.longitude)"
        }
        return nil
    }
    
}
