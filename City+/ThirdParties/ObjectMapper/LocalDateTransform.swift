//
//  LocalDateTransform.swift
//  City+
//
//  Created by Ivan Grab on 11/3/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

open class LocalDateTransform: DateFormatterTransform {
    
    public init() {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "dd.MM.YYYY"
        
        super.init(dateFormatter: formatter)
    }
 
    public func string(from date: Date) -> String? {
        return dateFormatter.string(from: date)
    }
    
}
