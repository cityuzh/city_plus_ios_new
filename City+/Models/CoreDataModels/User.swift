//
//  User.swift
//  PAMP
//
//  Created by Ivan Grab on 6/22/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit
import CoreData

enum AuthType: String {
    case email = "email"
    case phone = "phone"
    case facebook = "facebook"
}

@objc(User)
class User: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: String(describing: User.self))
    }
    
    // MARK: - Properties

    @NSManaged public var phone: String?
    @NSManaged public var name: String?
    @NSManaged public var email: String?
    @NSManaged public var gender: String?
    @NSManaged public var surname: String?
    @NSManaged public var imageURLString: String?
    @NSManaged public var identifier: NSNumber?
    
    @NSManaged public var additionalInfo: String?
    @NSManaged public var status: String?
    @NSManaged public var interests: String?
    
    @NSManaged fileprivate var authType: String?
    
    @NSManaged public var imageData: Data?
    @NSManaged public var birthday: Date?
    
    @NSManaged public var isPrivate: Bool
    
    @NSManaged public var isPushOnMessage: Bool
    @NSManaged public var isPushOnRequest: Bool
    @NSManaged public var isPushOnTopic: Bool
    @NSManaged public var isCanChangePassword: Bool
    
    @NSManaged public var chatBadgeCout: NSNumber?
    @NSManaged public var requestBadgeCout: NSNumber?
    @NSManaged public var forumBadgeCout: NSNumber?
    
    // MARK: - Computed

    var fullName: String {
        return (name ?? "") + " " + (surname ?? "")
    }
        
    var image: UIImage? {
        get {
            guard let imageData = imageData else { return nil }
            return UIImage(data: imageData)
        } set {
            guard let image = image else { return }
            imageData = image.jpegData(compressionQuality: 1)
        }
    }
    
    var authenticationType: AuthType {
        return AuthType(rawValue: self.authType ?? "") ?? .phone
    }
    
    var avatar: Avatar {
        get {
            let avatar = Avatar()
            avatar.urlPath = imageURLString
            avatar.image = image
            avatar.identifier = identifier?.intValue
            return avatar
        } set {
            image = avatar.image
            imageURLString = avatar.imageURL?.absoluteString
        }
    }
    
    // MARK: - Public
    
    func update(with json: [String : Any]) {
        
        if let name = json["first_name"] as? String {
            self.name = name
        }
        
        if let isCanChange = json["show_change_password"] as? Bool {
            self.isCanChangePassword = isCanChange
        }
        
        if let lastName = json["last_name"] as? String {
            self.surname = lastName
        }
        
        if let authType = json["auth_type"] as? String {
            self.authType = authType
        }
        
        if let isSocial = json["private"] as? Bool {
            self.isPrivate = isSocial
        }
        
        if let identifier = json["user"] as? Int {
            self.identifier = NSNumber(value: identifier)
        }
        
        if let interestString = json["interests"] as? String {
            self.interests = interestString
        }
        
        if let status = json["status"] as? String {
            self.status = status
        }
        
        if let about = json["about"] as? String {
            self.additionalInfo = about
        }
        
        if let email = json["email"] as? String {
            self.email = email
        }
        
        if let gender = json["sex"] as? String {
            self.gender = gender
        }
        
        if let images = json["photos"] as? [[String : Any]] {
            self.imageURLString = nil
            for image in images {
                if let avatarURL = image["image"] as? String, let bool = image["is_avatar"] as? Bool, bool {
                    self.imageURLString = avatarURL
                }
            }
            if self.imageURLString == nil, images.count > 0 {
                if let avatarURL = (images.first ?? [:])["image"] as? String {
                    self.imageURLString = avatarURL
                }
            }
        }

        if let birthday = json["birthday"] as? String {
            let formater = DateFormatter()
            formater.dateFormat = "yyyy-MM-dd"
            if let date = formater.date(from: birthday) {
                self.birthday = date
            }
        }
        
        if let bool = json["push_on_message"] as? Bool {
            self.isPushOnMessage = bool
        }
        
        if let bool = json["push_on_request"] as? Bool {
            self.isPushOnRequest = bool
        }
        
        if let bool = json["push_on_topic"] as? Bool {
            self.isPushOnTopic = bool
        }
        
        if let chatsUnread = json["unread_message_count"] as? Int {
            self.chatBadgeCout = NSNumber(value: chatsUnread)
        }
        
        if let friendNew = json["incoming_friend_requests_count"] as? Int {
            self.requestBadgeCout = NSNumber(value: friendNew)
        }
        
        if let forumChanges = json["unread_message_count"] as? Int {
            self.chatBadgeCout = NSNumber(value: forumChanges)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name.LocalStoredUserUpdated, object: nil)
        print("> CoreDataUser: User updated!")
        CoreDataManager.shared.saveContext()
    }
    
    // MARK: - Class methods
    
    class func coreDataObject() -> User? {
        guard let entityDescription = NSEntityDescription.entity(forEntityName: String(describing: User.self), in: CoreDataManager.shared.managedObjectContext) else {
            return nil
        }
        let user = User(entity: entityDescription, insertInto: CoreDataManager.shared.managedObjectContext)
        return user
    }
    
    class func lastUser() -> User? {
        return allUsers()?.first
    }
    
    class func deleteLastUser() {
        guard let lastuser  = lastUser() else { return }
        CoreDataManager.shared.managedObjectContext.delete(lastuser)
        CoreDataManager.shared.saveContext()
    }
    
    class func allUsers() -> [User]? {
        return CoreDataManager.shared.fetchAllObjects(className: String(describing: User.classForCoder())) as? [User]
    }
    
}
