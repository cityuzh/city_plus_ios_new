//
//  Comment.swift
//  City+
//
//  Created by Ivan Grab on 4/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class Comment: BaseNetworkModel, Mappable {

    //MARK: - Properties
    
    var text: String?
    
    var sender: UserModel?
    var receiver: UserModel?
    
    var createdAt: Date?
    var attachment: Avatar?
    
    var isSending: Bool = false
    
    var isMy: Bool {
        return AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue == sender?.identifier
    }
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        text <- map["text"]
        
        if map.mappingType == .fromJSON {
            identifier <- map["id"]
            
            let atachment = Avatar()
            atachment.urlPath <- map["attachment"]
            self.attachment = atachment
            
            createdAt <- (map["created_at"], ServerDateTransform())
            
            sender <- map["user"]
            receiver <- map["receiver"]
        }
    }
    
}

extension Comment {
    
    func post(for event: Event, with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.eventCommentsListPathFormat, event.identifier ?? 0)
            builder.method = .POST
            builder.body = .MappableObject(self, contentType: .ApplicationJSON)
        }, completionHandler: completion)
    }
    
    func delete(for event: Event, with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.eventCommentsDetailPathFormat, event.identifier ?? 0 , identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
}

extension Comment {
    
    struct Constants {
        static let commentsListRoute = "/comments/"
        
        static let eventCommentsListPathFormat = "/event/%d/comments/"
        static let eventCommentsDetailPathFormat = "/event/%d/comments/%d/"
    }
    
}
