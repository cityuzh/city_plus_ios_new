//
//  UserModel.swift
//  City+
//
//  Created by Ivan Grab on 02/114/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class UserModel: BaseNetworkModel {

    // MARK: - Properties
    
    var avatar: Avatar? {
        return photos.first
    }
    var firstName: String?
    var lastName: String?
    var birthday: Date?
    
    var status: String?
    var additionalInfo: String?
    
    var gender: Gender = .noDefine
    
    var requestID: Int?
    var isPrivate = false
    var isFriend = false
    var hasOutcomingRequest = false
    var hasIncomingRequest = false
    var isOnline = false
    
    var interests: String?
    var photos: [Avatar] = []
    
    var isBlockedMe = false
    var isBlockedByMe = false
    
    var phone: String?
    var email: String?
    
    var chatBadgeCout: Int = 0
    var requestBadgeCout: Int = 0
    var forumBadgeCout: Int = 0
    
    var isHasChanged: Bool {
        return chatBadgeCout > 0 || forumBadgeCout > 0 || requestBadgeCout > 0
    }
    
    // MARK: - Computed
    
    var isMe: Bool {
        return AppDelegate.shared.sessionManager.storedUser!.identifier!.intValue == identifier
    }

    var fullName: String {
        return (firstName ?? "") + " " + (lastName ?? "")
    }
    
    static func networkUser(with stored: User) -> UserModel {
        let user = UserModel()
        
        user.identifier = stored.identifier?.intValue
        user.firstName = stored.name
        user.lastName = stored.surname
        user.birthday = stored.birthday
        user.email = stored.email
        
        let avatar = Avatar()
        avatar.image = stored.image
        avatar.urlPath = stored.imageURLString
        if avatar.image != nil || avatar.urlPath != nil {
            user.photos = [avatar]
        }
        
        user.requestBadgeCout = stored.requestBadgeCout?.intValue ?? 0
        user.chatBadgeCout = stored.chatBadgeCout?.intValue ?? 0
        user.forumBadgeCout = stored.forumBadgeCout?.intValue ?? 0

        return user
    }
}

// MARK: - Mappable

extension UserModel: Mappable {

    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        identifier <- map["id"]
        requestID <- map["id"]
        if requestID == nil {        
            requestID <- map["request_id"]
        }
        if map.mappingType == .fromJSON {
            identifier <- map["user"]
            photos <- map["photos"]
            if photos.count == 0 {
                var chatURL: String?
                chatURL <- map["photos"]
                let avatar = Avatar()
                avatar.urlPath = chatURL
                photos = [avatar]
            }
            isFriend <- map["friend"]
            isPrivate <- map["private"]
            isOnline <- map["is_online"]
            hasOutcomingRequest <- map["outcoming_friend_request"]
            hasIncomingRequest <- map["incoming_friend_request"]

            isBlockedMe <- map["blocked_me"]
            isBlockedByMe <- map["blocked_by_me"]
            
            chatBadgeCout <- map["unread_message_count"]
            requestBadgeCout <- map["incoming_friend_requests_count"]
            forumBadgeCout <- map["has_incoming_requests"]
            
        }
        email <- map["email"]
        birthday <- (map["birthday"], ServerBirthdateTransform())
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        status <- map ["status"]
        gender <- (map["sex"], EnumTransform())
        
        isFriend <- map["is_friend"]
        
        phone <- map["phone"]
        
        interests <- map["interests"]

        additionalInfo <- map["about"]
    }
    
}

//MARK: - Validation

extension UserModel: Validatable {
    
    func validate() throws {
        guard let firstName = firstName, !firstName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Ім'я є обов'язковим!", comment: ""))
        }
        
        guard let lastName = lastName, !lastName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Прізвище є обов'язковим!", comment: ""))
        }
                
    }
    
}

//MARK: - Network Methods

extension UserModel {
    
    func addToBlackList(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.blackListPath
            builder.method = .POST
            builder.body = .JSON(JSON: ["block" : identifier ?? 0] as AnyObject)
        }, completionHandler: completion)
    }
    
    func deleteFromBlackList(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.blackListDetailPath, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func getProfile(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            if self.isMe {
                builder.path = Constants.profilePath
            } else {
                builder.path = String(format: Constants.profilePathFormat, identifier ?? 0)
            }
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            DispatchQueue.main.async {
                if self.isMe {
                    if let data = data, let JSON = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : AnyObject] {
                        AppDelegate.shared.sessionManager.storedUser?.update(with: JSON)
                    }
                }
                self.defaultSuccessHandler(data: data, request: request, response: response, completion: completion)
            }
        }) { (error, data, request, response) in
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
        }
    }
    
    func changePrivateState(to isPrivate: Bool, with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.profilePath
            builder.method = .PUT
            var json = [String : AnyObject]()
            json["private"] = isPrivate as AnyObject
            builder.body = .JSON(JSON: json as AnyObject)
        }, completionHandler: completion)
    }
    
    func sendFriendRequest(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.sendFriendRequestPathFormat, identifier ?? 0)
        }, completionHandler: completion)
    }
    
    func deleteFriend(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.removeFriendPathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func acceptFriendRequest(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.acceptFriendRequestPathFormat, requestID ?? (identifier ?? 0))
        }, completionHandler: completion)
    }
    
    func rejectFriendRequest(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.rejectFriendRequestPathFormat, requestID ?? (identifier ?? 0))
        }, completionHandler: completion)
    }
    
    func cancelFriendRequest(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.cancelMyFriendRequestPathFormat, requestID ?? (identifier ?? 0))
        }, completionHandler: completion)
    }
    
}

//MARK: - Constants

extension UserModel {
    
    enum Gender: String {
        case male = "man"
        case female = "woman"
        case noDefine = "none"
    }
    
    struct Constants {
        static let usersRoute = "/accounts/profile/search/"
        static let usersListPath = "/accounts/profile/search/"
        
        static let friendsListPath = "/friends/"
        static let friendRequestsListPath = "/friends/requests/incoming/"
        static let myRequestsListPath = "/friends/requests/outcoming/"
        
        static let sendFriendRequestPathFormat = "/friends/%d/add/"
        static let removeFriendPathFormat = "/friends/%d/delete/"
        
        static let acceptFriendRequestPathFormat = "/friends/request/%d/accept/"
        static let cancelMyFriendRequestPathFormat = "/friends/request/%d/reject/"
        static let rejectFriendRequestPathFormat = "/friends/request/%d/reject/"
        
        static let profileRoute = "/accounts/profile"
        static let profilePath = profileRoute + "/"
        static let profilePathFormat = profileRoute + "/%d/"
        static let likersPathFormat = "/topic/%d/comments/%d/liked_by/"
        
        static let blackListPath = "/accounts/blacklist/"
        static let blackListDetailPath = "/accounts/blacklist/%d/"
    }
    
}
