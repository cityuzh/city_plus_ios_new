//
//  Message.swift
//  PAMP
//
//  Created by Ivan Grab on 12/6/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit
import ObjectMapper

final class Message: BaseNetworkModel, Mappable, DateGroupeble  {
    
    // MARK: - Properties
    
    var text: String?
    var date: Date?
    
    var user: UserModel?
    var attachmentForSend: Attachment?
   
    var content: Avatar?
    var contentSize: CGSize?
    
    var messageType: MessageType = .text
    var socketEvent: SocketEvent = .sending
    
    var sendingID: String?

    var location: Location?
    
    // MARK: - Computed
    
    var isMy: Bool {
        return user?.identifier == AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue
    }
    
    var isRead = false {
        didSet {
            if isRead {
                
            }
        }
    }
    
    var isFailed: Bool {
        return socketEvent == .messageFailed
    }
    var isSending = false
    
    // MARK: - Mapable
    
    override init() {
        super.init()
        date = Date()
    }
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        text <- map["text"]
        sendingID <- map["request_id"]
        if map.mappingType == .toJSON {
            if messageType == .video || messageType == .image {
                let attach = attachmentForSend
                var fileID = attach?.fileId
                var type = attach?.mediaType.serverType
                fileID <- map["file"]
                type <- map["file_type"]
            } else if messageType == .location {
                
            }
        } else {
            identifier <- map["id"]
            user <- map["user"]
            location <- map["location"]
            
            let receivedContent = Avatar()
            receivedContent.urlPath <- map["attach"]
            receivedContent.identifier <- map["file"]
            if let _ = receivedContent.urlPath {
                self.content = receivedContent
            }
            
            let previewContent = Avatar()
            previewContent.urlPath <- map["attach_preview"]
            if let _ = previewContent.urlPath {
                self.content?.preview = previewContent
            }
            
            contentSize <- (map["image_size"], SizeTransform())
            
            isRead <- map["is_read"]
            
            messageType <- (map["message_type"], EnumTransform())
            socketEvent <- (map["event_type"], EnumTransform())
            
            date <- (map["created_at"], ServerDateTransform())            
        }
    }
    
}

// MARK: - Network

extension Message {
    
    func uploadImageAttachment(with completion: NetworkModelCompletion?) {
        guard let attachment = attachmentForSend else {
            print("> Attachment is nil")
            return
        }
        perform(request: { (builder) in
            builder.path = Constants.fileUploadPathFormat
            builder.method = .POST
            var params: [String : String] = [:]
            params["file_type"] = attachment.mediaType.serverType
            var urls = [String : NSURL]()
            var datas = [String : Data]()
            if let fileURL = attachment.fileURL {
                urls["file"] = fileURL as NSURL
                builder.body = URLRequestBody.MultipartFormData(params: params, URLs: urls, data: nil)
            } else if let image = attachment.image, let imageData = image.jpegData(compressionQuality: 1.0) {
                datas["file"] = imageData
                builder.body = URLRequestBody.MultipartFormData(params: params, URLs: nil, data: datas)
            }
        }, successHandler: { (data, request, response) in
            if let data = data, let JSON = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : AnyObject] {
                if let fileId = JSON["id"]  as? Int, let fileUrl = JSON["file"] as? String {
                    self.attachmentForSend?.fileId = fileId
                    let content = Avatar()
                    content.identifier = fileId
                    content.urlPath = fileUrl
                    self.attachmentForSend?.fileId = fileId
                    self.content = content
                    self.invoke(completionHandler: completion, success: true)
                } else {
                    self.invoke(completionHandler: completion, success: false)
                }
            } else {
                self.invoke(completionHandler: completion, success: false)
            }
        }) { (error, data, request, response) in
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
        }
    }
    
    func uploadVideoAttachment(with completion: NetworkModelCompletion?) {
        guard let attachment = attachmentForSend else {
            print("> Attachment is nil")
            return
        }
        perform(request: { (builder) in
            builder.path = Constants.fileUploadPathFormat
            builder.method = .POST
            var params: [String : String] = [:]
            params["file_type"] = attachment.mediaType.serverType
            var urls = [String : NSURL]()
            var datas = [String : Data]()
            if let fileURL = attachment.fileURL {
                if let thumb = attachment.thumbnail {
                    datas["preview"] = thumb.jpegData(compressionQuality: 1.0) ?? Data()
                }
                urls["file"] = fileURL as NSURL
                builder.body = URLRequestBody.MultipartFormData(params: params, URLs: urls, data: datas)
            } else if let image = attachment.image, let imageData = image.jpegData(compressionQuality: 1.0) {
                datas["file"] = imageData
                builder.body = URLRequestBody.MultipartFormData(params: params, URLs: nil, data: datas)
            }
        }, successHandler: { (data, request, response) in
            if let data = data, let JSON = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : AnyObject] {
                if let fileId = JSON["id"]  as? Int, let fileUrl = JSON["file"] as? String, let preview = JSON["preview"] as? String {
                    self.attachmentForSend?.fileId = fileId
                    let content = Avatar()
                    content.identifier = fileId
                    content.urlPath = fileUrl
                    
                    let previewAvatar = Avatar()
                    previewAvatar.urlPath = preview
                    content.preview = previewAvatar
                    
                    self.attachmentForSend?.fileId = fileId
                    self.content = content
                    self.invoke(completionHandler: completion, success: true)
                } else {
                    self.invoke(completionHandler: completion, success: false)
                }
            } else {
                self.invoke(completionHandler: completion, success: false)
            }
        }) { (error, data, request, response) in
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
        }
    }
    
}

// MARK: - Constants

extension Message {
    
    struct Constants {
        static let chatRoute = "/dialog/"
        static let messagesPathFormat = "/messages/dialog/%d/"
        
        static let fileUploadPathFormat = "/message/attach/"
    }
    
    enum SocketEvent: String {
        case sending
        case read = "read"
        case online = "online"
        case ofline = "offline"
        case typing = "user_typing"
        case typingStopped = "user_typing_stopped"
        case newMessage = "new_message"
        case messageSent = "message_sent"
        case messageFailed = "message_failed"
        case messageDeleted = "deleted"
        case messageDeletedForAll = "delete_for_all"
    }
    
    enum MessageType: String {
        case text
        case image
        case video
        case location
    }
    
}
