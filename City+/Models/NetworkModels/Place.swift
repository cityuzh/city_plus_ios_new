//
//  Place.swift
//  City+
//
//  Created by Ivan Grab on 3/5/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreLocation
import ObjectMapper

final class Place: BaseNetworkModel, Mappable {
    
    var category: PlaceCategory?
//    var subcategory: PlaceSubcategory?

    var rating: Int = 0
    var about: String?
    var phones: [String] = []

    var schedules: [Schedule] = []
    
    var address: String?
    var isBudget = false
    var isPremium = false
    var isFavorite = false
    var isFake = false
    var coordinates: CLLocationCoordinate2D?
    var site: String?
    var photos: [Avatar] = []
    var image: Avatar?
    var updatedAt: Date?
    var name: String?
    var touristDescription: String?
    var eventsCount: Int = 0
    
    var fullFormattedSchedule: String {
        var string = ""
        for day in schedules.sorted(by: { ($0.day?.rawValue)! < ($1.day?.rawValue)! }) {
            string += (day.fullFormatedWorkHours ?? "")
            string += "\n"
        }
        return string.isEmpty ? "Розклад відсутній!" : string
    }
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        identifier <- map["id"]
        eventsCount <- map["events_count"]
        rating <- map["rating"]
        isFake <- map["is_fake"]
        
        let category = PlaceCategory()
        category.name <- map["category_name"]
        category.identifier <- map["category"]
        self.category = category
        
//        let subcategory = PlaceSubcategory()
//        subcategory.name <- map["subcategory_name"]
//        subcategory.identifier <- map["subcategory"]
//        self.subcategory = subcategory

        about <- map["about"]
        isPremium <- map["premium"]
        isBudget <- map["budget"]
        isFavorite <- map["is_favorite"]
        phones <- map["phones"]
        address <- map["address"]
        site <- map["site"]
        photos <- map["photos2"]
        
        updatedAt <- (map["last_update"], ServerDateTransform())
        
        name <- map["name"]
        touristDescription <- map["description"]
        
        schedules <- map["schedule"]
        
        coordinates <- (map["coordinates"], LocationCoordinatesTransform())
        
    }
    
}

extension Place {
    
    func addToFavorite(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.favoritPlacePathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func removeFromFavorite(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.favoritPlacePathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func changeRating(to rating: Int, with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.ratingPathFormat, (identifier ?? 0), rating)
        }, completionHandler: completion)
    }
    
    func get(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.placeDetailPath, identifier ?? 0)
        }, completionHandler: completion)
    }
    
}


extension Place {
    
    struct Constants {
        static let placeRoute = "/place/"
        static let touristPlaceRoute = placeRoute + "by_tourism_categories/"
        static let placeDetailPath = "/place/%d/"
        static let placeFavoriteListPath = "/place/favorites/"
        static let placeFilterPath = placeRoute + "?"
        static let favoritPlacePath = placeRoute + "favorite/"
        static let favoritPlacePathFormat = "/place/%d/favorite/"
        static let ratingPathFormat = "/place/%d/set_rating/%d/"
    }
    
}
