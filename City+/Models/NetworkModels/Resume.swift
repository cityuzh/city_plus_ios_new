//
//  Resume.swift
//  City+
//
//  Created by Ivan Grab on 5/3/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class Resume: BaseNetworkModel, Mappable {
    
    
    var title: String?
    var workType: WorkType = .fullTime
    
    var createdAt: Date?
    var updatedAt: Date?
    
    var about: String?
    
    var salary: Int?
    
    var isPremium = false
    
    var isRead = false
    var isFavorite = false
    
    var ownerId: Int?
    var owner: UserModel? {
        didSet {
            ownerId = owner?.identifier
        }
    }
    
    var category: JobCategory?
    
    var isMy: Bool {
        return ownerId == AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue
    }
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        identifier <- map["id"]
        title <- map["title"]
        salary <- map["salary"]
        workType <- (map["work_type"], EnumTransform())
        about <- map["description"]
        
        if map.mappingType == .fromJSON {
            updatedAt <- (map["updated_at"], ServerDateTransform())
            createdAt <- (map["created_at"], ServerDateTransform())
            isPremium <- map["is_premium"]
            isFavorite <- map["is_favorite"]
            isRead <- map["is_read"]
            
            ownerId <- map["owner"]
            if ownerId == nil {
                owner <- map["owner"]
            }
            category <- map["category"]
        } else {
            var id = category?.identifier
            id <- map["category_id"]
        }
    }
    
}

extension Resume: Validatable {
    
    func validate() throws {
        
        guard !(title ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть назву!", comment: ""))
        }
        
        guard !(category?.name ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Виберіть категорію!", comment: ""))
        }
        
        guard !(about ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Додайте опис!", comment: ""))
        }
        
    }
    
}


extension Resume {
    
    func create(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.resumeRoute
            builder.method = .POST
            builder.body = .MappableObject(self, contentType: .ApplicationJSON)
        }, completionHandler: completion)
    }
    
    func update(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.resumeDetailPathFormat, identifier ?? 0)
            builder.method = .PUT
            builder.body = .MappableObject(self, contentType: .ApplicationJSON)
        }, completionHandler: completion)
    }
    
    func delete(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.resumeDetailPathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func addToFavorite(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.resumeFavoritePathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func removeFromFavorite(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.resumeFavoritePathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func markAsRead(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.resumeReadPathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
}


extension Resume {
    
    struct Constants {
        static let resumeRoute = "/resume/"
        static let resumeDetailPathFormat = resumeRoute + "%d/"
        static let resumeReadPathFormat =  resumeRoute + "%d/read/"
        static let resumeFavoriteListPath = resumeRoute + "my_favorite/"
        static let resumeMyListPath = resumeRoute + "my/"
        static let resumeFavoritePathFormat =  resumeRoute + "%d/favorite/"
        static let resumeFilterPath = resumeRoute + "?"
    }
    
}
