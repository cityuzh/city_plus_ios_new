//
//  Location.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/5/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreLocation
import ObjectMapper

final class Location: Mappable {
    
    //MARK: - Properties
    
    var longitude: CLLocationDegrees?
    var lattitude: CLLocationDegrees?
    var addresss: String?
    
    //MARK: - Lifecycle

    init() { }
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        lattitude <- map["lat"]
        longitude <- map["lng"]
        addresss  <- map["address"]
    }
    
}
