//
//  DiscountCatalog.swift
//  CityPlus
//
//  Created by Ivan Grab on 12/24/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class DiscountCatalog: BaseNetworkModel {

    var name: String?
    var image: Avatar?
    var userID: Int?
    var placeID: Int?
    var position: Int?
    var place: Place?
    var category: PlaceCategory?
    var isPublished = false
    var discountCount: Int = 0
    
    var isMy: Bool {
        return AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue == userID
    }
    
    /*
     {
     "id": 2,
     "category_name": "Cупермаркети",
     "name": "Вопак",
     "image": "http://185.87.199.40/media/discount_catalog_photos/kontrol_2012_vopak.jpg",
     "is_published": true,
     "user": 135,
     "place": 1286,
     "category": 5
     },
     */
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        placeID <- map["place"]
        if map.mappingType == .fromJSON {
            position <- map["position"]
            identifier <- map["id"]
            discountCount <- map["discounts_count"]
            isPublished <- map["is_published"]
            userID <- map["user"]
            
            let category = PlaceCategory()
            category.identifier <- map["category"]
            category.name <- map["category_name"]
            self.category = category
            
            let image = Avatar()
            image.urlPath <- map["image"]
            self.image =  image
        } else {
            var category = self.category?.identifier
            category <- map["category"]
        }
    }
    
}

extension DiscountCatalog: Mappable {}
extension DiscountCatalog {
    
    static func getCatalogsByCategories(with completion: @escaping ((_ catalogs: [DiscountCatalog])->())) {
        let instance = DiscountCatalog()
        instance.perform(request: { (builder) in
            builder.path = Constants.catalogsPath
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion([])
                }
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                guard let jsonArray = json as? [[String : Any]] else {
                    DispatchQueue.main.async {
                        completion([])
                    }
                    return
                }
                let catalags = Mapper<DiscountCatalog>().mapArray(JSONArray: jsonArray)
                DispatchQueue.main.async {
                    completion(catalags)
                }
            } catch {
                DispatchQueue.main.async {
                    completion([])
                }
            }
        }) { (error, data, request, response) in
            instance.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: { (success) in
                completion([])
            })
        }
    }
    
    static func getParentCategories(with completion: @escaping ((_ catalogs: [PlaceCategory])->())) {
        let instance = DiscountCatalog()
        instance.perform(request: { (builder) in
            builder.path = Constants.catalogsCategoriesPath
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            guard let data = data else {
                completion([])
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                guard let jsonArray = json as? [[String : Any]] else {
                    completion([])
                    return
                }
                let categories = Mapper<PlaceCategory>().mapArray(JSONArray: jsonArray)
                completion(categories.filter({ $0.parenId == nil }) )
            } catch {
                completion([])
            }
        }) { (error, data, request, response) in
            instance.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: { (success) in
                completion([])
            })
        }
    }
    
    static func getSubCategories(for categoryID: Int, with completion: @escaping ((_ catalogs: [PlaceCategory])->())) {
        let instance = DiscountCatalog()
        instance.perform(request: { (builder) in
            builder.path = Constants.catalogsCategoriesPath
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            guard let data = data else {
                completion([])
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                guard let jsonArray = json as? [[String : Any]] else {
                    completion([])
                    return
                }
                let categories = Mapper<PlaceCategory>().mapArray(JSONArray: jsonArray)
                completion(categories.filter({ $0.parenId == categoryID }) )
            } catch {
                completion([])
            }
        }) { (error, data, request, response) in
            instance.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: { (success) in
                completion([])
            })
        }
    }
    
    static func createSubCategories(for categoryID: Int, withName: String, with completion: @escaping ((_ category: PlaceCategory?)->())) {
        let instance = DiscountCatalog()
        instance.perform(request: { (builder) in
            builder.path = Constants.catalogsCategoriesPath
            builder.method = .POST
            var bodyJSON = [String : AnyObject]()
            bodyJSON["name"] = withName as AnyObject
            bodyJSON["parent"] = categoryID as AnyObject
            builder.body = .JSON(JSON: bodyJSON as AnyObject)
        }, successHandler: { (data, request, response) in
            guard let data = data else {
                completion(nil)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                guard let jsonObj = json as? [String : Any] else {
                    completion(nil)
                    return
                }
                let categories = Mapper<PlaceCategory>().map(JSON: jsonObj)
                completion(categories)
            } catch {
                completion(nil)
            }
        }) { (error, data, request, response) in
            instance.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: { (success) in
                completion(nil)
            })
        }
    }
    
    func create(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.method = .POST
            builder.path = Constants.catalogsRoute
            
            var bodyJSON = [String : String]()
            bodyJSON["name"] = self.name
            bodyJSON["category"] = "\(self.category?.identifier ?? 0)"
            bodyJSON["place"] = "\(self.placeID ?? 0)"
            builder.body = .MultipartFormData(params: bodyJSON, URLs: nil, data: ["image" : image!.image!.jpegData(compressionQuality: 1)!])
        }, completionHandler: completion)
    }
    
    func delete(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.method = .DELETE
            builder.path = String(format: Constants.catalogsPathFormat, identifier ?? 0)            
        }, completionHandler: completion)
    }
    
    func update(with updatedImage: UIImage?, completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.method = .PUT
            builder.path = String(format: Constants.catalogsPathFormat, identifier ?? 0)
            
            var bodyJSON = [String : String]()
            bodyJSON["name"] = self.name
            bodyJSON["category"] = "\(self.category?.identifier ?? 0)"
            bodyJSON["place"] = "\(self.placeID ?? 0)"
            if let image = updatedImage {
                builder.body = .MultipartFormData(params: bodyJSON, URLs: nil, data: ["image" : image.jpegData(compressionQuality: 1)!])
            } else {
                builder.body = .MultipartFormData(params: bodyJSON, URLs: nil, data: nil)
            }
        }, completionHandler: completion)
    }
    
}

extension DiscountCatalog: Validatable {
    
    func validate() throws {
        guard !(name ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть назву каталогу!", comment: ""))
        }
        
        guard category != nil else {
            throw ValueValidationError.required(NSLocalizedString("Виберіть категорію каталогу!", comment: ""))
        }
        
        guard placeID != nil else {
            throw ValueValidationError.required(NSLocalizedString("Виберіть місце!", comment: ""))
        }
        
        guard image != nil else {
            throw ValueValidationError.required(NSLocalizedString("Виберіть обкладинку каталогу!", comment: ""))
        }
    }
    
}

extension DiscountCatalog {
    
    struct Constants {
        static let catalogsRoute = "/discount-catalogs/"
        static let catalogsPathFormat = "/discount-catalogs/%d/"
        static let catalogsPath = "/discount-catalogs/by_categories/"
        static let catalogsCategoriesPath = "/discount-category/"
        static let myCatalogsPath = "/discount-catalogs/my/"
    }
    
}


