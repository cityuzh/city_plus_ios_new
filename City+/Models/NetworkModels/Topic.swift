//
//  Topic.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/11/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class Topic: BaseNetworkModel, Mappable {
    
    // MARK: - Properties
    
    var user: UserModel?
    var pubDate: Date?
    var createdAt: Date?
    
    var likesCount: Int = 0
    var commentCount: Int = 0
    
    var isLikedByMe = false
    
    var title: String?
    var topicContent: String?
    
    var attchments: [Avatar] = []
    
    var isMy: Bool {
        return user?.identifier == AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue
    }
    
    var lastLiked: UserModel?
    
    // MARK: - Mapable
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            identifier <- map["id"]
            isLikedByMe <- map["liked_by_me"]
            user <- map["user"]
            
            attchments <- map["photos"]
            
            likesCount <- map["likes_count"]
            commentCount <- map["comments_count"]
            pubDate <- (map["pub_date"], ServerDateTransform())
            createdAt <- (map["created_at"], ServerDateTransform())
            lastLiked <- map["last_like"]
        }
        
        title <- map["name"]
        topicContent <- map["description"]
        
    }
    
}

//MARK: - Validatable

extension Topic: Validatable {
    
    func validate() throws {
        
        guard !(title ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Тема обовязкова для заповнення!", comment: ""))
        }
        
        guard !(topicContent ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Текст обговорення обовязковий для заповнення!", comment: ""))
        }
    }
    
}

// MARK: - Network

extension Topic {
    
    func create(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = Constants.forumListRoute
            builder.method = .POST
            builder.body = .MappableObject(self, contentType: .ApplicationJSON)
        }, completionHandler: completion)
    }
    
    func update(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.forumDetailPathFormat, identifier ?? 0)
            builder.method = .PUT
            builder.body = .MappableObject(self, contentType: .ApplicationJSON)
        }, completionHandler: completion)
    }
    
    func get(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.forumDetailPathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func delete(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.forumDetailPathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func like(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.forumLikeDetailPathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func dislike(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.forumLikeDetailPathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func uploadPhoto(photo: UIImage, with completion: @escaping ((_ success: Bool, _ avatar: Avatar?)->())) {
        var avatar: Avatar? = Avatar()
        if let imadaData = photo.pngData() {
            avatar?.perform(request: { (builder) in
                builder.method = .POST
                builder.path = String(format: Constants.addPhotoPathFormat, identifier ?? 0)
                builder.body = URLRequestBody.MultipartFormData(params: nil, URLs: nil, data: ["image" : imadaData])
            }) { (success) in
                if !success {
                    avatar = nil
                }
                completion(success, avatar)
            }
        }
    }
    
    func deletePhoto(photoId: Int, with completion: @escaping ((_ success: Bool, _ avatar: Avatar?)->())) {
        var avatar: Avatar? = Avatar()
        avatar?.perform(request: { (builder) in
            builder.method = .DELETE
            builder.path = String(format: Constants.detailPhotoPathFormat, identifier ?? 0, photoId)
        }) { (success) in
            if !success {
                avatar = nil
            }
            completion(success, avatar)
        }
    }
}

// MARK: - Constants

extension Topic {
    
    struct Constants {
        static let forumListRoute = "/topic/"
        static let topicLikersListRouteFormat = "/topic/%d/liked_by/"
        static let myTopicsListRoute = "/topic/my/"
        static let forumDetailPathFormat = "/topic/%d/"
        static let addPhotoPathFormat = "/topic/%d/add_photo/"
        static let detailPhotoPathFormat = "/topic/%d/photo/%d/"
        static let forumLikeDetailPathFormat = "/topic/%d/like/"
    }
    
}
