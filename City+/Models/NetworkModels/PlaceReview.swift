//
//  PlaceReview.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/15/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class PlaceReview: BaseNetworkModel, Mappable {
    
    // MARK: - Properties
    
    var user: UserModel?
    var createdAt: Date?
    
    var rating: Int = 0
    var placeID: Int?
    
    var text: String?
    var attchment: Avatar?
    
    var isMy: Bool {
        return user?.identifier == AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue
    }
    
    var lastLiked: UserModel?
    
    // MARK: - Mapable
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            identifier <- map["id"]
            user <- map["user"]
            let atachment = Avatar()
            atachment.urlPath <- map["attach"]
            placeID <- map["place"]
            if atachment.urlPath != nil {
                self.attchment = atachment
            }
            createdAt <- (map["created_at"], ServerDateTransform())
        }
        text <- map["text"]
        rating <- map["rating"]
    }
    
}

extension PlaceReview {
    
    struct Constants {
        static let raviewPathFormat = "/place/%d/reviews/"
        static let addRaviewPathFormat = "/place/%d/add_reviews/"
    }
    
    func post(for place: Place, with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.addRaviewPathFormat, place.identifier ?? 0)
            builder.method = .POST
            var params = [String : Any]()
            params["text"] = self.toJSON()["text"]
            params["rating"] = self.toJSON()["rating"]
            if let image = attchment?.image {
                builder.body = .MultipartFormData(params: params, URLs: nil, data: ["attach" : image.jpegData(compressionQuality: 1)!])
            } else {
                builder.body = .MultipartFormData(params: params, URLs: nil, data: nil)
            }
        }, completionHandler: completion)
    }
    
}
