//
//  Event.swift
//  City+
//
//  Created by Ivan Grab on 3/21/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreLocation
import ObjectMapper

final class Event: BaseNetworkModel, Mappable, DateGroupeble {
    
    var date: Date?
    
    var isRead: Bool = true
    
    
    var name: String?
    var about: String?
    
    var totalAttendes: Int = 0
    var attendees: [UserModel] = []
    var address: String?
    var image: Avatar?
    
    var photos: [Avatar] = []
    
    var endDate: Date?
    var startDate: Date?
    
    var newPlace: String?
    
    var placeId: Int?
    var place: Place? {
        didSet {
            placeId = place?.identifier
        }
    }
    
    var coordinates: CLLocationCoordinate2D?
    
    var ownerID: Int?
    
    var isMy: Bool {
        return AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue == ownerID
    }
    
    var myAttendeeStatus: AttendeeStatus?
    var myAttendeeNotification: AttendeeNotification?
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        about <- map["about"]
        address <- map["address"]
        startDate <- (map["start_date"], ServerDateTransform())
        endDate <- (map["end_date"], ServerDateTransform())
        name <- map["name"]
        placeId <- map["place"]
        newPlace <- map["new_place"]
        if map.mappingType == .fromJSON {
            date <- (map["event_date"], EventServerDateTransform())
            attendees <- map["attendees"]
            totalAttendes <- map["total_attendee"]
            identifier <- map["id"]
            ownerID <- map["owner"]
            photos <- map["photos"]
            
            let image = Avatar()
            image.urlPath <- map["photo"]
            self.image = image
            if placeId == nil {
                place <- map["place"]
            }
            myAttendeeStatus <- (map["my_attendee_status"], EnumTransform())
            myAttendeeNotification <- (map["remind_in"], EnumTransform())
        } else {
            if newPlace != nil {
                var lat = coordinates?.latitude
                var lng = coordinates?.longitude
                lat <- map["lat"]
                lng <- map["lng"]
            }
        }
    }
    
}

extension Event: Validatable {
    
    func validate() throws {
        
        guard let _ = image else {
            throw ValueValidationError.required(NSLocalizedString("Виберіть фото!", comment: ""))
        }
        
        guard !(name ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть назву!", comment: ""))
        }
        
        guard let _ = startDate else {
            throw ValueValidationError.required(NSLocalizedString("Введіть дату початку!", comment: ""))
        }
        
        if let endDate = endDate {
            if startDate!.timeIntervalSince(endDate) > 0 {
                throw ValueValidationError.invalid(NSLocalizedString("Перевірте дату завершення!", comment: ""))
            }
        }

        guard !(about ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть опис події!", comment: ""))
        }
        
    }
    
}

extension Event {
    
    func changeAttendeStatus(to newStatus: AttendeeStatus, with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.eventAttendePathFormat, identifier ?? 0)
            builder.method = .POST
            builder.body = .JSON(JSON: ["attendee_status" : newStatus.rawValue, "remind_in" : 0] as AnyObject)
        }, completionHandler: completion)
    }
    
    func changeNotificationAttende(to newStatus: AttendeeNotification, with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.eventAttendePathFormat, identifier ?? 0)
            builder.method = .POST
            builder.body = .JSON(JSON: ["remind_in" : newStatus.rawValue] as AnyObject)
        }, completionHandler: completion)
    }
    
    func get(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.eventDetailPathFormat, identifier ?? 0)
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            self.defaultSuccessHandler(data: data, request: request, response: response, completion: completion)
        }) { (error, data, request, response) in
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
        }
    }
    
    func create(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.eventPath
            builder.method = .POST
            var bodyJSON = [String : String]()
            bodyJSON["name"] = name
            bodyJSON["about"] = about
            
            if placeId == nil {
                bodyJSON["new_place"] = newPlace ?? ""
                bodyJSON["lat"] = "\(coordinates?.latitude ?? 0)"
                bodyJSON["lng"] = "\(coordinates?.longitude ?? 0)"
            } else {
                bodyJSON["place_id"] = "\(placeId!)"
            }
            bodyJSON["start_date"] = ServerDateTransform().dateFormatter.string(from: startDate!)
            if let end = endDate {
                bodyJSON["end_date"] = ServerDateTransform().dateFormatter.string(from: end)
            }
            if let address = address {
                bodyJSON["address"] = address
            }
            builder.body = .MultipartFormData(params: bodyJSON, URLs: nil, data: ["photo" : image!.image!.jpegData(compressionQuality: 1)!])
        }, completionHandler: completion)
    }
    
    func update(with completion: NetworkModelCompletion? = nil, isImageChanged: Bool = false) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.eventDetailPathFormat, identifier ?? 0)
            builder.method = .PUT
            var bodyJSON = [String : String]()
            bodyJSON["name"] = name
            bodyJSON["about"] = about
            
            if placeId == nil {
                bodyJSON["new_place"] = newPlace ?? ""
            } else {
                bodyJSON["place_id"] = "\(placeId!)"
            }
            
            bodyJSON["start_date"] = ServerDateTransform().dateFormatter.string(from: startDate!)
            if let end = endDate {
                bodyJSON["end_date"] = ServerDateTransform().dateFormatter.string(from: end)
            }
            if let address = address {
                bodyJSON["address"] = address
            }
            var imageData = [String:Data]()
            if isImageChanged {
                imageData = ["photo" : image!.image!.jpegData(compressionQuality: 1)!]
            }
            builder.body = .MultipartFormData(params: bodyJSON, URLs: nil, data: imageData)
        }, completionHandler: completion)
    }
    
    func delete(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.eventDetailPathFormat, identifier ?? 0)
            builder.method = .DELETE
            builder.body = .MappableObject(self, contentType: .ApplicationJSON)
        }, completionHandler: completion)
    }
    
}


extension Event {

    enum AttendeeStatus: Int {
        case notGoing = 0
        case going
        case maybe
    }
    
    enum AttendeeNotification: Int {
        case notSend = 0
        case oneHourBefore
        case oneDayBefore
    }

    struct Constants {
        static let eventRoutePathFormat = "/place/%d/events/"
        static let eventFavoriteListPath = "/poster/favorite/"
        
        static let eventDetailPathFormat = "/events/%d/"
        static let myEventsPath = "/events/my/"
        static let eventPath = "/events/"
        static let dateEventPath = "/events/by_dates/"
        static let eventAttendePathFormat = "/event/%d/attendee/"
    }
    
}
