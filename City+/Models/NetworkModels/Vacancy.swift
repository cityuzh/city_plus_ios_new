//
//  Vacancy.swift
//  City+
//
//  Created by Ivan Grab on 4/26/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class Vacancy: BaseNetworkModel, Mappable {
    
    
    var site: String?
    var title: String?
    var workType: WorkType = .fullTime
    
    var createdAt: Date?
    var updatedAt: Date?
    
    var about: String?
    
    var salary: Int?
    var companyName: String?
    var requirements: String?
    
    var address: String?
    var isPremium = false
    
    var contactPerson = ContactPerson()
    
    var isRead = false
    var isFavorite = false
    
    var ownerId: Int?
    var owner: UserModel? {
        didSet {
            ownerId = owner?.identifier
        }
    }
    
    var category: JobCategory?
    
    var isMy: Bool {
        return ownerId == AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue
    }
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
    
        identifier <- map["id"]
        title <- map["title"]
        companyName <- map["company_name"]
        salary <- map["salary"]
        address <- map["address"]
        site <- map["site"]
        requirements <- map["requirements"]
        contactPerson <- map["contact_data"]
        workType <- (map["work_type"], EnumTransform())
        about <- map["description"]
        
        if map.mappingType == .fromJSON {
            updatedAt <- (map["updated_at"], ServerDateTransform())
            createdAt <- (map["created_at"], ServerDateTransform())
            isPremium <- map["is_premium"]
            isFavorite <- map["is_favorite"]
            isRead <- map["is_read"]
            
            ownerId <- map["owner"]
            if ownerId == nil {
                owner <- map["owner"]
            }
            category <- map["category"]
        } else {
            var id = category?.identifier
            id <- map["category_id"]
        }
    }
    
}

extension Vacancy: Validatable {
    
    func validate() throws {
        
        guard !(title ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть назву!", comment: ""))
        }
        
        guard !(category?.name ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Виберіть категорію!", comment: ""))
        }
        
        guard !(companyName ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть назву компанії!", comment: ""))
        }
        
        guard !(address ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть адресу компанії!", comment: ""))
        }
        
        guard !(address ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть адресу компанії!", comment: ""))
        }
        
        guard !(contactPerson.name ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть імя контактної особи!", comment: ""))
        }
        
        guard !(contactPerson.email ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть email для зв'язку!", comment: ""))
        }
        
        guard !(contactPerson.phone ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть номер телефону для зв'язку!", comment: ""))
        }
        
        guard !(about ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть опис вакансії!", comment: ""))
        }
        
    }
    
}


extension Vacancy {
    
    func create(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.vacancyRoute
            builder.method = .POST
            builder.body = .MappableObject(self, contentType: .ApplicationJSON)
        }, completionHandler: completion)
    }
    
    func update(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.vacancyDetailPathFormat, identifier ?? 0)
            builder.method = .PUT
            builder.body = .MappableObject(self, contentType: .ApplicationJSON)
        }, completionHandler: completion)
    }
    
    func delete(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.vacancyDetailPathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func addToFavorite(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.vacancyFavoritePathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func removeFromFavorite(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.vacancyFavoritePathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func markAsRead(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.vacancyReadPathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
}

enum WorkType: Int {
    case any = 0
    case fullTime
    case partTime
    case remote
    
    var title: String {
        switch self {
        case .any:
            return NSLocalizedString("Будь-який тип зайнятості", comment: "")
        case .fullTime:
            return NSLocalizedString("Повна зайнятість", comment: "")
        case .partTime:
            return NSLocalizedString("Часткова зайнятість", comment: "")
        case .remote:
            return NSLocalizedString("Віддалена", comment: "")
        }
    }
}

extension Vacancy {
    
    struct Constants {
        static let vacancyRoute = "/vacancy/"
        static let vacancyDetailPathFormat = vacancyRoute + "%d/"
        static let vacancyReadPathFormat =  vacancyRoute + "%d/read/"
        static let vacancyFavoriteListPath = vacancyRoute + "my_favorite/"
        static let vacancyMyListPath = vacancyRoute + "my/"
        static let vacancyFavoritePathFormat =  vacancyRoute + "%d/favorite/"
        static let vacancyFilterPath = vacancyRoute + "?"
    }
    
}
