//
//  TopicComment.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/16/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class TopicComment: BaseNetworkModel, Mappable {
    //MARK: - Properties
    
    var text: String?
    var topicId: Int?
    var sender: UserModel?
    var replier: UserModel?
    
    var createdAt: Date?
    var attachment: Avatar?
    
    var isSending: Bool = false
    
    var likesCount: Int = 0
    var isLikedByMe = false
    
    var isMy: Bool {
        return AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue == sender?.identifier
    }
    
    var lastLiked: UserModel?
    var replyUser: UserModel?
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        text <- map["text"]
        if map.mappingType == .fromJSON {
            identifier <- map["id"]
            topicId <- map["topic"]
            
            let atachment = Avatar()
            atachment.urlPath <- map["attach"]
            
            if atachment.urlPath != nil {
                self.attachment = atachment
            }
            
            
            createdAt <- (map["created_at"], ServerDateTransform())
            likesCount <- map["likes_count"]
            isLikedByMe <- map["liked_by_me"]
            sender <- map["user"]
            replier <- map["receiver"]
            lastLiked <- map["last_like"]
            replyUser <- map["reply_to"]
        } else {
            var id = replyUser?.identifier
            id <- map["reply_to_id"]
        }
    }
    
}

extension TopicComment {
    
    func post(for topic: Topic, with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.commentsListRouteFormat, topic.identifier ?? 0)
            builder.method = .POST
            var params = [String : String]()
            params["text"] = self.toJSON()["text"] as? String
            if replyUser != nil {
                params["reply_to_id"] = "\(replyUser!.identifier!)"
            }
            if let image = attachment?.image {
                builder.body = .MultipartFormData(params: params, URLs: nil, data: ["attach" : image.jpegData(compressionQuality: 1)!])
            } else {
                builder.body = .MultipartFormData(params: params, URLs: nil, data: nil)
            }
        }, completionHandler: completion)
    }
    
    func like(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.likeDislikePathFormat, topicId ?? 0, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func dislike(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.likeDislikePathFormat, topicId ?? 0, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func delete(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.commentCommentsDetailPathFormat, topicId ?? 0 , identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
}

extension TopicComment {
    
    struct Constants {
        static let commentsListRouteFormat = "/topic/%d/comments/"
        static let commentLikersListRouteFormat = "/topic/%d/comments/%d/liked_by/"
        static let likeDislikePathFormat = "/topic/%d/comments/%d/like/"
        static let commentCommentsDetailPathFormat = "/topic/%d/comments/%d/"
    }
    
}
