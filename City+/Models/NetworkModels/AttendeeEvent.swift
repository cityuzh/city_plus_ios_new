//
//  AttendeeEvent.swift
//  City+
//
//  Created by Ivan Grab on 4/12/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class AttendeeEvent: BaseNetworkModel, Mappable {
    
    var event: Event?
    var user: UserModel?
    var updatedAt = Date()
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        event <- map["event"]
        var attendeeStatus = Event.AttendeeStatus.notGoing
        attendeeStatus <- (map["attendee_status"], EnumTransform())
        event?.myAttendeeStatus = attendeeStatus
        user <- map["user"]
        updatedAt <- (map["updated_at"], ServerDateTransform())
    }
    
}

extension AttendeeEvent {
    
    struct Constants {
        static let myEventsPath = "/events/my_attendees/"
    }
    
}
