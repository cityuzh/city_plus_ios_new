//
//  Chat.swift
//  City+
//
//  Created by Ivan Grab on 5/25/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class Chat: BaseNetworkModel, Mappable  {
    
    // MARK: - Properties
    
    var user: UserModel?
    var lastDate: Date?
    var unreadCount: Int = 0
    var lastMessage: String?
    var attachment: Attachment?
    var isOnline: Bool {
        return user?.isOnline ?? false
    }
    
    var disableRead = false
    var isMuted = false
    var isReadStatusEnabled = false
    
    // MARK: - Mapable
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        identifier <- map["id"]
        if identifier == nil {
            identifier <- map["dialog"]
        }
        
        isReadStatusEnabled <- map["show_read"]
        
        isMuted <- map["push_off"]
        disableRead <- map["disable_read"]
        
        user <- map["partner"]
        lastMessage <- map["last_text"]
        unreadCount <- map["count_of_unread"]
        lastDate <- (map["last_date"], ServerDateTransform())
    }
    
}

// MARK: - Network

extension Chat {
    
    func create(for userID: Int, with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.startChatPathFormat, userID)
            builder.method = .GET
        }, completionHandler: completion)
    }
        
    func get(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.detailPathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func delete(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.detailPathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func enablePushNotification(_ enabled: Bool, with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.detailPathFormat, identifier ?? 0)
            builder.method = .PATCH
            builder.body = .JSON(JSON: ["push_off" : !enabled] as AnyObject)
        }, completionHandler: completion)
    }
    
    func enableReadStatus(_ enabled: Bool, with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.detailPathFormat, identifier ?? 0)
            builder.method = .PATCH
            builder.body = .JSON(JSON: ["disable_read" : !enabled] as AnyObject)
        }, completionHandler: completion)
    }
    
}

// MARK: - Constants

extension Chat {
    
    struct Constants {
        static let chatsListRoute = "/dialogs/"
        static let startChatPathFormat = "/dialog/user/%d/"
        static let detailPathFormat = "/dialog/%d/"
    }
    
}
