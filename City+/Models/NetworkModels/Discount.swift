//
//  Discount.swift
//  CityPlus
//
//  Created by Ivan Grab on 1/16/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class Discount:  BaseNetworkModel {
    
    var name: String?
    var image: Avatar?
    var discountInfo: String?
    var persentDiscount: Int = 0
    var oldPrice: Int = 0
    var newPrice: Int = 0
    
    
    var category: PlaceCategory?
    var catalogId: Int?
    var catalog: DiscountCatalog?
    
    var startDate: Date?
    var endDate: Date?
    
    var place: Place?
    var userID: Int?
    var isMy: Bool {
        return AppDelegate.shared.sessionManager.storedUser?.identifier?.intValue == userID
    }
    
    /*
     {
     "name": "string",
     "description": "string",
     "percent_discount": 0,
     "old_price": 0,
     "new_price": 0,
     "image": "string",
     "start_date": "string",
     "end_date": "string",
     "category": 0,
     "catalog": 0
     }
     */
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        identifier <- map["id"]
        
        name <- map["name"]
        discountInfo <- map["description"]
        userID <- map["user"]
        place <- map["place"]
        persentDiscount <- map["percent_discount"]
        oldPrice <- map["old_price"]
        newPrice <- map["new_price"]
        
        startDate <- (map["start_date"], ServerDateTransform())
        endDate <- (map["end_date"], ServerDateTransform())
        
        
        let image = Avatar()
        image.urlPath <- map["image"]
        self.image =  image
        
        let category = PlaceCategory()
        category.identifier <- map["category"]
        category.name <- map["category_name"]
        self.category = category
        
        catalogId <- map["catalog"]
    }
    
}

extension Discount: Validatable {
    
    func validate() throws {
        guard !(name ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть заголовок акції!", comment: ""))
        }
        
        guard category != nil else {
            throw ValueValidationError.required(NSLocalizedString("Виберіть категорію акції!", comment: ""))
        }
        
        guard !(discountInfo ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть опис акції!", comment: ""))
        }
        
        guard persentDiscount > 0 || !(discountInfo ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Введіть відсоток знижки!", comment: ""))
        }
        
//        guard oldPrice > 0 else {
//            throw ValueValidationError.required(NSLocalizedString("Введіть стару ціну товару!", comment: ""))
//        }
//        
//        guard newPrice > 0 else {
//            throw ValueValidationError.required(NSLocalizedString("Введіть ціну товару з урахуванням знижки!", comment: ""))
//        }
        
        guard startDate != nil else {
            throw ValueValidationError.required(NSLocalizedString("Введіть дату початку акції!", comment: ""))
        }
        
        guard endDate != nil else {
            throw ValueValidationError.required(NSLocalizedString("Введіть дату закінчення акції!", comment: ""))
        }
        
        guard startDate!.timeIntervalSince(endDate!) < 0 else {
            throw ValueValidationError.required(NSLocalizedString("Дата закінчення повинна бути пізніше дати початку!", comment: ""))
        }
        
        guard abs(startDate!.timeIntervalSince(endDate!)) <= 30 * 24 * 60 * 60 else {
            throw ValueValidationError.required(NSLocalizedString("Час дії акції не повинен перевищувати 30 днів!", comment: ""))
        }
        
        guard image != nil else {
            throw ValueValidationError.required(NSLocalizedString("Виберіть обкладинку акції!", comment: ""))
        }
    }
    
}

extension Discount: Mappable {}
extension Discount {
    
    func create(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = Constants.discountRoute
            builder.method = .POST
            
            var bodyJSON = [String : String]()
            bodyJSON["name"] = self.name
            bodyJSON["description"] = self.discountInfo
            bodyJSON["percent_discount"] = "\(self.persentDiscount)"
            bodyJSON["old_price"] = "\(self.oldPrice)"
            bodyJSON["new_price"] = "\(self.newPrice)"
            bodyJSON["start_date"] = ServerDateTransform().dateFormatter.string(from: self.startDate ?? Date())
            bodyJSON["end_date"] = ServerDateTransform().dateFormatter.string(from: self.endDate ?? Date())
            bodyJSON["category"] = "\(self.category?.identifier ?? 0)"
            bodyJSON["catalog"] = "\(self.catalogId ?? 0)"
            builder.body = .MultipartFormData(params: bodyJSON, URLs: nil, data: ["image" : image!.image!.jpegData(compressionQuality: 1)!])
        }, completionHandler: completion)
    }
    
    func getDetails(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.discountDetailPathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func delete(with completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.discountDetailPathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
    func update(with newImage: UIImage?, completion: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.discountDetailPathFormat, identifier ?? 0)
            builder.method = .PUT
            
            var bodyJSON = [String : String]()
            bodyJSON["name"] = self.name
            bodyJSON["description"] = self.discountInfo
            bodyJSON["percent_discount"] = "\(self.persentDiscount)"
            bodyJSON["old_price"] = "\(self.oldPrice)"
            bodyJSON["new_price"] = "\(self.newPrice)"
            bodyJSON["start_date"] = ServerDateTransform().dateFormatter.string(from: self.startDate ?? Date())
            bodyJSON["end_date"] = ServerDateTransform().dateFormatter.string(from: self.endDate ?? Date())
            bodyJSON["category"] = "\(self.category?.identifier ?? 0)"
            bodyJSON["catalog"] = "\(self.catalogId ?? 0)"
            if let image = newImage {
                builder.body = .MultipartFormData(params: bodyJSON, URLs: nil, data: ["image" : image.jpegData(compressionQuality: 1)!])
            } else {
                builder.body = .MultipartFormData(params: bodyJSON, URLs: nil, data: nil)
            }
        }, completionHandler: completion)
    }
    
}

extension Discount {
    
    struct Constants {
        static let discountRoute = "/discounts/"
        static let discountDetailPathFormat = "/discounts/%d/"
        static let discountListForCatalofPathFormat = "/discount-catalogs/%d/discounts/"
    }
    
}
