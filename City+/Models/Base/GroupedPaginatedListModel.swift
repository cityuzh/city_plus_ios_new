//
//  GroupedPaginatedListModel.swift
//  Shallet
//
//  Created by Ivan Grab on 07.09.16.
//  Copyright © 2016 Shallet. All rights reserved.
//

import UIKit
import ObjectMapper

final class GroupedPaginatedListModel<Element: Mappable>: PaginatedListModel<Element> where Element: DateGroupeble {
    
    // MARK: - Properties
    
    var sectionCount: Int {
        return groupedObjects.count
    }
    
    var isFreeExist = false
    var isEmptyExist = false
    var groupedObjects = [[Element]]()
    
    subscript(indexPath: IndexPath) -> Element {
        get {
            return groupedObjects[indexPath.section][indexPath.row]
        }
        set {
            groupedObjects[indexPath.section][indexPath.row] = newValue
        }
    }
    
    func indexPath(of element: Element) -> IndexPath? {
        for (sectionIndex, sectionObjects) in self.groupedObjects.enumerated() {
            if let rowIndex = sectionObjects.index(where: { $0.identifier == element.identifier }) {
                return IndexPath(row: rowIndex, section: sectionIndex)
            }
        }
        return nil
    }
    
    func tempIndexPath(of tempElement: Message) -> IndexPath? {
        for (sectionIndex, sectionObjects) in self.groupedObjects.enumerated() {
            if let rowIndex = sectionObjects.index(where: { ($0 as! Message).sendingID == tempElement.sendingID && tempElement.sendingID != nil }) {
                return IndexPath(row: rowIndex, section: sectionIndex)
            }
        }
        return nil
    }
    
    // MARK: - Lifecycle
    
    override init(path: String) {
        super.init(path: path)
    }
        
    override func clear() {
        super.clear()
        isFreeExist = false
        isEmptyExist = false
    }
    
    override func load(pageNumber: Int, completion: NetworkModelCompletion?) {
        state = .loading
        queryParams["offset"] = String(pageNumber * requestRowCount) as AnyObject?
        queryParams["limit"] = String(requestRowCount) as AnyObject?
        perform(request: build, successHandler: { [weak self] (data, request, response) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.currentPage = pageNumber
            if let data = data, data.count > 0 {
                do {
                    let JSON = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let JSONarray = (JSON as? [AnyHashable: Any])?["results"] as? [[String : Any]] {
                        let newObjects: [Element] = Mapper<Element>().mapArray(JSONArray: JSONarray) ?? []
                        if newObjects.count < strongSelf.requestRowCount {
                            strongSelf.state = .loadedAll
                        }
                        strongSelf.handle(newMappedObjects: newObjects, forPage: pageNumber)
                    }
                    strongSelf.invoke(completionHandler: completion, success: true)
                } catch {
                    print("Serialization Error: \(error)")
                    strongSelf.invoke(completionHandler: completion, success: false)
                }
            } else {
                strongSelf.lastChanges = nil
                strongSelf.state = .loadedAll
                strongSelf.invoke(completionHandler: completion, success: false)
            }
        }) { [weak self] (error, data, request, response) -> Void in
            self?.lastChanges = nil
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: nil)
            self?.invoke(completionHandler: completion, success: false)
        }
    }
    
    override func handle(newMappedObjects newObjects: [Element], forPage pageNumber: Int) {
        if pageNumber == 0 {
            if changesTrackingEnabled {
                let changes = trackChages(betweenOldArray: objects, andNewArray: newObjects)
                objects = newObjects
                lastChanges = changes
            } else {
                objects = newObjects
            }
        } else {
            if changesTrackingEnabled {
                let newObjects = removeDuplicates(with: newObjects)
                let range = objects.count..<(objects.count + newObjects.count)
                let doubleArray = objects + newObjects
                let insertedIndexPaths = range.map { getNewIndexPathForElement(doubleArray[$0], inEstimatedArray: doubleArray) }
                objects += newObjects
                lastChanges = ListModelChanges(insertedIndexPaths: insertedIndexPaths)
            } else {
                objects += newObjects
            }
        }
        groupedObjects = formGroupedArrayWithObjects(isReversed ? objects.reversed() : objects)
    }
    
    override func trackChages(betweenOldArray oldArray: [Element], andNewArray newArray: [Element]) -> ListModelChanges {
        var deletedIndexPaths = [IndexPath]()
        for element in oldArray {
            if !newArray.contains(where: { $0.identifier == element.identifier }) {
                for (section, oldElements) in formGroupedArrayWithObjects(oldArray).enumerated() {
                    for (oldIndex, _) in oldElements.enumerated() {
                        deletedIndexPaths.append(IndexPath(item: oldIndex, section: section))
                    }
                }
            }
        }
        
        var insertedIndexPaths = [IndexPath]()
        for (index, element) in newArray.enumerated() {
            if !oldArray.contains(where: { $0.identifier == element.identifier }) {
                var newListedArray = oldArray
                if index >= oldArray.count {
                    newListedArray.append(element)
                } else {
                    newListedArray.insert(element, at: index)
                }
                insertedIndexPaths.append(getNewIndexPathForElement(element, inEstimatedArray: newListedArray))
            }
        }
        var moves: [ListModelChanges.Move] = []
        var updatedIndexPaths = [IndexPath]()
        for (_, element) in newArray.enumerated() {
            if let indexInOldArray = oldArray.index(of: element) {
                let oldIndexPath = getNewIndexPathForElement(element, inEstimatedArray: oldArray)
                let newIndexPath = getNewIndexPathForElement(element, inEstimatedArray: newArray)
                if oldIndexPath != newIndexPath {
                    moves.append(ListModelChanges.Move(fromIndexPath: oldIndexPath, toIndexPath: newIndexPath))
                } else {
                    if element != oldArray[indexInOldArray] {
                        updatedIndexPaths.append(getNewIndexPathForElement(element, inEstimatedArray: oldArray))
                    }
                }
            }
        }
        return ListModelChanges(deletedIndexPaths: deletedIndexPaths, insertedIndexPaths: insertedIndexPaths, moves: moves, updatedIndexPaths: updatedIndexPaths)
    }
    
    // MARK: - Public
    
    func removeAtIndexPath(_ indexPath: IndexPath) -> Element {
        let element = groupedObjects[indexPath.section].remove(at: indexPath.row)
        if groupedObjects[indexPath.section].isEmpty {
            groupedObjects.remove(at: indexPath.section)
        }
        if let index = objects.index(of: element) {
            objects.remove(at: index)
        }
        lastChanges = ListModelChanges(deletedIndexPaths: [indexPath])
        return element
    }
    
    func rowsCountForSection(_ section: Int) -> Int {
        return groupedObjects[section].count
    }
    
    func headerTitleForSection(_ section: Int) -> String {
        if let header = groupedObjects[section].first?.getFormatedGroupedDateString() {
            if Calendar.current.isDateInToday(groupedObjects[section].first?.date ?? Date()) {
                return NSLocalizedString("Сьогодні", comment: "")
            }
            return header
        } else {
            return NSLocalizedString("Unknown error", comment: "")
        }
    }
    
    // MARK: - Private
    
    fileprivate func getNewIndexPathForElement(_ element: Element, inEstimatedArray: [Element]) -> IndexPath {
        let indexPath = IndexPath(row: 0, section: 0)
        let newGroupedArray = formGroupedArrayWithObjects(inEstimatedArray)
        for (section, oldElements) in newGroupedArray.enumerated() {
            for (newIndex, newElement) in oldElements.enumerated() {
                if newElement == element {
                    return IndexPath(row: newIndex, section: section)
                }
            }
        }
        return indexPath
    }
    
    fileprivate func formGroupedArrayWithObjects(_ objects: [Element]) -> [[Element]] {
        return formGroupedArrayForTrackModeWithObjects(objects)
    }
    
    fileprivate func formGroupedArrayForTrackModeWithObjects(_ objects: [Element]) -> [[Element]] {
        var groupedArray = [[Element]]()
        var elements = [Element]()
        var lastComponents: DateComponents?
        var lastReadStatus: Bool?
        let callendar = Calendar.current
        
        for (index, element) in objects.enumerated() {
//            if !element.isRead {
//                if let lastRead = lastReadStatus {
//                    if lastDateComponents.month != components.month || lastDateComponents.year != components.year || lastDateComponents.day != components.day {
//                        lastComponents = components
//                        appendGroupedArray(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: (index, element))
//                    } else {
//                        appendGroupedArrayIfNeeded(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: (index, element))
//                    }
//                } else {
//                    lastReadStatus = element.isRead
//                    appendGroupedArray(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: (index, element))
//                }
//            } else {
                if let elementDate = element.date {
                    let components = (callendar as NSCalendar).components([.month, .year, .day], from: elementDate as Date)
                    if let lastDateComponents = lastComponents {
                        if lastDateComponents.month != components.month || lastDateComponents.year != components.year || lastDateComponents.day != components.day {
                            lastComponents = components
                            appendGroupedArray(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: (index, element))
                        } else {
                            appendGroupedArrayIfNeeded(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: (index, element))
                        }
                    } else {
                        lastComponents = components
                        appendGroupedArray(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: (index, element))
                    }
                    
                } else {
                    lastComponents = nil
                    if groupedArray.count > 0 || elements.count > 0 {
                        appendGroupedArrayIfNeeded(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: (index, element))
                    } else {
                        appendGroupedArray(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: (index, element))
                    }
                }
//            }
        }
        
        groupedArray = groupedArray.reversed()
        var reversedGroupedArray = [[Element]]()
        for notReversedelements in groupedArray {
            reversedGroupedArray.append(notReversedelements.reversed())
        }
        
        return reversedGroupedArray
    }
    
    func appendGroupedArray(_ groupedArray: inout [[Element]], pendingElements elements: inout [Element], allObjects objects: [Element], currentData data: (index: Int, element: Element)) {
        if elements.count > 0 {
            groupedArray.append(elements)
        }
        elements.removeAll()
        
        appendGroupedArrayIfNeeded(&groupedArray, pendingElements: &elements, allObjects: objects, currentData: data)
    }
    
    func appendGroupedArrayIfNeeded(_ groupedArray: inout [[Element]], pendingElements elements: inout [Element], allObjects objects: [Element], currentData data: (index: Int, element: Element)) {
        elements.append(data.element)
        if data.index == objects.count - 1 {
            groupedArray.append(elements)
        }
    }
    
}
