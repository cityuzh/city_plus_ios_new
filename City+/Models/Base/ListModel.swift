//
//  ListModel.swift
//  PAMP
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation
import ObjectMapper

class ListModel<Element: Mappable>: BaseNetworkModel where Element: Identifiable {
    
    // MARK: - Properties
    
    var objects: [Element] = []
    var path = ""
    var dataKey: String? = "results"
    var method = URLRequestMethod.GET
    var queryParams: [String: AnyObject] = [:]
    var body: URLRequestBody?
    var changesTrackingEnabled = true
    var isReversed = false
    var initialSection = 0
    var lastChanges: ListModelChanges? {
        didSet {
            if lastChanges != nil {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: ListModelNotifications.didChange), object: self)
                }
            }
        }
    }
    
    var count: Int {
        return objects.count
    }
    
    subscript(index: Int) -> Element {
        get {
            return objects[index]
        }
        set {
            objects[index] = newValue
        }
    }
    
    // MARK: - Lifecycle
    
    init(path: String) {
        self.path = path
    }
    
    // MARK: - Public Overridable
    
    func load(completion: NetworkModelCompletion? = nil) {
        perform(request: build, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            
            guard let data = data, data.count > 0 else {
                strongSelf.clear()
                strongSelf.invoke(completionHandler: completion, success: false)
                return
            }
            
            do {
                var json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dataKey = strongSelf.dataKey, let dataArray = (json as? [String: Any])?[dataKey] {
                    json = dataArray
                }
                if let newObjects: [Element] = Mapper<Element>().mapArray(JSONObject: json) {
                    strongSelf.handle(newMappedObjects: strongSelf.isReversed ? newObjects.reversed() : newObjects)
                }
                strongSelf.invoke(completionHandler: completion, success: true)
            } catch {
                print("Serialization Error: \(error))")
                strongSelf.invoke(completionHandler: completion, success: false)
            }
            
        }) { [weak self] (error, data, request, response) in
            self?.lastChanges = nil
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
        }
    }
 
    func clear() {
        if changesTrackingEnabled {
            let range = 0..<objects.count
            let deletedIndexPaths = range.map { IndexPath(item: $0, section: initialSection) }
            lastChanges = ListModelChanges(deletedIndexPaths: deletedIndexPaths)
        }
        objects = []
    }
    
    func build(request builder: URLRequestBuilder) {
        builder.path = path
        builder.method = method
        builder.queryParams = queryParams
        builder.body = body
    }
    
    func trackChages(betweenOldArray oldArray: [Element], andNewArray newArray: [Element]) -> ListModelChanges {
        var deletedIndexPaths: [IndexPath] = []
        for (index, element) in oldArray.enumerated() {
            if !newArray.contains(where: { $0 == element }) {
                deletedIndexPaths.append(IndexPath(item: index, section: initialSection))
            }
        }
        
        var insertedIndexPaths: [IndexPath] = []
        if isReversed {
            for index in 0..<newArray.count {
                if !oldArray.contains(where: { $0 == newArray[index] }) {
                    insertedIndexPaths.append(IndexPath(item: index, section: initialSection))
                }
            }
        } else {
            for (index, element) in newArray.enumerated() {
                if !oldArray.contains(where: { $0 == element }) {
                    insertedIndexPaths.append(IndexPath(item: index, section: initialSection))
                }
            }
        }
        
        var moves: [ListModelChanges.Move] = []
        var updatedIndexPaths: [IndexPath] = []
        for (index, element) in newArray.enumerated() {
            if let indexInOldArray = oldArray.index(where: { $0 == element } ) {
                if index != indexInOldArray {
                    let fromIndexPath = IndexPath(row: indexInOldArray, section: initialSection)
                    let toIndexPath = IndexPath(row: index, section: initialSection)
                    moves.append(ListModelChanges.Move(fromIndexPath: fromIndexPath, toIndexPath: toIndexPath))
                } else {
                    if element != oldArray[indexInOldArray] {
                        updatedIndexPaths.append(IndexPath(row: index, section: initialSection))
                    }
                }
            }
        }
        return ListModelChanges(deletedIndexPaths: deletedIndexPaths, insertedIndexPaths: insertedIndexPaths, moves: moves, updatedIndexPaths: updatedIndexPaths)
    }
    
    func removeAtIndex(index: Int) -> Element {
        let element = objects.remove(at: index)
        lastChanges = ListModelChanges(deletedIndexPaths: [IndexPath(row: index, section: initialSection)])
        return element
    }
    
    func objects(at indexPaths: [IndexPath]) -> [Element] {
        var objects: [Element] = []
        for indexPath in indexPaths {
            objects.append(self.objects[indexPath.item])
        }
        return objects
    }
    
}

// MARK: - Public

extension ListModel {
    
    
}

// MARK: - Private

private extension ListModel {
    
    func handle(newMappedObjects newObjects: [Element]) {
        if changesTrackingEnabled {
            let changes = trackChages(betweenOldArray: objects, andNewArray: newObjects)
            objects = newObjects
            lastChanges = changes
        } else {
            objects = newObjects
        }
    }
    
}


// MARK: - Others

struct ListModelNotifications {
    static let didChange = "ListModelNotificationsDidChange"
}

struct ListModelChanges {
    
    var deletedIndexPaths: [IndexPath] = []
    var insertedIndexPaths: [IndexPath] = []
    var moves: [Move] = []
    var updatedIndexPaths: [IndexPath] = []
    
    var isEmpty: Bool {
        return deletedIndexPaths.isEmpty && insertedIndexPaths.isEmpty && moves.isEmpty && updatedIndexPaths.isEmpty
    }
    
    init(deletedIndexPaths: [IndexPath], insertedIndexPaths: [IndexPath], moves: [Move], updatedIndexPaths: [IndexPath]) {
        self.deletedIndexPaths = deletedIndexPaths
        self.insertedIndexPaths = insertedIndexPaths
        self.moves = moves
        self.updatedIndexPaths = updatedIndexPaths
    }
    
    init(deletedIndexPaths: [IndexPath]) {
        self.init(deletedIndexPaths: deletedIndexPaths, insertedIndexPaths: [], moves: [], updatedIndexPaths: [])
    }
    
    init(insertedIndexPaths: [IndexPath]) {
        self.init(deletedIndexPaths: [], insertedIndexPaths: insertedIndexPaths, moves: [], updatedIndexPaths: [])
    }
    
}

extension ListModelChanges  {
    
    struct Move {
        var fromIndexPath: IndexPath
        var toIndexPath: IndexPath
    }
    
}

