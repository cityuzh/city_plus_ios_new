//
//  PaginatedListModel.swift
//  PAMP
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation
import ObjectMapper

class PaginatedListModel<Element: Mappable>: ListModel<Element> where Element: Identifiable {
    
    // MARK: - Properties
    
    var requestRowCount = 25
    var state = PaginatedListModelState.canLoadMore
    var currentPage = 0
    
    // MARK: - Overridden
    
    override func load(completion: NetworkModelCompletion? = nil) {
        lastTask?.cancel()
//        objects.removeAll()
        load(pageNumber: 0, completion: completion)
    }

    
    func loadMore(completion: NetworkModelCompletion? = nil) {
        guard state == .canLoadMore else {
            completion?(false)
            return
        }
        load(pageNumber: currentPage + 1, completion: completion)
    }
    
    func invoke(completionHandler: NetworkModelCompletion?, success: Bool) {
        if state != .loadedAll {
            state = .canLoadMore
        }
        
        if let completionHandler = completionHandler {
            DispatchQueue.main.async {
                completionHandler(success)
            }
        }
    }
    
    func load(pageNumber: Int, completion: NetworkModelCompletion?) {
        state = .loading
        queryParams["offset"] = String(pageNumber * requestRowCount) as AnyObject?
        queryParams["limit"] = String(requestRowCount) as AnyObject?
        
        perform(request: build, successHandler: { [weak self] (data, request, response) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.currentPage = pageNumber
            
            guard let data = data, data.count > 0 else {
                strongSelf.lastChanges = nil
                strongSelf.state = .loadedAll
                strongSelf.invoke(completionHandler: completion, success: false)
                return
            }
            
            do {
                var json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dataKey = strongSelf.dataKey, let dataArray = (json as? [String: Any])?[dataKey] {
                    json = dataArray
                }
                if let newObjects: [Element] = Mapper<Element>().mapArray(JSONObject: json) {
                    if newObjects.count < strongSelf.requestRowCount {
                        strongSelf.state = .loadedAll
                    }
                    strongSelf.handle(newMappedObjects: newObjects, forPage: pageNumber)
                }
                strongSelf.invoke(completionHandler: completion, success: true)
            } catch {
                print("Serialization Error: \(error))")
                strongSelf.invoke(completionHandler: completion, success: false)
            }
            
        }) { [weak self] (error, data, request, response) -> Void in
            self?.lastChanges = nil
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: nil)
            self?.invoke(completionHandler: completion, success: false)
        }
    }
    
    func handle(newMappedObjects newObjects: [Element], forPage pageNumber: Int) {
        if pageNumber == 0 {
            if changesTrackingEnabled {
                let changes = trackChages(betweenOldArray: objects, andNewArray: newObjects)
                objects = isReversed ? newObjects.reversed() : newObjects
                lastChanges = changes
            } else {
                objects = isReversed ? newObjects.reversed() : newObjects
            }
        } else {
            if changesTrackingEnabled {
                let newObjects = removeDuplicates(with: newObjects)
                let range = isReversed ? 0..<newObjects.count : objects.count..<(objects.count + newObjects.count)
                let insertedIndexPaths = range.map { IndexPath(item: $0, section: initialSection) }
                if isReversed {
                    let oldRange = 0..<objects.count
                    let newRange = newObjects.count..<newObjects.count + objects.count
                    let oldIndexPaths = oldRange.map { IndexPath(item: $0, section: initialSection) }
                    let newIndexPaths = newRange.map { IndexPath(item: $0, section: initialSection) }
                    var moves = [ListModelChanges.Move]()
                    for i in oldRange {
                        moves.append(ListModelChanges.Move(fromIndexPath: oldIndexPaths[i], toIndexPath: newIndexPaths[i]))
                    }
                    objects.insert(contentsOf: newObjects.reversed(), at: 0)
                    lastChanges = ListModelChanges(insertedIndexPaths: insertedIndexPaths)
                    lastChanges?.moves = moves
                } else {
                    objects += newObjects
                    lastChanges = ListModelChanges(insertedIndexPaths: insertedIndexPaths)
                }
            } else {
                objects += newObjects
            }
        }
    }
    
    func removeDuplicates(with newObjects: [Element]) -> [Element] {
        var checkedObjects = [Element]()
        for object in newObjects {
            if !objects.contains(where: { $0 == object }) {
                checkedObjects.append(object)
            }
        }
        return checkedObjects
    }
    
}

// MARK: - Constants

enum PaginatedListModelState {
    case canLoadMore
    case loading
    case loadedAll
}
