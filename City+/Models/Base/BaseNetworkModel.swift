//
//  BaseNetworkModel.swift
//  PAMP
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation

class BaseNetworkModel: NSObject, NetworkModel, Identifiable {
    
    var identifier: Int?
    
    var lastTask: URLSessionTask?
    var lastRestClientError: RESTClient.ErrorType?
    var onNoInternetConnection: ((_ request: URLRequest) -> Void)?
    var onRequestFailure: ((_ error: RESTClient.ErrorType, _ data: Data?, _ request: URLRequest) -> Void)?
    
    var restClient: RESTClient? {
        return AppDelegate.shared.restClient
    }
    
}
