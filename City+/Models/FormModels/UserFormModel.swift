//
//  UserFormModel.swift
//  PAMP
//
//  Created by Ozzy   on 05.10.17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit

class UserFormModel {
 
    // MARK: - Properties
    var email = ""
    var phone = ""
    var password = ""
    
    var loginType: LoginType = .phone
}
