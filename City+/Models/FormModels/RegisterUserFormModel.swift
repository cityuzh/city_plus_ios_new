//
//  RegisterUserFormModel.swift
//  PAMP
//
//  Created by Ivan Grab on 10/10/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit

final class RegisterUserFormModel: UserFormModel {

    // MARK: - Properties
    var fbToken: String?
    var smsCode = ""
    var confirmPassword = ""
    
}

extension RegisterUserFormModel: Validatable {
    
    func validate() throws {
        
        do {
            try PasswordValidator().validate(password)
        } catch {
            throw RegisterUserFormError.InvalidPassword(NSLocalizedString("Пароль неправельний!", comment: ""))
        }
        
        do {
            try PasswordValidator().validate(confirmPassword)
        } catch {
            throw RegisterUserFormError.InvalidConfirmPassword(NSLocalizedString("Пароль неправельний!", comment: ""))
        }
        
        guard confirmPassword == password else {
            throw RegisterUserFormError.PasswordsDoNotMatch(NSLocalizedString("Паролі не співпадають!", comment: ""))
        }
        
    }
    
}

// MARK: - Errors

enum RegisterUserFormError: Error {
    
    case InvalidPassword(String)
    case InvalidConfirmPassword(String)
    case PasswordsDoNotMatch(String)
    
    
    var localizedDescription: String {
        switch self {
        case .InvalidPassword(let message):
            return message
        case .InvalidConfirmPassword(let message):
            return message
        case .PasswordsDoNotMatch(let message):
            return message
        }
    }
}

