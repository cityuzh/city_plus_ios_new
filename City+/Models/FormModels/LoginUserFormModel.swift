//
//  LoginUserFormModel.swift
//  City+
//
//  Created by Ivan Grab on 10/10/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit

final class LoginUserFormModel: UserFormModel {
    
    // MARK: - Properties    
}

extension LoginUserFormModel: Validatable {
    
    func validate() throws {
        
        if loginType == .phone {
            do {
                try PhoneValidator().validate(phone)
            } catch {
                let error = error as! ValueValidationError
                throw LoginUserFormError.InvalidPhone(error.localizedDescription)
            }
        } else {
            do {
                try EmailValidator().validate(email)
            } catch {
                let error = error as! ValueValidationError
                throw LoginUserFormError.InvalidEmail(error.localizedDescription)
            }
        }
        
        do {
            try PasswordValidator().validate(password)
        } catch {
            let error = error as! ValueValidationError
            throw LoginUserFormError.InvalidPassword(error.localizedDescription)
        }
    }
    
}

// MARK: - Errors

enum LoginUserFormError: Error {
    
    case InvalidPhone(String)
    case InvalidEmail(String)
    case InvalidPassword(String)

    var localizedDescription: String {
        switch self {
        case .InvalidPhone(let message):
            return message
        case .InvalidEmail(let message):
            return message
        case .InvalidPassword(let message):
            return message
        }
    }
}
