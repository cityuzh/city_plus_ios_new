//
//  Schedule.swift
//  City+
//
//  Created by Ivan Grab on 3/8/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class Schedule: Mappable, Identifiable {

    var identifier: Int?
    
    var day: Day?
    
    var timeStart: String?
    var timeEnd: String?
    
    var formatedWorkHours: String? {
        var string = ""
        if let startDay = day  {
            string += startDay.localisedDescription
        }
        if let startTime = timeStart  {
            string += " | "
            string += startTime
        }
        if let endTime = timeEnd {
            string += " - "
            string += endTime
        }
        return string.isEmpty ? nil : string
    }
    
    var fullFormatedWorkHours: String? {
        var string = ""
        if let startDay = day  {
            string += startDay.fullDescription
        }
        if let startTime = timeStart  {
            string += " \t|\t "
            string += startTime
        }
        if let endTime = timeEnd {
            string += " - "
            string += endTime
        }
        return string.isEmpty ? nil : string
    }
    
    //MARK: - Lifecycle

    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        day <- (map["day"], EnumTransform())
        timeEnd <- map["close"]
        timeStart <- map["open"]
    }
}

extension Schedule {
    
    enum Day: Int {
        case sunday = 0
        case monday = 1
        case tuesday = 2
        case wednesday = 3
        case thursday = 4
        case friday = 5
        case saturday = 6
        
        var fullDescription: String {
            switch self {
            case .sunday:
                return "Нд"
            case .monday:
                return "Пн"
            case .tuesday:
                return "Вт"
            case .friday:
                return "Пт"
            case .saturday:
                return "Сб"
            case .wednesday:
                return "Ср"
            case .thursday:
                return "Чт"
            }
        }
        
        var localisedDescription: String {
            switch self {
            case .sunday:
                return "НД"
            case .monday:
                return "ПН"
            case .tuesday:
                return "ВТ"
            case .friday:
                return "ПТ"
            case .saturday:
                return "СБ"
            case .wednesday:
                return "СР"
            case .thursday:
                return "ЧТ"
            }
        }
    }
    
}
