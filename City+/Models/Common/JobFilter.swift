//
//  JobFilter.swift
//  City+
//
//  Created by Ivan Grab on 5/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class JobFilter: NSObject {

    var category: JobCategory?
    var minimumSalary: Int?
    var workType: WorkType?
    
}
