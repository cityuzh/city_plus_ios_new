//
//  PlaceCategory.swift
//  City+
//
//  Created by Ivan Grab on 3/8/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class PlaceCategory: Mappable, Identifiable {

    var name: String?
    var identifier: Int?
    var parenId: Int?
    
    //MARK: - Mappable
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        identifier <- map["id"]
        parenId <- map["parent"]
    }
    
}

extension PlaceCategory {
    
    struct Constants {
        static let listPath = "/place/categories/"
    }
    
}
