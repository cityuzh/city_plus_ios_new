//
//  Attachment.swift
//  PetPals
//
//  Created by Ivan Grab on 11/2/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit
import Photos

final class Attachment: NSObject {

    var mediaType: MediaType = .photo

    var image: UIImage?
    var thumbnail: UIImage? {
        didSet {
            guard let thumb = thumbnail else { return }
            thumbCompletion?(thumb)
        }
    }
    
    var fileId: Int?
    
    var fileURL: URL?
    var data: Data?
    
    var thumbCompletion: ((_ image: UIImage)->())?
    
    convenience init(withAssetUrl: URL, mediaType: MediaType, thumbLoadCompletion: ((_ image: UIImage?)->())? = nil) {
        self.init()
        self.mediaType = mediaType
        let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [withAssetUrl], options: nil)
        if let photo = fetchResult.firstObject {
            if mediaType == .photo {
                PHImageManager.default().requestImage(for: photo, targetSize: CGSize(width: Configuration.MediaConstants.maxUploadImageSize, height: Configuration.MediaConstants.maxUploadImageSize), contentMode: PHImageContentMode.default, options: nil, resultHandler: { (image, info) in
                    
                    var resized = image
                    if resized?.size.width ?? 0 > Configuration.MediaConstants.maxUploadImageSize {
                        resized = image?.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)
                    }
                    if let image = resized {
                        let documentDirectory = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
                        let localPath: URL = documentDirectory.appendingPathComponent("ios_attachment_\(Date().timeIntervalSince1970).jpg")
                        
                        do {
                            let data = image.fixedOrientation().pngData()
                            try data?.write(to: localPath)
                        } catch {
                            print("> Attachment: Cant write image to file!")
                        }
                        
                        self.fileURL = localPath
                        self.image = image
                        DispatchQueue.main.async {
                            self.thumbnail = image
                            thumbLoadCompletion?(image)
                        }
                    }
                })
            } else {
                let asset = AVAsset(url: withAssetUrl)
                let duration = asset.duration
                let snapshot = CMTimeMake(value: duration.value / 2, timescale: duration.timescale)
                let generator = AVAssetImageGenerator(asset: asset)
                generator.appliesPreferredTrackTransform = true
                do {
                    let imageRef =  try generator.copyCGImage(at: snapshot, actualTime: nil)
                    let thumb = UIImage(cgImage: imageRef)
                    
                    var resized = thumb.fixedOrientation()
                    if resized.size.width > Configuration.MediaConstants.maxUploadImageSize {
                        resized = thumb.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
                    }
                    let documentDirectory = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
                    let localPath: URL = documentDirectory.appendingPathComponent("ios_attachment_preview\(Date().timeIntervalSince1970).jpg")
                    
                    do {
                        let data = thumb.fixedOrientation().pngData()
                        try data?.write(to: localPath)
                    } catch {
                        print("> Attachment: Cant write image to file!")
                    }
                    
                    self.fileURL = withAssetUrl
                    DispatchQueue.main.async {
                        self.thumbnail = resized
                        thumbLoadCompletion?(resized)
                    }
                } catch {
                    print(error)
                }
            }
        } else {
            let asset = AVAsset(url: withAssetUrl)
            let duration = asset.duration
            let snapshot = CMTimeMake(value: duration.value / 2, timescale: duration.timescale)
            let generator = AVAssetImageGenerator(asset: asset)
            generator.appliesPreferredTrackTransform = true
            do {
                let imageRef =  try generator.copyCGImage(at: snapshot, actualTime: nil)
                let thumb = UIImage(cgImage: imageRef)
                
                var resized = thumb.fixedOrientation()
                if resized.size.width > Configuration.MediaConstants.maxUploadImageSize {
                    resized = thumb.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
                }
                
                let documentDirectory = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
                let localPath: URL = documentDirectory.appendingPathComponent("ios_attachment_preview\(Date().timeIntervalSince1970).jpg")
                
                do {
                    let data = resized.fixedOrientation().pngData()
                    try data?.write(to: localPath)
                } catch {
                    print("> Attachment: Cant write image to file!")
                }
                
                self.fileURL = withAssetUrl
                DispatchQueue.main.async {
                    self.thumbnail = resized
                    thumbLoadCompletion?(resized)
                }
            } catch {
                print(error)
            }
        }
        
        
    }
    
    convenience init(withFileUrl: URL, mediaType: MediaType) {
        self.init()
        fileURL = withFileUrl
        do {
            data = try Data.init(contentsOf: withFileUrl)
        } catch {
            print("> Attachment: Cant read data from fileURL: \(withFileUrl.absoluteString) !")
        }
        self.mediaType = mediaType
        if mediaType == .photo, let data = data {
            if let image = UIImage(data: data) {
                var resized = image
                if resized.size.width > Configuration.MediaConstants.maxUploadImageSize {
                    resized = image.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
                }
                self.image = resized
                self.thumbnail = image.resizeTo(newWidth: 100)
            }
        }
    }
}

extension Attachment {
    
    enum MediaType {
        case photo
        case video
        
        var serverType: String {
            switch self {
            case .photo:
                return "image"
            case .video:
                return "video"
            }
        }
        
        var mimeType: String {
            switch self {
            case .photo:
                return "image/jpeg"
            case .video:
                return "video/quicktime"
            }
        }
    }
    
}
