//
//  Country.swift
//  PAMP
//
//  Created by Ivan Grab on 11/17/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit
import ObjectMapper

final class Country: BaseNetworkModel {
    
    var name = ""
    var callingCodes = [String]()
    var nativeName = ""
    var alpha2Code = ""
    var alpha3Code = ""
    
    // MARK: - Lifecycle
    
    override init() {
        super.init()
    }
    
    // MARK: - Mappable
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        callingCodes <- map["callingCodes"]
        nativeName <- map["nativeName"]
        alpha2Code <- map["alpha2Code"]
        alpha3Code <- map["alpha3Code"]
    }

}

extension Country: Mappable { }
extension Country {
    
    static func getCountrie(with completioh: @escaping ((_ countries: [Country])->())) {
        if let filePath = Bundle.main.path(forResource: "countries", ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.allowFragments) as? [[String:Any]] {
                    let couyntires = Mapper<Country>().mapArray(JSONArray: json)
                    DispatchQueue.main.async {
                        completioh(couyntires)
                    }
                }
            } catch {
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    completioh([])
                }
            }
        } else {
            DispatchQueue.main.async {
                completioh([])
            }
        }
    }
    
}

extension Country: NSCopying {
    
    func copy(with zone: NSZone? = nil) -> Any {
        let country = Country()
        country.name = name
        return country
    }
    
}

extension Country {
    
    struct Constants {
        static let countryPath = "/resources/countries"
        static let baseImageURL = AppDelegate.shared.restClient.baseURL.deletingLastPathComponent().absoluteString
    }

}
