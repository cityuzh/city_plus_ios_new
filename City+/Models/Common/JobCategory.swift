//
//  JobCategory.swift
//  City+
//
//  Created by Ivan Grab on 4/26/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class JobCategory: BaseNetworkModel, Mappable {
    
    var name: String?
    
    //MARK: - Mappable
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        identifier <- map["id"]
    }
    
    static func allCategories(with completion: @escaping (_ categories: [JobCategory]?)->()) {
        let instance = JobCategory()
        instance.perform(request: { (builder) in
            builder.path = Constants.categotyRoute
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            if let dataJSON = try? JSONSerialization.jsonObject(with: data ?? Data(), options: .allowFragments), let jsonArray = dataJSON as? [[String : Any]] {
                let array = Mapper<JobCategory>().mapArray(JSONArray: jsonArray)
                if array.count > 0 {
                    DispatchQueue.main.async {
                        completion(array)
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }) { (error, data, request, response) in
            instance.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: { (success) in
                DispatchQueue.main.async {
                    completion(nil)
                }
            })
        }
    }
    
}

extension JobCategory {
    
    struct Constants {
        static let categotyRoute = "/job_category/"
    }
    
}
