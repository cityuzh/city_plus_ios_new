//
//  ContactPerson.swift
//  City+
//
//  Created by Ivan Grab on 4/26/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class ContactPerson: BaseNetworkModel, Mappable {
    
    var name: String?
    var email: String?
    var phone: String?
    
    //MARK: - Mappable
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
    }
    
}
