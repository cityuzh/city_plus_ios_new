//
//  Avatar.swift
//  PAMP
//
//  Created by Ivan Grab on 7/5/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class Avatar: BaseNetworkModel, Mappable {
    
    // MARK: - Properties
    
    var urlPath: String?
    var imageURL: URL?
    var thumbPath: String?
    var thumbURL: URL?
    var image: UIImage?
    var isThumbnail = false
    var isAvatar = false
    var preview: Avatar?
    
    var referenceURL: String? {
        didSet {
            if let reference = referenceURL {
                imageURL = URL(string: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=500&photoreference=\(reference)&key=\("AIzaSyADtEWeM29WZ6jhsrPzmfNQ_XJwjzDXps4")")
                thumbURL = URL(string: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=150&photoreference=\(reference)&key=\("AIzaSyADtEWeM29WZ6jhsrPzmfNQ_XJwjzDXps4")")
            }
        }
    }
    var size: CGSize?
    var width: CGFloat?
    var height: CGFloat?
    // MARK: - Lifecycle
    
    override init() {
        super.init()
    }
    
    // MARK: - Mappable
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        identifier <- map["id"]
        urlPath <- map["image"]
        isAvatar <- map["is_avatar"]
        thumbPath <- map["thumbnail"]
        if thumbPath != nil {
            thumbURL = URL(string: thumbPath!)
        }
        if urlPath == nil {
            referenceURL <- map["photo_reference"]
            width <- map["width"]
            height <- map["height"]
        }
        
    }

}

// MARK: - Public

extension Avatar {
    
    class func uploadAvatar(_ image: UIImage, completion: @escaping (_ success: Bool, _ avatar: Avatar?) -> Void) {
        var avatar: Avatar? = Avatar()
        if let imadaData = image.pngData() {
            avatar?.perform(request: { (builder) in
                builder.method = .POST
                builder.path = Constants.imageUploadPath
                builder.body = URLRequestBody.MultipartFormData(params: nil, URLs: nil, data: ["image" : imadaData])
            }) { (success) in
                if !success {
                    avatar = nil
                }
                completion(success, avatar)
            }
        }
    }
    
    func makeAvatar(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.makeAvatarPathFormat, identifier ?? 0)
            builder.method = .GET
        }, completionHandler: completion)
    }
    
    func deletePhoto(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = String(format: Constants.deletePhotoPathFormat, identifier ?? 0)
            builder.method = .DELETE
        }, completionHandler: completion)
    }
    
}

// MARK: -  NSCopying

extension Avatar: NSCopying {
    
    func copy(with zone: NSZone? = nil) -> Any {
        let avatar = Avatar()
        avatar.identifier = identifier
        avatar.urlPath = urlPath
        avatar.imageURL = imageURL
        return avatar
    }
    
}

// MARK: - Constants

private extension Avatar {
    
    struct Constants {
        static let imageUploadPath = "/accounts/profile/add_photo/"
        static let deletePhotoPathFormat = "/accounts/profile/del_photo/%d/"
        static let makeAvatarPathFormat = "/accounts/profile/make_avatar/%d/"
        static let baseImageURL = AppDelegate.shared.restClient.baseURL.deletingLastPathComponent().absoluteString
    }
    
}
