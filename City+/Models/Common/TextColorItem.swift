//
//  TextColorItem.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/22/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class TextColorItem {
    
    var id: String
    var hexString: String?
    
    var color: UIColor {
        return UIColor(hexString: hexString ?? "#000000")
    }
    
    init() {
        self.id = "0"
    }
    
    convenience init(id: String) {
        self.init()
        self.id = id
        var myArray: NSArray?
        if let path = Bundle.main.path(forResource: "ChatColors", ofType: "plist") {
            myArray = NSArray(contentsOfFile: path)
        }
        if let array = myArray {
            for itemDict in array {
                if let dict = itemDict as? [String:String] {
                    if let idItem = dict[Keys.id],idItem == id, let hex = dict[Keys.hex], !hex.isEmpty {
                        self.hexString = hex
                    }
                }
            }
        }
    }
    
}

extension TextColorItem {
    
    static func loadAll() -> [TextColorItem] {
        var items: [TextColorItem] = []
        var myArray: NSArray?
        if let path = Bundle.main.path(forResource: "ChatColors", ofType: "plist") {
            myArray = NSArray(contentsOfFile: path)
        }
        if let array = myArray {
            for itemDict in array {
                if let dict = itemDict as? [String:String] {
                    if let id = dict[Keys.id] {
                        items.append(TextColorItem(id: id))
                    }
                }
            }
        }
        return items
    }
    
}


fileprivate extension TextColorItem {
    
    struct Keys {
        static let id = "id"
        static let hex = "hex"
    }
    
}

