//
//  BackgroundItem.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/20/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class BackgroundItem {
    
    var id: String
    var name: String?
    
    var image: UIImage {
        return UIImage(named: name ?? "back_default") ?? #imageLiteral(resourceName: "back_default")
    }
    
    init() {
        self.id = "0"
    }
    
    convenience init(id: String) {
        self.init()
        self.id = id
        var myArray: NSArray?
        if let path = Bundle.main.path(forResource: "ChatBackgrounds", ofType: "plist") {
            myArray = NSArray(contentsOfFile: path)
        }
        if let array = myArray {
            for itemDict in array {
                if let dict = itemDict as? [String:String] {
                    if let idItem = dict[Keys.id],idItem == id, let name = dict[Keys.name], !name.isEmpty {
                        self.name = name
                    }
                }
            }
        }
    }
    
}


extension BackgroundItem {
    
    static func loadAll() -> [BackgroundItem] {
        var items: [BackgroundItem] = []
        var myArray: NSArray?
        if let path = Bundle.main.path(forResource: "ChatBackgrounds", ofType: "plist") {
            myArray = NSArray(contentsOfFile: path)
        }
        if let array = myArray {
            for itemDict in array {
                if let dict = itemDict as? [String:String] {
                    if let id = dict[Keys.id] {
                        items.append(BackgroundItem(id: id))
                    }
                }
            }
        }
        return items
    }
    
}


fileprivate extension BackgroundItem {
    
    struct Keys {
        static let id = "id"
        static let name = "name"
    }
    
}
