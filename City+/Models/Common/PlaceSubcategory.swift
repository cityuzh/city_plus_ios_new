//
//  PlaceSubcategory.swift
//  City+
//
//  Created by Ivan Grab on 3/8/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

final class PlaceSubcategory: Mappable, Identifiable {

    var name: String?
    var identifier: Int?
    
    //MARK: - Mappable
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["subcategory_name"]
        identifier <- map["subcategory"]
    }
    
}
