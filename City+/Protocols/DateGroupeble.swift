//
//  DateGroupeble.swift
//  Shallet
//
//  Created by Ivan Grab on 07.09.16.
//  Copyright © 2016 Shallet. All rights reserved.
//

import Foundation

protocol DateGroupeble: Identifiable {
    
    var identifier: Int? { get }
    var date: Date? { get }
    var isRead: Bool { get }
    
}

extension DateGroupeble {
    
    var dateInterval: Double {
        get {
            guard let date = date else { return 0.0 }
            return date.timeIntervalSince1970
        } set {
            _ = Date(timeIntervalSince1970: newValue / 1000)
         
        }
    }
    
    func getFormatedGroupedDateString() -> String {
        return formatedGroupedDateString
    }
    
    private var formatedGroupedDateString: String {
        guard let date = date else { return NSLocalizedString("Немає Дати", comment: "") }
        
        let dateformater = DateFormatter()
        dateformater.dateFormat = "DD MMMM yyyy"
        return dateformater.string(from: date) 
    }
    
}
