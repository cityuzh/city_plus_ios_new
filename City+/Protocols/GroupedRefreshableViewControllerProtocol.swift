//
//  GroupedRefreshableViewControllerProtocol.swift
//  Shallet
//
//  Created by Ivan Grab on 09.09.16.
//  Copyright © 2016 Shallet. All rights reserved.
//

import UIKit
import ObjectMapper

protocol GroupedRefreshableViewControllerProtocol: class {
    
    associatedtype T: Mappable, DateGroupeble
    
    var itemList: GroupedPaginatedListModel<T> { get }
    var refreshControl: UIRefreshControl? { get set }
    
    func updateUI()
    func loadMoreData()
    func requestMoreDataIfNeededForItemAtIndex(_ index: Int)
    func endRefreshingWithCompletion(_ completion: @escaping () -> Void)
    func didFinishLoadingDataWithSuccess(_ success: Bool, didReloadDataCompletion: (() -> Void)?)
}

extension GroupedRefreshableViewControllerProtocol {
    
    func loadMoreData() {
        itemList.loadMore { [weak self] (success) -> Void in
            self?.didFinishLoadingDataWithSuccess(success)
        }
    }
    
    func requestMoreDataIfNeededForItemAtIndex(_ index: Int) {
        if (itemList.count - index) <= (itemList.requestRowCount / 4) && itemList.state == .canLoadMore {
            loadMoreData()
        }
    }
    
    func endRefreshingWithCompletion(_ completion: @escaping () -> Void) {
        if let refreshControl = refreshControl {
            CATransaction.begin()
            CATransaction.setCompletionBlock(completion)
//            refreshControl.endRefreshing()
            CATransaction.commit()
        } else {
            completion()
        }
    }
    
    func didFinishLoadingDataWithSuccess(_ success: Bool, didReloadDataCompletion: (() -> Void)? = nil) {
        if let refreshControl = refreshControl, refreshControl.isRefreshing  {
            endRefreshingWithCompletion { [weak self] () -> Void in
                if success {
                    self?.updateUI()
                    didReloadDataCompletion?()
                }
            }
        } else if success {
            updateUI()
            didReloadDataCompletion?()
        }
    }
    
}
