//
//  Validatable.swift
//  PAMP
//
//  Created by Ivan Grab on 10/9/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import Foundation

protocol Validatable {
 
    //implement in each validatable model
    func validate() throws
}
