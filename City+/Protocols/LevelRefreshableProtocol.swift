//
//  LevelRefreshableProtocol.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/9/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import ObjectMapper

protocol LevelRefreshableProtocol: class {
    
    associatedtype T: Mappable, Identifiable
    var itemList: LevelPaginatedListModel<T> { get set }
    var refreshControl: UIRefreshControl? { get set }
    
    func updateUI()
    func loadMoreData()
    func requestMoreDataIfNeededForItemAtIndex(index: Int)
    func endRefreshingWithCompletion(completion: () -> Void)
    func didFinishLoadingDataWithSuccess(_ success: Bool, didReloadDataCompletion: (() -> Void)?)
}

extension LevelRefreshableProtocol {
    
    func loadMoreData() {
        itemList.loadMore { [weak self] (success) -> Void in
            self?.didFinishLoadingDataWithSuccess(success)
        }
    }
    
    func requestMoreDataIfNeededForItemAtIndex(index: Int) {
        if (itemList.count - index) <= (itemList.objects.count / 4) && itemList.state == .canLoadMore {
            loadMoreData()
        }
    }
    
    func endRefreshingWithCompletion(completion: @escaping () -> Void) {
        if let refreshControl = refreshControl {
            CATransaction.begin()
            CATransaction.setCompletionBlock(completion)
            refreshControl.endRefreshing()
            CATransaction.commit()
        } else {
            completion()
        }
    }
    
    func didFinishLoadingDataWithSuccess(_ success: Bool, didReloadDataCompletion: (() -> Void)? = nil) {
        if let refreshControl = refreshControl, refreshControl.isRefreshing  {
            endRefreshingWithCompletion { [weak self] () -> Void in
                if success {
                    self?.updateUI()
                    didReloadDataCompletion?()
                }
            }
        } else if success {
            updateUI()
            didReloadDataCompletion?()
        }
    }
    
}
