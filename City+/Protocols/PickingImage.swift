//
//  PickingImage.swift
//  PAMP
//
//  Created by Ivan Grab on 12/1/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import MobileCoreServices

protocol PickingImage: class {
    
    var mediaTypes: [String] { get }
    
    func pickingDidCancel()
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?)
    
}

extension PickingImage {
    
    
}

extension UIViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imagePicker: UIImagePickerController? {
        get {
            return UIImagePickerController()
        }
        set {
            
        }
    }
    
    func startPicking(isCameraOnly: Bool, isAllowEditing: Bool = true, deniedAccess: (()->())? = nil, onCancelAction: (()->())? = nil) {
        let libraryAction = UIAlertAction(title: NSLocalizedString("Бібліотека", comment: ""), style: .default) { (action) in
            guard let picker = self.imagePicker else {
                fatalError("Oh nooo!")
                return
            }
           
            picker.delegate = (self as (UIImagePickerControllerDelegate & UINavigationControllerDelegate))
            picker.sourceType = .photoLibrary
            picker.mediaTypes = (self as! PickingImage).mediaTypes
            picker.videoMaximumDuration = Configuration.MediaConstants.maxVideoDuration
            picker.allowsEditing = isAllowEditing
            picker.modalPresentationStyle = .fullScreen
            picker.navigationBar.tintColor = UIColor.appTintColor
            picker.title = NSLocalizedString("Бібліотека", comment: "")
            
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    if let selfViewController = self as? UIViewController {
                        DispatchQueue.main.async {
                            selfViewController.present(picker, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        deniedAccess?()
                    }
                }
            })
            
        }
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("Камера", comment: ""), style: .default) { (action) in
            guard let picker = self.imagePicker else {
                fatalError("Oh nooo!")
            }
            picker.delegate = (self as (UIImagePickerControllerDelegate & UINavigationControllerDelegate))
            picker.sourceType = .camera
            picker.mediaTypes = (self as! PickingImage).mediaTypes
            picker.videoMaximumDuration = Configuration.MediaConstants.maxVideoDuration
            picker.allowsEditing = isAllowEditing
            picker.modalPresentationStyle = .fullScreen
            picker.navigationBar.tintColor = UIColor.appTintColor
            picker.title = NSLocalizedString("Камера", comment: "")
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                if granted {
                    if let selfViewController = self as? UIViewController {
                        DispatchQueue.main.async {
                            selfViewController.present(picker, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        deniedAccess?()
                    }
                }
            })
            
        }
        
        if let selfViewController = self as? UIViewController, !isCameraOnly {
            ShowAlert.showAlertContrller(with: .actionSheet, title: nil, message: nil, actions: [libraryAction, cameraAction], in: selfViewController, onCancelAction: onCancelAction)
        } else if isCameraOnly {
            guard let picker = self.imagePicker else {
                fatalError("Oh nooo!")
            }
            
            picker.delegate = (self as (UIImagePickerControllerDelegate & UINavigationControllerDelegate))
            picker.sourceType = .camera
            picker.videoMaximumDuration = Configuration.MediaConstants.maxVideoDuration
            picker.mediaTypes = (self as! PickingImage).mediaTypes
            picker.allowsEditing = isAllowEditing
            picker.modalPresentationStyle = .fullScreen
            picker.navigationBar.tintColor = UIColor.appTintColor
            picker.title = NSLocalizedString("Камера", comment: "")
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                if granted {
                    if let selfViewController = self as? UIViewController {
                        DispatchQueue.main.async {
                            selfViewController.present(picker, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        deniedAccess?()
                    }
                }
            })
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            if let pickingSelf = self as? PickingImage {
                pickingSelf.pickingDidCancel()
            }
        }
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        func finishPicking(with attachment: Attachment, thumb: UIImage) {
            var resized = thumb
            if thumb.size.width >= Configuration.MediaConstants.maxUploadImageSize {
                resized = thumb.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
            }
            picker.dismiss(animated: true) {
                if let picking = self as? PickingImage {
                    picking.didSelectMedia(with: attachment, thumbnail: thumb)
                }
            }
        }
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            var fixedImage = image.fixedOrientation()
            if fixedImage.size.width > Configuration.MediaConstants.maxUploadImageSize {
                fixedImage = fixedImage.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
            }
            let documentDirectory = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
            let localPath: URL = documentDirectory.appendingPathComponent("attachment.jpg")
            
            do {
                let data = fixedImage.pngData()
                try data?.write(to: localPath)
            } catch {
                print("> CreatePostTableViewController: Cant write image to file!")
            }
            
            let attachment = Attachment(withFileUrl: localPath, mediaType: .photo)
            finishPicking(with: attachment, thumb: fixedImage)
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            var fixedImage = image.fixedOrientation()
            if fixedImage.size.width > Configuration.MediaConstants.maxUploadImageSize {
                fixedImage = fixedImage.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
            }
            let documentDirectory = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
            let localPath: URL = documentDirectory.appendingPathComponent("attachment.jpg")
            
            do {
                let data = fixedImage.pngData()
                try data?.write(to: localPath)
            } catch {
                print("> CreatePostTableViewController: Cant write image to file!")
            }
            
            let attachment = Attachment(withFileUrl: localPath, mediaType: .photo)
            finishPicking(with: attachment, thumb:fixedImage)
        } else if let mediaURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            if let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String {
                if mediaType == kUTTypeImage as String {
                    let attachment = Attachment(withAssetUrl: mediaURL, mediaType: .photo)
                    attachment.thumbCompletion = { (thumb) in
                        finishPicking(with: attachment, thumb: thumb)
                    }
                } else {
                    let attachment = Attachment(withAssetUrl: mediaURL, mediaType: .video)
                    attachment.thumbCompletion = { (thumb) in
                        finishPicking(with: attachment, thumb: thumb)
                    }
                }
            }
        } else if let chosenImageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            if let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String {
                if mediaType == kUTTypeImage as String {
                    let attachment = Attachment(withAssetUrl: chosenImageURL, mediaType: .photo)
                    attachment.thumbCompletion = { (thumb) in
                        finishPicking(with: attachment, thumb: thumb)
                    }
                } else {
                    let attachment = Attachment(withAssetUrl: chosenImageURL, mediaType: .video)
                    attachment.thumbCompletion = { (thumb) in
                        finishPicking(with: attachment, thumb: thumb)
                    }
                }
            }
        }
    }
}
