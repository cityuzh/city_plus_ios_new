//
//  RefreshableViewControllerProtocol.swift
//  City+
//
//  Created by Ivan Grab on 02/13/18.
//  Copyright © 2016 Shallet. All rights reserved.
//

import UIKit
import ObjectMapper

protocol RefreshableViewControllerProtocol: class {

	associatedtype T: Mappable, Identifiable
	var itemList: PaginatedListModel<T> { get set }
	var refreshControl: UIRefreshControl? { get set }

	func updateUI()
	func loadMoreData()
	func requestMoreDataIfNeededForItemAtIndex(index: Int)
	func endRefreshingWithCompletion(completion: () -> Void)
	func didFinishLoadingDataWithSuccess(_ success: Bool, didReloadDataCompletion: (() -> Void)?)
}

extension RefreshableViewControllerProtocol {

	func loadMoreData() {
		itemList.loadMore { [weak self] (success) -> Void in
			self?.didFinishLoadingDataWithSuccess(success)
		}
	}

	func requestMoreDataIfNeededForItemAtIndex(index: Int) {
        if (itemList.count - index) <= (itemList.requestRowCount / 4) && itemList.state == .canLoadMore {
            loadMoreData()
        }
	}

	func endRefreshingWithCompletion(completion: @escaping () -> Void) {
		if let refreshControl = refreshControl {
			CATransaction.begin()
			CATransaction.setCompletionBlock(completion)
			refreshControl.endRefreshing()
			CATransaction.commit()
		} else {
			completion()
		}
	}

	func didFinishLoadingDataWithSuccess(_ success: Bool, didReloadDataCompletion: (() -> Void)? = nil) {
		if let refreshControl = refreshControl, refreshControl.isRefreshing  {
			endRefreshingWithCompletion { [weak self] () -> Void in
				if success {
					self?.updateUI()
					didReloadDataCompletion?()
				}
			}
		} else if success {
			updateUI()
			didReloadDataCompletion?()
		}
	}

}
