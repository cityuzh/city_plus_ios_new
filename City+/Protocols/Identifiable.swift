//
//  Identifiable.swift
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

protocol Identifiable: Equatable {
    
    associatedtype IdentifierType: ExpressibleByIntegerLiteral
    
    var identifier: IdentifierType? { get set }
    
}

extension Identifiable {
    
    static func ==(lhs: Self, rhs: Self) -> Bool {
        return String(describing: lhs.identifier) == String(describing: rhs.identifier)
    }
    
}
