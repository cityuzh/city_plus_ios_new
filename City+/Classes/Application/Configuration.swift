//
//  Configuration.swift
//  City+
//
//  Created by Ivan Grab on 12.02.18.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit
import CoreData

import Fabric
import Crashlytics

import Firebase

import GoogleMaps
import GooglePlaces

final class Configuration {
    
    struct MediaConstants {
        static let maxUploadImageSize: CGFloat = CGFloat(700)
        static let maxVideoDuration: TimeInterval = 30.0
    }
    
    struct TextBounds {
        static let profileDescriptionMaxCount: Int = 300
    }
    
    struct SocketEvent {
        struct Listened {
            static let newMessage = "message"
        }
        
        struct Emited {
            static let sendMessage = "message"
        }
    }
    
    // MARK: - Static Properties
    
    struct Constants {
        static let searchTextDelay: TimeInterval = 0.5
        static let defaultRadius = 7000
    }
    
//    #if !TEST
//        static let restAPIURL = URL(string: "http://6e32e3ba.ngrok.io")!
//        static let socketURLPathFormat = "ws://6e32e3ba.ngrok.io/ws/chat/%d/"
//        static let socketForumURLPathFormat = "ws://6e32e3ba.ngrok.io/ws/topic/%d/"
//    #else
        static let restAPIURL = URL(string: "http://3.120.96.33")!
        static let socketURLPathFormat = "ws://3.120.96.33/ws/chat/%d/"
        static let socketForumURLPathFormat = "ws://3.120.96.33/ws/topic/%d/"
//    #endif
    
    
    static let wetherAPIURL = URL(string: "http://samples.openweathermap.org/data/2.5/weather?")!

    static let googleAPIKey = "AIzaSyAv2k6JvEvUAIRQVYUOTdNAlVYbGvKOxhg"
    static let googlePlaceAPIKey = "AIzaSyBbdCbVGUQEnJaLxAlohTRJitgBlsSygJY"
    static let wheterAppID = "b1b15e88fa797225412429c1c50c122a1"
    static let appIdentifierFCM = "1:37792379424:ios:f52c64fc31fb266a"
    // MARK: - Properties
    
    static let deviceUUID = UIDevice.current.identifierForVendor
    
    private let restClient: RESTClient
    private let persistentContainer: NSPersistentContainer
    
    // MARK: - Initializers
    
    init(restClient: RESTClient, persistentContainer: NSPersistentContainer) {
        self.restClient = restClient
        self.persistentContainer = persistentContainer
    }
    
    // MARK: - Public Methods
    
    func configure() {
        configureDependencies()
    }
    
    // MARK: - Private Methods
    
    private func configureDependencies() {
        //Crashlytics
        Fabric.with([ Crashlytics.self ])

        //Firabase
        FirebaseApp.configure()
        
        //Google
        GMSServices.provideAPIKey(Configuration.googleAPIKey)
        GMSPlacesClient.provideAPIKey(Configuration.googlePlaceAPIKey)
    }
    
}
