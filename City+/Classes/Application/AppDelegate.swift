//
//  AppDelegate.swift
//  City+
//
//  Created by Ivan Grab on 2/11/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreData
import ObjectMapper

import UserNotifications
import Firebase
import FirebaseCore
import FirebaseInstanceID
import FirebaseMessaging
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Static
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var configuration: Configuration!
    let restClient = RESTClient(baseURL: Configuration.restAPIURL)
    let imageProvider = ImageProvider(baseURL: Configuration.restAPIURL as NSURL)
    
    lazy var sessionManager: SessionManager = {
        let sessionManager = SessionManager(restClient: self.restClient)
        self.restClient.sessionManager = sessionManager
        return sessionManager
    }()
    
    var mainContainerController: MainContainerViewController?
    
    let persistentContainer = NSPersistentContainer.default()
    
    var window: UIWindow?

    // MARK: - UIApplicationDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.applicationIconBadgeNumber = 0
        configuration = Configuration(restClient: restClient, persistentContainer: persistentContainer)
        configuration.configure()
        
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        Messaging.messaging().delegate = self
        
        application.registerForRemoteNotifications()
        
        setupNavigationBarAppereance()
        setUpRootViewController()
        setupTabBarAppereance()
        setupColorTintText()
        ApplicationDelegate.shared.application( application, didFinishLaunchingWithOptions: launchOptions)
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)
        Messaging.messaging().setAPNSToken(deviceToken, type: .prod)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        UIApplication.shared.applicationIconBadgeNumber = 0
        if UIApplication.shared.applicationState != .active {
            if let json = userInfo as? [String : Any] {
                let numberFormater = NumberFormatter()
                if let pushType = json["push_type"] as? String {
                    switch pushType {
                    case "new_topic":
                        if let userJSONString = json["topic"] as? String, let jsonData = userJSONString.data(using: String.Encoding.utf8), let JSON = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let topic = Mapper<Topic>().map(JSON: JSON!) {
                            mainContainerController?.showTopicDetail(for: topic)
                        }
                    case "new_comment":
                        if let userJSONString = json["topic"] as? String, let jsonData = userJSONString.data(using: String.Encoding.utf8), let JSON = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let topic = Mapper<Topic>().map(JSON: JSON!) {
                            mainContainerController?.showTopicDetail(for: topic)
                        }
                    case "message":
                        if let id = json["dialog_id"] as? String, let idNumber = numberFormater.number(from: id) {
                            mainContainerController?.showChat(for: idNumber.intValue)
                        }
                    case "incoming_friend_request":
                        if let userJSONString = json["user"] as? String, let jsonData = userJSONString.data(using: String.Encoding.utf8), let JSON = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let user = Mapper<UserModel>().map(JSON: JSON!) {
                            mainContainerController?.showFriendRequest(for: user)
                        }
                    case  "event_reminder":
                        if let eventIDString = json["event"] as? String, let placeIDString = json["place"] as? String, let eventID = eventIDString.toNumber, let placeID = placeIDString.toNumber {
                            mainContainerController?.showEventDetail(for: eventID.intValue, in: placeID.intValue)
                        }
                    default: break
                    }
                }
            }
        } else if UIApplication.shared.applicationState == .active {
            mainContainerController?.updateChats()
            sessionManager.getMyProfile()
        }
    }    

}

extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        sessionManager.pushToken = fcmToken
        if sessionManager.isLoggedIn && !UserDefaults.standard.bool(forKey: "AllNotificationKeyDisabled") {
            sessionManager.setDeviceToken(with: sessionManager.pushToken)
        }
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        sessionManager.pushToken = fcmToken
        if sessionManager.isLoggedIn && !UserDefaults.standard.bool(forKey: "AllNotificationKeyDisabled") {
            sessionManager.setDeviceToken(with: sessionManager.pushToken)
        }
    }
    
}

extension AppDelegate {
    
    func setupTabBarAppereance() {
        UITabBar.appearance().tintColor = UIColor.newAppTintColor
        UITabBar.appearance().unselectedItemTintColor = UIColor.newLightBlueColor
        
    }
    
    func setupColorTintText(){
        UITextView.appearance().tintColor = .systemBlue
    }
    
    func setupNavigationBarAppereance() {
        UINavigationBar.appearance().tintColor = UIColor.white
    }
    func setupStatusBar(){
        
    }
    func setStatusBarBackgroundColor(color:UIColor){
        guard let statusBar = UIApplication.shared.value(forKeyPath: "setStatusBarBackgroundColor") as? UIView else {return}
        statusBar.backgroundColor = UIColor.newAppTintColor
    }
    
    func setUpRootViewController() {
        let newRoot = UIStoryboard(.Authentication).instantiateViewController(withIdentifier: "SplashAnimationViewController") as! SplashAnimationViewController
        newRoot.completionHandler = { (vc) in
            if !self.sessionManager.isLoggedIn || (self.sessionManager.storedUser?.fullName ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                var newRoot = UIViewController()
                if self.sessionManager.isLoggedIn && (self.sessionManager.storedUser?.fullName ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    if let profile = UIStoryboard(.Profile).instantiateViewController(withIdentifier: "ProfileTableViewController") as? ProfileTableViewController {
                        profile.currentAction = .registration
                        newRoot = profile
                    }
                } else {
                    newRoot = UIStoryboard(.Authentication).instantiateViewController(withIdentifier: "PhoneConfirmationTableViewController")
                }
                
                let newNavigationVC = UINavigationController(rootViewController: newRoot)
                if let window = self.window {
                    window.rootViewController = newNavigationVC
                    window.makeKeyAndVisible()
                }
            } else {
                let mainVC: MainContainerViewController = UIStoryboard(.Main).instantiateViewController(withIdentifier: "MainContainerViewController") as! MainContainerViewController
                self.mainContainerController = mainVC
                if let window = self.window {
                    window.rootViewController = mainVC
                    window.makeKeyAndVisible()
                }
            }
        }
        if let window = window {
            window.rootViewController = newRoot
            window.makeKeyAndVisible()
        }
    }
    
}

