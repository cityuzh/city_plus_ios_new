//
//  ImageProvider.swift
//  City+
//
//  Created by Ivan Grab on 6/22/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

final class ImageProvider {

	private let secondLevelCache: URLCache
	private let firstLevelCache: NSCache<AnyObject, AnyObject>

	lazy var session: URLSession = {
		let configuration = URLSessionConfiguration.default
		configuration.urlCache = self.secondLevelCache
		return URLSession(configuration: configuration)
	}()

	let baseURL: NSURL

	var onNoInternetConnection: ((_ request: NSURLRequest) -> Void)?

	init(baseURL: NSURL) {
		self.baseURL = baseURL
		secondLevelCache = URLCache(memoryCapacity: 4 * 1024 * 1024, diskCapacity: 500 * 1024 * 1024, diskPath: Constants.cachePath)
		firstLevelCache = NSCache()
	}

	func imageForPath(path: String, completion: @escaping (_ image: UIImage?) -> Void) -> URLSessionTask? {
		let request = requestForPath(path)
		if let image = firstLevelCache.object(forKey: request) as? UIImage {
			completion(image)
		} else {
			let task = session.dataTask(with: request as URLRequest) { (data, _, error) -> Void in
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
				if let data = data {
					let image = UIImage(data: data)
					if let image = image {
						self.firstLevelCache.setObject(image, forKey: request)
					}
					DispatchQueue.main.async(execute: { 
                        completion(image)
                    })
				} else {
                    DispatchQueue.main.async(execute: {
                        completion(nil)
                    })
				}
           }
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
			task.resume()
			return task
		}
		return nil
	}

	private func requestForPath(_ path: String) -> NSURLRequest {
        var URL = baseURL.appendingPathComponent(path)
        
        if (baseURL.absoluteString ?? "" ).isEmpty {
            URL = NSURL(string: path) as URL?
        }
    
		let request = NSMutableURLRequest(url: URL!)
		request.cachePolicy = .returnCacheDataElseLoad
		return request
	}

}

extension ImageProvider {

	fileprivate struct Constants {
		static let cachePath = "ImageProviderCache"
	}

}
