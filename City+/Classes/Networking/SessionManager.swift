//
//  SessionManager.swift
//  City+
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

final class SessionManager: NSObject, NetworkModel {
    

    // MARK: - NetworkModel
    
    var restClient: RESTClient?
    var lastTask: URLSessionTask?
    var lastRestClientError: RESTClient.ErrorType?
    var onNoInternetConnection: ((_ request: URLRequest) -> Void)?
    var onRequestFailure: ((_ error: RESTClient.ErrorType, _ data: Data?, _ request: URLRequest) -> Void)?
    
    
    // MARK: - Properties
    
    var token: Token?
    var pushToken: String!
    var storedUser = User.lastUser()
    var pdfSavedInDocuments: NSData?
    var pdfSavedInDocumentsHanler: ((_ pdf: NSData?)->())?
    
    var isLoggedIn: Bool {
        return (token != nil)
    }
    
    var currentPassword: String? {
        return Credentials.loadFromKeyсhain()?.password
    }
    
    fileprivate(set) var authorizationInProgress = false
    
    // MARK: - Lifecycle
    
    init(restClient: RESTClient) {
        self.restClient = restClient
        super.init()
        cleanUpKeyChainAtFirstLaunch()
        token = Token.loadFromKeyсhain()
    }
    
    // MARK: - Private
    
    private func storeUser(with JSON: [String : Any], completion: NetworkModelCompletion? = nil) {
        var success = true
        if let stored = storedUser {
            stored.update(with: JSON)
        } else if let userModel = User.coreDataObject()  {
            userModel.update(with: JSON)
        } else {
            success = false
        }
        invoke(completionHandler: completion, success: success)
    }
    
    private func deleteUser() {
        User.deleteLastUser()
    }
}

// MARK: - Public

extension SessionManager {
    
    func login(with formModel: UserFormModel, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.signInPath
            builder.method = .POST
            let userName = formModel.loginType == .phone ? formModel.phone : formModel.email
            builder.body = .JSON(JSON: ["username" : userName,
                                  "password" : formModel.password] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.authorizationInProgress = false
            strongSelf.handleAuthorizationResponse(data: data, request: request, response: response, completion: { (success) in
                strongSelf.getMyProfile(with: { (success) in
                    strongSelf.invoke(completionHandler: completionHandler, success: true)
                })
            })
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
        
    }
    
    func login(with fbToken: String, phone: String? = nil, completionHandler: @escaping NetworkModelCompletion) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.method = .POST
            builder.path = Constants.facebookRoute
            builder.body = .JSON(JSON: ["access_token" : fbToken] as AnyObject)
        }, successHandler: { (data, request, response) in
            self.handleAuthorizationResponse(data: data, request: request, response: response, completion: completionHandler)
        }) { (error, data, request, response) in
            DispatchQueue.main.async {
                completionHandler(error == nil)
            }
        }
    }
    
    func loginApple(withToken access_token: String, withIdToken id_token: String, phone: String? = nil, completionHandler: @escaping NetworkModelCompletion) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.method = .POST
            builder.path = Constants.appleRoute
            builder.body = .JSON(JSON: ["access_token" : access_token, "id_token" : id_token ] as AnyObject)
        }, successHandler: { (data, request, response) in
            self.handleAuthorizationResponse(data: data, request: request, response: response, completion: completionHandler)
        }) { (error, data, request, response) in
            DispatchQueue.main.async {
                completionHandler(error == nil)
            }
        }
    }

    
    func sendSMS(to phone: String, isFaceBookReg: Bool = false, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.sendSMSPath
            builder.method = .POST
            if isFaceBookReg {
                builder.body = .JSON(JSON: ["phone" : phone,
                                            "sms_type" : "facebook" ] as AnyObject)
            } else {
                builder.body = .JSON(JSON: ["phone" : phone
                    ] as AnyObject)
            }
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func sendEmailCode(to email: String, isFaceBookReg: Bool = false, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.sendSMSPath
            builder.method = .POST
            builder.body = .JSON(JSON: ["email" : email ] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func confirmSMS(for phone: String, with code: String, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.validateSMSPath
            builder.method = .POST
            builder.body = .JSON(JSON: ["phone" : phone,
                                        "code" : code
                ] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func signUp(with formModel: RegisterUserFormModel, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.signUpPath
            builder.method = .POST
            let username = formModel.loginType == .phone ? formModel.phone : formModel.email
            let key = formModel.loginType == .phone ? "username" : "email"
            builder.body = .JSON(JSON: [ key : username,
                                        "password1" : formModel.password,
                                        "password2" : formModel.confirmPassword
                                        ] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.authorizationInProgress = false
            strongSelf.handleAuthorizationResponse(data: data, request: request, response: response, completion: { (success) in
                if success {
                    strongSelf.getMyProfile(with: { (success) in
                        strongSelf.invoke(completionHandler: completionHandler, success: true)
                    })
                }
            })
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
        
    }
    
    func getMyProfile(with completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.myProfilePathFormat
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            if let data = data, let JSON = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : AnyObject] {
                if self.storedUser != nil {
                    self.storedUser!.update(with: JSON)
                    completion?(true)
                } else {
                    if let user = User.coreDataObject() {
                        self.storedUser = user
                        self.storedUser!.update(with: JSON)
                        completion?(true)
                    } else {
                        completion?(false)
                    }
                }
            } else {
                self.invoke(completionHandler: completion, success: false)
            }
        }) { (error, data, request, response) in
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
        }
    }
    
    func updateMyProfile(with user: UserModel, completion: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.myProfilePathFormat
            builder.method = .PUT
            builder.body = .MappableObject(user, contentType: .ApplicationJSON)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            if let data = data, let JSON = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : AnyObject] {
                strongSelf.storeUser(with: JSON, completion: completion)
            } else {
                strongSelf.invoke(completionHandler: completion, success: false)
            }
        }) { (error, data, request, response) in
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
        }
    }
    
    func setDeviceToken(with tokenString: String, completionHandler: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.updateTokenPathFormat
            builder.method = .POST
            var body = [String : String]()
            body["registration_id"] = tokenString
            body["type"] = "ios"
            body["active"] = "true"
            body["device_id"] = Configuration.deviceUUID?.uuidString ?? "SIMULATOR"
            builder.body = .JSON(JSON: body as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { (error, data, request, response) in
            self.updateDeviceToken(with: tokenString, completionHandler: completionHandler)
        }
    }
    
    func updateDeviceToken(with tokenString: String, completionHandler: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.updateTokenPathFormat + tokenString + "/"
            builder.method = .PUT
            var body = [String : String]()
            body["registration_id"] = tokenString
            body["type"] = "ios"
            body["active"] = "true"
            body["device_id"] = Configuration.deviceUUID?.uuidString ?? "SIMULATOR"
            builder.body = .JSON(JSON: body as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { (error, data, request, response) in
            self.invoke(completionHandler: completionHandler, success: false)
        }
    }
    
    func deleteDeviceToken(with completionHandler: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.updateTokenPathFormat + pushToken + "/"
            builder.method = .DELETE
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { (error, data, request, response) in
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func sendForgotPasswordCode(for phone: String, completionHandler: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.forgotPasswordPath
            builder.method = .POST
            builder.body = .JSON(JSON: ["phone" : phone] as AnyObject)
        }, successHandler: { [weak self] (data, reuest, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func sendForgotPasswordCode(forEmail : String, completionHandler: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.forgotPasswordPath
            builder.method = .POST
            builder.body = .JSON(JSON: ["email" : forEmail] as AnyObject)
        }, successHandler: { [weak self] (data, reuest, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func setNewPassword(with formModel: RegisterUserFormModel, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.changeForgotPasswordPath
            builder.method = .POST
            let key = formModel.loginType == .email ? "email" : "phone"
            let value = formModel.loginType == .email ? formModel.email : formModel.phone
            builder.body = .JSON(JSON: [key : value,
                                        "code" : formModel.smsCode,
                                        "password1" : formModel.password,
                                        "password2" : formModel.confirmPassword
                ] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func changePhone(with phone: String, code: String, completionHandler: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = Constants.changePhonePath
            builder.method = .POST
            builder.body = .JSON(JSON: ["phone" : phone,
                                        "code" : code
                ] as AnyObject)
        }, completionHandler: completionHandler)
    }
    
    func changeEmail(with email: String, code: String, completionHandler: @escaping NetworkModelCompletion) {
        perform(request: { (builder) in
            builder.path = Constants.changePhonePath
            builder.method = .POST
            builder.body = .JSON(JSON: ["email" : email,
                                        "code" : code
                ] as AnyObject)
        }, completionHandler: completionHandler)
    }
    
    func changePassword(with formModel: RegisterUserFormModel, oldPassword: String, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.changePasswordPath
            builder.method = .POST
            builder.body = .JSON(JSON: ["old_password" : oldPassword,
                                        "new_password" : formModel.password
                ] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func changeMessagePush(with value: Bool, completionHandler: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.myProfilePathFormat
            builder.method = .PATCH
            builder.body = .JSON(JSON: ["push_on_message" : value
                ] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    func changeRequestPush(with value: Bool, completionHandler: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.myProfilePathFormat
            builder.method = .PATCH
            builder.body = .JSON(JSON: ["push_on_request" : value
                ] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    
    func changeTopicPush(with value: Bool, completionHandler: NetworkModelCompletion? = nil) {
        perform(request: { (builder) in
            builder.path = Constants.myProfilePathFormat
            builder.method = .PATCH
            builder.body = .JSON(JSON: ["push_on_topic" : value
                ] as AnyObject)
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.invoke(completionHandler: completionHandler, success: true)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    
    func logOut(completion: NetworkModelCompletion? = nil) {
        deleteDeviceToken { (success) in
            self.perform(request: { (builder) in
                builder.path = Constants.logOutPath
                builder.method = .POST
            }) { [weak self] (success) in
                self?.cleanUp()
                self?.invoke(completionHandler: completion, success: success)
            }
        }
    }
    
    func cleanUp() {
        deleteCookies()
        Token.deleteFromKeyсhain()
        token = nil
        Credentials.deleteFromKeyсhain()
        storedUser = nil
        DispatchQueue.main.async {
            User.deleteLastUser()
        }
    }
    
}

// MARK: - Private

private extension SessionManager {
    
    func handleAuthorizationResponse(data: Data?, request: URLRequest, response: URLResponse?, completion: NetworkModelCompletion? = nil) {
        guard let responseData = data, let token = Token(from: responseData) else {
            authorizationInProgress = false
            let error: RESTClient.ErrorType = response == nil ? .noHTTPResponse : .noAuthorizationToken
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
            return
        }
        
        DispatchQueue.main.async {
            self.token = token
            self.token?.storeToKeyсhain()
            
            self.restClient?.didLogin()
            if self.pushToken != nil {
                self.setDeviceToken(with: self.pushToken)
            }
            if let JSON = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String : Any] {
                if self.storedUser != nil {
                    self.storedUser!.update(with: JSON)
                    completion?(true)
                } else {
                    if let user = User.coreDataObject() {
                        self.storedUser = user
                        self.storedUser!.update(with: JSON)
                        completion?(true)
                    } else {
                        completion?(false)
                    }
                }
            } else {
                completion?(false)
            }
            
            
            self.authorizationInProgress = false
        }
    }
    
    func deleteCookies() {
        HTTPCookieStorage.shared.cookies?.forEach { HTTPCookieStorage.shared.deleteCookie($0) }
    }
    
    
    func cleanUpKeyChainAtFirstLaunch() {
        guard UserDefaults.standard.object(forKey: Constants.flagKey) != nil else {
            Token.deleteFromKeyсhain()
            UserDefaults.standard.set(Constants.flagKey, forKey: Constants.flagKey)
            return
        }
    }
    
}

// MARK: - Token

extension SessionManager {
    
    struct Token {
        
        var string = ""
        
        init(_ authorizationToken: String) {
            self.string = authorizationToken
        }
        
        init?(from httpData: Data) {
            guard let JSON = try! JSONSerialization.jsonObject(with: httpData, options: .allowFragments) as? [String : Any], let authorizationToken = JSON["key"] as? String  else {
                return nil
            }
            self.string = authorizationToken
        }
        
        func storeToKeyсhain() {
            KeychainWrapper.standard.set(string, forKey: Constants.authorizationTokenKey)
            print("> SessionManager: Toket.stored!")
        }
        
        static func loadFromKeyсhain() -> Token? {
            guard let authorizationToken = KeychainWrapper.standard.string(forKey: Constants.authorizationTokenKey) else { return nil }
            return Token(authorizationToken)
        }
        
        static func deleteFromKeyсhain() {
            KeychainWrapper.standard.removeObject(forKey: Constants.authorizationTokenKey)
        }
        
    }
    
}

// MARK: - Credentials

private extension SessionManager {
    
    struct Credentials {
        
        var email = ""
        var password = ""
        
        func storeToKeyсhain() {
            KeychainWrapper.standard.set(email, forKey: Constants.emailKey)
            KeychainWrapper.standard.set(password, forKey: Constants.passwordKey)
        }
        
        static func loadFromKeyсhain() -> Credentials? {
            guard
                let email = KeychainWrapper.standard.string(forKey: Constants.emailKey),
                let password = KeychainWrapper.standard.string(forKey: Constants.passwordKey)
                else { return nil }
            return Credentials(email: email, password: password)
        }
        
        static func deleteFromKeyсhain() {
            KeychainWrapper.standard.removeObject(forKey: Constants.emailKey)
            KeychainWrapper.standard.removeObject(forKey: Constants.passwordKey)
        }
        
    }
    
}

// MARK: - Constants

private extension SessionManager {
    
    enum Constants {
        static let appleRoute = "/rest-auth/apple/"
        static let userRoute = "/accounts"
        static let facebookRoute = "/rest-auth/facebook/"
        static let myProfilePathFormat = userRoute + "/profile/"
        static let updateTokenPathFormat = "/devices/"
        static let signInPath = userRoute + "/login/"
        static let signUpPath = userRoute + "/registration/"
        static let validateSMSPath = userRoute + "/profile/confirm_sms/"
        static let sendSMSPath = userRoute + "/profile/send_sms/"
        static let forgotPasswordPath = userRoute + "/password/reset/"
        static let changePasswordPath = userRoute + "/profile/change_password/"
        static let changeForgotPasswordPath = forgotPasswordPath + "confirm/"
        static let changePhonePath = "/accounts/profile/change_username/"
        static let logOutPath = userRoute + "/logout/"
        
        static let flagKey = "SessionManagerFlag"
        static let authorizationTokenKey = "authorizationToken"
        static let emailKey = "EmailKey"
        static let passwordKey = "PasswordKey"
    }
    
}

extension SessionManager: URLSessionDownloadDelegate {


func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
    
    
    let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("BonPlan\(AppDelegate.shared.sessionManager.storedUser!.name!).pdf")
    
    do {
        let dataFromURL = NSData(contentsOf: location)
        try dataFromURL?.write(to: fileURL, options: .atomic)
    } catch {
        print(error)
    }
    self.pdfSavedInDocumentsHanler!(NSData(contentsOf: fileURL))
}

}
