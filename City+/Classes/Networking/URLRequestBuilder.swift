//
//  URLRequestBuilder.swift
//  City+
//
//  Created by Ivan Grab on 6/21/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import Foundation
import ObjectMapper

final class URLRequestBuilder {
    
    // MARK: - Properties
    
    static var defaultHeaderFields: [String: String] = [:]
    
    var baseURL: URL
    var path: String?
    var method = URLRequestMethod.GET
    var queryParams: [String: AnyObject] = [:]
    var headerFields: [String: String] = [:]
    var body: URLRequestBody?
    
    var urlRequest: URLRequest? {
        guard let url = url else { return nil }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        let allHeaderFields = URLRequestBuilder.defaultHeaderFields + headerFields
        if !allHeaderFields.isEmpty {
            request.allHTTPHeaderFields = allHeaderFields
        }
        
        if let body = body?.build() {
            if let data = body.data {
                request.setValue("\(data.count)", forHTTPHeaderField: "Content-Length")
                request.httpBody = data as Data
            }
            
            if let contentType = body.contentType {
                request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            }
        }
        
        return request
    }
    
    var url: URL? {
        guard var urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: false), let path = path else { return nil }
        urlComponents.path += path
        
        if !queryParams.isEmpty {
            let percentEncodedQuery = (urlComponents.percentEncodedQuery.map { $0 + "&" } ?? "") + URLParametersSerializer.query(queryParams)
            urlComponents.percentEncodedQuery = percentEncodedQuery
        }
        
        return urlComponents.url
    }
    
    // MARK: - Lifecycle
    
    init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
}

// MARK: - Others

enum URLRequestMethod: String {
    
    case GET
    case POST
    case PUT
    case DELETE
    case HEAD
    case PATCH
    case OPTIONS
    
}

enum URLRequestContentType: String {
    
    case ApplicationJSON = "application/json"
    case FormURLEncoded = "application/x-www-form-urlencoded"
}

enum URLRequestBody {
    
    case RawData(data: Data)
    case JSON(JSON: AnyObject)
    case FormURLEncoded(params: [String: AnyObject])
    case MappableObject(Mappable, contentType: URLRequestContentType)
    case MappableObjects([Mappable])
    case MultipartFormData(params: [String: Any]?, URLs: [String : NSURL]?, data: [String : Data]?)
    
    func build() -> (data: Data?, contentType: String?) {
        switch self {
            
        case .RawData(let data):
            return (data, nil)
            
        case .JSON(let JSON):
            let data = try? JSONSerialization.data(withJSONObject: JSON, options: .prettyPrinted)
            return (data as Data?, URLRequestContentType.ApplicationJSON.rawValue)
            
        case .FormURLEncoded(let params):
            let data = URLParametersSerializer.query(params).data(using: String.Encoding.utf8)
            return (data as Data?, URLRequestContentType.FormURLEncoded.rawValue)
            
        case .MappableObject(let object, let contentType):
                let JSON = object.toJSON()
                let data: Data?
                switch contentType {
                case .ApplicationJSON:
                    data = try? JSONSerialization.data(withJSONObject: JSON, options: .prettyPrinted)
                case .FormURLEncoded:
                    data =  URLParametersSerializer.query(JSON as [String : AnyObject]).data(using: String.Encoding.utf8)
                }
                
                if let data = data {
                    return (data, contentType.rawValue)
                }
                return (nil, nil)
        
        case .MappableObjects(let objects):
            let JSON = objects.map({ (object) -> [String : Any] in
                return object.toJSON()
            })
            let data = try? JSONSerialization.data(withJSONObject: JSON, options: .prettyPrinted)
            if let data = data {
                return (data, URLRequestContentType.ApplicationJSON.rawValue)
            }
            return (nil, nil)
        case .MultipartFormData(let params, let URLs, let data):
            let multipartFormData = createMultipartFormData(parameters: params, URLs: URLs, data: data)
            return (multipartFormData.0, multipartFormData.1)
        }
    }
    
    private func createMultipartFormData(parameters: [String: Any]?, URLs: [String : NSURL]?, data: [String : Data]?) -> (Data, String) {
        let encoding = String.Encoding.utf8
        let boundary = "Boundary-\(NSUUID().uuidString)"
        var body = Data()
        
        if let parameters = parameters {
            for (key, value) in parameters {
                body.append("--\(boundary)\r\n".data(using: encoding)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: encoding)!)
                body.append("\(value)\r\n".data(using: encoding)!)
            }
        }
        
        let mimetype = "image/jpeg"
        if let URLs = URLs  {
            for URLKey in Array(URLs.keys) {
                if let URL = URLs[URLKey] {
                    let filename = URL.lastPathComponent!
                    let data = try? Data(contentsOf: URL as URL)
                    
                    body.append("--\(boundary)\r\n".data(using: encoding)!)
                    body.append("Content-Disposition: form-data; name=\"\(URLKey)\"; filename=\"\(filename)\"\r\n".data(using: encoding)!)
                    body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: encoding)!)
                    body.append(data!)
                    body.append("\r\n".data(using: encoding)!)
                }
            }
        }
        
        if let data = data {
            for dataItemKey in Array(data.keys) {
                if let itemData = data[dataItemKey] {
                    body.append("--\(boundary)\r\n".data(using: encoding)!)
                    body.append("Content-Disposition: form-data; name=\"\(dataItemKey)\"; filename=\"\((dataItemKey + "\(Date().timeIntervalSince1970)" + ".jpeg"))\"\r\n".data(using: encoding)!)
                    body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: encoding)!)
                    body.append(itemData)
                    body.append("\r\n".data(using: encoding)!)
                }
            }
        }
        
        body.append("--\(boundary)--\r\n".data(using: encoding)!)
        return (body, "multipart/form-data; boundary=\(boundary)")
    }
    
}

