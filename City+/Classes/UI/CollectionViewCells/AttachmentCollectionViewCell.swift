//
//  AttachmentCollectionViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/22/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

final class AttachmentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var attachmentImageView: UIImageView!

    var attachment: Avatar?
    var deleteHandler: ((_ avatar: Avatar?, _ inCell: AttachmentCollectionViewCell)->())?
    
    func setUp(with avatar: Avatar?) {
        attachment = avatar
    }
    
}

fileprivate extension AttachmentCollectionViewCell {
    
    @IBAction func deleteTapped(_ sender: UIButton) {
        deleteHandler?(attachment, self)
    }
    
}
