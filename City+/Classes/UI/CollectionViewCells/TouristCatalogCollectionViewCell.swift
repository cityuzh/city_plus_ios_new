//
//  CatalogTouristPlacesTableViewCell.swift
//  CityPlus
//
//  Created by Macbook Air 13 on 8/26/20.
//  Copyright © 2020 Ivan Grab. All rights reserved.
//

import UIKit

class TouristCatalogCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var catalogImageView: UIImageView!
    @IBOutlet weak var catalogTextInfo: UILabel!
    
    override func awakeFromNib() {
         super.awakeFromNib()
         layer.shadowColor = UIColor.black.cgColor
         layer.shadowRadius = 2.0
         layer.shadowOpacity = 0.3
         layer.shadowOffset = CGSize(width: 0, height: 1)
         
         shadowView.layer.cornerRadius = 8.0
         shadowView.layer.masksToBounds = true
     }
    
    func setUp(with model: Place) {
        catalogImageView.setImage(with: model.photos.first, placeholder: #imageLiteral(resourceName: "no-image"), isThumb: false, using: .gray, placeholderSettings: nil) { (success) in
        }
        catalogTextInfo.text = model.name
    }
                    
}
