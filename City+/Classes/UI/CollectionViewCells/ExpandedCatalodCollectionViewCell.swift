//
//  ExpandedCatalodCollectionViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 12.12.2019.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

class ExpandedCatalodCollectionViewCell: UICollectionViewCell {
    
//MARK: - IBOutlets
    
    @IBOutlet weak var catalogImageView: UIImageView!
    @IBOutlet weak var catalogName: UILabel!
    @IBOutlet weak var discountCount: UILabel?
    
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func setUp(with model: DiscountCatalog) {
        catalogImageView.setImage(with: model.image, placeholder: #imageLiteral(resourceName: "no-image"), isThumb: false, using: .gray, placeholderSettings: nil) { (success) in
            
        }
        catalogName.text = model.name
        var discountCount = "\(model.discountCount)"
        if model.discountCount == 1 {
            discountCount += NSLocalizedString(" акція", comment: "")
        } else {
            discountCount += NSLocalizedString(" акцій", comment: "")
        }
        self.discountCount?.text = discountCount
    }
        
}
