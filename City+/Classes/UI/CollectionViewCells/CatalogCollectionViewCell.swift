//
//  CatalogCollectionViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 12/25/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class CatalogCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var catalogImageView: UIImageView!
    @IBOutlet weak var catalogName: UILabel!
    @IBOutlet weak var discountCount: UILabel?
    
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0, height: 1)
        
        shadowView.layer.cornerRadius = 8.0
        shadowView.layer.masksToBounds = true
    }
    
    func setUp(with model: DiscountCatalog) {
        catalogImageView.setImage(with: model.image, placeholder: #imageLiteral(resourceName: "no-image"), isThumb: false, using: .gray, placeholderSettings: nil) { (success) in
        }
        catalogName.text = model.name
        var discountCount = "\(model.discountCount)"
        if model.discountCount == 1 {
            discountCount += NSLocalizedString(" акція", comment: "")
        } else {
            discountCount += NSLocalizedString(" акцій", comment: "")
        }
        self.discountCount?.text = discountCount
    }
    
}
