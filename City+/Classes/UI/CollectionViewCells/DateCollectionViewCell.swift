//
//  DateCollectionViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 11/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class DateCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var dateNameLabel: UILabel!
    @IBOutlet weak var dateContainer: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 8
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func setup(with date: Date) {
        dateContainer.isHidden = false
        
        let nameFormatter = DateFormatter()
        nameFormatter.locale = Locale.current
        nameFormatter.dateFormat = "EEE"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d"
        
        dateLabel.text = dateFormatter.string(from: date)
        dateNameLabel.text = nameFormatter.string(from: date)
        if Calendar.current.isDateInToday(date) {
            dateNameLabel.text = NSLocalizedString("Сьогодні", comment: "")
        }
    }
    
    func setupWeekCell() {
        dateContainer.isHidden = true
        
        dateNameLabel.text = NSLocalizedString("Через 7 днів", comment: "")
    }
    
}
