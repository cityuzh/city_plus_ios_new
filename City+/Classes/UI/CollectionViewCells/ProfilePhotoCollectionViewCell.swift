//
//  ProfilePhotoCollectionViewCell.swift
//  City+
//
//  Created by Ivan Grab on 2/19/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class ProfilePhotoCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var photoImageView: UIImageView!

    func setUp(with photo: Avatar?) {
        photoImageView.setImage(with: photo, placeholder: #imageLiteral(resourceName: "no-image"), using: .gray, placeholderSettings: (.center, nil), completion: nil)
    }

}
