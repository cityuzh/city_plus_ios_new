//
//  AttendeeUserCollectionViewCell.swift
//  City+
//
//  Created by Ivan Grab on 4/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class AttendeeUserCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var userPhoto: UIImageView!
    
    func setUp(with user: UserModel) {
        userPhoto.setImage(with: user.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: { (success) in
            if !success {
                self.userPhoto.setImageWith(user.fullName ?? "", color: UIColor.lightGray, circular: true)
            }
        })
    }
    
}
