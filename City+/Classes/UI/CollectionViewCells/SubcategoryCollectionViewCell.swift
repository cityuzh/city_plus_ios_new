//
//  SubcategoryCollectionViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 1/18/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

final class SubcategoryCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var title: UILabel!
    @IBOutlet weak var separator: UIView!
    
    func setUp(with model: PlaceCategory) {
        title.text = model.name
    }
    
    func setSelected(_ selected: Bool) {
        separator.isHidden = !selected
        title.textColor = selected ? UIColor(hex: 0x3B4DC4) : .black
        title.font = selected ? UIFont.monteserratBoldFont(ofSize: 14) : UIFont.monteserratRegularFont(ofSize: 14)
    }
    
}
