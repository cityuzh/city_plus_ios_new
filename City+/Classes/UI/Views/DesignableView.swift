//
//  DesignableView.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/28/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableView: UIView {

    @IBInspectable override var layerMasksToBounds: Bool {
        set { layer.masksToBounds = newValue }
        get { return layer.masksToBounds }
    }
    
    fileprivate var isRoundedSidesStored: Bool = false
    @IBInspectable override var isRoundedSides: Bool {
        set {
            isRoundedSidesStored = newValue
            layer.cornerRadius = newValue ? min(layer.bounds.width, layer.bounds.height) / 2.0 : 0
        }
        get { return isRoundedSidesStored }
    }
    
    @IBInspectable override var cornerRadius: CGFloat {
        get { return self.layer.cornerRadius }
        set { self.layer.cornerRadius = newValue }
    }
    
    // MARK: - Border
    
    @IBInspectable override var borderWidth: CGFloat {
        set { layer.borderWidth = newValue }
        get { return layer.borderWidth }
    }
    
    @IBInspectable override var borderColor: UIColor? {
        set { layer.borderColor = newValue?.cgColor }
        get { return UIColor(cgColor: layer.borderColor!) }
    }
    
    // MARK: - Shadow
    
    @IBInspectable override var shadowOffset: CGSize {
        set { layer.shadowOffset = newValue }
        get { return layer.shadowOffset }
    }
    
    @IBInspectable override var shadowOpacity: Float {
        set { layer.shadowOpacity = newValue }
        get { return layer.shadowOpacity }
    }
    
    @IBInspectable override var shadowRadius: CGFloat {
        set { layer.shadowRadius = newValue }
        get { return layer.shadowRadius }
    }
    
    @IBInspectable override var shadowColor: UIColor? {
        set { layer.shadowColor = newValue?.cgColor }
        get { return UIColor(cgColor: layer.shadowColor!) }
    }
    
    @IBInspectable override var isShadowPath: Bool {
        set { layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath }
        get { return layer.shadowPath != nil }
    }
    
    //MARK: - Lifecycle
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        if isRoundedSidesStored {
            layer.cornerRadius = min(layer.bounds.height, layer.bounds.width) / 2.0
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isRoundedSidesStored {
            layer.cornerRadius = min(layer.bounds.height, layer.bounds.width) / 2.0
        }
    }

}


