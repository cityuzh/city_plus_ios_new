//
//  TableViewPlaceholderView.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/6/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class TableViewPlaceholderView: UIView {

    @IBOutlet fileprivate weak var placehlderContentView: UIView!
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var messageTitle: UILabel!
    @IBOutlet fileprivate weak var messageSubTitle: UILabel!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    var currentType: PlaceholderMode = .noDataItems
    
    fileprivate var customImage: UIImage?
    fileprivate var customTitle: String?
    fileprivate var customSubTitle: String?
    
    //MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    convenience init(frame: CGRect, with mode: PlaceholderMode) {
        self.init(frame: frame)
        self.currentType = mode
    }
    
    convenience init(frame: CGRect, with mode: PlaceholderMode, customImage: UIImage? = nil, customTitle: String? = nil, customSubTitle: String? = nil) {
        self.init(frame: frame)
        self.currentType = mode
        self.customImage = customImage
        self.customTitle = customTitle
        self.customSubTitle = customSubTitle        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        activityIndicator.stopAnimating()
        if let customImage = customImage {            
            switch currentType {
            case .loading:
                imageView.image = nil
                activityIndicator.startAnimating()
            case .noInternetConnection:
                imageView.image = #imageLiteral(resourceName: "noInternetPlaceholder")
            default:
                imageView.image = customImage
            }
        } else {
            switch currentType {
            case .loading:
                imageView.image = nil
                activityIndicator.startAnimating()
            case .enterSearchText:
                imageView.image = #imageLiteral(resourceName: "noItemsPlaceholder")
            case .noDataItems:
                imageView.image = #imageLiteral(resourceName: "noItemsPlaceholder")
            case .noPhotoItems:
                imageView.image = #imageLiteral(resourceName: "img")
            case .noInternetConnection:
                imageView.image = #imageLiteral(resourceName: "noInternetPlaceholder")
            case .noDataInfo:
                imageView.image = #imageLiteral(resourceName: "img_empty state_no discussion")
            case .noDataResponse:
                imageView.image = #imageLiteral(resourceName: "imgResponse")
            case .noPlaceState:
                imageView.image = #imageLiteral(resourceName: "no_place")
            }
        }
        
        if let customTitle = customTitle {
            switch currentType {
            case .loading, .noInternetConnection:
                messageTitle.text = currentType.title
            default:
                messageTitle.text = customTitle
            }
        } else {
            messageTitle.text = currentType.title
            
        }
        
        if let customSubTitle = customSubTitle {
            switch currentType {
            case .loading, .noInternetConnection:
                messageSubTitle.text = ""
            case .noDataInfo :
                messageSubTitle.text = "Змінить параметри"
            
            default:
                messageSubTitle.text = customSubTitle
            }
        } else {
            switch currentType{
            case .noDataInfo :
                messageSubTitle.text = "Змінить параметри пошуку або створіть власне обговорення, натиснувши на + зверху"
            case .noPlaceState :
                messageSubTitle.text = "Змініть параметри пошуку"

            default:
                messageSubTitle.text = ""

            }
        }
    }

    fileprivate func commonInit() {
        Bundle.main.loadNibNamed(String(describing: TableViewPlaceholderView.classForCoder()), owner: self, options: nil)
        placehlderContentView.frame = self.bounds
        placehlderContentView.layoutIfNeeded()
        self.addSubview(placehlderContentView)
    }
}

extension TableViewPlaceholderView {
    
    enum PlaceholderMode {
        case loading
        case noDataItems
        case noPhotoItems
        case enterSearchText
        case noInternetConnection
        case noDataInfo
        case noDataResponse
        case noPlaceState
       
        var title: String {
            switch self {
            case .loading:
                return NSLocalizedString("Дані завантажуються...", comment: "")
            case .enterSearchText:
                return NSLocalizedString("Для пошуку введіть ім`я або прізвище!", comment: "")
            case .noDataItems:
                return NSLocalizedString("Дані відсутні", comment: "")
            case .noPhotoItems:
                return NSLocalizedString("Фото відсутні", comment: "")
            case .noInternetConnection:
                return NSLocalizedString("Інтернен з'єднання відсутнє", comment: "")
            case .noDataInfo:
                return NSLocalizedString("Обговорення відсутні", comment: "")
            case .noDataResponse :
                return NSLocalizedString("Відгуки відсутні", comment: "")
            case .noPlaceState:
                return NSLocalizedString("Заклади відсутні", comment: "")
            }
        }
    }
    
}
