//
//  MarkerView.swift
//
//  Created by Ivan Grab on 10/26/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit

final class MarkerView: UIView {

    @IBOutlet weak var markerView: UIView!
    @IBOutlet weak var markerImage: UIImageView!
    @IBOutlet weak var tailImage: UIImageView!
    @IBOutlet weak var bubbleMarkerView: UIView!
    @IBOutlet weak var markerTitle: UILabel!
    
    @IBOutlet weak var bubleHeight: NSLayoutConstraint!
    @IBOutlet weak var tailTopSpace: NSLayoutConstraint!

    var place: Place!
    
    var selected: Bool = false {
        didSet{
            highlightMarker(selected)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupMarkerView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupMarkerView()
    }
    
    func highlightMarker(_ selected: Bool, animated: Bool = true) {
        if animated {
            
//            if selected {
//                bubleHeight.constant = markerView.bounds.width
//                UIView.animate(withDuration: Constants.animationDuration, animations: {
//                    self.markerTitle.isHidden = false
//                    self.bubbleMarkerView.backgroundColor = UIColor.appTintColor
//                    self.bubbleMarkerView.layer.cornerRadius = self.bubleHeight.constant / 2
//                    self.markerImage.image = self.markerImage.image?.tint(with: UIColor.white)
//
//                    self.bubbleMarkerView.layoutIfNeeded()
//                }, completion: { (finished) in
//                    self.tailImage.isHidden = false
//                })
//            } else {
//                bubleHeight.constant = Constants.defaultMarkerWidth
//                UIView.animate(withDuration: Constants.animationDuration, animations: {
//                    self.markerTitle.isHidden = true
//                    self.bubbleMarkerView.backgroundColor = UIColor.white
//                    self.bubbleMarkerView.layer.cornerRadius = self.bubleHeight.constant / 2
//                    self.markerImage.image = self.markerImage.image?.tint(with: UIColor.appTintColor)
//                    self.tailImage.isHidden = true
//                    self.bubbleMarkerView.layoutIfNeeded()
//                }, completion: { (finished) in
//
//                })
//            }
//
        } else {
            
        }
    }
    
}

fileprivate extension MarkerView {
    
    func setupMarkerView() {
//        Bundle.main.loadNibNamed(String(describing: MarkerView.classForCoder()), owner: self, options: nil)
//        
//        bubbleMarkerView.layer.cornerRadius = bubbleMarkerView.bounds.width / 2
//        bubbleMarkerView.backgroundColor = UIColor.white
//        markerTitle.isHidden = true
//        
//        tailImage.image = #imageLiteral(resourceName: "pin_min_ic").tint(with: UIColor.appTintColor)
//        
//        self.addSubview(markerView)
    }
    
    
    
    
    
    struct Constants {
        static let animationDuration: TimeInterval = TimeInterval(0.3)
        static let springDumping = CGFloat(0.6)
        static let springVelocity = CGFloat(1)
        
        static let defaultMarkerWidth = CGFloat(44.0)
        static let defaultTailSpace = CGFloat(-15.0)
    }
    
}
