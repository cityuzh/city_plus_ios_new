//
//  EstablishmentMarker.swift
//  PetPals
//
//  Created by Ivan Grab on 10/25/17.
//  Copyright © 2017 WallTreeSoft. All rights reserved.
//

import UIKit
import GoogleMaps

public class EstablishmentMarker: GMSMarker, GMUClusterItem {

    // MARK: - Properties
    
    var place: Place!
    var selected: Bool = false
    // MARK: - Lifecycle

    convenience init(with place: Place!, coordinates: CLLocationCoordinate2D!) {
        self.init(position: coordinates)
        self.place = place
//        let iconView = MarkerView(frame: CGRect(x: 0, y: 0, width: 80, height: 120))
//        iconView.markerTitle.text = place.name
//        iconView.markerImage.setImage(with: place.image, placeholder: #imageLiteral(resourceName: "placeholder-on-map-paper-in-perspective"), using: .gray, placeholderSettings: nil) { (success) in
//            iconView.markerImage.image = iconView.markerImage.image?.tint(with: UIColor.appTintColor)
//        }
//        iconView.place = place
//        self.iconView = iconView
//        self.iconView!.clipsToBounds = false
    }
    
}

// MARK: - Private
