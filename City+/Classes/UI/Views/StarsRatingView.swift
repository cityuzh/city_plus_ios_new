//
//  StarsRatingView.swift
//  TreatMe
//
//  Created by Ivan Grab on 05.11.15.
//  Copyright © 2015 MutualCore. All rights reserved.
//

import UIKit

struct StarsSettings {
    static let starsCount = 5
    static let activeString = "★"
    static let notActiveString = "☆"
    static let halfActiveString = "☆"
    static func cellSizeWithHeight(height: CGFloat) -> CGSize {
        return CGSize(width: height, height: height)
    }
}

protocol StarsViewDelegate: NSObjectProtocol {
    
    func changedRatingAtView(rating: NSNumber)
    
}

class StarsRatingView: UIView {
    
    //MARK: - Properties
    
    var collectionView: UICollectionView?
    var rating: NSNumber = 0 {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    var activeImage: UIImage?
    var halfActiveImage: UIImage?
    var nonActiveImage: UIImage?
    
    weak var starsDelegate : StarsViewDelegate?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        
        activeImage = #imageLiteral(resourceName: "rateFull")
        nonActiveImage = #imageLiteral(resourceName: "rateEmpty")
        halfActiveImage = #imageLiteral(resourceName: "rateHalf")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        addCollectionView()
    }
    
    //MARK: - Private
    
    private func addCollectionView() {
        let flow = UICollectionViewFlowLayout()
        flow.itemSize = StarsSettings.cellSizeWithHeight(height: self.frame.height)
        flow.scrollDirection = UICollectionView.ScrollDirection.horizontal
        flow.sectionInset = UIEdgeInsets.zero
        flow.minimumInteritemSpacing = 5
        flow.minimumLineSpacing = 5
        collectionView = UICollectionView(frame: self.frame, collectionViewLayout: flow)
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.backgroundColor = UIColor.clear
        collectionView?.register(UICollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "StarsIdentifier")
        self.addSubview(collectionView!)
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-0.0-[view]-0.0-|", options: .alignAllLeft, metrics: nil, views:Dictionary(dictionaryLiteral: ("view", collectionView!))))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0.0-[view]-0.0-|", options: .alignAllLeft, metrics: nil, views: Dictionary(dictionaryLiteral: ("view", collectionView!))))
        self.setNeedsLayout()
    }
    
    private func imageFromString(string: String) -> UIImage {
        
        var fontSize: CGFloat = 1
        var fontBounds = CGRect()
        let inBounds = CGRect(x: 0, y: 0, width: StarsSettings.cellSizeWithHeight(height: self.frame.height).width, height: StarsSettings.cellSizeWithHeight(height: self.frame.height).width)
        
        while fontBounds.width < inBounds.width && fontBounds.height < inBounds.height {
            fontSize += 0.1
            fontBounds = NSString(string: string).boundingRect(with: inBounds.size, options: .usesFontLeading, attributes: [NSAttributedString.Key.font : iconFontWithSize(size: fontSize)], context: nil)
        }
        
        UIGraphicsBeginImageContextWithOptions(inBounds.size, false, UIScreen.main.scale)
        (string as NSString).draw(in: centerAlignmentedRect(rect: fontBounds, inRect: inBounds), withAttributes: [NSAttributedString.Key.font : iconFontWithSize(size: fontSize), NSAttributedString.Key.foregroundColor : UIColor.black])
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return result!
    }
    
    private func iconFontWithSize(size: CGFloat) -> UIFont {
        guard let font = UIFont(name: "FontAwesome", size: size) else {
            return UIFont.systemFont(ofSize: size)
        }
        return font
    }
    
    private func centerAlignmentedRect( rect: CGRect, inRect: CGRect) -> CGRect {
        var rect = rect
        rect.origin.x = (inRect.width-rect.width)*0.5
        rect.origin.y = (inRect.height-rect.height)*0.5
        return rect
    }
}

//MARK: - Extension

extension StarsRatingView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StarsIdentifier", for: indexPath)
        cell.backgroundColor = UIColor.clear
        
        guard let starImageView = cell.viewWithTag(3) as? UIImageView else {
            let starImageView = UIImageView(frame: cell.bounds)
            starImageView.tag = 3
            cell.addSubview(starImageView)
            starImageView.image = CGFloat(rating.floatValue) >= CGFloat(indexPath.item) ? CGFloat(rating.floatValue - Float(indexPath.item)) >= 1 ? self.activeImage : CGFloat(rating.floatValue - Float(indexPath.item)) >= 0.5 ? self.halfActiveImage : self.nonActiveImage : self.nonActiveImage
            if rating.floatValue == 5 && indexPath.item == 4 {
                starImageView.image = self.activeImage
            }
            return cell
        }
                
        starImageView.image = CGFloat(rating.floatValue) >= CGFloat(indexPath.item) ? CGFloat(rating.floatValue - Float(indexPath.item)) >= 1 ? self.activeImage : CGFloat(rating.floatValue - Float(indexPath.item)) >= 0.5 ? self.halfActiveImage : self.nonActiveImage : self.nonActiveImage
        if rating.floatValue == 5 && indexPath.item == 4 {
            starImageView.image = self.activeImage
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.rating = NSNumber(value: indexPath.row + 1)
        if let delegate = starsDelegate {
            delegate.changedRatingAtView(rating: self.rating)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = self.frame.height * 5
        let totalSpacingWidth: CGFloat = CGFloat(5 * 4)
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets.init(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return StarsSettings.starsCount
    }
    
}
