//
//  CustomisableTextField.swift
//  CityPlus
//
//  Created by Ivan Grab on 12/26/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

@IBDesignable
class CustomisableTextField: UITextField {

    //MARK - Inspectable
    
    @IBInspectable var showBorder: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var lefTextInsets: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var lefImageInsets: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var leftImage: UIImage? = nil {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var leftContentMode: UIView.ContentMode = .center {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var rightImage: UIImage? = nil {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var rightHighlitedImage: UIImage? = nil {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var rightContentMode: UIView.ContentMode = .center {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var rightViewActionHandler: ((_ sender: CustomisableTextField, _ button: UIButton)->())?
    
    //MARK: - Lifecycle
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupBorder()
        setupViews(for: rect)
        setNeedsDisplay()
    }
        
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        let rightInset = rightImage == nil ? lefTextInsets : bounds.height + lefTextInsets
        let newRect = rect.inset(by: UIEdgeInsets(top: 0, left: lefTextInsets, bottom: 0, right:  rightInset))
        return newRect
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        let rightInset = rightImage == nil ? lefTextInsets : bounds.height + lefTextInsets
        let newRect = rect.inset(by: UIEdgeInsets(top: 0, left: lefTextInsets, bottom: 0, right: rightInset))
        return newRect
    }
        
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.leftViewRect(forBounds: bounds)
        let newRect = CGRect(origin: CGPoint(x: lefImageInsets, y: rect.origin.y), size: rect.size)
        return newRect
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.rightViewRect(forBounds: bounds)
        let newRect = CGRect(origin: CGPoint(x: rect.origin.x - lefImageInsets, y: rect.origin.y), size: rect.size)
        return newRect
    }
    
}

fileprivate extension CustomisableTextField {
    
    func setupBorder() {
        if showBorder {
            layer.borderColor = borderColor?.cgColor
            layer.borderWidth = borderWidth
        } else {
            layer.borderColor = UIColor.clear.cgColor
            layer.borderWidth = 0.0
        }
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
    }
    
    func setupViews(for rect: CGRect) {
        setupLeftView()
        setupRightView(for: rect)
    }
    
    func setupLeftView() {
        if let leftImage = leftImage {
            leftViewMode = .always
            let imageView = getImageView(with: lefImageInsets)
            imageView.contentMode = self.leftContentMode
            imageView.image = leftImage
            leftView = imageView
        } else {
            leftView = nil
        }
    }
    
    func setupRightView(for rect: CGRect) {
        if let rightImage = rightImage {
            rightViewMode = .always
            let button = getButton(for: rect)
            button.setImage(rightImage, for: .normal)
            button.setImage(rightHighlitedImage, for: .selected)
            rightView = button
        } else {
            rightView = nil
        }
    }
    
    func getImageView(with insetesValue: CGFloat = 0.0) -> UIImageView {
        
        let size = bounds.height - 2.0 * insetesValue
        let rect = CGRect(origin: CGPoint(x: lefImageInsets, y: insetesValue), size: CGSize(width: size*2, height: size))
        
        let imageView = UIImageView(frame: rect)
        return imageView
    }
    
    func getButton(for rect: CGRect) -> UIButton {
        
        let size = bounds.height
        let rect = CGRect(origin: CGPoint(x: rect.width - size, y: 0), size: CGSize(width: size, height: size))
        
        let button = UIButton(frame: rect)
        button.addTarget(self, action: #selector(rightButtonAction(_:)), for: .touchUpInside)
        return button
    }
    
    @objc func rightButtonAction(_ sender: UIButton) {
        rightViewActionHandler?(self, sender)
        sender.isSelected = !sender.isSelected
    }
    
}
