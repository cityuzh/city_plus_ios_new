//
//  CustomisebleTextView.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/22/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

@IBDesignable
final class CustomisableTextView: UITextView {
    
    @IBInspectable var maxCharacters: Int = 0
    @IBInspectable var leftPlaceholderInsets: CGFloat = 10
    @IBInspectable var topPlaceholderInsets: CGFloat = 10
    @IBInspectable var placeholder: String = "" {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var placeholderColor: UIColor = .darkText {
        didSet {
            setNeedsLayout()
        }
    }
    
    fileprivate var placeholderLabel: UILabel?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(textViewDidChangeText(_:)), name: UITextView.textDidChangeNotification, object: nil)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        putPlaceholderIfNeed()
    }
    
}

fileprivate extension CustomisableTextView {
    
    func putPlaceholderIfNeed() {
        placeholderLabel?.removeFromSuperview()
        if !placeholder.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty, text.isEmpty {
            let placeholderLabel = createLabel(for: bounds.size)
            addSubview(placeholderLabel)
            self.placeholderLabel = placeholderLabel
        }
    }
    
    func createLabel(for size: CGSize) -> UILabel {
        let label = UILabel(frame: CGRect(origin: CGPoint(x: leftPlaceholderInsets, y: topPlaceholderInsets), size: CGSize(width: size.width - leftPlaceholderInsets * 2, height: size.height)))
        label.textAlignment = .left
        label.font = font
        label.numberOfLines = 0
        label.textColor = placeholderColor
        label.text = placeholder
        label.sizeToFit()
        return label
    }
    
    @objc func textViewDidChangeText(_ notification: NSNotification) {
        putPlaceholderIfNeed()
    }
    
}
