//
//  NewTopicTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/12/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Nuke
import MobileCoreServices

final class NewTopicTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var photoCollectionView: UICollectionView!
    
    @IBOutlet fileprivate weak var saveBarButtonItem: UIBarButtonItem!
    @IBOutlet fileprivate weak var addPhotoButton: UIButton!
    
    @IBOutlet fileprivate weak var titleTextView: UITextView!
    @IBOutlet fileprivate weak var contentTextView: UITextView!
    
    @IBOutlet var textViews: [UITextView]!
    //MARK: - Properties
    
    var topic = Topic()
    fileprivate var images: [Avatar] = []
    fileprivate var imagesForUpload: [Avatar] = []
    
    var isUpdating = false
    var creatingHandler: ((_: Topic)->())?
    var deleteHandler: (()->())?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isUpdating {
            navigationItem.rightBarButtonItem = saveBarButtonItem
            navigationItem.title = NSLocalizedString("Редагування обговорення", comment: "")
        }
        fillUI(with: topic)
        photoCollectionView.contentInset.left = 15
    }


    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 2:
            var freeSpace = tableView.bounds.height
            for i in 0..<2 {
                freeSpace -= self.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            freeSpace -= self.tableView(tableView, heightForRowAt: IndexPath(row: 3, section: 0))
            return freeSpace
        case 3:
            return isUpdating ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        textViews.forEach({ $0.resignFirstResponder() })
        
    }

}

fileprivate extension NewTopicTableViewController {
    
    @IBAction func create(_ sender: UIBarButtonItem) {
        topic.title = titleTextView.text
        topic.topicContent = contentTextView.text
        
        if imagesForUpload.count > 0 || images.count > 0 || !(topic.title ?? "").isEmpty || !(topic.topicContent ?? "").isEmpty {
            topic.onRequestFailure = requestFailureHandler
            present(activityVC, animated: true, completion: nil)
            if isUpdating {
                topic.update { (success) in
                    if success {
                        if self.imagesForUpload.count > 0 {
                            self.topic.attchments = [self.imagesForUpload[0]]
                            self.startUploadPhoto(self.imagesForUpload[0], with: { (success) in
                                self.activityVC.dismiss(animated: true, completion: {
                                    if success {
                                        self.creatingHandler?(self.topic)
                                        self.navigationController?.popViewController(animated: true)
                                    } else {
                                        ShowAlert.showInfoAlert(in: self, message: "Something gows wrong!")
                                    }
                                })
                            })
                        } else {
                            self.activityVC.dismiss(animated: true, completion: {
                                if success {
                                    self.creatingHandler?(self.topic)
                                    self.navigationController?.popViewController(animated: true)
                                } else {
                                    ShowAlert.showInfoAlert(in: self, message: "Something gows wrong!")
                                }
                            })
                        }
                    }
                }
            } else {
                topic.create { (success) in
                    if success {
                        if self.imagesForUpload.count > 0 {
                            self.topic.attchments = [self.imagesForUpload[0]]
                            self.startUploadPhoto(self.imagesForUpload[0], with: { (success) in
                                self.activityVC.dismiss(animated: true, completion: {
                                    if success {
                                        self.creatingHandler?(self.topic)
                                        self.navigationController?.popViewController(animated: true)
                                    } else {
                                        ShowAlert.showInfoAlert(in: self, message: "Something gows wrong!")
                                    }
                                })
                            })
                        } else {
                            self.activityVC.dismiss(animated: true, completion: {
                                if success {
                                    self.creatingHandler?(self.topic)
                                    self.navigationController?.popViewController(animated: true)
                                } else {
                                    ShowAlert.showInfoAlert(in: self, message: "Something gows wrong!")
                                }
                            })
                        }
                    }
                }
            }
        } else {
            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Тема, опис або фото обов`язкові для заповнення!", comment: ""))
        }
    }
    
    @IBAction func addPhotoAction(_ sender: UIButton) {
        startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: nil)
                }
            })
        }, onCancelAction: nil)
    }
    
    func startUploadPhoto(_ photo: Avatar,  with completion: @escaping NetworkModelCompletion) {
        guard let image = photo.image else {
            completion(false)
            return
        }
        topic.uploadPhoto(photo: image) { (success, avatar) in
            if let uploadedAvatar = avatar, success {
                self.topic.attchments.append(uploadedAvatar)
                self.imagesForUpload.remove(at: 0)
                guard self.imagesForUpload.count > 0 else {
                    completion(true)
                    return
                }
                self.startUploadPhoto(self.imagesForUpload[0], with: completion)
            } else {
                completion(false)
            }
        }
    }
    
    @IBAction func deleteTopic(_ sender: UIButton) {
        let delete = UIAlertAction(title: NSLocalizedString("Так", comment: ""), style: .destructive, handler: { (action) in
            self.present(self.activityVC, animated: true, completion: nil)
            self.topic.onRequestFailure = self.requestFailureHandler
            self.topic.delete(with: { (success) in
                if success {
                    self.deleteHandler?()
                    self.activityVC.dismiss(animated: true, completion: {
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                }
            })
        })
        ShowAlert.showAlertContrller(with: .alert, title: nil, message: NSLocalizedString("Ви дійсно бажаєте видалити це обговорення?", comment: ""), actions: [delete], in: self)
    }
    
    func fillUI(with topic: Topic) {
        self.titleTextView.text = topic.title
        self.contentTextView.text = topic.topicContent
        self.images = topic.attchments
        self.photoCollectionView.reloadData()
    }
    
}

//MARK: - PikingImage

extension NewTopicTableViewController: PickingImage {
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    
    func pickingDidCancel() {
        
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        if let newImage = thumbnail {
            let avatarModel = Avatar()
            avatarModel.image = newImage
            images.append(avatarModel)
            imagesForUpload.append(avatarModel)
        }
        photoCollectionView.reloadData()
    }
    
}

extension NewTopicTableViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let imageView = UIImageView(frame: collectionView.bounds)
        imageView.image = #imageLiteral(resourceName: "Image.png")
        imageView.contentMode = .center
        collectionView.backgroundView = images.count == 0 ? imageView : nil
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AttachmentCollectionViewCell.classForCoder()), for: indexPath) as! AttachmentCollectionViewCell
        cell.setUp(with: images.count > 0 ? images[indexPath.row] : nil)
        let option = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "no-image"), transition: .fadeIn(duration: 0.3), failureImage: #imageLiteral(resourceName: "profilePlaceholder"), failureImageTransition: .fadeIn(duration: 0.3), contentModes: nil)
        if let image = images[indexPath.row].image {
            cell.attachmentImageView.image = image
        } else if let url = URL(string: images[indexPath.row].urlPath ?? "") {
            Nuke.loadImage(with: url, options: option, into: cell.attachmentImageView, progress: nil, completion: nil)
        }
        cell.deleteHandler = { (image, cell) in
            if let indexPath = collectionView.indexPath(for: cell), let attachment = image, let imageIndex = self.images.firstIndex(where: { $0.identifier == attachment.identifier })  {
                if let existedIndex = self.topic.attchments.firstIndex(where: { $0.identifier == attachment.identifier }), attachment.identifier != nil {
                    self.topic.deletePhoto(photoId: self.topic.attchments[existedIndex].identifier!) { (deleted, nil) in
                        self.images.remove(at: existedIndex)
                        self.topic.attchments.remove(at: existedIndex)
                        collectionView.performBatchUpdates({
                            collectionView.deleteItems(at: [indexPath])
                        }, completion: nil)
                    }
                } else {
                    self.images.remove(at: imageIndex)
                    self.imagesForUpload.remove(at: imageIndex)
                    collectionView.performBatchUpdates({
                        collectionView.deleteItems(at: [indexPath])
                    }, completion: nil)
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.height - 30, height: collectionView.bounds.height - 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

extension NewTopicTableViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == titleTextView {
            return textView.text.count + text.count <= 120
        } else {
            return textView.text.count + text.count <= 1000
        }
    }
    
}
