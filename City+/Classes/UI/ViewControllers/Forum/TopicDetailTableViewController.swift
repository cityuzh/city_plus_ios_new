//
//  TopicDetailTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/15/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Nuke
import Starscream
import MobileCoreServices
import ALTextInputBar
import ObjectMapper

final class TopicDetailTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var moreButton: UIBarButtonItem!
    
    //MARK: - Properties
    //ws/topic/{topic_id}/
    var selectedAttachment: Avatar? {
        didSet {
            leftBarView.isSelected = selectedAttachment != nil
            inputBar.alwaysShowRightButton = selectedAttachment != nil
            reloadInputViews()
        }
    }
    var topic: Topic!
    var replyedUser: UserModel? {
        didSet {
            if let name = replyedUser?.fullName {
                if !inputBar.text.contains(name) {
                    inputBar.textView.insertText(name)
                }
            }
        }
    }
    lazy var itemList: PaginatedListModel<TopicComment> = {
        let list = PaginatedListModel<TopicComment>.init(path: String(format: TopicComment.Constants.commentsListRouteFormat, topic.identifier ?? 0))
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        list.requestRowCount = 10
        list.isReversed = true
        return list
    }()
    
    lazy var inputBar: ALTextInputBar = {
        let bar = ALTextInputBar()
        bar.showTextViewBorder = true
        bar.textViewCornerRadius = 15
        bar.textViewBorderColor = UIColor(hex: 0xB0B8E8)
        bar.backgroundColor = .white
        bar.defaultHeight = 50
        bar.delegate = self
        bar.horizontalSpacing = 0
        bar.rightView = rightBarView
        bar.leftView = leftBarView
        bar.textView.placeholder = NSLocalizedString("Прокоментуйте", comment: "")
        return bar
    }()
    
    lazy var leftBarView: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.addTarget(self, action: #selector(attachmendAction(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "clip"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "clipActive"), for: .selected)
        return button
    }()
    
    lazy var rightBarView: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.addTarget(self, action: #selector(sendAction(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "send").tint(with: UIColor(hex:0xB0B8E8)), for: .normal)
        return button
    }()
    
    var keyboardHeight: CGFloat = 50
    var lastInputHeight: CGFloat = 50
    
    var webSocket: WebSocket?
    
    override var inputAccessoryView: UIView? {
        get {
            return canBecome ? inputBar : nil
        }
    }
    
    var canBecome: Bool = true
    override var canBecomeFirstResponder: Bool {
        return canBecome
    }
    
    var deleteHandler: ((_: Topic)->())?
    var updateHandler: ((_: Topic)->())?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "TopicCommentsHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "TopicCommentsHeaderView")
        itemList.load { (success) in
            self.tableView.reloadData()
            var request = URLRequest(url: URL(string: String(format: Configuration.socketForumURLPathFormat, self.topic.identifier ?? 0))!)
            request.timeoutInterval = 5
            request.setValue("Token \((self.sessionManager.token?.string ?? ""))", forHTTPHeaderField: "Authorization")
            self.webSocket = WebSocket(request: request)
            self.connectSockets()
            self.scrollToBottom()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = topic.title
        if !topic.isMy {
            navigationItem.rightBarButtonItem = nil
            navigationItem.rightBarButtonItems = nil
        }
        refreshContent(nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disconnectSockets()
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        topic.get { (success) in
            if success {
                self.itemList.load(completion: { (success) in
                    sender?.endRefreshing()
                    if success {
                        self.placeholderMode = .noDataItems
                    }
                    self.tableView.reloadData()
                })
            } else {
                sender?.endRefreshing()
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? topic.attchments.count > 0 ? 3 : 2 : itemList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopicTableViewCell.classForCoder()), for: indexPath) as! TopicTableViewCell
                cell.setUp(with: topic)
                cell.profileHandler = { (actionCell) in
                    let user = self.topic.user
                    let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                    profileVC.user = user
                    profileVC.showMenuButton = false
                    self.show(profileVC, sender: self)
                }
                let option = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "profilePlaceholder"), transition: .fadeIn(duration: 0.3), failureImage: #imageLiteral(resourceName: "profilePlaceholder"), failureImageTransition: .fadeIn(duration: 0.3), contentModes: nil)
                if let url = URL(string: topic.user?.avatar?.urlPath ?? "") {
                    Nuke.loadImage(with: url, options: option, into: cell.userImageView, progress: nil, completion: nil)
                }
                return cell
            case 1:
                if topic.attchments.count > 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopicPhotosTableViewCell.classForCoder()), for: indexPath) as! TopicPhotosTableViewCell
                    cell.setUp(with: topic)
                    cell.photoTappedHandler = { (imageIndex) in
                        let previewVC: ImagePreviewViewController = UIStoryboard(.Chats).instantiateViewController()
                        previewVC.attachment = self.topic.attchments[imageIndex]
                        self.show(previewVC, sender: self)
                    }
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopicLikeInfoTableViewCell.classForCoder()), for: indexPath) as! TopicLikeInfoTableViewCell
                    cell.setUp(with: topic)
                    cell.likeHandler = { (likedCell, countLabel, button, image) in
                        button.isEnabled = false
                        if !self.topic.isLikedByMe {
                            self.topic.like(with: { (success) in
                                button.isEnabled = true
                                if success {
                                    self.topic.isLikedByMe = true
                                    self.topic.likesCount += 1
                                    countLabel.text = "\(self.topic.likesCount)"
                                    self.tableView.beginUpdates()
                                    self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                                    self.tableView.endUpdates()
                                    image.isHighlighted = true
                                }
                            })
                        } else {
                            self.topic.dislike(with: { (success) in
                                button.isEnabled = true
                                if success {
                                    self.topic.isLikedByMe = false
                                    self.topic.likesCount -= 1
                                    countLabel.text = "\(self.topic.likesCount)"
                                    image.isHighlighted = false
                                    self.tableView.beginUpdates()
                                    self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                                    self.tableView.endUpdates()
                                }
                            })
                        }
                    }
                    cell.likersHandler = { (likedCell) in
                        if let likedIndexPath = tableView.indexPath(for: likedCell) {
                            let likersContainerVC: LikerContainerViewController = UIStoryboard(.Forum).instantiateViewController()
                            likersContainerVC.topic = self.topic
                            likersContainerVC.comment = self.itemList[likedIndexPath.row]
                            
                            let nVC = UINavigationController(rootViewController: likersContainerVC)
                            nVC.setNavigationBarHidden(true, animated: false)
                            nVC.modalPresentationStyle = .overCurrentContext
                            self.present(nVC, animated: true, completion: nil)
                        }
                    }
                    return cell
                }
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopicLikeInfoTableViewCell.classForCoder()), for: indexPath) as! TopicLikeInfoTableViewCell
                cell.setUp(with: topic)
                cell.likeHandler = { (likedCell, countLabel, button, image) in
                    button.isEnabled = false
                    if !self.topic.isLikedByMe {
                        self.topic.like(with: { (success) in
                            button.isEnabled = true
                            if success {
                                self.topic.isLikedByMe = true
                                self.topic.likesCount += 1
                                countLabel.text = "\(self.topic.likesCount)"
                                image.isHighlighted = true
                                likedCell.setUp(with: self.topic)
                            }
                        })
                    } else {
                        self.topic.dislike(with: { (success) in
                            button.isEnabled = true
                            if success {
                                self.topic.isLikedByMe = false
                                self.topic.likesCount -= 1
                                countLabel.text = "\(self.topic.likesCount)"
                                image.isHighlighted = false
                                likedCell.setUp(with: self.topic)                            
                            }
                        })
                    }
                }
                cell.likersHandler = { (likedCell) in
                    let likersContainerVC: LikerContainerViewController = UIStoryboard(.Forum).instantiateViewController()
                    likersContainerVC.topic = self.topic
                    let nVC = UINavigationController(rootViewController: likersContainerVC)
                    nVC.setNavigationBarHidden(true, animated: false)
                    nVC.modalPresentationStyle = .overCurrentContext
                    self.present(nVC, animated: true, completion: nil)
                }
                return cell
            default:
                let cell = UITableViewCell()
                return cell
            }
        } else {
            var identifier = String(describing: TopicCommentTableViewCell.classForCoder())
            if itemList[indexPath.row].attachment != nil {
                identifier = "Attahment" + String(describing: TopicCommentTableViewCell.classForCoder())
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! TopicCommentTableViewCell
            cell.setUp(with: itemList[indexPath.row])
            cell.profileHandler = { (actionCell) in
                if let index = tableView.indexPath(for: actionCell) {
                    let user = self.itemList[index.row].sender
                    let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                    profileVC.user = user
                    profileVC.showMenuButton = false
                    self.show(profileVC, sender: self)
                }
            }
            cell.likeHandler = { (likedCell, countLabel, button, image) in
                if let likedIndexPath = tableView.indexPath(for: likedCell) {
                    let likedComment = self.itemList[likedIndexPath.row]
                    button.isEnabled = false
                    if !likedComment.isLikedByMe {
                        likedComment.like(with: { (success) in
                            button.isEnabled = true
                            if success {
                                likedComment.likesCount += 1
                                likedComment.isLikedByMe = true
                                countLabel.text = "\(likedComment.likesCount)"
                                image.isHighlighted = true
                                likedCell.setUp(with: likedComment)
                            }
                        })
                    } else {
                        likedComment.dislike(with: { (success) in
                            button.isEnabled = true
                            if success {
                                likedComment.likesCount -= 1
                                likedComment.isLikedByMe = false
                                countLabel.text = "\(likedComment.likesCount)"
                                image.isHighlighted = false
                                likedCell.setUp(with: likedComment)
                            }
                        })
                    }
                }
            }
            cell.replyHandler = { (actionCell) in
                if let likedIndexPath = tableView.indexPath(for: actionCell), let user = self.itemList[likedIndexPath.row].sender, !user.isMe {
                    self.replyedUser = user
                }
            }
            cell.attachmentHandler = { (sender) in
                if let index = tableView.indexPath(for: sender)?.row {
                    let attachment = self.itemList[index].attachment
                    let imagePreviewVC: ImagePreviewViewController = UIStoryboard(.Chats).instantiateViewController()
                    imagePreviewVC.attachment = attachment
                    self.show(imagePreviewVC, sender: self)
                }
            }
            cell.moreHandler = { (actionCell) in
                let delete = UIAlertAction(title: NSLocalizedString("Видалити", comment: ""), style: .destructive, handler: { (action) in
                    if let likedIndexPath = tableView.indexPath(for: actionCell) {
                        let currentComment = self.itemList[likedIndexPath.row]
                        actionCell.isUserInteractionEnabled = false
                        actionCell.alpha = 0.5
                        currentComment.delete(with: { (success) in
                            actionCell.isUserInteractionEnabled = true
                            actionCell.alpha = 0.5
                            if success {
                                self.topic.commentCount -= 1
                                _ = self.itemList.removeAtIndex(index: likedIndexPath.row)
                                tableView.beginUpdates()
                                tableView.deleteRows(at: [likedIndexPath], with: .automatic)
                                if self.topic.attchments.count > 0 {
                                    tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
                                } else {
                                    tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
                                }
                                tableView.endUpdates()
                            }
                        })
                    }
                })
                self.inputBar.textView.resignFirstResponder()
                ShowAlert.showAlertContrller(with: .alert, title: "City+", message: NSLocalizedString("Ви дійсно бажаєте видалите коментар?", comment: ""), actions: [delete], in: self)
            }
            cell.likersHandler = { (likedCell) in
                if let likedIndexPath = tableView.indexPath(for: likedCell) {
                    let likersContainerVC: LikerContainerViewController = UIStoryboard(.Forum).instantiateViewController()
                    likersContainerVC.topic = self.topic
                    likersContainerVC.comment = self.itemList[likedIndexPath.row]
                    let nVC = UINavigationController(rootViewController: likersContainerVC)
                    nVC.setNavigationBarHidden(true, animated: false)
                    nVC.modalPresentationStyle = .overCurrentContext
                    self.present(nVC, animated: true, completion: nil)
                }
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : itemList.state == .canLoadMore ? 30 : 0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: TopicCommentsHeaderView.classForCoder())) as? TopicCommentsHeaderView else { return nil }
        view.tapHandler = { () in
            let commentsVC: AllCommentsTableViewController = UIStoryboard(.Forum).instantiateViewController()
            commentsVC.topic = self.topic
            self.show(commentsVC, sender: self)
        }
        return view
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        canBecome = false
        inputBar.textView.resignFirstResponder()
        resignFirstResponder()
    }
    
    override func menuWillClose() {
        super.menuWillClose()
        canBecome = true
        becomeFirstResponder()
    }
    
}

extension TopicDetailTableViewController: PickingImage {
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    
    func pickingDidCancel() {
        self.canBecome = true
        self.becomeFirstResponder()
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        if let image = image {
            let avatar = Avatar()
            avatar.image = image.image
            selectedAttachment = avatar
        }
        self.canBecome = true
        self.becomeFirstResponder()
    }
    
}

fileprivate extension TopicDetailTableViewController {
    
    @objc func attachmendAction(_ sender: UIButton) {
        canBecome = false
        inputBar.textView.resignFirstResponder()
        resignFirstResponder()
        if selectedAttachment == nil {
            self.startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    }
                })
            }) {
                self.canBecome = true
                self.becomeFirstResponder()
            }
        } else {
            let delete = UIAlertAction(title: "Видалити", style: .destructive) { (action) in
                self.selectedAttachment = nil
                self.canBecome = true
                self.becomeFirstResponder()
            }
            let change = UIAlertAction(title: "Змінити", style: .default) { (action) in
                self.startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
                    ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: nil)
                        }
                    })
                }) {
                    self.canBecome = true
                    self.becomeFirstResponder()
                }                
            }
            ShowAlert.showAlertContrller(with: .actionSheet, title: nil, message: nil, actions: [change, delete], in: self)
        }
    }
    
    @objc func sendAction(_ sender: UIButton) {
        if !inputBar.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || selectedAttachment != nil  {
            let newComment = TopicComment()
            newComment.text = inputBar.text
            newComment.isSending = true
            newComment.sender = UserModel.networkUser(with: sessionManager.storedUser!)
            newComment.attachment = selectedAttachment
            
            if let reply = replyedUser, inputBar.text.contains(reply.fullName) {
                newComment.replyUser = reply
            }
            
            inputBar.text.removeAll()
            let indexPath = IndexPath(row:self.itemList.count, section: 1)
            itemList.objects.append(newComment)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
            newComment.post(for: topic) { (success) in
                self.tableView.beginUpdates()
                if success {
                    self.itemList[indexPath.row].isSending = false
                    self.topic.commentCount += 1
                    self.selectedAttachment = nil
                    let likeIndexPatth = self.topic.attchments.count > 0 ? IndexPath(row: 2, section: 0) : IndexPath(row: 1, section: 0)
                    self.tableView.reloadRows(at: [indexPath, likeIndexPatth], with: .automatic)
                } else {
                    self.itemList.objects.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                }
                self.tableView.endUpdates()
            }
            self.scrollToBottom()
        }
    }
    
    func scrollToBottom() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            if self.tableView.contentSize.height > self.tableView.bounds.height - self.keyboardHeight {
                let lastIndexPath = IndexPath(row: self.tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1) - 1, section: self.tableView.numberOfSections - 1)
                if lastIndexPath.section > 0 , lastIndexPath.row > 0 {
                    self.scroll(to: lastIndexPath)
                }
            }
        })
    }
    
    func scroll(to indexPath: IndexPath, animated: Bool = true) {
        if !self.tableView.isDecelerating {
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
        }
    }
    
}

// MARK: - UITextViewDelegate

extension TopicDetailTableViewController: ALTextInputBarDelegate {
    
    func textViewDidBeginEditing(textView: ALTextView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.scrollToBottom()
        }
    }
    
    func inputBarDidChangeHeight(height: CGFloat) {
        if lastInputHeight != height {
            DispatchQueue.main.async {
                self.lastInputHeight = height
                self.scrollToBottom()
            }
        }
    }
    
}

//MARK: - SocketIO

extension TopicDetailTableViewController {
    
    func connectSockets() {
        guard let socket = webSocket else { return }
        socket.onConnect = {
            DispatchQueue.main.async(execute: {
                self.startListeningMessage()
                self.becomeFirstResponder()
            })
            print("\n> TopicDetailTableViewController: WebSocket connected successful!")
        }
        socket.onDisconnect = { (error: Error?) -> () in
            if let erro = error as? NSError, erro.code != 1 {
                print("\n> TopicDetailTableViewController: WebSocket disconnected becouse an error: ", erro.localizedDescription)
            } else {
                print("\n> TopicDetailTableViewController: WebSocket disconnected successfull!\n")
            }
        }
        
        socket.connect()
        print("\n> TopicDetailTableViewController: WebSocket connecting!\n")
        
    }
    
    public func disconnectSockets() {
        guard let socket = webSocket else { return }
        socket.disconnect()
        print("\n> TopicDetailTableViewController: SocketIO disconecting!\n")
    }
    
    public func startListeningMessage() {
        guard let socket = webSocket else { return }
        socket.onText = { (message: String) -> () in
            if let jsonData = message.data(using: String.Encoding.utf8), let messageJSON = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let commentObj = Mapper<TopicComment>().map(JSON: messageJSON) {
                print("\n> TopicDetailTableViewController: WebSocket receive message: !", messageJSON.description)
                if !commentObj.isMy {
                    self.insertMessageToTheBottom(commentObj)
                }
            }
        }
    }
    
    fileprivate func insertMessageToTheBottom(_ commnet: TopicComment) {
        self.itemList.objects.append(commnet)
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: [IndexPath(row: self.itemList.objects.count - 1, section: 1)], with: .none)
        self.tableView.endUpdates()
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.scrollToBottom()
        }
    }
    
}


fileprivate extension TopicDetailTableViewController {
    
    @IBAction func more(_ sender: UIBarButtonItem) {
        let newTopicVC: NewTopicTableViewController = UIStoryboard(.Forum).instantiateViewController()
        newTopicVC.topic = self.topic
        newTopicVC.isUpdating = true
        newTopicVC.deleteHandler = {
            self.deleteHandler?(self.topic)
        }
        newTopicVC.creatingHandler = { (topic) in
            self.topic = topic
            self.updateHandler?(topic)
            self.refreshContent(nil)
        }
        show(newTopicVC, sender: self)
    }
    
}

