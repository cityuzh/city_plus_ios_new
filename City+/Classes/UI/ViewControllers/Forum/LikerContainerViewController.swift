//
//  LikerContainerViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/25/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class LikerContainerViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var likerContainer: UIView! {
        didSet {
            likerContainer.roundedCorners(on: [.topLeft, .topRight], with: 20)
        }
    }
    
    //MARK: - Properties
    
    var topic: Topic? {
        didSet {
            addDependeciesToChilds()
        }
    }
    var comment: TopicComment? {
        didSet {
            addDependeciesToChilds()
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        blureView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        likerContainer.roundedCorners(on: [.topLeft, .topRight], with: 20)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destnation = segue.destination as? LikedUsersTableViewController {
            destnation.topic = topic
            destnation.comment = comment
        }
    }

}

fileprivate extension LikerContainerViewController {
    
    @IBAction func close(_ sender: UIButton) {
        clearView()
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func addDependeciesToChilds() {
        for child in children {
            if let likerTVC = child as? LikedUsersTableViewController {
                likerTVC.topic = topic
                likerTVC.comment = comment
            }
        }
    }
    
    func blureView() {
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }
    }
    
    func clearView() {
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = UIColor.clear
        }
    }
    
}
