//
//  ForumTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/11/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Nuke
import UIKit
import RxSwift
import RxCocoa

final class ForumTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    @IBOutlet fileprivate weak var searchContainer: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    //MARK: - Properties
    
    var disposeBag = DisposeBag()
    lazy var itemList: PaginatedListModel<Topic> = {
        let list = PaginatedListModel<Topic>.init(path: currentList == .all ? Topic.Constants.forumListRoute : Topic.Constants.myTopicsListRoute)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                itemList.queryParams["name"] = searchText as AnyObject
            } else {
                itemList.queryParams.removeValue(forKey: "name")
            }
        }
    }
    
    var currentList: ListType = .all {
        didSet {
            if currentList == .all {
                itemList.queryParams["ordering"] = "-created_at" as AnyObject
                tableView.tableHeaderView = searchContainer
            } else {
                if currentList == .popular {
                    itemList.queryParams["ordering"] = "-comments_count" as AnyObject
                } else {
                    navigationItem.leftBarButtonItem = nil
                    navigationItem.leftBarButtonItems = nil
                }
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshContent(nil)
        changeSegment()
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                self.searchText = text.element ?? ""
                self.refreshContent(nil)
            }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchText = self.searchBar.text ?? ""
                self.searchBar.resignFirstResponder()
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchText = ""
                self.searchBar.text = nil
                self.searchBar.resignFirstResponder()
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
        
        if currentList == .all {
            tableView.tableHeaderView = searchContainer
        } else {
            navigationItem.leftBarButtonItem = nil
            navigationItem.leftBarButtonItems = nil
        }

        placeholderView.removeFromSuperview()
        placeholderView = TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode, customImage: UIImage(named: "img_empty state_no discussion"), customTitle: NSLocalizedString("Обговорення відсутні", comment: ""), customSubTitle: NSLocalizedString("Змініть параметри пошуку або створіть власне обговорення, натиснувши на + зверху", comment: ""))
        segmentedControl.cornerRadius = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("didReceiveMemoryWarning()")
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataInfo
            }
            self.tableView.reloadData()
        }
    }
    
    func showTopic(_ topic: Topic) {
        let detailVC: TopicDetailTableViewController = UIStoryboard.init(.Forum).instantiateViewController()
        detailVC.topic = topic
        detailVC.deleteHandler = { (deletedTopic) in
            if let index = self.itemList.objects.firstIndex(where: { $0.identifier == deletedTopic.identifier }) {
                self.itemList.objects.remove(at: index)
                self.tableView.beginUpdates()
                self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .none)
                self.tableView.endUpdates()
            }
        }
        detailVC.updateHandler = { (updatedTopic) in
            if let index = self.itemList.objects.firstIndex(where: { $0.identifier == updatedTopic.identifier }) {
                self.itemList.objects.remove(at: index)
                self.itemList.objects.insert(updatedTopic, at: index)
                self.tableView.beginUpdates()
                self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                self.tableView.endUpdates()
            }
        }
        show(detailVC, sender: self)
    }
    
    //MARK: - UITableViewDelegate / UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentTopic = itemList[indexPath.row]
        var identifier = String(describing: TopicTableViewCell.classForCoder())
        if currentTopic.attchments.count > 0 {
            identifier = "Attachment" + String(describing: TopicTableViewCell.classForCoder())
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! TopicTableViewCell
        cell.moreImageView?.isHidden = currentTopic.attchments.count == 1
        
        var option = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "profilePlaceholder"), transition: .fadeIn(duration: 0.3), failureImage: #imageLiteral(resourceName: "profilePlaceholder"), failureImageTransition: .fadeIn(duration: 0.3), contentModes: nil)
        if let url = URL(string: currentTopic.user?.avatar?.urlPath ?? "") {
            Nuke.loadImage(with: url, options: option, into: cell.userImageView, progress: nil, completion: nil)
        }
        
        option = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "no-image.png"), transition: .fadeIn(duration: 0.3), failureImage: #imageLiteral(resourceName: "no-image.png"), failureImageTransition: .fadeIn(duration: 0.3), contentModes: nil)
        if let url = URL(string: currentTopic.attchments.first?.urlPath ?? ""), let imageView = cell.attachmentImageView {
            Nuke.loadImage(with: url, options: option, into: imageView, progress: nil, completion: nil)
        }
        
        cell.setUp(with: currentTopic)
        cell.profileHandler = { (actionCell) in
            if let index = tableView.indexPath(for: actionCell) {
                let user = self.itemList[index.row].user
                let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                profileVC.user = user
                profileVC.showMenuButton = false
                self.show(profileVC, sender: self)
            }
        }
        cell.likeHandler = { (likedCell, countLabel, button, image) in
            if let likedIndexPath = tableView.indexPath(for: likedCell) {
                let likedTopic = self.itemList[likedIndexPath.row]
                button.isEnabled = false
                if !likedTopic.isLikedByMe {
                    likedTopic.like(with: { (success) in
                        button.isEnabled = true
                        if success {
                            likedTopic.likesCount += 1
                            likedTopic.isLikedByMe = true
                            countLabel.text = "\(likedTopic.likesCount)"
                            image.isHighlighted = true
                            likedCell.setUp(with: likedTopic)
                        }
                    })
                } else {
                    likedTopic.dislike(with: { (success) in
                        button.isEnabled = true
                        if success {
                            likedTopic.likesCount -= 1
                            likedTopic.isLikedByMe = false
                            countLabel.text = "\(likedTopic.likesCount)"
                            image.isHighlighted = false
                            likedCell.setUp(with: likedTopic)
                        }
                    })
                }
            }
        }
        cell.likersHandler = { (likedCell) in
            if let likedIndexPath = tableView.indexPath(for: likedCell) {
                let likersContainerVC: LikerContainerViewController = UIStoryboard(.Forum).instantiateViewController()
                likersContainerVC.topic = self.itemList[likedIndexPath.row]
                let nVC = UINavigationController(rootViewController: likersContainerVC)
                nVC.setNavigationBarHidden(true, animated: false)
                nVC.modalPresentationStyle = .overCurrentContext
                self.present(nVC, animated: true, completion: nil)
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let topic = itemList[indexPath.row]
        let detailVC: TopicDetailTableViewController = UIStoryboard.init(.Forum).instantiateViewController()
        detailVC.topic = topic
        detailVC.updateHandler = { (updatedTopic) in
            if let index = self.itemList.objects.firstIndex(where: { $0.identifier == updatedTopic.identifier }) {
                self.itemList.objects.remove(at: index)
                self.itemList.objects.insert(updatedTopic, at: index)
                self.tableView.reloadData()
            }
        }
        detailVC.deleteHandler = { (deletedTopic) in
            if let index = self.itemList.objects.firstIndex(of: deletedTopic) {
                self.itemList.objects.remove(at: index)
                self.tableView.beginUpdates()
                self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                self.tableView.endUpdates()
            }
        }
        show(detailVC, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? NewTopicTableViewController {
            destination.creatingHandler = { (newTopic) in
                self.itemList.objects.insert(newTopic, at: 0)
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .top)
                self.tableView.endUpdates()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                })
            }
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        searchBar.resignFirstResponder()
    }
    
}

extension ForumTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}

extension ForumTableViewController {
    
    enum ListType {
        case my
        case all
        case popular
    }
    
    @IBAction func segmenteChanged(_ sender: UISegmentedControl) {
        itemList.lastTask?.cancel()
        if sender.selectedSegmentIndex == 0 {
            currentList = .all
        } else {
            currentList = .popular
        }
         
        refreshContent(nil)
    }
    
    func changeSegment(){
        segmentedControl.tintColor = .clear
        if #available(iOS 13.0, *) {
            segmentedControl.selectedSegmentTintColor = .white
        } else {
            // Fallback on earlier versions
        }
        let blue = UIColor(hexString: "3B4DC4")
        self.segmentedControl.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        self.segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: blue ], for: .selected)
    }
}
