//
//  AllCommentsTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/24/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Starscream
import MobileCoreServices
import ALTextInputBar
import ObjectMapper

final class AllCommentsTableViewController: BaseTableViewController {

    var topic: Topic!
    var replyedUser: UserModel? {
        willSet {
            if let old = replyedUser?.fullName {
                if inputBar.text.contains(old) {
                    inputBar.textView.text = inputBar.textView.text.replacingOccurrences(of: old, with: newValue?.fullName ?? "")
                }
            }
        }
        didSet {
            if let name = replyedUser?.fullName {
                if !inputBar.text.contains(name) {
                    inputBar.textView.insertText(name)
                }
            }
        }
    }
    lazy var itemList: PaginatedListModel<TopicComment> = {
        let list = PaginatedListModel<TopicComment>.init(path: String(format: TopicComment.Constants.commentsListRouteFormat, topic.identifier ?? 0))
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        list.isReversed = true
        return list
    }()
    
    lazy var inputBar: ALTextInputBar = {
        let bar = ALTextInputBar()
        bar.showTextViewBorder = true
        bar.defaultHeight = 50
        bar.delegate = self
        bar.horizontalSpacing = 0
        bar.rightView = rightBarView
        bar.leftView = leftBarView
        return bar
    }()
    
    lazy var leftBarView: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.addTarget(self, action: #selector(attachmendAction(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "clip"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "clipActive"), for: .selected)
        return button
    }()
    
    lazy var rightBarView: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.addTarget(self, action: #selector(sendAction(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "send"), for: .normal)
        return button
    }()
    
    var keyboardHeight: CGFloat = 50
    var lastInputHeight: CGFloat = 50
    
    
    override var inputAccessoryView: UIView? {
        get {
            return canBecome ? inputBar : nil
        }
    }
    
    var canBecome: Bool = true
    override var canBecomeFirstResponder: Bool {
        return canBecome
    }
    
    var webSocket: WebSocket?
    
    var selectedAttachment: Avatar? {
        didSet {
            leftBarView.isSelected = selectedAttachment != nil
            inputBar.alwaysShowRightButton = selectedAttachment != nil
            reloadInputViews()
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        itemList.load { (success) in
            self.tableView.reloadData()
            var request = URLRequest(url: URL(string: String(format: Configuration.socketForumURLPathFormat, self.topic.identifier ?? 0))!)
            request.timeoutInterval = 5
            request.setValue("Token \((self.sessionManager.token?.string ?? ""))", forHTTPHeaderField: "Authorization")
            self.webSocket = WebSocket(request: request)
            self.connectSockets()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.scrollToBottom()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = topic.title
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disconnectSockets()
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        if itemList.state == .canLoadMore, itemList.state != .loading  {
            requestMoreDataIfNeededForItemAtIndex(index: itemList.count - 1)
        } else {
            sender?.endRefreshing()
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = String(describing: TopicCommentTableViewCell.classForCoder())
        if itemList[indexPath.row].attachment != nil {
            identifier = "Attahment" + String(describing: TopicCommentTableViewCell.classForCoder())
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! TopicCommentTableViewCell
        cell.setUp(with: itemList[indexPath.row])
        cell.profileHandler = { (actionCell) in
            if let index = tableView.indexPath(for: actionCell) {
                let user = self.itemList[index.row].sender
                let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                profileVC.user = user
                profileVC.showMenuButton = false
                self.show(profileVC, sender: self)
            }
        }
        cell.attachmentHandler = { (sender) in
            if let index = tableView.indexPath(for: sender)?.row {
                let attachment = self.itemList[index].attachment
                let imagePreviewVC: ImagePreviewViewController = UIStoryboard(.Chats).instantiateViewController()
                imagePreviewVC.attachment = attachment
                self.show(imagePreviewVC, sender: self)
            }
        }        
        cell.likersHandler = { (likedCell) in
            if let likedIndexPath = tableView.indexPath(for: likedCell) {
                let likersContainerVC: LikerContainerViewController = UIStoryboard(.Forum).instantiateViewController()
                likersContainerVC.topic = self.topic
                likersContainerVC.comment = self.itemList[likedIndexPath.row]
                let nVC = UINavigationController(rootViewController: likersContainerVC)
                nVC.setNavigationBarHidden(true, animated: false)
                nVC.modalPresentationStyle = .overCurrentContext
                self.present(nVC, animated: true, completion: nil)
            }
        }
        cell.likeHandler = { (likedCell, countLabel, button, image) in
            if let likedIndexPath = tableView.indexPath(for: likedCell) {
                let likedComment = self.itemList[likedIndexPath.row]
                button.isEnabled = false
                if !likedComment.isLikedByMe {
                    likedComment.like(with: { (success) in
                        button.isEnabled = true
                        if success {
                            likedComment.likesCount += 1
                            likedComment.isLikedByMe = true
                            countLabel.text = "\(likedComment.likesCount)"
                            image.isHighlighted = true
                            likedCell.setUp(with: likedComment)
                        }
                    })
                } else {
                    likedComment.dislike(with: { (success) in
                        button.isEnabled = true
                        if success {
                            likedComment.likesCount -= 1
                            likedComment.isLikedByMe = false
                            countLabel.text = "\(likedComment.likesCount)"
                            image.isHighlighted = false
                            likedCell.setUp(with: likedComment)
                        }
                    })
                }
            }
        }
        cell.replyHandler = { (actionCell) in
            if let likedIndexPath = tableView.indexPath(for: actionCell), let user = self.itemList[likedIndexPath.row].sender, !user.isMe {
                self.replyedUser = user
            }
        }
        cell.moreHandler = { (actionCell) in
            let delete = UIAlertAction(title: NSLocalizedString("Видалити", comment: ""), style: .destructive, handler: { (action) in
                if let likedIndexPath = tableView.indexPath(for: actionCell) {
                    let currentComment = self.itemList[likedIndexPath.row]
                    actionCell.isUserInteractionEnabled = false
                    actionCell.alpha = 0.5
                    currentComment.delete(with: { (success) in
                        actionCell.isUserInteractionEnabled = false
                        actionCell.alpha = 0.5
                        if success {
                            self.topic.commentCount -= 1
                            _ = self.itemList.removeAtIndex(index: likedIndexPath.row)
                            tableView.beginUpdates()
                            tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
                            tableView.deleteRows(at: [likedIndexPath], with: .automatic)
                            tableView.endUpdates()
                        }
                    })
                }
            })
            ShowAlert.showAlertContrller(with: .alert, title: "City+", message: NSLocalizedString("Ви дійсно бажаєте видалите коментар?", comment: ""), actions: [delete], in: self)
        }
        return cell
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        canBecome = false
        inputBar.textView.resignFirstResponder()
        resignFirstResponder()
    }
    
    override func menuWillClose() {
        super.menuWillClose()
        canBecome = true
        becomeFirstResponder()
    }

}

fileprivate extension AllCommentsTableViewController {
    
    @objc func attachmendAction(_ sender: UIButton) {
        canBecome = false
        inputBar.textView.resignFirstResponder()
        resignFirstResponder()
        if selectedAttachment == nil {
            self.startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    }
                })
            }) {
                self.canBecome = true
                self.becomeFirstResponder()
            }
        } else {
            let delete = UIAlertAction(title: "Видалити", style: .destructive) { (action) in
                self.selectedAttachment = nil
                self.canBecome = true
                self.becomeFirstResponder()
            }
            let change = UIAlertAction(title: "Змінити", style: .default) { (action) in
                self.startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
                    ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: nil)
                        }
                    })
                }) {
                    self.canBecome = true
                    self.becomeFirstResponder()
                }
            }
            ShowAlert.showAlertContrller(with: .actionSheet, title: nil, message: nil, actions: [change, delete], in: self)
        }
    }
    
    @objc func sendAction(_ sender: UIButton) {
        if !inputBar.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || selectedAttachment != nil  {
            let newComment = TopicComment()
            newComment.text = inputBar.text
            newComment.isSending = true
            newComment.sender = UserModel.networkUser(with: sessionManager.storedUser!)
            newComment.attachment = selectedAttachment
            
            if let reply = replyedUser, inputBar.text.contains(reply.fullName) {
                newComment.replyUser = reply
            }
            
            inputBar.text.removeAll()
            let indexPath = IndexPath(row:self.itemList.count, section: 0)
            itemList.objects.append(newComment)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
            newComment.post(for: topic) { (success) in
                self.tableView.beginUpdates()
                if success {
                    self.itemList[indexPath.row].isSending = false
                    self.topic.commentCount += 1
                    self.selectedAttachment = nil
                } else {
                    _ = self.itemList.removeAtIndex(index: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                }
                self.tableView.endUpdates()
            }
            self.scrollToBottom()
        }
    }
    
    func scrollToBottom() {
        if tableView.contentSize.height > tableView.bounds.height - keyboardHeight {
            let lastIndexPath = IndexPath(row: self.tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1) - 1, section: self.tableView.numberOfSections - 1)
            scroll(to: lastIndexPath)
        }
    }
    
    func scroll(to indexPath: IndexPath, animated: Bool = true) {
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
    }
    
}

//MARK: - SocketIO

extension AllCommentsTableViewController {
    
    func connectSockets() {
        guard let socket = webSocket else { return }
        socket.onConnect = {
            DispatchQueue.main.async(execute: {
                self.startListeningMessage()
                self.becomeFirstResponder()
            })
            print("\n> AllCommentsTableViewController: WebSocket connected successful!")
        }
        socket.onDisconnect = { (error: Error?) -> () in
            if let erro = error as? NSError, erro.code != 1 {
                print("\n> AllCommentsTableViewController: WebSocket disconnected becouse an error: ", erro.localizedDescription)
            } else {
                print("\n> AllCommentsTableViewController: WebSocket disconnected successfull!\n")
            }
        }
        
        socket.connect()
        print("\n> AllCommentsTableViewController: WebSocket connecting!\n")
        
    }
    
    public func disconnectSockets() {
        guard let socket = webSocket else { return }
        socket.disconnect()
        print("\n> AllCommentsTableViewController: SocketIO disconecting!\n")
    }
    
    public func startListeningMessage() {
        guard let socket = webSocket else { return }
        socket.onText = { (message: String) -> () in
            if let jsonData = message.data(using: String.Encoding.utf8), let messageJSON = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let commentObj = Mapper<TopicComment>().map(JSON: messageJSON) {
                print("\n> AllCommentsTableViewController: WebSocket receive message: !", messageJSON.description)
                if !commentObj.isMy {
                    self.insertMessageToTheBottom(commentObj)
                }
            }
        }
    }
    
    fileprivate func insertMessageToTheBottom(_ commnet: TopicComment) {
        self.itemList.objects.append(commnet)
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: [IndexPath(row: self.itemList.objects.count - 1, section: 0)], with: .none)
        self.tableView.endUpdates()
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.scrollToBottom()
        }
    }
    
}

extension AllCommentsTableViewController: RefreshableViewControllerProtocol {
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
}

extension AllCommentsTableViewController: PickingImage {
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    
    func pickingDidCancel() {
        self.canBecome = true
        self.becomeFirstResponder()
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        if let image = image {
            let avatar = Avatar()
            avatar.image = image.image
            selectedAttachment = avatar
        }
        self.canBecome = true
        self.becomeFirstResponder()
    }
    
}

// MARK: - UITextViewDelegate

extension AllCommentsTableViewController: ALTextInputBarDelegate {
    
    func textViewDidBeginEditing(textView: ALTextView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.scrollToBottom()
        }
    }
    
    func inputBarDidChangeHeight(height: CGFloat) {
        if lastInputHeight != height {
            DispatchQueue.main.async {
                self.lastInputHeight = height
                self.scrollToBottom()
            }
        }
    }
    
}
