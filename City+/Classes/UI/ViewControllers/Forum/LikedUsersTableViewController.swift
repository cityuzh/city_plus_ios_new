//
//  LikedUsersTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/24/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class LikedUsersTableViewController: BaseTableViewController {

    var topic: Topic?
    var comment: TopicComment?

    lazy var itemList: PaginatedListModel<UserModel> = {
        var path = ""
        if let topic = topic {
            path = String(format: Topic.Constants.topicLikersListRouteFormat, topic.identifier ?? 0)
            if let comment = comment {
                path = String(format: TopicComment.Constants.commentLikersListRouteFormat, topic.identifier ?? 0, comment.identifier ?? 0)
            }
        }
        
        let list = PaginatedListModel<UserModel>.init(path: path)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshContent(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func refreshContent(_ sender: UIRefreshControl?) {
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userModel = itemList[indexPath.row]
        let identifier = UserTableViewCell.cellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! UserTableViewCell
        cell.setUp(with: userModel)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = itemList[indexPath.row]
        let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
        profileVC.user = user
        navigationController?.setNavigationBarHidden(false, animated: true)
        show(profileVC, sender: self)
    }
    
}

extension LikedUsersTableViewController: RefreshableViewControllerProtocol {
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
}

