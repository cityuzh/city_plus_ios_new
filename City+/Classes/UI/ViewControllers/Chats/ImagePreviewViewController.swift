//
//  ImagePreviewViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/22/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Nuke

final class ImagePreviewViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    var attachment: Avatar! {
        didSet {
            attachment.image = nil
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setImage(with: attachment)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ImagePreviewViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
}

fileprivate extension ImagePreviewViewController {
    
    @IBAction func share(_ sender: UIBarButtonItem) {
        var items = [Any]()
        if let image = attachment.image {
            items.append(image)
            let activityController = UIActivityViewController.init(activityItems: items, applicationActivities: nil)
            present(activityController, animated: true, completion: nil)
        } else if let image = imageView.image {
            items.append(image)
            let activityController = UIActivityViewController.init(activityItems: items, applicationActivities: nil)
            present(activityController, animated: true, completion: nil)
        } else {
            present(activityVC, animated: true, completion: nil)
            _ = imageProvider?.imageForPath(path: attachment.urlPath ?? "", completion: { (image) in
                self.activityVC.dismiss(animated: true, completion: {
                    if let downloaded = image {
                        items.append(downloaded)
                        let activityController = UIActivityViewController.init(activityItems: items, applicationActivities: nil)
                        self.present(activityController, animated: true, completion: nil)
                    } else {
                        ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Сталася помилка при завантаженні! Спробуйте ще раз!", comment: ""))
                    }
                })
            })
        }
    }
    
    func setImage(with attachment: Avatar) {
        let option = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "no-image"), transition: .fadeIn(duration: 0.3), failureImage: #imageLiteral(resourceName: "no-image"), failureImageTransition: .fadeIn(duration: 0.3), contentModes: nil)
        if let url = URL(string: attachment.urlPath ?? "") {
            Nuke.loadImage(with: url, options: option, into: imageView, progress: nil, completion: nil)
        }
    }
    
}
