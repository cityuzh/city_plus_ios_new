//
//  ChatRoomTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/31/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Starscream
import AVFoundation
import MobileCoreServices
import ALTextInputBar
import ObjectMapper

final class ChatRoomTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var typingContainer: UIView!
    @IBOutlet weak var typingLabel: UILabel!
    
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var onlineStatusLabel: UILabel!
    
    lazy var leftBarView: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.addTarget(self, action: #selector(attachmendAction(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "clip"), for: .normal)
        return button
    }()
    
    lazy var rightBarView: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.addTarget(self, action: #selector(sendAction(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "send"), for: .normal)
        return button
    }()
    
    lazy var inputBar: ALTextInputBar = {
        let bar = ALTextInputBar()
        bar.showTextViewBorder = true
        bar.defaultHeight = 50
        bar.delegate = self
        bar.horizontalSpacing = 0
        bar.rightView = rightBarView
        bar.leftView = leftBarView
        return bar
    }()
    
    var settings: ChatSettings {
        return ChatSettings.loadOrInitSaved()
    }
    
    override var inputAccessoryView: UIView? {
        get {
            return inputBar
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return canBecome
    }
    
    //MARK: - Properties
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String, kUTTypeMovie as String]
    }
    
    fileprivate var typingTimer: Timer?
    
    var lastTouchCell: MessageTableViewCell?
    
    var webSocket: WebSocket?
    
    var chat: Chat!
    var isTyping: Bool = false {
        didSet {
            if isTyping {
                tableView.tableFooterView = typingContainer
            } else {
                tableView.tableFooterView = UIView()
            }
        }
    }
    var isFriendOnline: Bool = false {
        didSet {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
                    self.onlineStatusLabel.alpha = 0
                }, completion: { (finished) in
                    self.onlineStatusLabel.text = self.isFriendOnline ? NSLocalizedString("В мережі", comment: "") : NSLocalizedString("Не в мережі", comment: "")
                    UIView.animate(withDuration: 0.3, animations: {
                        self.onlineStatusLabel.alpha = 1
                    })
                })
            }
        }
    }
    
    lazy var itemList: GroupedPaginatedListModel<Message> = {
        let list = GroupedPaginatedListModel<Message>.init(path: String(format: Message.Constants.messagesPathFormat, chat.identifier ?? 0))
        list.requestRowCount = 50
        list.onRequestFailure = requestFailureHandler
        return list
    }()
    
    var canBecome = false
    var canDisconnect = true
    var keyboardHeight: CGFloat = 50
    var lastInputHeight: CGFloat = 50
    
    var isReconecting = false
    
    var backgroundView: UIImageView?
    var canScrollToBottom = true
    var scrollOnce = true
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIMenuController.shared.menuItems = [UIMenuItem(title: NSLocalizedString("Видалити повідомлення", comment: ""), action: #selector(deleteMessageTapped(_:)))]
        UIMenuController.shared.update()
        
        
        navigationItem.titleView = titleContainer
        userNameLabel.text = chat.user?.fullName
        isFriendOnline = false
        
        
        tableView.register(UINib(nibName: "MessageDateHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "MessageDateHeaderView")
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        backgroundView?.frame = self.view.bounds
        backgroundView?.layoutIfNeeded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterBackground(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        if chat != nil && !(webSocket?.isConnected ?? false) {
            self.tableView.reloadData()
            var request = URLRequest(url: URL(string: String(format: Configuration.socketURLPathFormat, chat.identifier ?? 0))!)
            request.timeoutInterval = 5
            request.setValue("Token \((sessionManager.token?.string ?? ""))", forHTTPHeaderField: "Authorization")
            webSocket = WebSocket(request: request)
            itemList.load { (success) in
                if success {
                    self.connectSockets()
                    self.tableView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.scrollToBottom()
                    }
                }
            }
        }
        
        
        if settings.currentBackgroundItem.id != "0" {
            backgroundView = UIImageView(frame: self.view.bounds)
            backgroundView?.contentMode = .scaleAspectFill
            backgroundView?.image = settings.currentBackgroundItem.image
            tableView.backgroundView = backgroundView
        } else {
            tableView.backgroundView = nil
        }
        chat.get { (success) in
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        inputBar.resignFirstResponder()
        resignFirstResponder()
        emitTyping(false)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if canDisconnect {
            disconnectSockets()
        }
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        if itemList.state == .canLoadMore, itemList.state != .loading  {
            requestMoreDataIfNeededForItemAtIndex(itemList.count - 1)
        } else {
            sender?.endRefreshing()
        }
    }

    func refreshChat() {
        chat.get { (success) in
            if success {
                self.userNameLabel.text = self.chat.user?.fullName
                self.isFriendOnline = self.chat.isOnline
            }
        }
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChatSettingsTableViewController {
            destination.chat = chat
            destination.chatRoomVC = self
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return itemList.sectionCount
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.rowsCountForSection(section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = itemList[indexPath]
        let isNew = isShowNewHeader(in: message)
        var identifier = MessageTableViewCell.cellIdentifier
        
        switch message.messageType {
        case .text:
            identifier = "TEXT" + MessageTableViewCell.cellIdentifier
        case .image:
            identifier = "IMG" + MessageTableViewCell.cellIdentifier
        case .video:
            identifier = "VIDEO" + MessageTableViewCell.cellIdentifier
        case .location:
            identifier = "LOCATION" + MessageTableViewCell.cellIdentifier
        }
        
        if !isShowUser(in: message, at: indexPath), message.socketEvent != .typing, !isNew {
            identifier = "EMPTY" + identifier
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MessageTableViewCell
        
        var isLastMessage = false
        if let lastMyMessage = self.itemList.groupedObjects.flatMap({ $0 }).filter({ $0.isMy && $0.isRead }).last, let lastIndexPath = self.itemList.indexPath(of: lastMyMessage) {
            if lastIndexPath == indexPath {
                isLastMessage = true
            }
        }
        
        cell.setUp(with: message, isLastMessage: isLastMessage, isNew: isNew, textColor: settings.currentTextColorItem.color, in: chat)
        cell.errorHandler = { (actionCell) in
            if let actionIndexPath = tableView.indexPath(for: actionCell) {
                let failedMessage = self.itemList[actionIndexPath]
                switch failedMessage.messageType {
                case .text:
                    _ = self.itemList.removeAtIndexPath(actionIndexPath)
                    tableView.beginUpdates()
                    tableView.deleteRows(at: [actionIndexPath], with: .none)
                    tableView.endUpdates()
                    self.emitMessage(failedMessage)
                default: break
                }
            }
        }
        cell.profileHandler = { (actionCell) in
            if let index = tableView.indexPath(for: actionCell) {
                self.canScrollToBottom = false
                let user = self.itemList[index].user
                let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                profileVC.user = user
                profileVC.showMenuButton = false
                self.show(profileVC, sender: self)
            }
        }
        cell.previewHandler = { (actionCell) in
            if let index = tableView.indexPath(for: actionCell) {
                let attachmtn = self.itemList[index].content
                self.canScrollToBottom = false
                if self.itemList[index].messageType == .image {
                    let imagePreviewVC: ImagePreviewViewController = UIStoryboard(.Chats).instantiateViewController()
                    imagePreviewVC.attachment = attachmtn
                    self.show(imagePreviewVC, sender: self)
                } else {
                    let videoPreviewVC: VideoPreviewViewController = UIStoryboard(.Chats).instantiateViewController()
                    videoPreviewVC.content = attachmtn
                    self.show(videoPreviewVC, sender: self)
                }
            }
        }
        cell.menuTouchHandler = { (actionCell, showImmediatly) in
            self.lastTouchCell = actionCell
            if showImmediatly {
                self.canScrollToBottom = false
                if let lastCell = self.lastTouchCell, let indexPath = tableView.indexPath(for: lastCell) {
                    let message = self.itemList[indexPath]
                    let sheetVC = UIAlertController(title: "Видалити повідомлення?", message: nil, preferredStyle: .actionSheet)
                    if message.isMy {
                        sheetVC.addAction(UIAlertAction(title: "Видалити для всіх!", style: .default, handler: { (action) in
                            self.emitDeleteMessageForAll(message)
                            lastCell.alpha = 0.5
                            lastCell.isUserInteractionEnabled = false
                        }))
                    }
                    sheetVC.addAction(UIAlertAction(title: "Видилити для себе!", style: .default, handler: { (action) in
                        self.emitDeleteMessage(message)
                        lastCell.alpha = 0.5
                        lastCell.isUserInteractionEnabled = false
                    }))
                    sheetVC.addAction(UIAlertAction(title: "Відмінити", style: .cancel, handler: nil))
                    self.present(sheetVC, animated: true, completion: nil)
                }
            }
        }
        cell.alpha = message.isSending ? 0.5 : 1.0
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    override func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.itemList.groupedObjects[section].filter({ $0.date != nil }).count > 0 {
            return Constants.headerHeight
        } else {
            return CGFloat.leastNonzeroMagnitude
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = self.itemList[indexPath]
        switch message.messageType {
        case .text:
            let isNew = isShowNewHeader(in: message)
            var isLastMessage = false
            var isShow = false
            if let lastMyMessage = self.itemList.groupedObjects.flatMap({ $0 }).filter({ $0.isMy && $0.isRead }).last, let lastIndexPath = self.itemList.indexPath(of: lastMyMessage) {
                if lastIndexPath == indexPath {
                    isLastMessage = message.isRead
                }
            }
            isShow = isShowUser(in: message, at: indexPath)
            
            let textHeight = message.text?.height(withConstrainedWidth: tableView.bounds.width - 110, font: UIFont.systemFont(ofSize: 13))
            let controlsHeight = (isNew ? 44 : 0) + (isShow ? 30 : 0) + (isLastMessage ? 15 : 0)
            return CGFloat(max((textHeight ?? 0), (isNew) ? 50 : 25)) + CGFloat(controlsHeight) + 5.0
        default:
            return UITableView.automaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: MessageDateHeaderView.classForCoder())) as? MessageDateHeaderView else { return nil }
        view.dateLabel?.text = itemList.headerTitleForSection(section)
        view.textColor = settings.currentTextColorItem.color
        return view
    }

    override func menuWillOpen() {
        super.menuWillOpen()
        canBecome = false
        inputBar.textView.resignFirstResponder()
        resignFirstResponder()
    }
    
    override func menuWillClose() {
        super.menuWillClose()
        canBecome = true
        becomeFirstResponder()
    }
    
}

extension ChatRoomTableViewController: GroupedRefreshableViewControllerProtocol {
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        tableView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.refreshControl?.endRefreshing()
        }
//        guard !changes.moves.isEmpty else {
//
//            return
//        }
//
//        tableView.beginUpdates()
//        
//        if !changes.deletedIndexPaths.isEmpty {
//            var section: Int
//            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
//                section = indexPath.section
//                var deletedIndexPath = [IndexPath]()
//                var rowsInSection =  tableView.numberOfRows(inSection: section)
//                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
//                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
//                        rowsInSection -= 1
//                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .none)
//                        deletedIndexPath.append(secondIndexPath as IndexPath)
//
//                        if rowsInSection == 0 {
//                            tableView.deleteSections(IndexSet(integer: section), with: .none)
//                        }
//                    } else {
//                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .none)
//                        deletedIndexPath.append(secondIndexPath as IndexPath)
//                    }
//                }
//            }
//            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .none)
//        }
//
//        if !changes.insertedIndexPaths.isEmpty {
//            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
//                let sectionForInsert = NSMutableIndexSet()
//                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
//                    if indexPath.section > lastVisibleCell.section {
//                        if !sectionForInsert.contains(indexPath.section) {
//                            sectionForInsert.add(indexPath.section)
//                        }
//                    }
//                }
//                tableView.insertSections(sectionForInsert as IndexSet, with: .none)
//            }
//            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .none)
//        }
//
//        if !changes.updatedIndexPaths.isEmpty {
//            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .none)
//        }
//
//        tableView.endUpdates()
    }
    
}

// MARK: - UITextViewDelegate

extension ChatRoomTableViewController: ALTextInputBarDelegate {
    
    func textViewDidBeginEditing(textView: ALTextView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.scrollToBottom()
        }
    }
    
    func textView(textView: ALTextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        func restartTimer() {
            typingTimer?.invalidate()
            typingTimer = Timer.scheduledTimer(withTimeInterval: Constants.typingTimeout, repeats: false, block: { (timer) in
                self.emitTyping(false)
                self.typingTimer?.invalidate()
                self.typingTimer = nil
            })
        }
        
        if typingTimer == nil {
            emitTyping(true)
        }
        
        restartTimer()
        return true
    }
    
    func inputBarDidChangeHeight(height: CGFloat) {
        if lastInputHeight != height {
            DispatchQueue.main.async {
                self.lastInputHeight = height
                self.scrollToBottom()
            }
        }
    }
    
    func textViewDidEndEditing(textView: ALTextView) {
        emitTyping(false)
    }
}

//MARK: - Input Methods
fileprivate extension ChatRoomTableViewController {
    
    @IBAction func attachmendAction(_ sender: UIButton) {
        canDisconnect = false
        canBecome = false
        inputBar.textView.resignFirstResponder()
        resignFirstResponder()
        self.startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: nil)
                }
            })
        }) {
            self.canDisconnect = true
            self.canBecome = true
            self.becomeFirstResponder()
        }
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        if !inputBar.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty  {
            let newMessage = Message()
            newMessage.text = inputBar.text
            newMessage.user = UserModel.networkUser(with: sessionManager.storedUser!)
            emitMessage(newMessage)
            inputBar.text.removeAll()
        }
    }
    
    func insert(_ message: Message, at indexPath: IndexPath, isScrollToBottom: Bool = true) {
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.scrollOnce = true
            self.scrollToBottom()
        }
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: [indexPath], with: .none)
        self.tableView.endUpdates()
        CATransaction.commit()
    }
    
    func scrollToBottom() {
        guard canScrollToBottom || scrollOnce else { return }
        if isTyping {
            tableView.scrollRectToVisible(typingContainer.frame, animated: true)
        } else {
            if tableView.contentSize.height > tableView.bounds.height - keyboardHeight {
                
                let lastIndexPath = IndexPath(row: self.tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1) - 1, section: self.tableView.numberOfSections - 1)
                scroll(to: lastIndexPath)
            }
        }
        scrollOnce = false
    }
    
    func scroll(to indexPath: IndexPath, animated: Bool = true) {
        CATransaction.begin()
        CATransaction.setCompletionBlock({ () -> Void in
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
        })
        
        let contentOffset = tableView.contentOffset.y
        let newContentOffset = CGPoint(x:0, y:contentOffset + 1)
        tableView.setContentOffset(newContentOffset, animated: true)
        
        CATransaction.commit()

    }
    
    func isShowUser(in message: Message, at indexPath: IndexPath) -> Bool {
        var previous: Message?
        if indexPath.row > 0 {
            let previousIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)
            previous = itemList[previousIndexPath]
            if let previousMessage = previous, previousMessage.user?.identifier == message.user?.identifier {
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    func isShowNewHeader(in message: Message) -> Bool {
        if let firstUnread = itemList.objects.sorted(by: { ($0.date ?? Date()) < ($1.date ?? Date()) }).first(where: { !$0.isMy && !$0.isRead }) {
            if message.identifier == firstUnread.identifier {
                return true
            }
        }
        return false
    }
    
    func markAsRead() {
        itemList.objects.forEach({ $0.isRead = true })
    }
    
    @objc func deleteMessageTapped(_ sender: UIMenuController) {
        if let lastCell = lastTouchCell, let indexPath = tableView.indexPath(for: lastCell) {
            let message = itemList[indexPath]
            let sheetVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            if message.isMy {
                sheetVC.addAction(UIAlertAction(title: "Видалити для всіх!", style: .default, handler: { (action) in
                    self.emitDeleteMessageForAll(message)
                    lastCell.alpha = 0.5
                    lastCell.isUserInteractionEnabled = false
                }))
            }
            sheetVC.addAction(UIAlertAction(title: "Видилити для себе!", style: .default, handler: { (action) in
                self.emitDeleteMessage(message)
                lastCell.alpha = 0.5
                lastCell.isUserInteractionEnabled = false
            }))
            sheetVC.addAction(UIAlertAction(title: "Відмінити", style: .cancel, handler: nil))
            present(sheetVC, animated: true, completion: nil)
        }
    }
    
}

extension ChatRoomTableViewController: PickingImage {
    
    func pickingDidCancel() {
        canDisconnect = true
        canBecome = true
        becomeFirstResponder()
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        canDisconnect = true
        canBecome = true
        becomeFirstResponder()
        if let image = image {
            let avatarModel = Avatar()
            avatarModel.image = image.image
            let newMessage = Message()
            newMessage.user = UserModel.networkUser(with: sessionManager.storedUser!)
            newMessage.isSending = true
            newMessage.attachmentForSend = image
            if image.mediaType == .photo {
                newMessage.messageType = .image
                newMessage.contentSize = image.image?.size
                newMessage.content = avatarModel
                insertMessageToTheBottom(newMessage)
                newMessage.uploadImageAttachment { (success) in
                    if success {
                        self.emitMessage(newMessage, insertIntoTableView: false)
                    } else {
                        newMessage.socketEvent = .messageFailed
                        if let reloadIndexPath = self.itemList.tempIndexPath(of: newMessage) {
                            self.tableView.beginUpdates()
                            self.tableView.reloadRows(at: [reloadIndexPath], with: .none)
                            self.tableView.endUpdates()
                        }
                    }
                }
            } else {
                newMessage.messageType = .video
                insertMessageToTheBottom(newMessage)
                newMessage.uploadVideoAttachment { (success) in
                    if success {
                        self.emitMessage(newMessage, insertIntoTableView: false)
                    } else {
                        newMessage.socketEvent = .messageFailed
                        if let reloadIndexPath = self.itemList.tempIndexPath(of: newMessage) {
                            self.tableView.beginUpdates()
                            self.tableView.reloadRows(at: [reloadIndexPath], with: .none)
                            self.tableView.endUpdates()
                        }
                    }
                }
            }
        }
    }
    
}

extension ChatRoomTableViewController {
    
    struct Constants {
        static let maxInputHeight: CGFloat = 100
        static let estimatedRowHeight: CGFloat = 140
        
        static let cellHeight: CGFloat = 44
        static let footerHeight: CGFloat = 8
        static let headerHeight: CGFloat = 36
        
        static let typingTimeout: TimeInterval = 10
        
    }
    
    // MARK: - SuspendSupporting Notification
    
    @objc func appWillEnterBackground(_ notification: NSNotification) {
        DispatchQueue.global(qos: .background).async {
            self.disconnectSockets()
        }
    }
    
    @objc func appWillEnterForeground(_ notification: NSNotification) {
        itemList.load { (success) in
            if success {
                self.connectSockets()
                self.tableView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.scrollToBottom()
                }
            }
        }
    }
    
    // MARK: - KeyboardAppearNotifications
    @objc func keyboardDidShow(_ notification: NSNotification) {
        if let height  = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
            keyboardHeight = height
        }
    }
    
    @objc func keyboardDidHide(_ notification: NSNotification) {
        if let _  = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
            keyboardHeight = 50
        }
    }
    
}

//MARK: - SocketIO

extension ChatRoomTableViewController {
    
    func tryReconnect() {
        self.isReconecting = true
        self.connectSockets()
        onlineStatusLabel.text = "Підключення..."
    }
    
    func connectSockets() {
        guard let socket = webSocket else { return }
        socket.onConnect = {
            DispatchQueue.main.async(execute: {
                
                self.canBecome = true
                self.startListeningData()
                self.startListeningMessage()
                self.emitReadStatus()
                self.isFriendOnline = self.chat.isOnline
                if self.isReconecting {
                    self.refreshChat()
                }
                self.becomeFirstResponder()
                self.isReconecting = false
            })
            print("\n> ChatRoomTableViewController: WebSocket connected successful!")
        }
        socket.onDisconnect = { (error: Error?) -> () in
            if let erro = error as? NSError, erro.code != 1 {
                self.canBecome = false
                self.resignFirstResponder()
                self.tryReconnect()
                print("\n> ChatRoomTableViewController: WebSocket disconnected becouse an error: ", erro.localizedDescription)
            } else {
                print("\n> ChatRoomTableViewController: WebSocket disconnected successfull!\n")
            }
        }
        
        socket.connect()
        print("\n> ChatRoomTableViewController: WebSocket connecting!\n")
        
    }
    
    public func disconnectSockets() {
        guard let socket = webSocket else { return }
        socket.disconnect()
        print("\n> ChatRoomTableViewController: SocketIO disconecting!\n")
    }
    
    public func startListeningMessage() {
        guard let socket = webSocket else { return }
        socket.onText = { (message: String) -> () in
            if let jsonData = message.data(using: String.Encoding.utf8), let JSON = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let messageJSON = JSON?["message"] as? [String : Any], let messageObj = Mapper<Message>().map(JSON: messageJSON) {
                print("\n> ChatRoomTableViewController: WebSocket receive message: !", messageJSON.description)
                switch messageObj.socketEvent {
                case .typingStopped:
                    if messageObj.isMy { break }
                    self.isTyping = false
                case .online:
                    if messageObj.isMy { break }
                    self.isFriendOnline = true
                case .ofline:
                    if messageObj.isMy { break }
                    self.isFriendOnline = false
                case .typing:
                    if messageObj.isMy { break }
                    self.isTyping = true
                    self.typingLabel.text = "\(messageObj.user?.fullName ?? "") набирає повідомлення..."
                    self.scrollToBottom()
                case .messageFailed:
                    if let failedMessage = self.itemList.objects.first(where: { $0.sendingID == messageObj.sendingID }) {
                        if  let sendingIndexPath = self.itemList.tempIndexPath(of: failedMessage) {
                            failedMessage.socketEvent = messageObj.socketEvent
                            self.tableView.beginUpdates()
                            self.tableView.reloadRows(at: [sendingIndexPath], with: .none)
                            self.tableView.endUpdates()
                            AudioServicesPlaySystemSound(1003)
                            AudioServicesPlaySystemSound(4095)
                        }
                    }
                case .messageDeleted, .messageDeletedForAll:
                    guard messageObj.user != nil || messageObj.socketEvent == .messageDeleted else { return }
                    var messageForDelete = [Message]()
                    var indexPaths = [IndexPath]()
                    
                    var indexPathForDelete = [IndexPath]()
                    var sectionsForReload = [Int]()
                    var sectionsForDelete = [Int]()
                    
                    if let ids = messageJSON["ids"] as? [Int] {
                        messageForDelete = self.itemList.objects.filter({ ids.contains(($0.identifier ?? 0)) })
                    }
                    self.tableView.beginUpdates()
                    indexPaths = messageForDelete.map({ self.itemList.indexPath(of: $0)! })
                    for deleteIndexPath in indexPaths {
                        let sectionItems = self.itemList.groupedObjects[deleteIndexPath.section]
                        if sectionItems.count - 1 > 0 {
                            _ = self.itemList.removeAtIndexPath(deleteIndexPath)
                            indexPathForDelete.append(deleteIndexPath)
                            sectionsForReload.append(deleteIndexPath.section)
                        } else {
                            indexPathForDelete.append(deleteIndexPath)
                            sectionsForDelete.append(deleteIndexPath.section)
                            self.itemList.groupedObjects.remove(at: deleteIndexPath.section)
                            for mes in messageForDelete {
                                if let index = self.itemList.objects.index(of: mes) {
                                    self.itemList.objects.remove(at: index)
                                }
                            }
                        }
                    }
                    self.tableView.deleteRows(at: indexPathForDelete, with: .none)
                    self.tableView.deleteSections(IndexSet(sectionsForDelete), with: .none)
                    self.tableView.reloadSections(IndexSet(sectionsForReload), with: .none)
                    self.tableView.endUpdates()
                case .messageSent:
                    if let sendingMessage = self.itemList.objects.first(where: { $0.sendingID == messageObj.sendingID }) {
                        if  let sendingIndexPath = self.itemList.indexPath(of: sendingMessage) {
                            sendingMessage.isSending = false
                            sendingMessage.identifier = messageObj.identifier
//                            self.itemList[sendingIndexPath] = messageObj
                            self.tableView.beginUpdates()
                            self.tableView.reloadRows(at: [sendingIndexPath], with: .none)
                            self.tableView.endUpdates()
                        }
                    }
                case .read:
                    if !messageObj.isMy {
                        self.chat.isReadStatusEnabled = true
                        var reloadPaths: [IndexPath] = []
                        if let lastReadMessage = self.itemList.groupedObjects.flatMap({ $0 }).filter({ $0.isMy && $0.isRead }).last, let lastIndexPath = self.itemList.indexPath(of: lastReadMessage) {
                            reloadPaths.append(lastIndexPath)
                        }
                        self.itemList.groupedObjects.forEach({ $0.filter({ $0.isMy && !$0.isRead }).forEach({ $0.isRead = true })})
                        if let lastMyMessage = self.itemList.objects.filter({ $0.isMy }).last, let idxPath = self.itemList.indexPath(of: lastMyMessage) {
                            self.itemList[idxPath].isRead = true
                            reloadPaths.append(idxPath)
                        }
                        self.tableView.beginUpdates()
                        self.tableView.reloadRows(at: reloadPaths, with: .none)
                        self.tableView.endUpdates()
                    }
                    break
                default:
                    if messageObj.isMy {
//                        if let existedMessage = self.itemList.objects.first(where: { $0.identifier == messageObj.identifier}), let indexPath = self.itemList.indexPath(of: existedMessage) {
//                            self.itemList[indexPath] = messageObj
//                            self.tableView.beginUpdates()
//                            self.tableView.reloadRows(at: [indexPath], with: .none)
//                            self.tableView.endUpdates()
//                        }
                        break
                    }
                    var indexPath: IndexPath!
                    messageObj.isRead = true
                    self.emitReadStatus()
                    if self.itemList.objects.count == 0 {
                        self.itemList.objects = [messageObj]
                        self.itemList.groupedObjects = [[messageObj]]
                        self.tableView.reloadData()
                        break
                    } else {
                        indexPath = IndexPath(row: self.tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1), section: self.tableView.numberOfSections - 1)
                        self.itemList.objects.append(messageObj)
                        self.itemList.groupedObjects[self.tableView.numberOfSections - 1].append(messageObj)
                    }
                    AudioServicesPlaySystemSound(1003)
                    AudioServicesPlaySystemSound(4095)
                    self.insert(messageObj, at: indexPath)
                }
            }
        }
    }
    
    public func startListeningData() {
        guard let socket = webSocket else { return }
        socket.onData = { (data: Data) -> () in
            
        }
    }
    
    // MARK: - Socket Emiters
    
    fileprivate func emitMessage(_ newMessage: Message, insertIntoTableView insert: Bool = true) {
        guard let socket = webSocket else { return }
        var payload = [String : Any]()
        newMessage.isSending = true
        newMessage.sendingID = String(Date().timeIntervalSince1970)
        payload["message"] = newMessage.toJSON()
        guard let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted) else { return }
        guard let dataString = String.init(data: data, encoding: .utf8) else { return }
        
        if insert {
            insertMessageToTheBottom(newMessage)
        }
        
        socket.write(string: dataString) {
            print("\n> ChatRoomTableViewController: WebSocket sent message: !", payload.description)
        }
    }
    
    fileprivate func emitDeleteMessage(_ messageForDelete: Message) {
        guard let socket = webSocket else { return }
        var payload = [String : Any]()
        var messageDetail = [String : Any] ()
        messageDetail["event_type"] = "delete"
        messageDetail["ids"] = [messageForDelete.identifier ?? 0]
        payload["message"] = messageDetail
        
        guard let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted) else { return }
        guard let dataString = String.init(data: data, encoding: .utf8) else { return }
        
        socket.write(string: dataString) {
            print("\n> ChatRoomTableViewController: WebSocket sent delete message: !", payload.description)
        }
    }
    
    fileprivate func emitDeleteMessageForAll(_ messageForDelete: Message) {
        guard let socket = webSocket else { return }
        var payload = [String : Any]()
        var messageDetail = [String : Any] ()
        messageDetail["event_type"] = "delete_for_all"
        messageDetail["ids"] = [messageForDelete.identifier ?? 0]
        payload["message"] = messageDetail
        guard let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted) else { return }
        guard let dataString = String.init(data: data, encoding: .utf8) else { return }
        
        socket.write(string: dataString) {
            print("\n> ChatRoomTableViewController: WebSocket sent delete message for all: !", payload.description)
        }
    }
    
    fileprivate func insertMessageToTheBottom(_ message: Message) {
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.scrollOnce = true
            self.scrollToBottom()
        }
        if self.itemList.objects.count > 0 {
            let lastIndexPath = IndexPath(row: self.tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1) - 1, section: self.tableView.numberOfSections - 1)
            let lastMessage = self.itemList[lastIndexPath]
            if Calendar.current.isDateInToday(lastMessage.date ?? Date()) {
                self.itemList.objects.append(message)
                self.itemList.groupedObjects[self.tableView.numberOfSections - 1].append(message)
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: [IndexPath(row: self.tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1), section: self.tableView.numberOfSections - 1)], with: .none)
                self.tableView.endUpdates()
            } else {
                let newSectionMessages = [message]
                self.itemList.objects.append(message)
                self.itemList.groupedObjects.append(newSectionMessages)
                self.tableView.beginUpdates()
                self.tableView.insertSections(IndexSet([self.tableView.numberOfSections]), with: .top)
                self.tableView.insertRows(at: [IndexPath(row: 0, section: self.tableView.numberOfSections)], with: .none)
                self.tableView.endUpdates()
            }
        } else {
            let newSectionMessages = [message]
            self.itemList.objects.append(message)
            self.itemList.groupedObjects.append(newSectionMessages)
            self.tableView.reloadData()
//            self.tableView.beginUpdates()
//            self.tableView.insertSections(IndexSet([0]), with: .top)
//            self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .none)
//            self.tableView.endUpdates()
        }
        CATransaction.commit()
    }
    
    fileprivate func emitTyping(_ isTyping: Bool) {
        guard let socket = webSocket, socket.isConnected else { return }
        var payload = [String : Any]()
        var message = [String : Any]()
        message["event_type"] = isTyping ? "user_typing" : "user_typing_stopped"
        payload["message"] = message
        guard let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted) else { return }
        guard let dataString = String.init(data: data, encoding: .utf8) else { return }
        socket.write(string: dataString) {
            print("\n> ChatRoomTableViewController: WebSocket sent typing message: !", payload.description)
        }
    }
    
    fileprivate func emitOnlineStatus(_ isOnline: Bool) {
        guard let socket = webSocket, socket.isConnected else { return }
        var payload = [String : Any]()
        var message = [String : Any]()
        message["event_type"] = isOnline ? "is_online" : "is_offline"
        payload["message"] = message
        guard let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted) else { return }
        guard let dataString = String.init(data: data, encoding: .utf8) else { return }
        socket.write(string: dataString) {
            print("\n> ChatRoomTableViewController: WebSocket sent Online Status message: !", payload.description)
        }
    }
    
    fileprivate func emitReadStatus() {
        guard let socket = webSocket else { return }
        var payload = [String : Any]()
        var message = [String : Any]()
        message["event_type"] = "read"
        payload["message"] = message
        guard let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted) else { return }
        guard let dataString = String.init(data: data, encoding: .utf8) else { return }
        socket.write(string: dataString) {
            print("\n> ChatRoomTableViewController: WebSocket sent Read Status message: !", payload.description)
        }
    }
    
}
