//
//  DialogsTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/30/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class DialogsTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    //MARK: - Properties
    
    
    lazy var itemList: PaginatedListModel<Chat> = {
        let list = PaginatedListModel<Chat>.init(path: Chat.Constants.chatsListRoute)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGradientedNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshContent(nil)
    }

    override func refreshContent(_ sender: UIRefreshControl?) {
        guard itemList.state != .loading else {
            sender?.endRefreshing()
            return
        }
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
        }
    }
    
    func openChat(with id: Int) {
        guard itemList.state != .loading else { return }
        present(activityVC, animated: true, completion: nil)
        itemList.load { (success) in
            self.activityVC.dismiss(animated: true, completion: {
                if success {
                    if let dialog = self.itemList.objects.first(where: { $0.identifier == id }) {
                        let chatRoomVC: ChatRoomTableViewController = UIStoryboard(.Chats).instantiateViewController()
                        chatRoomVC.chat = dialog
                        self.show(chatRoomVC, sender: self)
                    } else {
                        ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Не знайдено такого діалогу", comment: ""))
                    }
                }
            })
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chat = itemList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: DialogTableViewCell.cellIdentifier, for: indexPath) as! DialogTableViewCell
        cell.setUp(with: chat)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chat = itemList[indexPath.row]
        let chatRoomVC: ChatRoomTableViewController = UIStoryboard(.Chats).instantiateViewController()
        chatRoomVC.chat = chat
        show(chatRoomVC, sender: self)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actions = [UITableViewRowAction]()
        let delete = UITableViewRowAction(style: .destructive, title: NSLocalizedString("Видалити", comment: "")) { (action, editingIndexPath) in
            let chat = self.itemList[editingIndexPath.row]
            guard let cell = tableView.cellForRow(at: editingIndexPath) else { return }
            cell.alpha = 0.5
            cell.isUserInteractionEnabled = false
            chat.onRequestFailure = self.requestFailureHandler
            self.present(self.activityVC, animated: true, completion: nil)
            chat.delete(with: { (success) in
                cell.alpha = 1.0
                cell.isUserInteractionEnabled = true
                if success {
                    self.activityVC.dismiss(animated: true, completion: {
                        self.itemList.removeAtIndex(index: editingIndexPath.row)
                        tableView.beginUpdates()
                        tableView.deleteRows(at: [editingIndexPath], with: .automatic)
                        tableView.endUpdates()
                    })
                }
            })
        }
        actions.append(delete)
        return actions
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
}

fileprivate extension DialogsTableViewController {
    
    @IBAction func createChat(_ sender: UIBarButtonItem) {
        let friendsVC: FindUserTableViewController = UIStoryboard(.Profile).instantiateViewController()
        friendsVC.cellsType = .chats
        friendsVC.listType = .friends
        friendsVC.startChatHandler = { (viewController, user) in
            viewController.navigationController?.popViewController(animated: false)
            self.present(self.activityVC, animated: true, completion: nil)
            let newChat = Chat()
            newChat.user = user
            newChat.create(for: user.identifier ?? 0, with: { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        let chatVC: ChatRoomTableViewController = UIStoryboard(.Chats).instantiateViewController()
                        chatVC.chat = newChat
                        self.show(chatVC, sender: self)
                    }
                })
            })
        }
        show(friendsVC, sender: self)
    }
    
}

extension DialogsTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}
