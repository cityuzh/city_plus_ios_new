//
//  VideoPreviewViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/22/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Photos
import AVKit

final class VideoPreviewViewController: AVPlayerViewController {
    
    var content: Avatar!
    
    fileprivate var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration)
    }()
    
    lazy var activityVC: ActivityViewController = {
        return ActivityViewController(viewController: self, sourceVeiw: nil)
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPlayer(with: content)
    }
    
}

fileprivate extension VideoPreviewViewController {
    
    @IBAction func share(_ sender: UIBarButtonItem) {
        let items = [URL(fileURLWithPath: content.urlPath!)]
        let activityController = UIActivityViewController.init(activityItems: items, applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        var request = URLRequest(url: URL(string: content.urlPath!)!)
        request.cachePolicy = .returnCacheDataElseLoad
        present(activityVC, animated: true, completion: nil)
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            guard let data = data else { return }
            DispatchQueue.global(qos: .background).async {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/\(String(describing: URL(string: self.content.urlPath!)?.lastPathComponent)).mp4"
                DispatchQueue.main.async {
                    do {
                        try data.write(to: URL(fileURLWithPath: filePath), options: [])
                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                        }) { saved, error in                            
                            DispatchQueue.main.async {
                                self.activityVC.dismiss(animated: true, completion: {
                                    if saved {
                                        ShowAlert.showInfoAlertWithOKAction(in: self, message: NSLocalizedString("Відео збережене до галереї", comment: ""))
                                    } else {
                                        ShowAlert.showInfoAlertWithOKAction(in: self, message: error?.localizedDescription ?? "Щось пішло не так!")
                                    }
                                })
                            }
                        }
                    } catch {
                        print(error)
                    }
                }
            }
        }
        task.resume()
    }
    
    func setupPlayer(with content: Avatar) {
        if let url = URL(string: content.urlPath ?? "") {
            player = AVPlayer(url: url)
            player?.play()
        }
    }
    
}
