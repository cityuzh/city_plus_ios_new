//
//  ChatSettingsTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/20/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class ChatSettingsTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var backgroundCollectionView: UICollectionView!
    @IBOutlet weak var textColorCollectionView: UICollectionView!
    
    @IBOutlet weak var hideReadStatusSwitch: UISwitch!
    @IBOutlet weak var muteNotifications: UISwitch!
    
    var chatRoomVC: ChatRoomTableViewController!
    
    lazy var settings: ChatSettings = {
        let settings = ChatSettings.loadOrInitSaved()
        return settings
    }()
    
    lazy var allBackGroundItems: [BackgroundItem] = {
        let items = BackgroundItem.loadAll()
        return items
    }()
    lazy var allTextColors: [TextColorItem] = {
        let items = TextColorItem.loadAll()
        return items
    }()
    
    var chat: Chat!
    var isSendingRequest = false
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayout = backgroundCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 15
        flowLayout.minimumInteritemSpacing = 15
        flowLayout.sectionInset = UIEdgeInsets.init(top: 10, left: 15, bottom: 10, right: 0)
        let height = tableView(tableView, heightForRowAt: IndexPath(row:0, section: 0))
        let width = height * 2/3
        flowLayout.itemSize = CGSize(width: width, height: height)
        backgroundCollectionView.collectionViewLayout = flowLayout
        
        
        let flowColorLayout = textColorCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowColorLayout.scrollDirection = .horizontal
        flowColorLayout.minimumLineSpacing = 5
        flowColorLayout.minimumInteritemSpacing = 5
        flowColorLayout.sectionInset = UIEdgeInsets.init(top: 5, left: 15, bottom: 5, right: 0)
        let colorHeight = tableView(tableView, heightForRowAt: IndexPath(row:0, section: 1)) - 10
        flowColorLayout.itemSize = CGSize(width: colorHeight, height: colorHeight)
        textColorCollectionView.collectionViewLayout = flowColorLayout
        
        muteNotifications.isOn = !chat.isMuted
        hideReadStatusSwitch.isOn = !chat.disableRead
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterBackground(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func appWillEnterBackground(_ notification: NSNotification) {
        chatRoomVC.appWillEnterBackground(notification)
    }
    
    @objc func appWillEnterForeground(_ notification: NSNotification) {
        chatRoomVC.appWillEnterForeground(notification)
    }

}

extension ChatSettingsTableViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == backgroundCollectionView ? allBackGroundItems.count : allTextColors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ChatBkgrdCollectionViewCell.classForCoder()), for: indexPath) as! ChatBkgrdCollectionViewCell
        if collectionView == backgroundCollectionView {
            cell.fonImageView.image = allBackGroundItems[indexPath.item].image
            cell.chekedMark.isHighlighted = settings.currentBackgroundItem.id == allBackGroundItems[indexPath.item].id
            cell.layer.borderWidth = 0
        } else {
            cell.fonImageView.backgroundColor = allTextColors[indexPath.item].color
            cell.chekedMark.isHighlighted = settings.currentTextColorItem.id == allTextColors[indexPath.item].id
            if indexPath.row == 1 {
                cell.layer.borderColor = UIColor.black.cgColor
                cell.layer.borderWidth = 1.0 / UIScreen.main.scale
            } else {
                cell.layer.borderWidth = 0.0
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == backgroundCollectionView {
            settings.currentBackgroundItem = allBackGroundItems[indexPath.row]
        } else {
            settings.currentTextColorItem = allTextColors[indexPath.row]
        }
        ChatSettings.saveSettings(settings)
        collectionView.reloadData()
    }
    
}

fileprivate extension ChatSettingsTableViewController {
    
    @IBAction func deleteChat(_ sender: UIButton) {
        present(activityVC, animated: true, completion: nil)
        chat.onRequestFailure = requestFailureHandler
        chat.delete { (success) in
            if success {
                self.activityVC.dismiss(animated: true, completion: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
            }
        }
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        if sender == muteNotifications {
            guard !isSendingRequest else { return }
            chat.onRequestFailure = requestFailureHandler
            if sender.isOn {
                muteNotifications.isEnabled = false
                muteNotifications.setOn(false, animated: true)
                self.muteNotifications.isEnabled = false
                isSendingRequest = true
                chat.enablePushNotification(true, with: { (success) in
                    self.isSendingRequest = false
                    self.muteNotifications.isEnabled = true
                    if success {
                        self.chat.isMuted = false;
                        self.muteNotifications.setOn(true, animated: true)
                    }
                })
            } else {
                muteNotifications.isEnabled = false
                muteNotifications.setOn(true, animated: true)
                isSendingRequest = true
                chat.enablePushNotification(false, with: { (success) in
                    self.isSendingRequest = false
                    self.muteNotifications.isEnabled = true
                    if success {
                        self.chat.isMuted = true;
                        self.muteNotifications.setOn(false, animated: true)
                    }
                })
            }
        } else {
            guard !isSendingRequest else { return }
            chat.onRequestFailure = requestFailureHandler
            if sender.isOn {
                hideReadStatusSwitch.setOn(false, animated: true)
                hideReadStatusSwitch.isEnabled = false
                isSendingRequest = true
                chat.enableReadStatus(true, with: { (success) in
                    self.isSendingRequest = false
                    self.hideReadStatusSwitch.isEnabled = true
                    if success {
                        self.chat.disableRead = false;
                        self.hideReadStatusSwitch.setOn(true, animated: true)
                    }
                })
            } else {
                hideReadStatusSwitch.isEnabled = false
                hideReadStatusSwitch.setOn(false, animated: true)
                isSendingRequest = true
                chat.enableReadStatus(false, with: { (success) in
                    self.isSendingRequest = false
                    self.hideReadStatusSwitch.isEnabled = true
                    if success {
                        self.chat.disableRead = true;
                        self.hideReadStatusSwitch.setOn(false, animated: true)
                    }
                })
            }
        }
    }
    
}
