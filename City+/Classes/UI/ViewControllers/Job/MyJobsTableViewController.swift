//
//  MyJobsTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/2/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class MyJobsTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    //MARK: - Lifecycle
    
    lazy var itemList: LevelPaginatedListModel<Vacancy> = {
       let list = LevelPaginatedListModel<Vacancy>.init(path: Vacancy.Constants.vacancyMyListPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    var dataSourse: Variable<[Vacancy]> = Variable([])
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = nil
        tableView.dataSource = nil
        
        placeholderView.removeFromSuperview()
        placeholderView = TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode, customImage: UIImage(named: "img_empty state_no cv"), customTitle: NSLocalizedString("Вакансії відсутні", comment: ""), customSubTitle: NSLocalizedString("Створіть нову вакансію зараз", comment: ""))
        tableView.rx.modelSelected(Vacancy.self)
        .subscribe(onNext: { (vacansy) in
            let detailVC: JobDetailTableViewController = UIStoryboard(.Job).instantiateViewController()
            detailVC.vacancy = vacansy
            self.show(detailVC, sender: self)
        }, onError: nil, onCompleted: nil, onDisposed: nil)
        .disposed(by: disposeBag)
        
        dataSourse.asObservable()
            .subscribe({ (dataSourse) in
                self.tableView.backgroundView = dataSourse.element?.count == 0 ? self.placeholderView : nil
            })
            .disposed(by: disposeBag)
        
        dataSourse.asObservable()
        .bind(to: tableView.rx.items(cellIdentifier: "VacancyTableViewCell", cellType: VacancyTableViewCell.self)) { row, vacancy, cell in
            cell.setUp(with: vacancy)
            if (self.itemList.count - row) <= (self.itemList.requestRowCount / 4) && self.itemList.state == .canLoadMore {
                self.itemList.loadMore { (success) -> Void in
                    if success {
                        self.dataSourse.value.append(contentsOf: self.itemList.newObjects ?? [])
                    }
                }
            }
        }
        .disposed(by: disposeBag)
        
        searchBar.rx.text
        .orEmpty
        .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
        .distinctUntilChanged()
        .subscribe { (text) in
            if !(self.searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                self.itemList.queryParams["title"] = self.searchBar.text! as AnyObject
            } else {
                self.itemList.queryParams.removeValue(forKey: "title")
            }
            self.refreshContent(nil)
        }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchBar.endEditing(true)
                if !(self.searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    self.itemList.queryParams["title"] = self.searchBar.text! as AnyObject
                } else {
                    self.itemList.queryParams.removeValue(forKey: "title")
                }
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchBar.endEditing(true)
                self.searchBar.text?.removeAll()
                self.itemList.queryParams.removeValue(forKey: "title")
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshContent(nil)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        self.itemList.objects.removeAll()
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
                self.dataSourse.value = self.itemList.objects
            }
        }
    }
    
}
