//
//  JobFilterTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class JobFilterTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var setButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var salary: UITextField!
    @IBOutlet weak var categoryName: UITextField!
    
    var currentFilter: JobFilter!
    var filterSettedHandler: ((_ filter: JobFilter)->())!
    
    var disposeBag = DisposeBag()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryName.text = currentFilter.category?.name
        salary.text = currentFilter.minimumSalary != nil ? String(currentFilter.minimumSalary!) : ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for i in 0...3 {
            tableView.cellForRow(at: IndexPath(row: i, section:2))?.accessoryType = .none
            tableView.cellForRow(at: IndexPath(row: i, section:2))?.textLabel?.textColor = .lightGray
        }
        if let cell = tableView.cellForRow(at: IndexPath(row: currentFilter.workType?.rawValue ?? 0, section: 2)) {
            cell.accessoryType = .checkmark
            cell.textLabel?.textColor = .darkGray
        }
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        func clearSelection() {
            for i in 0...3 {
                tableView.cellForRow(at: IndexPath(row: i, section:2))?.accessoryType = .none
                tableView.cellForRow(at: IndexPath(row: i, section:2))?.textLabel?.textColor = .lightGray
            }
        }
        switch indexPath {
        case IndexPath(row: 0, section:2):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            currentFilter.workType = .any
        case IndexPath(row: 1, section:2):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            currentFilter.workType = .fullTime
        case IndexPath(row: 2, section:2):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            currentFilter.workType = .partTime
        case IndexPath(row: 3, section:2):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            currentFilter.workType = .remote
        case IndexPath(row: 0, section:0):
            let categoryVC: JobCategoriesTableViewController = UIStoryboard(.Job).instantiateViewController()
            categoryVC.selectedCategory.asObservable()
                .subscribe(onNext: {
                    self.currentFilter.category = $0
                    self.categoryName.text = $0.name
                })
                .disposed(by: disposeBag)
            show(categoryVC, sender: self)
        default: break
        }
    }
    

}
fileprivate extension JobFilterTableViewController {
    
    @IBAction func setAction(_ sender: UIButton) {
        let formatter = NumberFormatter()
        if let salaryNumber = formatter.number(from: salary.text ?? "")?.intValue {
            currentFilter.minimumSalary = salaryNumber
        }
        navigationController?.popViewController(animated: true)
        filterSettedHandler?(currentFilter)
    }
    
    @IBAction func clearAction(_ sender: UIButton) {
        filterSettedHandler?(JobFilter())
        navigationController?.popViewController(animated: true)
    }
    
}
