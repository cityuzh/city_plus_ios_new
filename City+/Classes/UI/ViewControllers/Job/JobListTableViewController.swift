//
//  JobListTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/25/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class JobListTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterButton: UIBarButtonItem!
    
    var activFilter = JobFilter() {
        didSet {
            if let salary = activFilter.minimumSalary {
                vacanCyList.queryParams["salary_0"] = salary as AnyObject
            } else {
                vacanCyList.queryParams.removeValue(forKey: "salary_0")
            }
            
            if let category = activFilter.category {
                vacanCyList.queryParams["category"] = category.identifier as AnyObject
            } else {
                vacanCyList.queryParams.removeValue(forKey: "category")
            }
            
            if let workType = activFilter.workType {
                vacanCyList.queryParams["work_type"] = workType.rawValue as AnyObject
            } else {
                vacanCyList.queryParams.removeValue(forKey: "work_type")
            }
            refreshContent(nil)
        }
    }
    
    
    //MARK: - Properties
    
    lazy var vacanCyList: PaginatedListModel<Vacancy> = {
        let list = PaginatedListModel<Vacancy>.init(path: Vacancy.Constants.vacancyRoute)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    fileprivate var searchTimer: Timer?
    fileprivate var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                vacanCyList.queryParams["title"] = searchText as AnyObject
            } else {
                vacanCyList.queryParams.removeValue(forKey: "title")
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vacanCyList.load()
        placeholderView.removeFromSuperview()
        placeholderView = TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode, customImage: UIImage(named: "img_empty state_no cv"), customTitle: NSLocalizedString("Вакансії відсутні", comment: ""), customSubTitle: NSLocalizedString("Спробуйте інші налаштування фільтру", comment: ""))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
        refreshContent(nil)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        vacanCyList.load(completion: { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
        })
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = vacanCyList.count == 0 ? placeholderView : nil
        return vacanCyList.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VacancyTableViewCell.identitfier, for: indexPath) as! VacancyTableViewCell
        cell.setUp(with: vacanCyList[indexPath.row])
        cell.favouriteHandler = { (cell) in
            if let cellIndexPath = tableView.indexPath(for: cell) {
                let item = self.vacanCyList[cellIndexPath.row]
                if item.isFavorite {
                    self.present(self.activityVC, animated: true, completion: nil)
                    item.removeFromFavorite { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                item.isFavorite = false
                                cell.favouriteButton?.setImage(#imageLiteral(resourceName: "favoritesUnselectedButton"), for: .normal)
                            }
                        })
                    }
                } else {
                    self.present(self.activityVC, animated: true, completion: nil)
                    item.addToFavorite { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                item.isFavorite = true
                                cell.favouriteButton?.setImage(#imageLiteral(resourceName: "favoriteSelectedButton"), for: .normal)
                            }
                        })
                    }
                }
            }
        }
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destination = segue.destination as? JobDetailTableViewController, let cell = sender as? VacancyTableViewCell, let index = tableView.indexPath(for: cell)?.row {
            destination.vacancy = vacanCyList[index]
        }
        if let destination = segue.destination as? JobFilterTableViewController {
            destination.currentFilter = activFilter
            destination.filterSettedHandler = { (filter) in
                self.activFilter = filter
            }
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        searchBar.resignFirstResponder()
    }
    
}

//MARK: - Private

fileprivate extension JobListTableViewController {
    
    @IBAction func addAction(_ sender: UIBarButtonItem) {
        let createJob: CreateJobTableViewController = UIStoryboard(.Job).instantiateViewController()
        show(createJob, sender: self)
    }
    
    @objc func searchAction(_ sender: Timer) {
        searchText = searchBar.text ?? ""
        refreshContent(nil)
    }
    
}

extension JobListTableViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchText = ""
        searchBar.text = nil
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTimer?.invalidate()
        searchTimer = Timer.scheduledTimer(timeInterval: Configuration.Constants.searchTextDelay, target: self, selector: #selector(searchAction(_:)), userInfo: nil, repeats: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchText = searchBar.text ?? ""
        refreshContent(nil)
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if (searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            searchText = searchBar.text ?? ""
            refreshContent(nil)
        }
    }
    
}
