//
//  ResumeListTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 5/20/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

class ResumeListTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterButton: UIBarButtonItem!

    var activFilter = JobFilter() {
        didSet {
            if let salary = activFilter.minimumSalary {
                resumeList.queryParams["salary_0"] = salary as AnyObject
            } else {
                resumeList.queryParams.removeValue(forKey: "salary_0")
            }
            
            if let category = activFilter.category {
                resumeList.queryParams["category"] = category.identifier as AnyObject
            } else {
                resumeList.queryParams.removeValue(forKey: "category")
            }
            
            if let workType = activFilter.workType {
                resumeList.queryParams["work_type"] = workType.rawValue as AnyObject
            } else {
                resumeList.queryParams.removeValue(forKey: "work_type")
            }
            refreshContent(nil)
        }
    }
    
    
    //MARK: - Properties
    
    lazy var resumeList: PaginatedListModel<Resume> = {
        let list = PaginatedListModel<Resume>.init(path: Resume.Constants.resumeRoute)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    fileprivate var searchTimer: Timer?
    fileprivate var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                resumeList.queryParams["title"] = searchText as AnyObject
            } else {
                resumeList.queryParams.removeValue(forKey: "title")
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resumeList.load()
        placeholderView.removeFromSuperview()
        placeholderView = TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode, customImage: UIImage(named: "img_empty state_no cv"), customTitle: NSLocalizedString("Резюме відсутні", comment: ""), customSubTitle: NSLocalizedString("Спробуйте інші налаштування фільтру", comment: ""))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
        refreshContent(nil)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        resumeList.load(completion: { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
        })
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = resumeList.count == 0 ? placeholderView : nil
        return resumeList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ResumeTableViewCell.identitfier, for: indexPath) as! ResumeTableViewCell
        cell.setUp(with: resumeList[indexPath.row])
        cell.profileHandler = { (cell) in
            if let cellIndexPath = tableView.indexPath(for: cell) {
                let item = self.resumeList[cellIndexPath.row]
                let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                profileVC.user = item.owner
                profileVC.showMenuButton = false
                self.show(profileVC, sender: self)
            }
        }
        cell.favouriteHandler = { (cell) in
            if let cellIndexPath = tableView.indexPath(for: cell) {
                let item = self.resumeList[cellIndexPath.row]
                if item.isFavorite {
                    self.present(self.activityVC, animated: true, completion: nil)
                    item.removeFromFavorite { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                item.isFavorite = false
                                cell.favouriteButton?.setImage(#imageLiteral(resourceName: "favoritesUnselectedButton"), for: .normal)
                            }
                        })
                    }
                } else {
                    self.present(self.activityVC, animated: true, completion: nil)
                    item.addToFavorite { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                item.isFavorite = true
                                cell.favouriteButton?.setImage(#imageLiteral(resourceName: "favoriteSelectedButton"), for: .normal)
                            }
                        })
                    }
                }
            }
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destination = segue.destination as? ResumeDetailTableViewController, let cell = sender as? ResumeTableViewCell, let index = tableView.indexPath(for: cell)?.row {
            destination.resume = resumeList[index]
        }
        if let destination = segue.destination as? JobFilterTableViewController {
            destination.currentFilter = activFilter
            destination.filterSettedHandler = { (filter) in
                self.activFilter = filter
            }
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        searchBar.resignFirstResponder()
    }
    
}

//MARK: - Private

fileprivate extension ResumeListTableViewController {
    
    @IBAction func addAction(_ sender: UIBarButtonItem) {
        let createResume: CreateResumeTableViewController = UIStoryboard(.Job).instantiateViewController()
        show(createResume, sender: self)
    }
    
    @objc func searchAction(_ sender: Timer) {
        searchText = searchBar.text ?? ""
        refreshContent(nil)
    }
    
}

extension ResumeListTableViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchText = ""
        searchBar.text = nil
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTimer?.invalidate()
        searchTimer = Timer.scheduledTimer(timeInterval: Configuration.Constants.searchTextDelay, target: self, selector: #selector(searchAction(_:)), userInfo: nil, repeats: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchText = searchBar.text ?? ""
        refreshContent(nil)
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if (searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            searchText = searchBar.text ?? ""
            refreshContent(nil)
        }
    }
    
}
