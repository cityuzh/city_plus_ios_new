//
//  JobDetailTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/26/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import MessageUI

final class JobDetailTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var elapsedTime: UILabel!
    @IBOutlet weak var vacancyTitle: UILabel!
    @IBOutlet weak var isPremiumImage: UIImageView!
    
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyAddress: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var workType: UILabel!
    @IBOutlet weak var salary: UILabel!
    
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var persomEmail: UILabel!
    @IBOutlet weak var personPhone: UILabel!

    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var requirements: UILabel!
    
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    @IBOutlet weak var favoriteBarButton: UIBarButtonItem!
    
    //MARK: - Properties
    
    var vacancy: Vacancy!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillUI(with: vacancy)
        if !vacancy.isRead {
            vacancy.markAsRead { (success) in
                if success {
                    self.vacancy.isRead = true
                }
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat.leastNonzeroMagnitude : super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section != 3 ? CGFloat.leastNonzeroMagnitude : super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 9:
            return vacancy.isMy ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case 3:
            return (vacancy.address ?? "").isEmpty ? 0 : UITableView.automaticDimension
        case 4:
            return (vacancy.contactPerson.name ?? "").isEmpty ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case 5:
            return (vacancy.contactPerson.phone ?? "").isEmpty ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case 6:
            return (vacancy.contactPerson.email ?? "").isEmpty ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case 1,2,8:
            return UITableView.automaticDimension
        default: return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destination =  segue.destination as? CreateJobTableViewController {
            destination.vacancy = vacancy
            destination.currentAction = .update
        }
    }

}

extension JobDetailTableViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .sent:
            controller.dismiss(animated: true) {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Ваше резюме надіслано!", comment: ""))
            }
        default:
            controller.dismiss(animated: true, completion: nil)
        }
    }
    
}

fileprivate extension JobDetailTableViewController {
    
    @IBAction func sendResume(_ sender: UIButton) {
        let composer = MFMailComposeViewController()
        composer.setToRecipients([vacancy.contactPerson.email ?? ""])
        composer.setSubject(vacancy.title ?? "")
        composer.mailComposeDelegate = self
        composer.navigationController?.navigationBar.tintColor = .appTintColor
        present(composer, animated: true, completion: nil)
    }
    
    @IBAction func addFavorite(_ sender: UIBarButtonItem) {
        if vacancy.isFavorite {
            present(activityVC, animated: true, completion: nil)
            vacancy.removeFromFavorite { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.vacancy.isFavorite = false
                        self.fillUI(with: self.vacancy)
                    }
                })
            }
        } else {
            present(activityVC, animated: true, completion: nil)
            vacancy.addToFavorite { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.vacancy.isFavorite = true
                        self.fillUI(with: self.vacancy)
                    }
                })
            }
        }
    }
    
    @IBAction func editVacancy(_ sender: UIBarButtonItem) {
        
    }
    
    @IBAction func photeTapped(_ sender: UITapGestureRecognizer) {
        if let phone = vacancy.contactPerson.phone {
            if let phone = URL(string: "tel://" + (phone.hasPrefix("38") ? "+" + phone : phone).replacingOccurrences(of: " ", with: "")) {
                UIApplication.shared.open(phone, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
    }
    
    @IBAction func emailTapped(_ sender: UITapGestureRecognizer) {
        if let email = vacancy.contactPerson.email {
            let composer = MFMailComposeViewController()
            composer.setToRecipients([email])
            composer.setSubject(vacancy.title ?? "")
            composer.mailComposeDelegate = self
            composer.navigationController?.navigationBar.tintColor = .appTintColor
            present(composer, animated: true, completion: nil)
        }
    }
    
    func fillUI(with vacancy: Vacancy) {
        
        isPremiumImage.isHidden = !vacancy.isPremium
        elapsedTime.text = vacancy.createdAt?.getElapsedInterval()
        vacancyTitle.text = vacancy.title
        
        workType.text = vacancy.workType.title
        
        companyName.text = vacancy.companyName
        companyAddress.text = vacancy.address
        categoryName.text = vacancy.category?.name
        salary.text = String(vacancy.salary ?? 0) + " грн"
        salary.isHidden = vacancy.salary == nil
        
        personName.text = vacancy.contactPerson.name
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.appTintColor,
            NSAttributedString.Key.underlineColor: UIColor.appTintColor,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        
        persomEmail.attributedText = NSAttributedString(string: vacancy.contactPerson.email ?? "", attributes: linkAttributes)
        personPhone.attributedText = NSAttributedString(string: vacancy.contactPerson.phone ?? "", attributes: linkAttributes)
        
        requirements.text = vacancy.requirements
        about.text = vacancy.about
        
        if vacancy.isMy {
            navigationItem.rightBarButtonItems = [editBarButton]
        } else {
            favoriteBarButton.image = vacancy.isFavorite ? #imageLiteral(resourceName: "ic_favorite_selected_navbar") : #imageLiteral(resourceName: "ic_favorite_navbar")
            navigationItem.rightBarButtonItems = [favoriteBarButton]
        }
        
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
