//
//  ResumeDetailTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/4/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import MessageUI

class ResumeDetailTableViewController: BaseTableViewController {
    //MARK: - IBOutlets
    
    @IBOutlet weak var elapsedTime: UILabel!
    @IBOutlet weak var resumeTitle: UILabel!
    @IBOutlet weak var isPremiumImage: UIImageView!
    
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var workType: UILabel!
    @IBOutlet weak var salary: UILabel!
    
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var personEmail: UILabel!
    @IBOutlet weak var personPhone: UILabel!
    @IBOutlet weak var personImage: UIImageView!
    
    @IBOutlet weak var about: UILabel!
    
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    @IBOutlet weak var favoriteBarButton: UIBarButtonItem!
    
    //MARK: - Properties
    
    var resume: Resume!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillUI(with: resume)
        if !resume.isRead {
            resume.markAsRead { (success) in
                if success {
                    self.resume.isRead = true
                }
            }
        }
    }
    // MARK: - Table view data source
        
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 6:
            return resume.isMy ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case 2:
            return (resume.owner?.phone ?? "").isEmpty ? 0 : UITableView.automaticDimension
        case 3:
            return (resume.owner?.email ?? "").isEmpty ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case 5:
            return UITableView.automaticDimension
        default: return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destination =  segue.destination as? CreateResumeTableViewController {
            destination.resume = resume
            destination.currentAction = .update
        }
    }
    
}

extension ResumeDetailTableViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .sent:
            controller.dismiss(animated: true) {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Ваше резюме надіслано!", comment: ""))
            }
        default:
            controller.dismiss(animated: true, completion: nil)
        }
    }
    
}

fileprivate extension ResumeDetailTableViewController {
    
    @IBAction func sendResume(_ sender: UIButton) {
        let composer = MFMailComposeViewController()
        composer.setToRecipients([resume.owner?.email ?? ""])
        composer.setSubject(resume.title ?? "")
        composer.mailComposeDelegate = self
        composer.navigationController?.navigationBar.tintColor = .appTintColor
        present(composer, animated: true, completion: nil)
    }
    
    @IBAction func addFavorite(_ sender: UIBarButtonItem) {
        if resume.isFavorite {
            present(activityVC, animated: true, completion: nil)
            resume.removeFromFavorite { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.resume.isFavorite = false
                        self.fillUI(with: self.resume)
                    }
                })
            }
        } else {
            present(activityVC, animated: true, completion: nil)
            resume.addToFavorite { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.resume.isFavorite = true
                        self.fillUI(with: self.resume)
                    }
                })
            }
        }
    }
    
    @IBAction func editresume(_ sender: UIBarButtonItem) {
        
    }
    
    @IBAction func ownerTapped(_ sender: UITapGestureRecognizer) {
        if let owner = resume.owner {
            let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
            profileVC.user = owner
            profileVC.showMenuButton = false
            self.show(profileVC, sender: self)
        }
    }
    
    @IBAction func photeTapped(_ sender: UITapGestureRecognizer) {
        if let phone = resume.owner?.phone {
            if let phone = URL(string: "tel://" + (phone.hasPrefix("38") ? "+" + phone : phone).replacingOccurrences(of: " ", with: "")) {
                UIApplication.shared.open(phone, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
    }
    
    @IBAction func emailTapped(_ sender: UITapGestureRecognizer) {
        if let email = resume.owner?.email {
            let composer = MFMailComposeViewController()
            composer.setToRecipients([email])
            composer.setSubject(resume.title ?? "")
            composer.mailComposeDelegate = self
            composer.navigationController?.navigationBar.tintColor = .appTintColor
            present(composer, animated: true, completion: nil)
        }
    }
    
    func fillUI(with resume: Resume) {
        
        
        
        isPremiumImage.isHidden = !resume.isPremium
        elapsedTime.text = resume.createdAt?.getElapsedInterval()
        resumeTitle.text = resume.title
        
        workType.text = resume.workType.title
        
        categoryName.text = resume.category?.name
        salary.text = String(resume.salary ?? 0) + " грн"
        salary.isHidden = resume.salary == nil
        
        personName.text = resume.owner?.fullName
        personImage.setImage(with: resume.owner?.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: nil)
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.newAppTintColor,
            NSAttributedString.Key.underlineColor: UIColor.newAppTintColor,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        
        personEmail.attributedText = NSAttributedString(string: resume.owner?.email ?? "", attributes: linkAttributes)
        personPhone.attributedText = NSAttributedString(string: resume.owner?.phone ?? "", attributes: linkAttributes)
        
        about.text = resume.about
        
        if resume.isMy {
            navigationItem.rightBarButtonItems = [editBarButton]
        } else {
            favoriteBarButton.image = resume.isFavorite ? #imageLiteral(resourceName: "ic_favorite_selected_navbar") : #imageLiteral(resourceName: "ic_favorite_navbar")
            navigationItem.rightBarButtonItems = [favoriteBarButton]
        }
        
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
