//
//  JobCategoriesTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/2/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class JobCategoriesTableViewController: BaseTableViewController {

    var selectedCategory: PublishSubject<JobCategory> = PublishSubject()
    
    var dataSource: Variable<[JobCategory]> =  Variable([])
    
    //MARK: - Properties
    
    var disposeBag = DisposeBag()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        JobCategory.allCategories { (categories) in
            self.dataSource.value.append(contentsOf: categories ?? [])
        }
        
        tableView.delegate = nil
        tableView.dataSource = nil
        
        dataSource.asObservable()
        .bind(to: self.tableView.rx.items(cellIdentifier: "CategoryCell")) { row, category, cell in
            cell.textLabel?.text = category.name
        }
        .disposed(by: self.disposeBag)
        
        tableView.rx.modelSelected(JobCategory.self)
            .bind(to: selectedCategory)
            .disposed(by: disposeBag)
        
        selectedCategory.asObservable()
            .subscribe(onNext: { (category) in
                self.navigationController?.popViewController(animated: true)
            }, onError: nil, onCompleted: nil, onDisposed: nil)
            .disposed(by: disposeBag)
    }

}
