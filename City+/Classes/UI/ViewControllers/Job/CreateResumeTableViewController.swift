//
//  CreateResumeTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class CreateResumeTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var resumeName: UITextField!
    @IBOutlet weak var categoryName: UITextField!
    
    @IBOutlet weak var salary: UITextField!
    
    @IBOutlet weak var resumeDescription: UITextView!
    
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var saveButton: UIBarButtonItem?
    
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet var textViews: [UITextView]!
    //MARK: - Properties
    
    var resume = Resume()
    var currentAction: Action = .create
    
    var resumeNameObs: Observable<String>!
    
    var salaryObs: Observable<Int>!
    
    var aboutObs: Observable<String>!
    
    var disposeBag = DisposeBag()
    
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if currentAction == .create {
            navigationItem.rightBarButtonItem = nil
            navigationItem.rightBarButtonItems = []
        }
        
        categoryName.text = resume.category?.name
        
        subscribeRxSubjects()
        subscribeRxUI()
    }

    //MARK: - UITableViewDataSource/UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat.leastNonzeroMagnitude : super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section != 3 ? CGFloat.leastNonzeroMagnitude : super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath {
        case IndexPath(row: 0, section:1):
            cell.accessoryType = resume.workType == .any ? .checkmark : .none
            cell.textLabel?.textColor = resume.workType == .any ? .darkGray : .lightGray
        case IndexPath(row: 1, section:1):
            cell.accessoryType = resume.workType == .fullTime ? .checkmark : .none
            cell.textLabel?.textColor = resume.workType == .fullTime ? .darkGray : .lightGray
        case IndexPath(row: 2, section:1):
            cell.accessoryType = resume.workType == .partTime ? .checkmark : .none
            cell.textLabel?.textColor = resume.workType == .partTime ? .darkGray : .lightGray
        case IndexPath(row: 3, section:1):
            cell.accessoryType = resume.workType == .remote ? .checkmark : .none
            cell.textLabel?.textColor = resume.workType == .remote ? .darkGray : .lightGray
        default: break
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath {
        case IndexPath(row: 0, section: 3): return currentAction == .update ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case IndexPath(row: 1, section: 3): return currentAction == .create ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        default: return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        func clearSelection() {
            for i in 0...3 {
                tableView.cellForRow(at: IndexPath(row: i, section:1))?.accessoryType = .none
                tableView.cellForRow(at: IndexPath(row: i, section:1))?.textLabel?.textColor = .lightGray
            }
        }
        switch indexPath {
        case IndexPath(row: 0, section:1):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            resume.workType = .any
        case IndexPath(row: 1, section:1):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            resume.workType = .fullTime
        case IndexPath(row: 2, section:1):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            resume.workType = .partTime
        case IndexPath(row: 3, section:1):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            resume.workType = .remote
        case IndexPath(row: 1, section:0):
            let categoryVC: JobCategoriesTableViewController = UIStoryboard(.Job).instantiateViewController()
            categoryVC.selectedCategory.asObservable()
                .subscribe(onNext: {
                    self.resume.category = $0
                    self.categoryName.text = $0.name
                })
                .disposed(by: disposeBag)
            show(categoryVC, sender: self)
        default: break
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        textViews.forEach({ $0.resignFirstResponder() })
        textFields.forEach({ $0.resignFirstResponder() })
    }

 
}

fileprivate extension CreateResumeTableViewController {
    
    func subscribeRxSubjects() {
        resumeNameObs = Observable.just(resume.title ?? "")
        resumeNameObs.asObservable()
            .bind(to: resumeName.rx.text)
            .disposed(by: disposeBag)
        
        salaryObs = Observable.just(resume.salary ?? 0)
        salaryObs.asObservable()
            .map({String($0)})
            .bind(to: salary.rx.text)
            .disposed(by: disposeBag)
        
        aboutObs = Observable.just(resume.about ?? "")
        aboutObs.asObservable()
            .bind(to: resumeDescription.rx.text)
            .disposed(by: disposeBag)
    }
    
    func subscribeRxUI() {
        salary.rx.text
            .orEmpty
            .map({ (salaryText) in
                return Int(salaryText) ?? 0
            })
            .bind(onNext: { self.resume.salary = $0})
            .disposed(by: disposeBag)
        
        
        resumeName.rx.text
            .orEmpty
            .bind(onNext: { self.resume.title = $0 })
            .disposed(by: disposeBag)
        
        resumeDescription.rx.text
            .orEmpty
            .bind(onNext: { self.resume.about = $0 })
            .disposed(by: disposeBag)
        
        saveButton?.rx.tap.subscribe(onNext: {
            self.view.endEditing(true)
            do {
                try self.resume.validate()
                self.present(self.activityVC, animated: true, completion: nil)
                self.resume.onRequestFailure = self.requestFailureHandler
                self.resume.update(with: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    })
                })
            } catch {
                let error = error as! ValueValidationError
                ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            }
        })
            .disposed(by: disposeBag)
        
        createButton.rx.tap.subscribe(onNext: {
            self.view.endEditing(true)
            do {
                try self.resume.validate()
                self.present(self.activityVC, animated: true, completion: nil)
                self.resume.onRequestFailure = self.requestFailureHandler
                self.resume.create(with: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                })
            } catch {
                let error = error as! ValueValidationError
                ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            }
            
        })
            .disposed(by: disposeBag)
        deleteButton.rx.tap.subscribe(onNext: {
            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Ви дійсно бажаєте видалити резюме?", comment: ""), okHandler: { (action) in
                self.present(self.activityVC, animated: true, completion: nil)
                self.resume.onRequestFailure = self.requestFailureHandler
                self.resume.delete(with: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            if let myIndex = self.navigationController?.viewControllers.index(of: self), let toVC = self.navigationController?.viewControllers[myIndex - 2] {
                                self.navigationController?.popToViewController(toVC, animated: true)
                            } else {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                    })
                })
            })
        })
            .disposed(by: disposeBag)
    }
    
}

enum Action {
    case create, update
}

