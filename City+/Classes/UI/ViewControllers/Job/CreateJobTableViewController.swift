//
//  CreateJobTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/2/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class CreateJobTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    
    @IBOutlet weak var vacancyName: UITextField!
    @IBOutlet weak var categoryName: UITextField!
    
    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var companyAddress: UITextField!
    @IBOutlet weak var salary: UITextField!
    
    @IBOutlet weak var contactPersonName: UITextField!
    @IBOutlet weak var contactPersonEmail: UITextField!
    @IBOutlet weak var contactPersonPhone: UITextField!
    
    @IBOutlet weak var requirements: UITextView!
    @IBOutlet weak var vacancyDescription: UITextView!
    
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var saveButton: UIBarButtonItem?
    
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet var textViews: [UITextView]!
    //MARK: - Lifecycle
    
    var vacancy = Vacancy()
    var currentAction: Action = .create
    
    var vacancyNameObs: Observable<String>!
    
    var companyNameObs: Observable<String>!
    var companyAddressObs: Observable<String>!
    var salaryObs: Observable<Int>!
    
    var contactNameObs: Observable<String>!
    var contactEmailObs: Observable<String>!
    var contactPhoneObs: Observable<String>!
    
    var requirementsObs: Observable<String>!
    var aboutObs: Observable<String>!
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if currentAction == .create {
            navigationItem.rightBarButtonItem = nil
            navigationItem.rightBarButtonItems = []
        }
        
        categoryName.text = vacancy.category?.name
        
        if vacancy.contactPerson.name == nil {
            let person = ContactPerson()
            person.name = AppDelegate.shared.sessionManager.storedUser?.name
            person.phone = AppDelegate.shared.sessionManager.storedUser?.phone
            vacancy.contactPerson = person
        }
        
        subscribeRxSubjects()
        subscribeRxUI()
        
    }

    //MARK: - UITableViewDataSource/UITableViewDelegate

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat.leastNonzeroMagnitude : super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section != 6 ? CGFloat.leastNonzeroMagnitude : super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath {
        case IndexPath(row: 0, section: 6): return currentAction == .update ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case IndexPath(row: 1, section: 6): return currentAction == .create ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        default: return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath {
        case IndexPath(row: 0, section:1):
            cell.accessoryType = vacancy.workType == .any ? .checkmark : .none
            cell.textLabel?.textColor = vacancy.workType == .any ? .darkGray : .lightGray
        case IndexPath(row: 1, section:1):
            cell.accessoryType = vacancy.workType == .fullTime ? .checkmark : .none
            cell.textLabel?.textColor = vacancy.workType == .fullTime ? .darkGray : .lightGray
        case IndexPath(row: 2, section:1):
            cell.accessoryType = vacancy.workType == .partTime ? .checkmark : .none
            cell.textLabel?.textColor = vacancy.workType == .partTime ? .darkGray : .lightGray
        case IndexPath(row: 3, section:1):
            cell.accessoryType = vacancy.workType == .remote ? .checkmark : .none
            cell.textLabel?.textColor = vacancy.workType == .remote ? .darkGray : .lightGray
        default: break
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        func clearSelection() {
            for i in 0...3 {
                tableView.cellForRow(at: IndexPath(row: i, section:1))?.accessoryType = .none
                tableView.cellForRow(at: IndexPath(row: i, section:1))?.textLabel?.textColor = .lightGray
            }
        }
        switch indexPath {
        case IndexPath(row: 0, section:1):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            vacancy.workType = .any
        case IndexPath(row: 1, section:1):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            vacancy.workType = .fullTime
        case IndexPath(row: 2, section:1):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            vacancy.workType = .partTime
        case IndexPath(row: 3, section:1):
            clearSelection()
            tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .darkGray
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            vacancy.workType = .remote
        case IndexPath(row: 1, section:0):
            let categoryVC: JobCategoriesTableViewController = UIStoryboard(.Job).instantiateViewController()
            categoryVC.selectedCategory.asObservable()
            .subscribe(onNext: {
                self.vacancy.category = $0
                self.categoryName.text = $0.name
            })
            .disposed(by: disposeBag)
            show(categoryVC, sender: self)
        default: break
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        textViews.forEach({ $0.resignFirstResponder() })
        textFields.forEach({ $0.resignFirstResponder() })
    }
    
    override func menuWillClose() {
        super.menuWillClose()
        
    }

}

fileprivate extension CreateJobTableViewController {
    
    func subscribeRxSubjects() {
        vacancyNameObs = Observable.just(vacancy.title ?? "")
        vacancyNameObs.asObservable()
            .bind(to: vacancyName.rx.text)
            .disposed(by: disposeBag)
        
        companyNameObs = Observable.just(vacancy.companyName ?? "")
        companyNameObs.asObservable()
            .bind(to: companyName.rx.text)
            .disposed(by: disposeBag)
        
        companyAddressObs = Observable.just(vacancy.address ?? "")
        companyAddressObs.asObservable()
            .bind(to: companyAddress.rx.text)
            .disposed(by: disposeBag)
        
        salaryObs = Observable.just(vacancy.salary ?? 0)
        salaryObs.asObservable()
            .map({String($0)})
            .bind(to: salary.rx.text)
            .disposed(by: disposeBag)
        
        contactNameObs = Observable.just(vacancy.contactPerson.name ?? "")
        contactNameObs.asObservable()
            .bind(to: contactPersonName.rx.text)
            .disposed(by: disposeBag)
        
        contactEmailObs = Observable.just(vacancy.contactPerson.email ?? "")
        contactEmailObs.asObservable()
            .bind(to: contactPersonEmail.rx.text)
            .disposed(by: disposeBag)
        
        contactPhoneObs = Observable.just(vacancy.contactPerson.phone ?? "")
        contactPhoneObs.asObservable()
            .bind(to: contactPersonPhone.rx.text)
            .disposed(by: disposeBag)
        
        requirementsObs = Observable.just(vacancy.requirements ?? "")
        requirementsObs.asObservable()
            .bind(to: requirements.rx.text)
            .disposed(by: disposeBag)
        
        aboutObs = Observable.just(vacancy.about ?? "")
        aboutObs.asObservable()
            .bind(to: vacancyDescription.rx.text)
            .disposed(by: disposeBag)
    }
    
    func subscribeRxUI() {
        companyName.rx.text
        .orEmpty
        .bind(onNext: { self.vacancy.companyName = $0 })
        .disposed(by: disposeBag)
        
        companyAddress.rx.text
        .orEmpty
        .bind(onNext: { self.vacancy.address = $0})
        .disposed(by: disposeBag)
        
        salary.rx.text
        .orEmpty
        .map({ (salaryText) in
            return Int(salaryText) ?? 0
        })
        .bind(onNext: { self.vacancy.salary = $0})
        .disposed(by: disposeBag)
        
        
        vacancyName.rx.text
        .orEmpty
        .bind(onNext: { self.vacancy.title = $0 })
        .disposed(by: disposeBag)
        
        contactPersonName.rx.text
        .orEmpty
        .bind(onNext: { self.vacancy.contactPerson.name = $0})
        .disposed(by: disposeBag)
        
        contactPersonEmail.rx.text
        .orEmpty
        .bind(onNext: { self.vacancy.contactPerson.email = $0})
        .disposed(by: disposeBag)
        
        contactPersonPhone.rx.text
        .orEmpty
        .bind(onNext: { self.vacancy.contactPerson.phone = $0})
        .disposed(by: disposeBag)
        
        requirements.rx.text
        .orEmpty
        .bind(onNext: { self.vacancy.requirements = $0 })
        .disposed(by: disposeBag)
        
        vacancyDescription.rx.text
        .orEmpty
        .bind(onNext: { self.vacancy.about = $0 })
        .disposed(by: disposeBag)
        
        saveButton?.rx.tap.subscribe(onNext: {
            self.view.endEditing(true)
            do {
                try self.vacancy.validate()
                self.present(self.activityVC, animated: true, completion: nil)
                self.vacancy.onRequestFailure = self.requestFailureHandler
                self.vacancy.update(with: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    })
                })
            } catch {
                let error = error as! ValueValidationError
                ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            }
        })
        .disposed(by: disposeBag)
        
        createButton.rx.tap.subscribe(onNext: {
            self.view.endEditing(true)
            do {
                try self.vacancy.validate()
                self.present(self.activityVC, animated: true, completion: nil)
                self.vacancy.onRequestFailure = self.requestFailureHandler
                self.vacancy.create(with: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                })
            } catch {
                let error = error as! ValueValidationError
                ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            }
            
        })
        .disposed(by: disposeBag)
        deleteButton.rx.tap.subscribe(onNext: {
            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Ви дійсно бажаєте видалити вакансію?", comment: ""), okHandler: { (action) in
                self.present(self.activityVC, animated: true, completion: nil)
                self.vacancy.onRequestFailure = self.requestFailureHandler
                self.vacancy.delete(with: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            if let myIndex = self.navigationController?.viewControllers.index(of: self), let toVC = self.navigationController?.viewControllers[myIndex - 2] {
                                self.navigationController?.popToViewController(toVC, animated: true)
                            } else {
                                self.navigationController?.popToRootViewController(animated: true)
                            }                        
                        }
                    })
                })
            })
        })
            .disposed(by: disposeBag)
    }
    
}
