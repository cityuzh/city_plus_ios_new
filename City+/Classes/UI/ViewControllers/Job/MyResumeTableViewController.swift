//
//  MyResumeTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 5/4/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MyResumeTableViewController: BaseTableViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    //MARK: - Lifecycle
    
    lazy var itemList: LevelPaginatedListModel<Resume> = {
        let list = LevelPaginatedListModel<Resume>.init(path: Resume.Constants.resumeMyListPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    var dataSourse: Variable<[Resume]> = Variable([])
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = nil
        tableView.dataSource = nil
        placeholderView.removeFromSuperview()
        placeholderView = TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode, customImage: UIImage(named: "img_empty state_no cv"), customTitle: NSLocalizedString("Резюме відсутні", comment: ""), customSubTitle: NSLocalizedString("Створіть своє резюме зараз", comment: ""))
        
        tableView.rx.modelSelected(Resume.self)
            .subscribe(onNext: { (resume) in
                let detailVC: ResumeDetailTableViewController = UIStoryboard(.Job).instantiateViewController()
                detailVC.resume = resume
                self.show(detailVC, sender: self)
            }, onError: nil, onCompleted: nil, onDisposed: nil)
            .disposed(by: disposeBag)
        
        dataSourse.asObservable()
            .subscribe({ (dataSourse) in
                self.tableView.backgroundView = dataSourse.element?.count == 0 ? self.placeholderView : nil
            })
            .disposed(by: disposeBag)
        dataSourse.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: "ResumeTableViewCell", cellType: ResumeTableViewCell.self)) { row, resume, cell in
                cell.setUp(with: resume)
                if (self.itemList.count - row) <= (self.itemList.requestRowCount / 4) && self.itemList.state == .canLoadMore {
                    self.itemList.loadMore { (success) -> Void in
                        if success {
                            self.dataSourse.value.append(contentsOf: self.itemList.newObjects ?? [])
                        }
                    }
                }
            }
            .disposed(by: disposeBag)
        
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                if !(self.searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    self.itemList.queryParams["title"] = self.searchBar.text! as AnyObject
                } else {
                    self.itemList.queryParams.removeValue(forKey: "title")
                }
                self.refreshContent(nil)
            }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchBar.endEditing(true)
                if !(self.searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    self.itemList.queryParams["title"] = self.searchBar.text! as AnyObject
                } else {
                    self.itemList.queryParams.removeValue(forKey: "title")
                }
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchBar.endEditing(true)
                self.searchBar.text?.removeAll()
                self.itemList.queryParams.removeValue(forKey: "title")
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshContent(nil)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        self.itemList.objects.removeAll()
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
                self.dataSourse.value = self.itemList.objects
            }
        }
    }
    
}
