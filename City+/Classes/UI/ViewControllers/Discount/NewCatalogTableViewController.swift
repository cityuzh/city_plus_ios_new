//
//  NewCatalogTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 12/26/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import MobileCoreServices

final class NewCatalogTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var catalogName: UITextField!
    @IBOutlet weak var catalogCategory: UITextField!
    @IBOutlet weak var catalogPlace: UITextField!
    
    @IBOutlet weak var catalogPhoto: UIImageView!
    
    @IBOutlet weak var changePhotoButton: UIButton! {
       didSet {
           changePhotoButton.layer.cornerRadius = changePhotoButton.bounds.height / 2
       }
    }
    @IBOutlet weak var createCatalog: UIBarButtonItem!
    @IBOutlet weak var removePhotoButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    //MARK: - Properties
    
    var actionType: Action = .create
    
    var newCatalog = DiscountCatalog()
    var updatedImage: UIImage? {
        didSet {
            catalogPhoto.image = updatedImage
        }
    }

    var selectedImage: UIImage? {
        didSet {
            catalogPhoto.image = selectedImage
            
            let avatar = Avatar()
            avatar.image = selectedImage
            newCatalog.image = avatar
        }
    }
    
    var categories: [PlaceCategory] = []
    lazy var categoriesPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    
    lazy var categoriesAccessory: UIView = {
        let accessory = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        accessory.backgroundColor = UIColor.groupTableViewBackground
        
        let cancelButton = UIButton(type: .roundedRect)
        cancelButton.setTitle(NSLocalizedString("Відмінити", comment: ""), for: .normal)
        cancelButton.setTitleColor(UIColor.darkGray, for: .normal)
        cancelButton.contentHorizontalAlignment = .left
        cancelButton.addTarget(self, action: #selector(cancelTapped(_:)), for: .touchUpInside)
        
        let saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle(NSLocalizedString("Вибрати", comment: ""), for: .normal)
        saveButton.setTitleColor(UIColor.darkGray, for: .normal)
        saveButton.contentHorizontalAlignment = .right
        saveButton.addTarget(self, action: #selector(setTapped(_:)), for: .touchUpInside)
        
        accessory.addSubview(saveButton)
        accessory.addSubview(cancelButton)
        
        let views = ["save": saveButton, "cancel": cancelButton]
        let metrics = ["inset": 15]
        var constraints = [NSLayoutConstraint]()
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-inset-[cancel]-[save]-inset-|", options: [], metrics: metrics, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[cancel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[save]-|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        return accessory
    }()
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCategories { (success) in
            if success, self.catalogCategory.isFirstResponder {
                self.categoriesPicker.reloadAllComponents()
            }
        }
        fillUI(with: newCatalog)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if actionType == .update {
            fillUI(with: newCatalog)
            navigationItem.title = NSLocalizedString("Редагувати каталог", comment: "")
            deleteButton.isHidden = false
        }
        deleteButton.isHidden = actionType == .create
        setupGradientedNavigationBar()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath {
        case IndexPath(row: 0, section: 1):
            var leftSpace = UIScreen.main.bounds.height - 64
            if UIDevice().userInterfaceIdiom == .phone, UIScreen.main.nativeBounds.height >= 1792 {
                leftSpace -= 58
            }
            for i in 0...3 {
                leftSpace -= super.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            return max(50, leftSpace)
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}

extension NewCatalogTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        newCatalog.category = categories[row]
        catalogCategory.text = categories[row].name
    }
        
}

extension NewCatalogTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case catalogPlace:
            let selectPlaceVC: SelectPlaceTableViewController = UIStoryboard(.Events).instantiateViewController()
            selectPlaceVC.canCreateNewPlace = false
            selectPlaceVC.selectionHandler = { (place, isNew) in
                self.catalogPlace.text = place.name
                self.newCatalog.placeID = place.identifier
            }
            show(selectPlaceVC, sender: self)
            catalogName.resignFirstResponder()
            catalogCategory.resignFirstResponder()
            return false
        case catalogCategory:
            textField.inputAccessoryView = categoriesAccessory
            textField.inputView = categoriesPicker
            textField.reloadInputViews()
            return true
        default: return true
        }
    }
    
}

extension NewCatalogTableViewController: PickingImage {
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    func pickingDidCancel() {
        
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        if actionType == .update {
            updatedImage = image?.image
        } else {
            selectedImage = image?.image
        }
    }
    
}


fileprivate extension NewCatalogTableViewController {
    
    @IBAction func removePhotoAction(_ sender: UIButton) {
        updatedImage = nil
        catalogPhoto.image = #imageLiteral(resourceName: "create_ivent_placeholde")
        newCatalog.image = nil
    }
    
    @IBAction func chnagePhoto(_ sender: UIButton) {
        startPicking(isCameraOnly: false, isAllowEditing: false, deniedAccess: nil, onCancelAction: nil)
    }
    
    @IBAction func deleteCatalog(_ sender: UIButton) {
        newCatalog.onNoInternetConnection = noInternetConnectionHandler
        newCatalog.onRequestFailure = requestFailureHandler
        ShowAlert.showInfoAlertWithCancel(in: self, message: NSLocalizedString("Ви дійсно бажаєте видалити цей каталог? Всі акції також будуть видалені!", comment: "")) { (action) in
            self.present(self.activityVC, animated: true, completion: nil)
            self.newCatalog.delete { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                })
            }
        }
    }
    
    @IBAction func create(_ sender: UIButton) {
        newCatalog.name = catalogName.text
        do {
            try newCatalog.validate()
            present(activityVC, animated: true, completion: nil)
            newCatalog.onNoInternetConnection = noInternetConnectionHandler
            newCatalog.onRequestFailure = requestFailureHandler
            if actionType == .create {
                newCatalog.create { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            ShowAlert.showAlertForOneAction(controller: self, firstAction: {
                                self.navigationController?.popViewController(animated: true)
                            }, firstActionTitle: "OK", title: "City+", message: NSLocalizedString("Каталог створений!", comment: ""))
                        }
                    })
                }
            } else {
                newCatalog.update(with: updatedImage) { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                }
            }
        } catch {
            ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
        }
    }
    
    @objc func cancelTapped(_ sender: UIButton) {
        catalogCategory.resignFirstResponder()
    }
    
    @objc func setTapped(_ sender: UIButton) {
        if catalogCategory.isFirstResponder {
            if (catalogCategory.text ?? "").isEmpty {
                newCatalog.category = categories[categoriesPicker.selectedRow(inComponent: 0)]
                catalogCategory.text = newCatalog.category?.name
            }
            catalogCategory.resignFirstResponder()
        }        
    }
    
    func loadCategories(_ completion: @escaping NetworkModelCompletion) {
        DiscountCatalog.getParentCategories { (categories) in
            self.categories = categories
            DispatchQueue.main.async {
                completion(categories.count > 0)
            }
        }
    }
    
    func fillUI(with model: DiscountCatalog) {
        catalogName.text = model.name
        catalogCategory.text = model.category?.name
//        selectedImage = model.image?.image
        catalogPhoto.setImage(with: model.image, placeholder: #imageLiteral(resourceName: "create_ivent_placeholde"))
        catalogPlace.text = model.place?.name
    }
    
}
