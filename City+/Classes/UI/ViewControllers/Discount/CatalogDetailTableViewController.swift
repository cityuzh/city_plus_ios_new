//
//  CatalogDetailTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 1/16/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

final class CatalogDetailTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    //MARK: - Properties
    
    var catalog: DiscountCatalog! {
        didSet {
            itemList.path = String(format: Discount.Constants.discountListForCatalofPathFormat, catalog.identifier ?? 0)
            navigationItem.title = catalog.name
        }
    }
    
    var selectedCategory: PlaceCategory! {
        didSet {
            if selectedCategory.identifier == 0 {
                itemList.queryParams.removeValue(forKey: "category")
            } else {
                itemList.queryParams["category"] = selectedCategory.identifier! as AnyObject
            }
            refreshContent(nil)
        }
    }

    
    var isMy: Bool {
        return catalog.isMy
    }
    
    lazy var itemList: PaginatedListModel<Discount> = {
        let list = PaginatedListModel<Discount>.init(path: String(format: Discount.Constants.discountListForCatalofPathFormat, catalog.identifier ?? 0))
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placeholderView.removeFromSuperview()
        placeholderView = TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode, customImage: #imageLiteral(resourceName: "img_empty state_no promotions"), customTitle: NSLocalizedString("Акції відсутні", comment: ""), customSubTitle: nil)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        placeholderMode = .loading
        itemList.load { (success) in
            sender?.endRefreshing()
            self.placeholderMode = .noDataItems
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DiscountTableViewCell.classForCoder()), for: indexPath) as! DiscountTableViewCell
        cell.setUp(with: itemList[indexPath.row])
        cell.discountImageView.setImage(with: itemList[indexPath.row].image, placeholder:#imageLiteral(resourceName: "no-image"))
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? NewDiscountTableViewController {
            destination.catalog = catalog
            destination.creationHandler = { (newDiscount) in
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: [IndexPath(row: self.itemList.objects.count, section: 0)], with: .automatic)
                self.itemList.objects.append(newDiscount)
                self.tableView.endUpdates()
                self.catalog.discountCount += 1
            }
        }
        if let destination = segue.destination as? DiscountDetailTableViewController, let cell = sender as? UITableViewCell, let index = tableView.indexPath(for: cell)?.row {
            let discount = itemList[index]
            discount.place = catalog.place
            discount.catalog = catalog
            destination.discount = discount
            destination.deleteHandler = { (discountForDelete) in
                self.tableView.beginUpdates()
                if let index = self.itemList.objects.firstIndex(where: { $0.identifier == discountForDelete.identifier }) {
                    self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    self.itemList.objects.remove(at: index)
                }
                self.tableView.endUpdates()
                self.catalog.discountCount -= 1
            }
        }
        if let destination = segue.destination as? NewCatalogTableViewController {
            destination.newCatalog = catalog
            destination.actionType = .update
        }
    }

}
