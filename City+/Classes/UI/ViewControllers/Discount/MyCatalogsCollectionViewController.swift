//
//  MyCatalogsCollectionViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 12/26/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class MyCatalogsCollectionViewController: UICollectionViewController {
    
    //MARK: - Properties
    
    var placeholderMode: TableViewPlaceholderView.PlaceholderMode = .loading {
        didSet {
            placeholderView.currentType = placeholderMode
            placeholderView.setNeedsDisplay()
        }
    }
    
    lazy var placeholderView: TableViewPlaceholderView = {
        return TableViewPlaceholderView(frame: collectionView.bounds, with: placeholderMode)
    }()
    
    lazy var itemList: PaginatedListModel<DiscountCatalog> = {
        let list = PaginatedListModel<DiscountCatalog>.init(path: isMy ? DiscountCatalog.Constants.myCatalogsPath : DiscountCatalog.Constants.catalogsRoute)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = { (request) in
            self.placeholderMode = .noInternetConnection
        }
        return list
    }()
    
    var isMy: Bool {
        return category == nil
    }
    var category: PlaceCategory? {
        didSet {
            if let id = category?.identifier {
                itemList.queryParams["category"] = id as AnyObject
                navigationItem.title = category?.name
                navigationItem.rightBarButtonItems = nil
            } else {
                itemList.queryParams.removeValue(forKey: "category")
            }
        }
    }
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let collectionFlowLayot = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionView.collectionViewLayout = collectionFlowLayot
        placeholderView.removeFromSuperview()
        placeholderView = TableViewPlaceholderView(frame: collectionView.bounds, with: placeholderMode, customImage: #imageLiteral(resourceName: "img_empty state_no promotions"), customTitle: NSLocalizedString("Акції відсутні", comment: ""), customSubTitle: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        placeholderMode = .loading
        itemList.load { (success) in
            self.placeholderMode = .noDataItems
            self.collectionView.reloadData()
        }
        setupGradientedNavigationBar()
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExpandedCatalodCollectionViewCell.cellIdentifier, for: indexPath) as! ExpandedCatalodCollectionViewCell
        cell.setUp(with: itemList[indexPath.item])
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailCatalog: CatalogDetailContainerViewController = UIStoryboard(.Discount).instantiateViewController()
        detailCatalog.catalog = itemList[indexPath.row]
        self.show(detailCatalog, sender: self)
    }

}

extension MyCatalogsCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let clearWidth = collectionView.bounds.width - 30
        let width = clearWidth
        let height = width * 0.7
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 15, bottom: 10, right: 15)
    }

    
}
