//
//  CatalogDetailContainerViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 12.12.2019.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import Nuke
import UIKit
import MapKit

final class CatalogDetailContainerViewController: BaseViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var openMapButton: UIButton!
    @IBOutlet weak var addBarButtonItem: UIBarButtonItem!    
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!

    // MARK: - Properties
    
    var catalog: DiscountCatalog! {
        didSet {
            if isViewLoaded {
                updateUI(with: catalog)
            }
        }
    }
    
    var categories: [PlaceCategory] = []
    var listTableViewController: CatalogDetailTableViewController?
    var selectedCategory: PlaceCategory! {
        didSet {
            listTableViewController?.selectedCategory = selectedCategory
        }
    }

    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI(with: catalog)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupClearNavigationBar()
    }
    
    override func addChild(_ childController: UIViewController) {
        super.addChild(childController)
        if let listVC = childController as? CatalogDetailTableViewController {
            listVC.catalog = catalog
            listTableViewController = listVC
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? NewDiscountTableViewController {
            destination.catalog = catalog
            destination.creationHandler = { (newDiscount) in
                self.listTableViewController?.tableView.beginUpdates()
                self.listTableViewController?.tableView.insertRows(at: [IndexPath(row: self.listTableViewController?.itemList.objects.count ?? 0, section: 0)], with: .automatic)
                self.listTableViewController?.itemList.objects.append(newDiscount)
                self.listTableViewController?.tableView.endUpdates()
            }
        }        
        if let destination = segue.destination as? NewCatalogTableViewController {
            destination.newCatalog = catalog
            destination.actionType = .update
        }
    }
    
    
}

fileprivate extension CatalogDetailContainerViewController {
    
    func updateUI(with catalog: DiscountCatalog) {
        editButton.isHidden = !catalog.isMy
        navigationItem.rightBarButtonItems = [addBarButtonItem]
        let place = Place()
        place.identifier = catalog.placeID
        place.get { (success) in
            self.catalog.place = place
            
            self.placeName.text = catalog.place?.name
            let option = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "create_ivent_placeholde"), transition: .fadeIn(duration: 0.3), failureImage: #imageLiteral(resourceName: "create_ivent_placeholde"), failureImageTransition: .fadeIn(duration: 0.3), contentModes: nil)
            if let url = URL(string: catalog.image?.urlPath ?? "") {
                Nuke.loadImage(with: url, options: option, into: self.backgroundImage, progress: nil, completion: nil)
            }
            self.addGradient()
            
            let allCategory = PlaceCategory()
            allCategory.identifier = 0
            allCategory.name = NSLocalizedString("Всі", comment: "")
            
            self.selectedCategory = allCategory
            self.fillHeader(with: catalog, categories: [allCategory])
            DiscountCatalog.getSubCategories(for: catalog.category?.identifier ?? 0) { (categories) in
                DispatchQueue.main.async {
                    self.fillHeader(with: catalog, categories: categories)
                }
            }
        }
    }
    
    func addGradient() {
        backgroundImage.addGradient(top: .clear, bottom: UIColor.black.withAlphaComponent(0.5))
    }
    
   @IBAction func openMap(_ sender: UIButton) {
        openMapForPlace(for: catalog.place)
    }
    
    func fillHeader(with catalog: DiscountCatalog, categories: [PlaceCategory]) {
        var discountCount = "\(catalog.discountCount)"
        if catalog.discountCount == 1 {
            discountCount += NSLocalizedString(" акція", comment: "")
        } else {
            discountCount += NSLocalizedString(" акцій", comment: "")
        }
        
        self.categories.append(contentsOf: categories)
        
        self.categoryCollectionView.reloadData()
    }
    
    func openMapForPlace(for place: Place?) {
        guard let coordinates = place?.coordinates else { return }
        let regionDistance:CLLocationDistance = 10000
        let regionSpan = MKCoordinateRegion.init(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = place?.name
        mapItem.openInMaps(launchOptions: options)
    }
 
}

extension CatalogDetailContainerViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SubcategoryCollectionViewCell.classForCoder()), for: indexPath) as! SubcategoryCollectionViewCell
        cell.setUp(with: categories[indexPath.row])
        cell.setSelected(selectedCategory.identifier == categories[indexPath.row].identifier)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategory = categories[indexPath.row]
        collectionView.reloadData()
    }
    
}

extension CatalogDetailContainerViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = 100.0
        return CGSize(width: (categories[indexPath.item].name?.width(withConstrainedHeight: 40, font: UIFont.systemFont(ofSize: 14)) ?? width) + 20, height: collectionView.bounds.height)
    }
    
}
