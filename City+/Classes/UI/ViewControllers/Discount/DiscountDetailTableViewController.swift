//
//  DiscountDetailTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 1/19/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit
import MapKit

final class DiscountDetailTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var shadowContainer: UIView!
    @IBOutlet weak var discountImageView: UIImageView!
    
    @IBOutlet fileprivate weak var discountTitle: UILabel!
    @IBOutlet fileprivate weak var discountInfo: UILabel!
    @IBOutlet fileprivate weak var oldPrice: UILabel!
    @IBOutlet fileprivate weak var newPrice: UILabel!
    @IBOutlet fileprivate weak var lineView: UIView!
    @IBOutlet fileprivate weak var percentage: UILabel!
    @IBOutlet fileprivate weak var percentageContainer: UIView!
    @IBOutlet fileprivate weak var discountDate: UILabel!
    
    @IBOutlet fileprivate weak var placeName: UILabel!
    @IBOutlet fileprivate weak var placeRate: StarsRatingView!
    @IBOutlet fileprivate weak var placeMapView: GMSMapView!        
    
    //MARK: - Properties
    
    var discount: Discount!
    var deleteHandler: ((_ discountForDelete: Discount) -> ())?
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var freeSpace = 64
        if UIDevice().userInterfaceIdiom == .phone, UIScreen.main.nativeBounds.height >= 1792 {
            freeSpace += 24
        }
        tableView.contentInset = UIEdgeInsets(top: CGFloat(-freeSpace), left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: CGFloat(-freeSpace), left: 0, bottom: 0, right: 0)
        fillUI(with: discount)
        discountImageView.addGradient(top: .clear, bottom: UIColor.black.withAlphaComponent(0.5))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupClearNavigationBar()
        discount.getDetails { (success) in
            DispatchQueue.main.async {
                self.fillUI(with: self.discount)
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1,2:
            return UITableView.automaticDimension
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 5:
            openMapForPlace(for: discount.place)
        default: break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let desctination = segue.destination as? NewDiscountTableViewController {
            desctination.actionType = .update
            desctination.newDiscount = discount
            desctination.catalog = discount.catalog
        }
    }

}

fileprivate extension DiscountDetailTableViewController {
    
    @IBAction func deleteDiscount(_ sender: UIBarButtonItem) {
        ShowAlert.showInfoAlertWithCancel(in: self, message: NSLocalizedString("Ви дійсно бажаєте видалити цю акцію?", comment: "")) { (action) in
            self.present(self.activityVC, animated: true, completion: nil)
            self.discount.onRequestFailure = self.requestFailureHandler
            self.discount.onNoInternetConnection = self.noInternetConnectionHandler
            self.discount.delete { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.deleteHandler?(self.discount)
                        self.navigationController?.popViewController(animated: true)
                    }
                })
            }
        }
    }
    
    func fillUI(with model: Discount) {
        
        if !model.isMy {
            navigationItem.rightBarButtonItems = nil
        }
        
        discountImageView.setImage(with: model.image, placeholder: #imageLiteral(resourceName: "create_ivent_placeholde"))
        discountTitle.text = model.name
        discountInfo.text = model.discountInfo
        
        percentage.text = " -\(model.persentDiscount)% "
        oldPrice.text = "\(model.oldPrice) ₴"
        newPrice.text = "\(model.newPrice) ₴"
        
        percentage.isHidden = model.persentDiscount == 0
        percentageContainer.isHidden = model.persentDiscount == 0
        oldPrice.isHidden = model.oldPrice == 0
        newPrice.isHidden = model.newPrice == 0
        lineView.isHidden = model.oldPrice == 0
        
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "dd MMM"
        
        discountDate.text = "\(formatter.string(from: model.startDate ?? Date())) - \(formatter.string(from: model.endDate ?? Date()))"
        
        fillMap()
    }
    
    func fillMap() {
        if let place = discount.place, let coordinates = place.coordinates  {
            let newCamera = GMSCameraPosition.camera(withTarget: coordinates, zoom: 14)
            placeMapView.camera = newCamera
            
            let establishminMarker = EstablishmentMarker(with:place, coordinates: coordinates)
            establishminMarker.map = placeMapView
            
            placeName.text = place.name
            placeRate.rating = NSNumber(value: place.rating)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func openMapForPlace(for place: Place?) {
        guard let coordinates = place?.coordinates else { return }
        let regionDistance:CLLocationDistance = 10000
        let regionSpan = MKCoordinateRegion.init(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = place?.name
        mapItem.openInMaps(launchOptions: options)
    }
    
}
