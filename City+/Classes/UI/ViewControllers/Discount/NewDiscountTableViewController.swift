//
//  NewDiscountTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 1/19/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit
import MobileCoreServices

final class NewDiscountTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var category: UITextField!
    @IBOutlet weak var info: UITextField!
    @IBOutlet weak var persentage: UITextField!
    @IBOutlet weak var oldPrice: UITextField!
    @IBOutlet weak var newPrice: UITextField!
    @IBOutlet weak var startDate: UITextField!
    @IBOutlet weak var endDate: UITextField!
    
    @IBOutlet weak var discountPhoto: UIImageView!
    
    @IBOutlet weak var changePhotoButton: UIButton!{
        didSet {
            changePhotoButton.layer.borderColor = UIColor.appTintColor.cgColor
            changePhotoButton.layer.borderWidth = 2
        }
    }
    @IBOutlet weak var createButton: UIBarButtonItem!
    @IBOutlet weak var deletePhotoButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!

    //MARK: - Properties
    
    var creationHandler: ((_ newDiscount: Discount)->())?
    
    var actionType: Action = .create
    
    var catalog: DiscountCatalog!
    var newDiscount = Discount()
    var updatedImage: UIImage? {
        didSet {
            discountPhoto.image = updatedImage
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    var selectedImage: UIImage? {
        didSet {
            discountPhoto.image = selectedImage
            
            if actionType == .create {
                let avatar = Avatar()
                avatar.image = selectedImage
                newDiscount.image = avatar
            }
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    var categories: [PlaceCategory] = []
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.minimumDate = Date()
        picker.date = Date()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        return picker
    }()
    lazy var categoriesPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    lazy var categoriesAccessory: UIView = {
        let accessory = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        accessory.backgroundColor = UIColor.groupTableViewBackground
        
        let cancelButton = UIButton(type: .roundedRect)
        cancelButton.setTitle(NSLocalizedString("Відмінити", comment: ""), for: .normal)
        cancelButton.setTitleColor(UIColor.darkGray, for: .normal)
        cancelButton.contentHorizontalAlignment = .left
        cancelButton.addTarget(self, action: #selector(cancelTapped(_:)), for: .touchUpInside)
        
        let saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle(NSLocalizedString("Вибрати", comment: ""), for: .normal)
        saveButton.setTitleColor(UIColor.darkGray, for: .normal)
        saveButton.contentHorizontalAlignment = .right
        saveButton.addTarget(self, action: #selector(setTapped(_:)), for: .touchUpInside)
        
        accessory.addSubview(saveButton)
        accessory.addSubview(cancelButton)
        
        let views = ["save": saveButton, "cancel": cancelButton]
        let metrics = ["inset": 15]
        var constraints = [NSLayoutConstraint]()
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-inset-[cancel]-[save]-inset-|", options: [], metrics: metrics, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[cancel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[save]-|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        return accessory
    }()
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        DiscountCatalog.getSubCategories(for: catalog.category?.identifier ?? 0) { (categories) in
            var categoriesWithAdd = categories
            let lastCategory = PlaceCategory()
            lastCategory.name = NSLocalizedString("Додати нову", comment: "")
            categoriesWithAdd.append(lastCategory)
            self.categories = categoriesWithAdd
            DispatchQueue.main.async {
                if self.category.isFirstResponder {
                    self.categoriesPicker.reloadAllComponents()
                }
            }
        }
        if actionType == .update {
            navigationItem.title = NSLocalizedString("Редагувати акцію", comment: "")
            fillUI(with: newDiscount)
        } else {
            navigationItem.title = NSLocalizedString("Створити акцію", comment: "")
        }
        deleteButton.isHidden = actionType == .create
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
}

extension NewDiscountTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case category:
            textField.inputAccessoryView = categoriesAccessory
            textField.inputView = categoriesPicker
            textField.reloadInputViews()
            return true
        case startDate, endDate:
            textField.inputAccessoryView = categoriesAccessory
            textField.inputView = datePicker
            textField.reloadInputViews()
            return true
        default: break
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case persentage:
            if let int = ((textField.text ?? "") + string).toNumber, int.intValue <= 100, int.intValue > 0 {
                newDiscount.persentDiscount = int.intValue
                return true
            } else {
                return false
            }
        case oldPrice:
            if let int = ((textField.text ?? "") + string).toNumber, int.intValue >= 0 {
                newDiscount.oldPrice = int.intValue
                return true
            } else {
                return false
            }
        case newPrice:
            if let int = ((textField.text ?? "") + string).toNumber, int.intValue >= 0 {
                newDiscount.newPrice = int.intValue
                return true
            } else {
                return false
            }
        default: break
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case persentage:
            guard let int = (textField.text ?? "").toNumber else { return true }
            if newDiscount.oldPrice > 0 && newDiscount.newPrice == 0 {
                let newPrice = newDiscount.oldPrice - (newDiscount.oldPrice * int.intValue / 100)
                self.newPrice.text = "\(newPrice)"
                newDiscount.newPrice = newPrice
            } else if newDiscount.newPrice > 0 && newDiscount.oldPrice == 0 {
                let oldPrice = newDiscount.newPrice * 100 / int.intValue
                newPrice.text = "\(oldPrice)"
                newDiscount.oldPrice = oldPrice
            }
        case oldPrice:
            guard let int = (textField.text ?? "").toNumber else { return true }
            if newDiscount.persentDiscount > 0 {
                let newPrice = int.intValue - (newDiscount.oldPrice * newDiscount.persentDiscount / 100)
                self.newPrice.text = "\(newPrice)"
                newDiscount.newPrice = newPrice
            } else if newDiscount.newPrice > 0 && newDiscount.persentDiscount == 0 {
                let persentage = newDiscount.newPrice * 100 / int.intValue
                self.persentage.text = "\(persentage)"
                newDiscount.persentDiscount = persentage
            }
        default: break
        }
        return true
    }
    
}

extension NewDiscountTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard row != categories.count - 1 else { return }
        newDiscount.category = categories[row]
        category.text = categories[row].name
    }
    
}

extension NewDiscountTableViewController: PickingImage {
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    func pickingDidCancel() {
        
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        if actionType == .update {
            updatedImage = image?.image
        } else {
            selectedImage = image?.image
        }
    }
    
}


fileprivate extension NewDiscountTableViewController {
    
    @IBAction func removePhotoAction(_ sender: UIButton) {
        updatedImage = nil
        discountPhoto.image = #imageLiteral(resourceName: "create_ivent_placeholde")
        newDiscount.image = nil
    }
    
    @IBAction func deleteDiscount(_ sender: UIButton) {
        newDiscount.onNoInternetConnection = noInternetConnectionHandler
        newDiscount.onRequestFailure = requestFailureHandler
        ShowAlert.showInfoAlertWithCancel(in: self, message: NSLocalizedString("Ви дійсно бажаєте видалити цю акцію?", comment: "")) { (action) in
            self.present(self.activityVC, animated: true, completion: nil)
            self.newDiscount.delete { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                })
            }
        }
    }
    
    @IBAction func chnagePhoto(_ sender: UIButton) {
        startPicking(isCameraOnly: false, isAllowEditing: false, deniedAccess: nil, onCancelAction: nil)
    }
    
    @IBAction func create(_ sender: UIButton) {
        func createDiscount() {
            present(activityVC, animated: true, completion: nil)
            newDiscount.onRequestFailure = requestFailureHandler
            newDiscount.onNoInternetConnection = noInternetConnectionHandler
            newDiscount.create { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.creationHandler?(self.newDiscount)
                        self.navigationController?.popViewController(animated: true)
                    }
                })
            }
        }
        
        func updateDiscount() {
            present(activityVC, animated: true, completion: nil)
            newDiscount.onRequestFailure = requestFailureHandler
            newDiscount.onNoInternetConnection = noInternetConnectionHandler
            newDiscount.update(with: updatedImage) { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.navigationController?.popViewController(animated: true)
                    }
                })
            }
        }
        newDiscount.name = name.text
        newDiscount.discountInfo = info.text
        newDiscount.catalogId = catalog.identifier
        
        if let _ = newDiscount.category?.identifier {
            do {
                try newDiscount.validate()
                DispatchQueue.main.async {
                    if self.actionType == .create {
                        createDiscount()
                    } else {
                        updateDiscount()
                    }
                }
            } catch {
                ShowAlert.showInfoAlert(in: self, message: (error as! ValueValidationError).localizedDescription)
            }
        } else {
            DiscountCatalog.createSubCategories(for: catalog.category?.identifier ?? 0, withName: newDiscount.category?.name ?? "") { (newCategory) in
                if let category = newCategory {
                    self.newDiscount.category = category
                    DispatchQueue.main.async {
                        if self.actionType == .create {
                            createDiscount()
                        } else {
                            updateDiscount()
                        }
                    }
                } else {
                    ShowAlert.showInfoAlert(in: self, message: "Спробуйте ще раз!")
                }
            }
        }
    }
    
    @objc func cancelTapped(_ sender: UIButton) {
        if category.isFirstResponder {
            newDiscount.category = nil
            category.text = nil
            category.resignFirstResponder()
        } else if startDate.isFirstResponder {
            startDate.text = nil
            newDiscount.startDate = nil
            startDate.resignFirstResponder()
        } else {
            endDate.text = nil
            newDiscount.endDate = nil
            endDate.resignFirstResponder()
        }
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd / MM / yy"
        var components = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute, .second], from: sender.date)
        if startDate.isFirstResponder {
            components.hour = 0
            components.minute = 1
            components.second = 0
            if let newDate = Calendar.current.date(from: components) {
                newDiscount.startDate = newDate
                startDate.text = formatter.string(from: newDate)
            }
        } else {
            components.hour = 23
            components.minute = 59
            components.second = 0
            if let newDate = Calendar.current.date(from: components) {
                newDiscount.endDate = newDate
                endDate.text = formatter.string(from: newDate)
            }
        }
    }
    
    @objc func setTapped(_ sender: UIButton) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd / MM / yy"
        if category.isFirstResponder {
            if categoriesPicker.selectedRow(inComponent: 0) == categories.count - 1 {
                createCategory()
            }
            if (category.text ?? "").isEmpty {
                newDiscount.category = categories[categoriesPicker.selectedRow(inComponent: 0)]
                category.text = newDiscount.category?.name
            }
            category.resignFirstResponder()
        } else if startDate.isFirstResponder {
            startDate.resignFirstResponder()
            var components = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute, .second], from: datePicker.date)
            if (startDate.text ?? "").isEmpty {
                components.hour = 0
                components.minute = 1
                components.second = 0
                if let newDate = Calendar.current.date(from: components) {
                    newDiscount.startDate = newDate
                    startDate.text = formatter.string(from: newDate)
                }
            }
        } else {
            if (endDate.text ?? "").isEmpty {
                var components = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute, .second], from: datePicker.date)
                components.hour = 23
                components.minute = 59
                components.second = 0
                if let newDate = Calendar.current.date(from: components) {
                    newDiscount.endDate = newDate
                    endDate.text = formatter.string(from: newDate)
                }
            }
            endDate.resignFirstResponder()
        }
    }
    
    func loadCategories(_ completion: @escaping NetworkModelCompletion) {
        DiscountCatalog.getParentCategories { (categories) in
            self.categories = categories
            DispatchQueue.main.async {
                completion(categories.count > 0)
            }
        }
    }
    
    func createCategory() {
        let createPlaceAlertVC = UIAlertController(title: NSLocalizedString("Введіть назву категорії", comment: "") , message: nil, preferredStyle: .alert)
        createPlaceAlertVC.addTextField { (textField) in
            textField.placeholder = NSLocalizedString("Введіть назву категорії!", comment: "")
        }
        createPlaceAlertVC.addAction(UIAlertAction(title: NSLocalizedString("Відмінити", comment: ""), style: .default, handler: nil))
        createPlaceAlertVC.addAction(UIAlertAction(title: NSLocalizedString("Створити", comment: ""), style: .default, handler: { (action) in
            if let name = createPlaceAlertVC.textFields?.first?.text {
                self.category.text = name.capitalized
                let created = PlaceCategory()
                created.name = name
                self.newDiscount.category = created
                self.category.resignFirstResponder()
            } else {
                createPlaceAlertVC.dismiss(animated: true, completion: {
                    ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Введіть будь ласка коректну назву категорії!", comment: ""), okHandler: { (action) in
                        self.createCategory()
                    })
                })
            }
        }))
        present(createPlaceAlertVC, animated: true, completion: nil)
    }
    
    func fillUI(with model: Discount) {
        name.text = model.name
        category.text = model.category?.name
        selectedImage = model.image?.image
        discountPhoto.setImage(with: model.image, placeholder: #imageLiteral(resourceName: "create_ivent_placeholde"))
        oldPrice.text = "\(model.oldPrice)"
        newPrice.text = "\(model.newPrice)"
        persentage.text = "\(model.persentDiscount)"
        info.text = model.discountInfo
        let formatter = DateFormatter()
        formatter.dateFormat = "dd / MM / yy"
        startDate.text = formatter.string(from: model.startDate ?? Date())
        endDate.text = formatter.string(from: model.endDate ?? Date())
    }
    
}
