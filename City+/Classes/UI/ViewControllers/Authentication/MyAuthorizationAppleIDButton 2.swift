//
//  MyAuthorizationAppleIDButton.swift
//  CityPlus
//
//  Created by Macbook Air 13 on 15.09.2020.
//  Copyright © 2020 Ivan Grab. All rights reserved.
//

import UIKit
import AuthenticationServices

@available(iOS 13.0, *)

@IBDesignable
class MyAuthorizationAppleIDButton: UIButton {
    
    private  var authorizationButton: ASAuthorizationAppleIDButton = {
        let button =  ASAuthorizationAppleIDButton(authorizationButtonType: .continue, authorizationButtonStyle: .black)
        button.addTarget(self, action: #selector(authorizationAppleIDButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    @IBInspectable var buttonCornerRadius: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // MARK: - Lifecycle
    
    override public init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder )
        
        addSubview(authorizationButton)
        authorizationButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            authorizationButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 0.0),
            authorizationButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0.0),
            authorizationButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0.0),
            authorizationButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0.0)
        ])
    }
    
   override public func draw(_ rect: CGRect) {
        super.draw(rect)
        (authorizationButton as? UIControl)?.cornerRadius = buttonCornerRadius
    }
    
    @objc func authorizationAppleIDButtonTapped(_ sender: Any) {
        // Forward the touch up inside event to MyAuthorizationAppleIDButton
        sendActions(for: .touchUpInside)
    }
    
}

@available(iOS 13.0, *)
extension MyAuthorizationAppleIDButton{
    @available(iOS 13.0, *)
    public enum ButtonType: Int{
        
        case signIn
        case `continue`
        
        @available(iOS 13.2, *)
        case signUP
        
    }
    @available(iOS 13.0, *)
       public enum Style: Int{
           
           case whine
           case whiteOutline
           case black
    }
}
