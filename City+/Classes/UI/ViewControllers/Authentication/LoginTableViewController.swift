//
//  LoginTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 2/13/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreTelephony
import FBSDKLoginKit
import AuthenticationServices


enum LoginType {
    case phone
    case email
}

final class LoginTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var codePhoneContainer: UIView!
    
    @IBOutlet weak var loginWithPhone: UIButton!
    @IBOutlet weak var loginWithEmail: UIButton!
    
    @IBOutlet weak var code: CustomisableTextField!
    @IBOutlet weak var phone: CustomisableTextField!
    @IBOutlet weak var email: CustomisableTextField!
    @IBOutlet weak var password: CustomisableTextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var fbCustomLoginButton: UIButton!
    
    //MARK: - Properties
    
    var authorizationCode: Data?
    var identityToken: Data?

    
    var loginType: LoginType = .phone {
        didSet {
            userFormModel.loginType = loginType
            email.isHidden = loginType == .phone
            loginWithPhone.isHidden = loginType == .phone
            loginWithEmail.isHidden = loginType == .email
        }
    }
    var userFormModel = LoginUserFormModel()
    
    var countries = [Country]() {
        didSet {
            countryPicker.reloadAllComponents()
        }
    }
    lazy var countryPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    
    lazy var countryAccessory: UIView = {
        let accessory = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        accessory.backgroundColor = UIColor.groupTableViewBackground
        
        let cancelButton = UIButton(type: .roundedRect)
        cancelButton.setTitle(NSLocalizedString("Відмінити", comment: ""), for: .normal)
        cancelButton.setTitleColor(UIColor.darkGray, for: .normal)
        cancelButton.contentHorizontalAlignment = .left
        cancelButton.addTarget(self, action: #selector(cancelTapped(_:)), for: .touchUpInside)
        
        let saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle(NSLocalizedString("Вибрати", comment: ""), for: .normal)
        saveButton.setTitleColor(UIColor.darkGray, for: .normal)
        saveButton.contentHorizontalAlignment = .right
        saveButton.addTarget(self, action: #selector(setTapped(_:)), for: .touchUpInside)
        
        accessory.addSubview(saveButton)
        accessory.addSubview(cancelButton)
        
        let views = ["save": saveButton, "cancel": cancelButton]
        let metrics = ["inset": 15]
        var constraints = [NSLayoutConstraint]()
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-inset-[cancel]-[save]-inset-|", options: [], metrics: metrics, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[cancel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[save]-|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        return accessory
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginType = .phone
        showMenuButton = false
        setupClearNavigationBar()
        setupTableView()
        
        password.rightViewActionHandler = { (textField, button) in
            textField.isSecureTextEntry = !textField.isSecureTextEntry
        }
        
        Country.getCountrie { (countries) in
            self.countries = countries
            let networkInfo = CTTelephonyNetworkInfo()
            
            if let carrier = networkInfo.subscriberCellularProvider {
                if let code = carrier.isoCountryCode {
                    if let country = self.countries.first(where: { $0.alpha2Code.uppercased() == code.uppercased() || $0.alpha3Code.uppercased() == code.uppercased() }), let index = self.countries.firstIndex(where: {$0.name == country.name }) {
                        self.code.text = "+" + country.callingCodes.first!
                        self.countryPicker.selectRow(index, inComponent: 0, animated: false)
                    }
                }
            }
        }        
    }
    
    override func setupHandlers() {
        super.setupHandlers()
        sessionManager.onRequestFailure = requestFailureHandler
    }
    
    //MARK: - UITableViewDelegate / UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var freeSpace = UIScreen.main.bounds.height - (navigationController?.navigationBar.bounds.height ?? 0)
        var bottomPadding: CGFloat = 0.0
        var topPadding: CGFloat = 0.0
        if #available(iOS 11.0, *) {
             let window = UIApplication.shared.keyWindow
             bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
             topPadding = window?.safeAreaInsets.top ?? 0.0
        } else {
            if UIDevice().userInterfaceIdiom == .phone, UIScreen.main.nativeBounds.height >= 1792 {
                freeSpace -= 58
            }
        }
        freeSpace -= bottomPadding + topPadding
        switch indexPath.row {
        case 0:
            for i in 1...8 {
                freeSpace -= super.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            return max(180, freeSpace / 2)
        case 6:
            var height: CGFloat = 0
            if #available(iOS 13.0, *) {
                height = super.tableView(tableView, heightForRowAt: IndexPath(row: 6, section: 0))
            }
            return height
        case 9:
            for i in 1...8 {
                freeSpace -= super.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            return max(20, freeSpace / 2)
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

    //MARK: - IBActions
    
    @IBAction func loginAction(_ sender: UIButton) {
        if loginType == .phone {
            userFormModel.phone = (code.text ?? "") + (phone.text ?? "")
        } else {
            userFormModel.email = (email.text ?? "")
        }
        userFormModel.password = password.text ?? ""
        
        do {
            try userFormModel.validate()
            present(activityVC, animated: true, completion: nil)
            sessionManager.login(with: userFormModel, completionHandler: { [weak self] (success) in
                self?.activityVC.dismiss(animated: true, completion: {
                    if success {
                        UIWindow.replaceStoryboard(name: .Main)
                    }
                })
            })
        } catch {
            let error = error as! LoginUserFormError
            ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
        }        
    }
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        let phoneConfirmationVC: PhoneConfirmationTableViewController = UIStoryboard(.Authentication).instantiateViewController()
        phoneConfirmationVC.confirmType = .forgotPassword
        show(phoneConfirmationVC, sender: self)
    }
    
    @IBAction func fbLoginAction(_ sender: UIButton) {
        LoginManager().logIn(permissions: ["email"], from: self) { (results, error) in
            LoginManager().logOut()
            guard let facebookResult = results, let token = facebookResult.token else { return }
            if !facebookResult.isCancelled {
                self.sessionManager.onRequestFailure = self.requestFailureHandler
                self.sessionManager.login(with: token.tokenString, completionHandler: { (success) in
                    if success {
                        UIWindow.replaceStoryboard(name: .Main)
                    }
                })
            } else {
                return
            }
        }
    }
    
    @IBAction func changeLoginAction(_ sender: UIButton) {
        loginType = sender == loginWithPhone ? .phone : .email
    }
        
}

@available(iOS 13.0, *)
extension LoginTableViewController:  ASAuthorizationControllerDelegate{
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("something bad happened")
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
          if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
        let identityToken = appleIDCredential.identityToken
        let tokeStr = String(data: identityToken!, encoding: .utf8)
        let authorizationCode = appleIDCredential.authorizationCode
        let codeStr = String(data: authorizationCode!, encoding: .utf8)
        print("identityToken\(String(describing: tokeStr)) and authorizationCode \(String(describing: codeStr))")

        self.sessionManager.loginApple(withToken: codeStr!, withIdToken:  tokeStr!, completionHandler: { (success) in
                if success {
                    UIWindow.replaceStoryboard(name: .Main)
                }
            })
        } else {
            return
        }
    }
    
    @IBAction func appleLoginAction(_ sender: UIButton) {
            let provider = ASAuthorizationAppleIDProvider()
          let request = provider.createRequest()
          request.requestedScopes = [.fullName, .email]
        
          let controller = ASAuthorizationController(authorizationRequests: [request])
          controller.delegate = self
          controller.presentationContextProvider = self
          controller.performRequests()
    }

}

@available(iOS 13.0, *)
extension LoginTableViewController: ASAuthorizationControllerPresentationContextProviding{
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
        
    }
}



//MARK: - UITextFieldDelegate

extension LoginTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == code {
            textField.inputAccessoryView = countryAccessory
            textField.inputView = countryPicker
        } else {
            textField.inputAccessoryView = nil
            textField.inputView = nil
        }
        textField.reloadInputViews()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = loginType == .phone ? (phone.text ?? "") + string : (email.text ?? "") + string
        let pass = password.text ?? ""
        loginButton.isEnabled = text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && pass.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        loginButton.alpha = text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && pass.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 ? 1 : 0.5
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == password) {
            loginAction(loginButton)
        }
        return true
    }
    
}

//MARK: - Private

private extension LoginTableViewController {
    
    func setupTableView() {
        tableView.contentInset = UIEdgeInsets.init(top: -44, left: 0, bottom: 0, right: 0)
    }
    
    @objc func cancelTapped(_ sender: UIButton) {
        code.resignFirstResponder()
    }
    
    @objc func setTapped(_ sender: UIButton) {
        code.resignFirstResponder()
        phone.becomeFirstResponder()
    }
}

extension LoginTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(countries[row].name)" + " (\(countries[row].nativeName)): +" + countries[row].callingCodes.first!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        code.text = "+" + countries[row].callingCodes.first!
    }
    
}
