//
//  PhoneConfirmationTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 2/15/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import CoreTelephony
import AuthenticationServices



class PhoneConfirmationTableViewController: BaseTableViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var codePhoneContainer: UIView!
    @IBOutlet weak var loginWithPhone: UIButton!
    @IBOutlet weak var loginWithEmail: UIButton!
    
    @IBOutlet weak var code: CustomisableTextField!
    @IBOutlet weak var phone: CustomisableTextField!
    @IBOutlet weak var email: CustomisableTextField!
    @IBOutlet weak var password: CustomisableTextField!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var goToLoginLabel: UILabel!
    @IBOutlet weak var goToLoginButton: UIButton!
    @IBOutlet weak var confirmPolicyButton: UIButton!
    @IBOutlet weak var confirmPolicyImage: UIImageView!
    @IBOutlet weak var confirmPolicyTextView: UITextView!
    
    @IBOutlet weak var fbCustomLoginButton: UIButton!
    @IBOutlet weak var fbbuttonTitle: UILabel!
    
    @IBOutlet weak var appleLigonButton: UIButton!
    
    //MARK: - Properties
    
    fileprivate var isAccepted = false {
        didSet {
            confirmButton.isEnabled = isAccepted
            confirmButton.alpha = isAccepted ? 1.0 : 0.5
            confirmPolicyImage.isHighlighted = isAccepted
        }
    }
    
    fileprivate var action: ConfirmAction = .sms {
        didSet {
            phone.isEnabled = action == .sms
            phone.alpha = action == .sms ? 1 : 0.5
            confirmButton.setTitle( action == .sms ? confirmType == ConfirmType.signUp ? NSLocalizedString("Зареєструватись", comment: "") : NSLocalizedString("Надіслати СМС", comment: "") : NSLocalizedString("Підтвердити", comment: "") , for: .normal)
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    var confirmType: ConfirmType = .signUp
    var registrationType: LoginType = .phone {
        didSet {
            registrationModel.loginType = registrationType
            email.isHidden = registrationType == .phone
            loginWithPhone.isHidden = registrationType == .phone
            loginWithEmail.isHidden = registrationType == .email
            
            let titleButton = registrationType == .email ? NSLocalizedString("Далі", comment: "") : confirmType == ConfirmType.signUp ? NSLocalizedString("Зареєструватись", comment: "") : NSLocalizedString("Надіслати СМС", comment: "")
            confirmButton.setTitle(titleButton, for: .normal)
        }
    }
    
    var registrationModel = RegisterUserFormModel()
    
    var countries = [Country]() {
        didSet {
            countryPicker.reloadAllComponents()
        }
    }
    lazy var countryPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    
    lazy var countryAccessory: UIView = {
        let accessory = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        accessory.backgroundColor = UIColor.groupTableViewBackground
        
        let cancelButton = UIButton(type: .roundedRect)
        cancelButton.setTitle(NSLocalizedString("Відмінити", comment: ""), for: .normal)
        cancelButton.setTitleColor(UIColor.darkGray, for: .normal)
        cancelButton.contentHorizontalAlignment = .left
        cancelButton.addTarget(self, action: #selector(cancelTapped(_:)), for: .touchUpInside)
        
        let saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle(NSLocalizedString("Вибрати", comment: ""), for: .normal)
        saveButton.setTitleColor(UIColor.darkGray, for: .normal)
        saveButton.contentHorizontalAlignment = .right
        saveButton.addTarget(self, action: #selector(setTapped(_:)), for: .touchUpInside)
        
        accessory.addSubview(saveButton)
        accessory.addSubview(cancelButton)
        
        let views = ["save": saveButton, "cancel": cancelButton]
        let metrics = ["inset": 15]
        var constraints = [NSLayoutConstraint]()
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-inset-[cancel]-[save]-inset-|", options: [], metrics: metrics, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[cancel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[save]-|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        return accessory
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registrationType = .phone
        setupClearNavigationBar()
        setupTableView()
        
        password.rightViewActionHandler = { (textField, button) in
            textField.isSecureTextEntry = !textField.isSecureTextEntry
        }
        
        let attributedText = NSMutableAttributedString(string: "Погоджуюся з умовами користування додатком. Детальніше.", attributes: [ .font : UIFont.systemFont(ofSize: 12), .foregroundColor : UIColor.newMintTextColor])
        let range = NSString(string: "Погоджуюся з умовами користування додатком. Детальніше.").range(of: "Детальніше.")
        attributedText.addAttributes([.link : URL(string:"http://cityplus.com.ua/ugoda-koristuvacha/")!, .underlineStyle : NSUnderlineStyle.single.rawValue], range: range)
        confirmPolicyTextView.attributedText = attributedText
        
        if confirmType == .changePhone || confirmType == .forgotPassword {
            confirmPolicyTextView.isHidden = true
            confirmPolicyImage.isHidden = true
            confirmPolicyButton.isHidden = true
            goToLoginLabel.isHidden = true
            goToLoginButton.isHidden = true
            fbCustomLoginButton.isHidden = true
            fbbuttonTitle.isHidden = true
            appleLigonButton.isHidden = true
        }
        
        Country.getCountrie { (countries) in
            self.countries = countries
            let networkInfo = CTTelephonyNetworkInfo()
            
            if let carrier = networkInfo.subscriberCellularProvider {
                if let code = carrier.isoCountryCode {
                    if let country = self.countries.first(where: { $0.alpha2Code.uppercased() == code.uppercased() || $0.alpha3Code.uppercased() == code.uppercased() }), let index = self.countries.firstIndex(where: {$0.name == country.name }) {
                        self.code.text = "+" + country.callingCodes.first!
                        self.countryPicker.selectRow(index, inComponent: 0, animated: false)
                    }
                }
            }
        }
    }
    
    override func setupHandlers() {
        super.setupHandlers()
        sessionManager.onRequestFailure = requestFailureHandler
    }
    
    //MARK: - UITableViewDelegate / UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var freeSpace = UIScreen.main.bounds.height - (navigationController?.navigationBar.bounds.height ?? 0)
        var bottomPadding: CGFloat = 0.0
        var topPadding: CGFloat = 0.0
        if #available(iOS 11.0, *) {
             let window = UIApplication.shared.keyWindow
             bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
             topPadding = window?.safeAreaInsets.top ?? 0.0
        } else {
            if UIDevice().userInterfaceIdiom == .phone, UIScreen.main.nativeBounds.height >= 1792 {
                freeSpace -= 58
            }
        }
        freeSpace -= bottomPadding + topPadding
        switch indexPath.row {
        case 0:
            for i in 1...6{
                freeSpace -= self.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            freeSpace -= self.tableView(tableView, heightForRowAt: IndexPath(row: 6, section: 0))
            return max(180, freeSpace / 2)
        case 3:
            return action == .sms ? 0 : super.tableView(tableView, heightForRowAt: IndexPath(row: 3, section: 0))
        case 6:
            var height: CGFloat = 0
            if #available(iOS 13.0, *) {
                height = super.tableView(tableView, heightForRowAt: IndexPath(row: 6, section: 0))
            }
            return height
        case 7:
            for i in 1...6{
                freeSpace -= self.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            return max(70, freeSpace / 2)
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func acceptAction(_ sender: UIButton) {
        isAccepted = !isAccepted
    }
    
    @IBAction func fbLoginAction(_ sender: UIButton) {
        LoginManager().logIn(permissions: ["email"], from: self) { (results, error) in
            LoginManager().logOut()
            guard let facebookResult = results, let token = facebookResult.token else { return }
            if !facebookResult.isCancelled {
                self.sessionManager.onRequestFailure = self.requestFailureHandler
                self.sessionManager.login(with: token.tokenString, completionHandler: { (success) in
                    if success {
                        UIWindow.replaceStoryboard(name: .Main)
                    }
                })
            } else {
                return
            }
        }
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        if action == .sms {
            do {
                if registrationType == .phone {
                    try PhoneValidator().validate((code.text ?? "") + (phone.text ?? ""))
                } else {
                    try EmailValidator().validate(email.text ?? "")
                }
                present(activityVC, animated: true, completion: nil)
                switch confirmType {
                case .fbSignUp:
                    sessionManager.sendSMS(to: (code.text ?? "") + (phone.text ?? ""), isFaceBookReg: true) { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                self.registrationModel.phone = (self.code.text ?? "") + (self.phone.text ?? "")
                                ShowAlert.showInfoAlert(in: self, message: String(format: NSLocalizedString("СМС Код надіслано на: %@", comment: ""), self.registrationModel.phone))
                                self.action = .code
                            }
                        })
                    }
                case .signUp:
                    if registrationType == .email {
                        self.activityVC.dismiss(animated: true, completion: {
                            self.registrationModel.email = self.email.text ?? ""
                            let setPasswordVC: ResetPasswordTableViewController = UIStoryboard(.Authentication).instantiateViewController()
                            setPasswordVC.registrationModel = self.registrationModel
                            setPasswordVC.passwordType = .signUp
                            self.show(setPasswordVC, sender: self)
                        })
                    } else {
                        sessionManager.sendSMS(to: (code.text ?? "") + (phone.text ?? ""), completionHandler: { (success) in
                            self.activityVC.dismiss(animated: true, completion: {
                                if success {
                                    self.registrationModel.phone = (self.code.text ?? "") + (self.phone.text ?? "")
                                    ShowAlert.showInfoAlert(in: self, message: String(format: NSLocalizedString("СМС Код надіслано на: %@", comment: ""), self.registrationModel.phone))
                                    self.action = .code
                                }
                            })
                        })
                    }
                case .forgotPassword:
                    if registrationType == .email {
                        sessionManager.sendForgotPasswordCode(forEmail: email.text ?? "") { (success) in
                            self.activityVC.dismiss(animated: true, completion: {
                                if success {
                                    self.registrationModel.email = (self.email.text ?? "")
                                    let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
                                        let setPasswordVC: ResetPasswordTableViewController = UIStoryboard(.Authentication).instantiateViewController()
                                        setPasswordVC.passwordType = .forgotPassword
                                        setPasswordVC.registrationModel = self.registrationModel
                                        self.show(setPasswordVC, sender: self)
                                    })
                                    ShowAlert.showAlertContrller(with: .alert, title: NSLocalizedString("City+", comment: ""), message: String(format: NSLocalizedString("Код надіслано на: %@", comment: ""), self.registrationModel.email), actions: [okAction], in: self)
                                }
                            })
                        }
                    } else {
                        sessionManager.sendForgotPasswordCode(for: (code.text ?? "") + (phone.text ?? ""), completionHandler: { (success) in
                            self.activityVC.dismiss(animated: true, completion: {
                                if success {
                                    self.registrationModel.phone = (self.code.text ?? "") + (self.phone.text ?? "")
                                    let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
                                        let setPasswordVC: ResetPasswordTableViewController = UIStoryboard(.Authentication).instantiateViewController()
                                        setPasswordVC.passwordType = .forgotPassword
                                        setPasswordVC.registrationModel = self.registrationModel
                                        self.show(setPasswordVC, sender: self)
                                    })
                                    ShowAlert.showAlertContrller(with: .alert, title: NSLocalizedString("City+", comment: ""), message: String(format: NSLocalizedString("СМС Код надіслано на: %@", comment: ""), self.registrationModel.phone), actions: [okAction], in: self)
                                }
                            })
                        })
                    }
                case .changePhone:
                    sessionManager.sendSMS(to: (code.text ?? "") + (phone.text ?? ""), completionHandler: { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                self.registrationModel.phone = (self.code.text ?? "") + (self.phone.text ?? "")
                                ShowAlert.showInfoAlert(in: self, message: String(format: NSLocalizedString("СМС Код надіслано на: %@", comment: ""), self.registrationModel.phone))
                                self.action = .code
                            }
                        })
                    })
                }
            } catch {
                let error = error as! ValueValidationError
                ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            }
        } else {
            if (password.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Будь ласка введіть код!!", comment: ""))
            } else {
                present(activityVC, animated: true, completion: nil)
                sessionManager.confirmSMS(for: registrationModel.phone, with: password.text!, completionHandler: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            switch self.confirmType {
                            case .fbSignUp:
                                self.present(self.activityVC, animated: false, completion: nil)
                                self.sessionManager.login(with: self.registrationModel.fbToken!, phone: self.registrationModel.phone, completionHandler: { (success) in
                                    self.activityVC.dismiss(animated: true, completion: {
                                        if success {
                                            UIWindow.replaceStoryboard(name: .Main)
                                        }
                                    })
                                })
                                break
                            case .signUp:
                                let setPasswordVC: ResetPasswordTableViewController = UIStoryboard(.Authentication).instantiateViewController()
                                setPasswordVC.registrationModel = self.registrationModel
                                self.show(setPasswordVC, sender: self)
                            case .forgotPassword: break
                            case .changePhone: break
                            }
                        }
                    })
                })
            }
        }
    }
    
    @IBAction func changeLoginAction(_ sender: UIButton) {
        registrationType = sender == loginWithPhone ? .phone : .email
    }

}

@available(iOS 13.0, *)
extension PhoneConfirmationTableViewController:  ASAuthorizationControllerDelegate{
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("something bad happened")
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let identityToken = appleIDCredential.identityToken
            let tokeStr = String(data: identityToken!, encoding: .utf8)
            let authorizationCode = appleIDCredential.authorizationCode
            let codeStr = String(data: authorizationCode!, encoding: .utf8)
            print("identityToken\(String(describing: tokeStr)) and authorizationCode \(String(describing: codeStr))")

            self.sessionManager.loginApple(withToken: codeStr!, withIdToken:  tokeStr!, completionHandler: { (success) in
                if success {
                    UIWindow.replaceStoryboard(name: .Main)
                }
            })
        } else {
            return
        }
    }
    
    @IBAction func didTapAppleButton(_ sender: UIButton) {
        let provider = ASAuthorizationAppleIDProvider()
        let request = provider.createRequest()
        request.requestedScopes = [.fullName, .email]
      
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.presentationContextProvider = self
        controller.performRequests()
    }
}

@available(iOS 13.0, *)
extension PhoneConfirmationTableViewController: ASAuthorizationControllerPresentationContextProviding{
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

extension PhoneConfirmationTableViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
    
}

//MARK: - UITextFieldDelegate

extension PhoneConfirmationTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == code {
            textField.inputAccessoryView = countryAccessory
            textField.inputView = countryPicker
        } else {
            textField.inputAccessoryView = nil
            textField.inputView = nil
        }
        textField.reloadInputViews()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if action == .sms {
            let text = registrationType == .phone ? (phone.text ?? "") + string : (email.text ?? "") + string
            confirmButton.isEnabled = text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && (isAccepted || confirmType == .forgotPassword || confirmType == .changePhone)
            confirmButton.alpha = text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && (isAccepted || confirmType == .forgotPassword || confirmType == .changePhone) ? 1 : 0.5
        } else {
            confirmButton.isEnabled = ((password.text ?? "") + string).trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && (isAccepted || confirmType == .forgotPassword || confirmType == .changePhone)
            confirmButton.alpha = ((password.text ?? "") + string).trimmingCharacters(in: .whitespacesAndNewlines).count > 0  && (isAccepted || confirmType == .forgotPassword || confirmType == .changePhone) ? 1 : 0.5
        }
        return true
    }
    
}

//MARK: - Public

extension PhoneConfirmationTableViewController {
    
    enum ConfirmType {
        case signUp
        case fbSignUp
        case forgotPassword
        case changePhone
    }
    
}

//MARK: - Private

private extension PhoneConfirmationTableViewController {
    
    enum ConfirmAction {
        case sms
        case code
    }
    
    func setupTableView() {
        tableView.contentInset = UIEdgeInsets.init(top: -44, left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets.init(top: -44, left: 0, bottom: 0, right: 0)
    }
    
    @objc func cancelTapped(_ sender: UIButton) {
        code.resignFirstResponder()
    }
    
    @objc func setTapped(_ sender: UIButton) {
        code.resignFirstResponder()
        phone.becomeFirstResponder()
    }
    
}

extension PhoneConfirmationTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(countries[row].name)" + " (\(countries[row].nativeName)): +" + countries[row].callingCodes.first!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        code.text = "+" + countries[row].callingCodes.first!
    }
    
}
