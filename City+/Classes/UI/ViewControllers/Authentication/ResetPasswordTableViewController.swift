//
//  ResetPasswordTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 2/15/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class ResetPasswordTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var code: UITextField!
    
    @IBOutlet weak var confirmButton: UIButton! {
        didSet {
            confirmButton.layer.cornerRadius = confirmButton.bounds.height / 2
            confirmButton.setTitle(passwordType == .signUp ? NSLocalizedString("Зареєструватись", comment: "") : NSLocalizedString("Змінити пароль", comment: ""), for: .normal)
        }
    }
    
    var passwordType: PhoneConfirmationTableViewController.ConfirmType = .signUp
    
    //MARK: - Properties
    
    var registrationModel: RegisterUserFormModel!
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupClearNavigationBar()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if passwordType == .signUp {
            newPassword.placeholder = NSLocalizedString("Пароль", comment: "")            
        }
    }
    
    override func setupHandlers() {
        super.setupHandlers()
        sessionManager.onRequestFailure = requestFailureHandler
    }
    
    //MARK: - UITableViewDelegate / UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var freeSpace = UIScreen.main.bounds.height - 20
        if UIDevice().userInterfaceIdiom == .phone, UIScreen.main.nativeBounds.height >= 1792 {
            freeSpace -= 58
        }
        
        switch indexPath.row { 
        case 0:
            for i in 1...3 {
                freeSpace -= self.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            return max(180, freeSpace / 2)
        case 3:
            return  passwordType == .signUp ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        case 4:
            for i in 0...3 {
                freeSpace -= self.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            return max(100, freeSpace / 2)
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func confirmAction(_ sender: UIButton) {
        registrationModel.password = newPassword.text ?? ""
        registrationModel.confirmPassword = confirmPassword.text ?? ""
        registrationModel.smsCode = code.text ?? ""
        
        do {
            try registrationModel.validate()
            if passwordType == .signUp {
                present(activityVC, animated: true, completion: nil)
                sessionManager.signUp(with: self.registrationModel, completionHandler: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                            profileVC.currentAction = .registration
                            self.show(profileVC, sender: self)
                        }
                    })
                })
            } else {
                present(activityVC, animated: true, completion: nil)
                sessionManager.setNewPassword(with: self.registrationModel, completionHandler: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    })
                })
            }
        } catch {
            let error = error as! RegisterUserFormError
            ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            return
        }
        
    }
    
}

//MARK: - UITextFieldDelegate

extension ResetPasswordTableViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        confirmButton.isEnabled = (newPassword.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && (confirmPassword.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        confirmButton.alpha = (newPassword.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && (confirmPassword.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).count > 0 ? 1 : 0.5
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == code && passwordType == .signUp) || (textField == confirmPassword && passwordType == .signUp) {
            confirmAction(confirmButton)
        }
        return true
    }
    
}

//MARK: - Public

@available(iOS 13.0, *)
extension ResetPasswordTableViewController {
    
}

//MARK: - Private

private extension ResetPasswordTableViewController {
        
    func setupTableView() {
        tableView.contentInset = UIEdgeInsets.init(top: -44, left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets.init(top: -44, left: 0, bottom: 0, right: 0)
    }
    
}
