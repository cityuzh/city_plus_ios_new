//
//  PlaceListTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class PlaceListTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - Properties
    
    var itemList: LevelPaginatedListModel<Place> {
        get {
            return PlaceDataSource.shared.itemList
        }
        set {
            PlaceDataSource.shared.itemList = newValue
        }
    }

    var disposeBag = DisposeBag()
    var searchText: String = "" {
        didSet {
            PlaceDataSource.shared.searchText = searchText
        }
    }
    
    var containerViewController: PlaceContainerViewController!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                if self.searchText != self.searchBar.text {
                    self.searchText = self.searchBar.text ?? ""
                    self.itemList.objects.removeAll()
                    self.refreshContent(nil)
                }
            }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchText = self.searchBar.text ?? ""
                self.itemList.objects.removeAll()
                self.refreshContent(nil)
                self.searchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchText = ""
                self.searchBar.text = nil
                self.itemList.objects.removeAll()
                if let location = LocationManager.shared.currentLocation {
                    self.itemList.queryParams["lat"] = location.coordinate.latitude as AnyObject
                    self.itemList.queryParams["lng"] = location.coordinate.longitude as AnyObject
                    self.itemList.queryParams["radius"] = Configuration.Constants.defaultRadius as AnyObject
                    self.refreshContent(nil)
                }
                self.searchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        refreshContent(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if searchBar.text != PlaceDataSource.shared.searchText {
            searchBar.text = PlaceDataSource.shared.searchText
            refreshContent(nil)
        }
        self.tableView.reloadData()
        setupGradientedNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        itemList.onNoInternetConnection = noInternetConnectionHandler
        placeholderMode = .loading
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noPlaceState
            }
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.defaultCellHeight
    } 

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PlaceTableViewCell.classForCoder()), for: indexPath) as! PlaceTableViewCell
        cell.setUp(with: itemList[indexPath.row])
        cell.favoriteHandler = { (sender) in
            sender.isEnabled = false
            sender.alpha = 0.5
            if self.itemList[indexPath.row].isFavorite {
                self.itemList[indexPath.row].removeFromFavorite(with: { (success) in
                    self.itemList[indexPath.row].isFavorite = false
                    sender.isEnabled = true
                    sender.alpha = 1
                    if success {
                        sender.isSelected = false
                    }
                })
            } else {
                self.itemList[indexPath.row].addToFavorite(with: { (success) in
                    self.itemList[indexPath.row].isFavorite = true
                    sender.isEnabled = true
                    sender.alpha = 1
                    if success {
                        sender.isSelected = true
                    }
                })
            }
        }
        cell.callHandler = { (sender) in
            if let phone = URL(string: "tel://" + (self.itemList[indexPath.row].phones.first != nil ? (self.itemList[indexPath.row].phones.first!.hasPrefix("38") ? "+" + self.itemList[indexPath.row].phones.first! : self.itemList[indexPath.row].phones.first)! : "").replacingOccurrences(of: " ", with: "")) {
                UIApplication.shared.open(phone, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
        cell.siteHandler = { (sender) in
            if let url = URL(string: self.itemList[indexPath.row].site ?? "") {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let place = itemList[indexPath.row]
        let detailVC: PlaceDetailContainerTableViewController = UIStoryboard(.Place).instantiateViewController()
        detailVC.place = place
        show(detailVC, sender: self)
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
    }
    
    @IBAction func filterAction(_ sender: UIBarButtonItem) {
        if let mainTabBar = tabBarController as? MainTabBarViewController {
            mainTabBar.showCategoryFiler()
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        if isViewLoaded {
            searchBar.resignFirstResponder()
        }
    }
    
    override func menuWillClose() {
        super.menuWillClose()
        
    }
    
}

extension PlaceListTableViewController: LevelRefreshableProtocol {

    typealias T = Place
    
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}

extension PlaceListTableViewController {
    
    struct Constants {
        static let defaultCellHeight: CGFloat = 102.0
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
