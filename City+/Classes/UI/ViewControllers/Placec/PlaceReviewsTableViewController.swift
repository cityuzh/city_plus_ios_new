//
//  PlaceReviewsTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/15/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class PlaceReviewsTableViewController: BaseTableViewController {

    var place: Place! {
        didSet {
            itemList.path = String(format: PlaceReview.Constants.raviewPathFormat, place.identifier ?? 0)
        }
    }
    fileprivate var disposeBag = DisposeBag()
    internal lazy var itemList: PaginatedListModel<PlaceReview> = {
        let list = PaginatedListModel<PlaceReview>.init(path: String(format: PlaceReview.Constants.raviewPathFormat, place.identifier ?? 0))
        list.onRequestFailure = requestFailureHandler
        list.dataKey = "results"
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    fileprivate var dataSourse: Variable<[PlaceReview]> = Variable([])
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRxComponents()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        itemList.load { (success) in
            self.placeholderMode = .noDataResponse
            self.dataSourse.value = self.itemList.objects
        }
    }
    
}

extension PlaceReviewsTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}


fileprivate extension PlaceReviewsTableViewController {
    
    func setupRxComponents() {
        tableView.delegate = nil
        tableView.dataSource = nil
        
        
        dataSourse.asObservable()
            .subscribe({ (dataSourse) in
                self.tableView.backgroundView = dataSourse.element?.count == 0 ? self.placeholderView : nil
            })
            .disposed(by: disposeBag)
        
        dataSourse.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: "PlaceReviewTableViewCell", cellType: PlaceReviewTableViewCell.self)) { row, review, cell in
                cell.setUp(with: review)
                cell.attachmentHandler = { (cell) in
                    if let index = self.tableView.indexPath(for: cell)?.row {
                        let attachment = self.itemList[index].attchment
                        let imagePreviewVC: ImagePreviewViewController = UIStoryboard(.Chats).instantiateViewController()
                        imagePreviewVC.attachment = attachment
                        self.show(imagePreviewVC, sender: self)
                    }
                }
            }
            .disposed(by: disposeBag)
        tableView.rx
            .willDisplayCell
            .subscribe(onNext: { cell, indexPath in
                self.requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
            })
            .disposed(by: disposeBag)
    }
    
}
