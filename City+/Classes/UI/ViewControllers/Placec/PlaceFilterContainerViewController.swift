//
//  PlaceFilterContainerViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/16/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class PlaceFilterContainerViewController: BaseViewController {

    var filter = [PlaceCategory]()
    
    var endSetupHandler: ((_ filter: [PlaceCategory])->())?
    var listCategoryVC: PlaceCategoryTableViewController!
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func addChild(_ childController: UIViewController) {
        super.addChild(childController)
        if let categoryList = childController as? PlaceCategoryTableViewController {
            categoryList.filter = filter
            listCategoryVC = categoryList
        }
    }
    
}

fileprivate extension PlaceFilterContainerViewController {
    
    @IBAction func setTapped(_ sender: UIButton) {
        filter = listCategoryVC.filter
        endSetupHandler?(filter)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetTapped(_ sender: UIButton) {
        filter = []
        if let child = children.first(where: { $0.isKind(of: PlaceCategoryTableViewController.classForCoder()) }) as? PlaceCategoryTableViewController {
            child.filter = filter
        }
        filter = listCategoryVC.filter
        endSetupHandler?(filter)
        dismiss(animated: true, completion: nil)
    }
    
}
