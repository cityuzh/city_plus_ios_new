//
//  RateViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/21/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import MobileCoreServices

final class RateViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var ratingView: StarsRatingView! {
        didSet {
            ratingView.starsDelegate = self
        }
    }
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var attachButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    //MARK: - Properties
    
    var place: Place!
    var placeReview = PlaceReview()
    var selectedAttachment: Avatar? {
        didSet {
            attachButton.isSelected = selectedAttachment != nil
        }
    }
    var isTextViewVisible = false
    var completion: ((_ success: Bool)->())?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placeName.text = place.name
    }


}

extension RateViewController: PickingImage {
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        let avatar = Avatar()
        if let image = thumbnail {
            avatar.image = image
            selectedAttachment = avatar
        }
    }
    
    func pickingDidCancel() {}
    
}

extension RateViewController: StarsViewDelegate {
    
    func changedRatingAtView(rating: NSNumber) {
        place.rating = rating.intValue
        confirmButton.alpha = rating.doubleValue > 0 ? 1.0 : 0.5
        confirmButton.isEnabled = rating.doubleValue > 0
        if rating.doubleValue > 0 && !isTextViewVisible {
            showTextView()
        } else if rating.doubleValue == 0 {
            hideTextView()
        }
    }
    
    func showTextView() {
        isTextViewVisible = true
        attachButton.isHidden = false
        changeTextViewHeight(to: 100)
    }
    
    func hideTextView() {
        isTextViewVisible = false
        attachButton.isHidden = true
        changeTextViewHeight(to: 0)
    }
    
    func changeTextViewHeight(to height: CGFloat) {
        textViewConstraint.constant = height
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
}

fileprivate extension RateViewController {
    
    @IBAction func rate(_ sender: UIButton) {
        if !textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            placeReview.text = textView.text
        }
        placeReview.attchment = selectedAttachment
        placeReview.rating = ratingView.rating.intValue
        present(activityVC, animated: true, completion: nil)
        placeReview.onRequestFailure = requestFailureHandler
        placeReview.onNoInternetConnection = noInternetConnectionHandler
        placeReview.post(for: place) { (success) in
            if success {
                self.activityVC.dismiss(animated: true, completion: {
                    self.cancel(self.cancelButton)
                })
            }
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: {
            self.completion?(true)
        })
    }
    
    @IBAction func attach(_ sender: UIButton) {
        if let _ = selectedAttachment {
            let changeActin = UIAlertAction(title: "Змінити", style: .default) { (action) in
                self.startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
                        ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                UIApplication.shared.open(settingsUrl, completionHandler: nil)
                            }
                        })
                }, onCancelAction: nil)
            }
            let deleteAction = UIAlertAction(title: "Видалити", style: .destructive) { (action) in
                self.selectedAttachment = nil
            }
            ShowAlert.showAlertContrller(with: .actionSheet, title: nil, message: nil, actions: [changeActin, deleteAction], in: self)
        } else {
            startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess:  {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    }
                })
            })
        }
    }
    
}
