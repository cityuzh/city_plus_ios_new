//
//  SelectPlaceTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/16/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class SelectPlaceTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var createContainer: UIView!
    
    //MARK: - Properties

    var selectionHandler: ((_ place: Place, _ isNew: Bool)->())?

    lazy var itemList: PaginatedListModel<Place> = {
        let list = PaginatedListModel<Place>.init(path: Place.Constants.placeRoute)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                itemList.queryParams["name"] = searchText as AnyObject
            } else {
                itemList.queryParams.removeValue(forKey: "name")
            }
        }
    }
    
    var canCreateNewPlace = true
    
    var disposeBag = DisposeBag()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshContent(nil)
        
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                self.searchText = self.searchBar.text ?? ""
                self.refreshContent(nil)
            }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchText = self.searchBar.text ?? ""
                self.refreshContent(nil)
                self.searchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchText = ""
                self.searchBar.text = nil
                self.searchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        itemList.load { (success) in
            sender?.endRefreshing()
            if self.isViewLoaded {
                self.tableView.tableFooterView = self.itemList.count == 0 && self.canCreateNewPlace ? self.createContainer : UIView()
            }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        placeholderMode = .noDataItems
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PlaceTableViewCell.classForCoder()), for: indexPath) as! PlaceTableViewCell
        cell.setUp(with: itemList[indexPath.row])
        cell.favoriteHandler = { (sender) in
            sender.isEnabled = false
            sender.alpha = 0.5
            if self.itemList[indexPath.row].isFavorite {
                self.itemList[indexPath.row].removeFromFavorite(with: { (success) in
                    self.itemList[indexPath.row].isFavorite = false
                    sender.isEnabled = true
                    sender.alpha = 1
                    if success {
                        sender.isSelected = false
                    }
                })
            } else {
                self.itemList[indexPath.row].addToFavorite(with: { (success) in
                    self.itemList[indexPath.row].isFavorite = true
                    sender.isEnabled = true
                    sender.alpha = 1
                    if success {
                        sender.isSelected = true
                    }
                })
            }
        }
        cell.callHandler = { (sender) in
            if let phone = URL(string: "tel://" + (self.itemList[indexPath.row].phones.first != nil ? (self.itemList[indexPath.row].phones.first!.hasPrefix("38") ? "+" + self.itemList[indexPath.row].phones.first! : self.itemList[indexPath.row].phones.first)! : "").replacingOccurrences(of: " ", with: "")) {
                UIApplication.shared.open(phone, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
        cell.siteHandler = { (sender) in
            if let url = URL(string: self.itemList[indexPath.row].site ?? "") {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectionHandler?(itemList[indexPath.row], false)
        navigationController?.popViewController(animated: true)
    }

    @IBAction func createPlace(_ sender: UIButton) {
        let createPlaceAlertVC = UIAlertController(title: NSLocalizedString("Введіть назву місця", comment: "") , message: nil, preferredStyle: .alert)
        createPlaceAlertVC.addTextField { (textField) in
            textField.placeholder = NSLocalizedString("Назва місця", comment: "")
            textField.text = self.searchBar.text
        }
        createPlaceAlertVC.addAction(UIAlertAction(title: NSLocalizedString("Відмінити", comment: ""), style: .default, handler: nil))
        createPlaceAlertVC.addAction(UIAlertAction(title: NSLocalizedString("Створити", comment: ""), style: .default, handler: { (action) in
            if let name = createPlaceAlertVC.textFields?.first?.text {
                let place = Place()
                place.name = name
                self.selectionHandler?(place, true)
                self.navigationController?.popViewController(animated: true)
            } else {
                createPlaceAlertVC.dismiss(animated: true, completion: {
                    ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Введіть будь ласка коректну назву місця!", comment: ""), okHandler: { (action) in
                        self.createPlace(self.createButton)
                    })
                })
            }
        }))
        present(createPlaceAlertVC, animated: true, completion: nil)
    }
    
}

extension SelectPlaceTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
