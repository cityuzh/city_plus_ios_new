//
//  PlaceInfoTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/21/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import MapKit

final class PlaceInfoTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var touristMapView: GMSMapView!
    @IBOutlet weak var touristPlacesAdress: UILabel!
    @IBOutlet weak var TouristPlacesRatingView: StarsRatingView!
    
    @IBOutlet weak var textInfoTouristPlaces: UILabel!
    @IBOutlet weak var touristPlacesName: UILabel!
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var workHours: UILabel!
    @IBOutlet weak var placeCategory: UILabel!
    
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var siteButton: UIButton!
  
    @IBOutlet weak var ratingView: StarsRatingView!
    @IBOutlet var tableViewToCharge: UITableView!
    
    //MARK: - Properties
    
    var place: Place!
    
    var isTouristPlace = false
    var touristPlacesCatalog: Place!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillUI(with: place)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reload()
    }

    func reload() {
        place.get { (success) in
            self.fillUI(with: self.place)
        }
    }
    
  
    
    
    //MARK: - UITableViewDelegate/UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath {
        case IndexPath(row: 1, section: 0):
            if  place.touristDescription != nil{
             return UITableView.automaticDimension
            } else{
                return 0
            }
        case IndexPath(row: 0, section: 1): return UITableView.automaticDimension
        case IndexPath(row: 1, section: 1): return UITableView.automaticDimension
        case IndexPath(row: 1, section: 2):
            return 0
      
        default: return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: self)
        if let destination = segue.destination as? RateViewController {
            destination.place = place
            destination.completion = { (finished) in 
                self.reload()
            }
        }
    }
    
}

fileprivate extension PlaceInfoTableViewController {
    
    @IBAction func call(_ sender: UIButton) {
        if let phone = URL(string: "tel://" + (phone.text ?? "").replacingOccurrences(of: " ", with: "")) {
            UIApplication.shared.open(phone, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }
    
    @IBAction func visitSite(_ sender: UIButton) {
        if let url = URL(string: place.site ?? "") {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }
    
    @IBAction func openMap(_ sender: UIButton) {
          openMapForPlace(for: place)
      }
    
//    @IBAction func favoriteAction(_ sender: UIButton) {
//        sender.isEnabled = false
//        sender.alpha = 0.5
//        if place.isFavorite {
//            place.removeFromFavorite(with: { (success) in
//                sender.isEnabled = true
//                sender.alpha = 1
//                if success {
//                    self.place.isFavorite = false
//                    self.favoriteImage.image = #imageLiteral(resourceName: "likeInActive")
//                }
//            })
//        } else {
//            place.addToFavorite(with: { (success) in
//                sender.isEnabled = true
//                sender.alpha = 1
//                if success {
//                    self.place.isFavorite = true
//                    self.favoriteImage.image = #imageLiteral(resourceName: "likeActive")
//                }
//            })
//        }
//    }
    
    @IBAction func rateAction(_ sender: UIButton) {
    
    }
    
    func fillUI(with place: Place) {
        
        textInfoTouristPlaces.text = place.touristDescription
        textInfoTouristPlaces.font = UIFont.monteserratRegularFont(ofSize: 17)
        touristPlacesName.text = place.name
        touristPlacesName.font = UIFont.monteserratMediumFont(ofSize: 17)
        address.text = place.address
        placeCategory.text = (place.category?.name ?? "Дані відсутні")
        phone.text = place.phones.first != nil ? place.phones.first!.hasPrefix("38") ? "+" + place.phones.first! : place.phones.first : "Дані відсутні"
        website.text = place.site ?? "Дані відсутні"
        callButton.isEnabled = place.phones.first != nil
        siteButton.isEnabled = place.site != nil
        
        //favoriteImage.image = place.isFavorite ? #imageLiteral(resourceName: "likeActive") : #imageLiteral(resourceName: "likeInActive")
        ratingView.rating = NSNumber(value: place.rating)
        
        workHours.text = place.fullFormattedSchedule
        
        fillMap()
    }
// MARK: - Map logic
    
    func fillMap() {
        
        if let place = place, let coordinates = place.coordinates  {
        let newCamera = GMSCameraPosition.camera(withTarget: coordinates, zoom: 18)
        touristMapView.camera = newCamera
        
        let establishminMarker = EstablishmentMarker(with:place, coordinates: coordinates)
        establishminMarker.map = touristMapView
        
        touristPlacesAdress.text = place.address
        TouristPlacesRatingView.rating = NSNumber(value: place.rating)
        }
    
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func openMapForPlace(for place: Place?) {
        
        guard let coordinates = place?.coordinates else { return }
        let regionDistance:CLLocationDistance = 10000
        let regionSpan = MKCoordinateRegion.init(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = place?.name
        mapItem.openInMaps(launchOptions: options)
    }    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
