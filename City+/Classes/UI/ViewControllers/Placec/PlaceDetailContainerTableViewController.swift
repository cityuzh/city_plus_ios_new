//
//  PlaceDetailContainerTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/21/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class PlaceDetailContainerTableViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bluredView: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var reviewButton: UIButton!
    
    @IBOutlet weak var selectionIndicatorView: UIView!
        
    @IBOutlet weak var scrollView: UIScrollView!
    
    fileprivate var placeInfoVC: PlaceInfoTableViewController?
    fileprivate var placePhotosVC: PhotosCollectionViewController?
    fileprivate var placeReviewVC: PlaceReviewsTableViewController?
    
    //MARK: - Properties
    
    var place: Place!
    var isTouristPlace = false

    
    fileprivate var isCanChangeCategory = false
    fileprivate var selectedButton: UIButton?
    var currentCategory: InfoCategory = .info
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundImage.setImage(with: place.photos.first, placeholder: #imageLiteral(resourceName: "no-image"), using: .white, placeholderSettings: nil, completion: nil)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeFont()
        setupClearNavigationBar()
        selectCurrentButton(for: currentCategory)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        placeInfoVC?.reload()
        preSetups()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destination = segue.destination as? PlaceInfoTableViewController {
            destination.place = place
            destination.isTouristPlace = isTouristPlace
            placeInfoVC = destination
        }
        if let destination = segue.destination as? PhotosCollectionViewController {
            destination.photos = place.photos
            placePhotosVC = destination
        }
        if let destination = segue.destination as? PlaceReviewsTableViewController {
            destination.place = place
            placeReviewVC = destination
        }
    }
    
 
    @IBAction func shareAction(_ sender: UIButton) {
           var items: [Any] = []
           items.append(place.name ?? "City+")
           items.append(place.site ?? "City+")
           items.append(place.address ?? "City+")
           if let image = place.photos.first?.image {
               items.append(image)
           }
           let activityVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
           present(activityVC, animated: true, completion: nil)
       }
    
    @IBAction func favoriteAction(_ sender: UIButton) {
           sender.isEnabled = false
           sender.alpha = 0.5
           
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "ic_favorite_navbar"), for: .normal)
            sender.isSelected = false
        }else{
            sender.setImage(#imageLiteral(resourceName: "ic_favorite_selected_navbar"), for: .selected)
            sender.isSelected = true
        }
        
        if place.isFavorite {
               place.removeFromFavorite(with: { (success) in
                   sender.isEnabled = true
                   sender.alpha = 1
                   if success {
                     self.place.isFavorite = false
                    self.favoriteButton.setImage(#imageLiteral(resourceName: "ic_favorite_navbar"), for: .normal)
                    sender.isEnabled = false
                   }
               })
           } else {
               place.addToFavorite(with: { (success) in
                   sender.isEnabled = true
                   sender.alpha = 1
                   if success {
                       self.place.isFavorite = true
                    self.favoriteButton.setImage(#imageLiteral(resourceName: "ic_favorite_selected_navbar"), for: .normal)
                        sender.isEnabled = true
                   }
               })
           }
       }
    
}

extension PlaceDetailContainerTableViewController {
    
    enum InfoCategory: Int {
        case info = 0
        case photo
        case revies
    }
}

fileprivate extension PlaceDetailContainerTableViewController {
    
    @IBAction func categoryButtonTapper(_ sender: UIButton) {
        if let category = InfoCategory(rawValue: sender.tag) {
            scroll(to: category)
        }
    }
    
    func preSetups() {
        let gradiend = CAGradientLayer()
        gradiend.frame = bluredView.bounds
        gradiend.colors = [UIColor.clear.cgColor, UIColor.black.withAlphaComponent(0.6).cgColor]
        guard bluredView.layer.sublayers?.first(where: {$0.isKind(of: CAGradientLayer.classForCoder() )}) == nil else {
            return
        }
        bluredView.backgroundColor = .clear
        bluredView.layer.addSublayer(gradiend)
    }
    
    func selectCurrentButton(for category: InfoCategory) {
        selectedButton?.titleLabel?.font = UIFont.monteserratRegularFont(ofSize: 17)
        selectedButton?.setTitleColor(.white, for: .normal)
        favoriteButton.isSelected = place.isFavorite

        switch category {
        case .info:
            selectedButton = infoButton
            UIView.animate(withDuration: 0.3) {
                self.selectionIndicatorView.transform = CGAffineTransform.identity
            }
            return
        case .photo:
            selectedButton = photoButton
        case .revies:
            selectedButton = reviewButton
            placeReviewVC?.viewWillAppear(true)
        }
        guard let selected = selectedButton else { return }
        updateButtonState(for: selected)
    }
    
    func updateButtonState(for button: UIButton) {
        UIView.animate(withDuration: 0.3) {
            let scale = CGAffineTransform(scaleX: button.bounds.width / self.selectionIndicatorView.bounds.width, y: 1)
            let translate = CGAffineTransform(translationX: button.center.x - self.selectionIndicatorView.center.x, y: 0)
            self.selectionIndicatorView.transform = scale.concatenating(translate)
        }
        button.titleLabel?.font = UIFont.monteserratBoldFont(ofSize: 17)
        button.setTitleColor(.white, for: .normal)
    }
    
    func scroll(to category: InfoCategory, canChange: Bool = true) {
        var offset = CGPoint.zero
        switch category {
        case .info: break
        case .photo:
            offset = CGPoint(x: scrollView.bounds.width, y: 0)
        case .revies:
            offset = CGPoint(x: 2 * scrollView.bounds.width, y: 0)
        }
        isCanChangeCategory = canChange
        scrollView.setContentOffset(offset, animated: true)
        selectCurrentButton(for: category)
        currentCategory = category
    }
    
    private func changeFont(){
        infoButton.titleLabel?.font = UIFont.monteserratRegularFont(ofSize: 17.0)
        photoButton.titleLabel?.font = UIFont.monteserratRegularFont(ofSize: 17.0)
        reviewButton.titleLabel?.font = UIFont.monteserratRegularFont(ofSize: 17.0)
    }
}
