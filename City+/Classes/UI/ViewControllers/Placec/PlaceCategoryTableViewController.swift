//
//  PlaceCategoryTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/16/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class PlaceCategoryTableViewController: BaseTableViewController {

    //MARK: - Properties

    lazy var itemList: PaginatedListModel<PlaceCategory> = {
        let list = PaginatedListModel<PlaceCategory>.init(path: PlaceCategory.Constants.listPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        list.requestRowCount = 50
        return list
    }()
    
    var filter: [PlaceCategory]! {
        didSet {
            if filter.count == 0 {
                selectedIndexPaths = []
                reloadCategories()
            }
        }
    }
    var selectedIndexPaths = [IndexPath]()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        reloadCategories()
    }
    
    func reloadCategories() {
        itemList.load { (success) in
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if filter.contains(itemList[indexPath.row]), !selectedIndexPaths.contains(indexPath) {
            selectedIndexPaths.append(indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        cell.textLabel?.text = itemList[indexPath.row].name
        cell.accessoryType = selectedIndexPaths.contains(indexPath) ? .checkmark : .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let index = selectedIndexPaths.index(of: indexPath), let filerIndex = filter.index(of: itemList[indexPath.row]) {
            selectedIndexPaths.remove(at: index)
            filter.remove(at: filerIndex)
        } else {
            filter.append(itemList[indexPath.row])
            selectedIndexPaths.append(indexPath)
        }
        tableView.reloadData()
    }
    
}
