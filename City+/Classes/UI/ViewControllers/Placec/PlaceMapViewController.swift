//
//  PlaceMapViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/3/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import GoogleMaps
import CoreLocation

final class PlaceMapViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var mapView: GMSMapView!

   // @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeWorkHours: UILabel!
    @IBOutlet weak var placeAddress: UILabel!
    
    @IBOutlet weak var ratingView: StarsRatingView!
    
    @IBOutlet weak var previewContainer: UIView!
    
  //  @IBOutlet weak var siteButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
  //  @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var bottomContainerConstraint: NSLayoutConstraint!
    
    var places = [Place]()
    var currentZoom: Float = 0
    
    var disposeBag = DisposeBag()
    var clasterManager: GMUClusterManager!
    
    var currentLocation: CLLocation? {
        didSet {
            if let location = currentLocation, oldValue == nil {
                setupMapCamera(in: location)
                self.currentZoom = Constants.defaulZoom
            }
        }
    }
    
    var isInfoViewHidden: Bool {
        return bottomContainerConstraint.constant < 0
    }
    
    var selectedMarker: EstablishmentMarker?
    
    var itemList: LevelPaginatedListModel<Place> {
        return PlaceDataSource.shared.itemList
    }
    
    var searchText: String = "" {
        didSet {
            PlaceDataSource.shared.searchText = searchText
        }
    }
    
    var containerViewController: PlaceContainerViewController!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let loc = LocationManager.shared.currentLocation {
            currentLocation = loc
            setupMapView(for: loc)
        } else {
            LocationManager.shared.updateLocationWith(coordinateCompletionHandler: { (currentLocation) in
                if let location = currentLocation {
                    self.setupMapView(for: location)
                    self.locationDetected(location)
                    LocationManager.shared.stopLocationUpdates()
                }
            }, authStatusCompletionHandler: locationManagerAuthStateDidChange(_:))
        }
        
        let generator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let render = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: generator)
        clasterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: render)
        clasterManager.setDelegate(self, mapDelegate: self)
        
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                self.searchText = self.searchBar.text ?? ""
                self.reloadItemListAndUpdate()
            }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchText = self.searchBar.text ?? ""
                self.reloadItemListAndUpdate()
                self.searchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchText = ""
                self.searchBar.text = nil
                if let location = LocationManager.shared.currentLocation {
                    self.itemList.queryParams["lat"] = location.coordinate.latitude as AnyObject
                    self.itemList.queryParams["lng"] = location.coordinate.longitude as AnyObject
                    self.itemList.queryParams["radius"] = Configuration.Constants.defaultRadius as AnyObject
                    self.reloadItemListAndUpdate()
                }
                self.searchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.text = PlaceDataSource.shared.searchText
        updateMap(with: itemList.objects, isReload: true)
        setupGradientedNavigationBar()
    }
    
    func updateMap(with newPlaces: [Place], isReload: Bool = false) {
        if isReload {
            places = []
            self.clasterManager.clearItems()
        }
        places.append(contentsOf: newPlaces)
        for place in newPlaces {
            guard let location = place.coordinates else { return }
            let establishminMarker = EstablishmentMarker(with:place, coordinates: location)
            self.clasterManager.add(establishminMarker)
        }
        
        DispatchQueue.main.async {
            self.clasterManager.cluster()
        }
    }
    
    func reloadItemListAndUpdate() {
        itemList.newObjects = nil
        itemList.load { (success) in
            if success {
                self.updateMap(with: self.itemList.objects, isReload: true)
                self.itemList.newObjects = nil
            }
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        if isViewLoaded {
            searchBar.resignFirstResponder()
        }
    }
    
    override func menuWillClose() {
        super.menuWillClose()
        
    }
    
}


fileprivate extension PlaceMapViewController {
    
    //MARK: - IBActions
    
//    @IBAction func siteAction(_ sender: UIButton) {
//        if let url = URL(string: selectedMarker?.place.site ?? "") {
//            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
//        }
//    }
    
    @IBAction func callAction(_ sender: UIButton) {
        if let phone = URL(string: "tel://" + (selectedMarker?.place.phones.first != nil ? (selectedMarker!.place!.phones.first!.hasPrefix("38") ? "+" + selectedMarker!.place!.phones.first! : selectedMarker!.place.phones.first)! : "").replacingOccurrences(of: " ", with: "")) {
            UIApplication.shared.open(phone, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }
    
    @IBAction func filterAction(_ sender: UIBarButtonItem) {
        if let mainTabBar = tabBarController as? MainTabBarViewController {
            mainTabBar.showCategoryFiler()
        }
    }
    
//    @IBAction func shareAction(_ sender: UIButton) {
//        var items: [Any] = []
//        items.append(selectedMarker?.place.name ?? "City+")
//        items.append(selectedMarker?.place.site ?? "City+")
//        items.append(selectedMarker?.place.address ?? "City+")
//        if let image = selectedMarker?.place.photos.first?.image {
//            items.append(image)
//        }
//        let activityVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
//        present(activityVC, animated: true, completion: nil)
//    }
    
    @IBAction func favoriteAction(_ sender: UIButton) {
        sender.isEnabled = false
        sender.alpha = 0.5
        if (selectedMarker?.place.isFavorite)! {
            selectedMarker?.place.removeFromFavorite(with: { (success) in
                self.selectedMarker?.place.isFavorite = false
                sender.isEnabled = true
                sender.alpha = 1
                if success {
                    sender.isSelected = false
                }
            })
        } else {
            selectedMarker?.place.addToFavorite(with: { (success) in
                self.selectedMarker?.place.isFavorite = true
                sender.isEnabled = true
                sender.alpha = 1
                if success {
                    sender.isSelected = true
                }
            })
        }
    }
    
    @IBAction func moreAction(_ sender: UIButton) {
        if let place = selectedMarker?.place {
            let detailVC: PlaceDetailContainerTableViewController = UIStoryboard(.Place).instantiateViewController()
            detailVC.place = place
            show(detailVC, sender: self)
        }
    }
    
    func setupMapView(for location: CLLocation) {
        let newCamera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: Constants.defaulZoom)
        mapView.camera = newCamera
        do {
            mapView.mapStyle = try GMSMapStyle(jsonString: Constants.kMapStyle)
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
        currentZoom = Constants.defaulZoom
        mapView.isMyLocationEnabled = true
    }
    
    func locationManagerAuthStateDidChange(_ status: CLAuthorizationStatus?) {
        if status != CLAuthorizationStatus.authorizedWhenInUse {
            print("> PlaceMapViewController: Location access is denied!")
        }
    }
    
    func showInfoView(for marker: EstablishmentMarker, animated: Bool = true) {
        if let selected = selectedMarker {
            selected.selected = false
        }
        selectedMarker = marker
        fillInfoUI(with: marker.place)
        bottomContainerConstraint.constant = 0
        if let location = marker.place.coordinates {
            setupMapCamera(in: CLLocation.init(latitude: location.latitude, longitude: location.longitude))
        }
        if animated {
            UIView.animate(withDuration: Constants.animationDuration, delay: TimeInterval(0), usingSpringWithDamping: Constants.dumpingValue, initialSpringVelocity: Constants.spingVelocity, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
                self.previewContainer.layoutIfNeeded()
                //self.previewContainer.roundedCorners(on: [.topLeft, .topRight], with: 15)
            }, completion: nil)
        }
    }
    
    func hideInfoView(_ animated: Bool = true) {
        selectedMarker?.selected = false
        selectedMarker = nil
        
        bottomContainerConstraint.constant = -previewContainer.bounds.height
        if animated {
            UIView.animate(withDuration: Constants.animationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func fillInfoUI(with place: Place) {
        placeName.text = place.name
        placeAddress.text = place.address
        placeWorkHours.text = place.schedules.first?.formatedWorkHours
        ratingView.rating = NSNumber(value: Float(place.rating))
        
        searchBar.text = PlaceDataSource.shared.searchText
        
//        placeImageView.setImage(with: place.photos.first, placeholder: #imageLiteral(resourceName: "placePlaceholder"), using: .gray, placeholderSettings: (.center, nil), completion: nil)
//
//        placeImageView.layer.borderColor = UIColor.white.cgColor
//        placeImageView.layer.borderWidth = 1.0
//        placeImageView.layer.cornerRadius = 10
//        placeImageView.layer.masksToBounds = true
        
        favoriteButton.isSelected = place.isFavorite
        
       // siteButton.isEnabled = place.site != nil
        callButton.isEnabled = place.phones.count > 0
        
       // siteButton.alpha = place.site != nil ? 1 : 0.5
        callButton.alpha = place.phones.count > 0 ? 1 : 0.5
    }
    
    func setupMapCamera(in placeCoordinate: CLLocation, inCurrentZoom: Bool = true) {
        mapView.isMyLocationEnabled = true
        mapView.moveCamera(GMSCameraUpdate.setTarget(placeCoordinate.coordinate, zoom: inCurrentZoom ? mapView.camera.zoom : Constants.defaulZoom))
        self.currentZoom = Constants.defaulZoom
    }
    
    func locationDetected(_ location: CLLocation?) {
        if let location = location {
            self.itemList.queryParams["lat"] = location.coordinate.latitude as AnyObject
            self.itemList.queryParams["lng"] = location.coordinate.longitude as AnyObject
            self.itemList.queryParams["radius"] = 7000 as AnyObject
        } else {
            self.itemList.queryParams.removeValue(forKey: "lat")
            self.itemList.queryParams.removeValue(forKey: "lng")
            self.itemList.queryParams.removeValue(forKey: "radius")
        }
        self.reloadItemListAndUpdate()
    }
    
    
    
    func loadMoreIfNeededAndUpdate() {
        itemList.loadMore { (success) in
            if success {
                self.updateMap(with: self.itemList.newObjects ?? [])
                self.itemList.newObjects = nil
            }
        }
    }
    
}

// MARK: - GMUClasterManagerDelegate

extension PlaceMapViewController {
    
    
    
}

extension PlaceMapViewController: GMUClusterManagerDelegate {
    
    
    
}

// MARK: - GMSMapViewDelegate

extension PlaceMapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let selected = selectedMarker {
            hideInfoView(false)
            if let place = self.itemList.objects.first(where: { ($0.coordinates?.latitude ?? 0) == marker.position.latitude && ($0.coordinates?.longitude ?? 0) == marker.position.longitude }) {
                let establishmentMarker = EstablishmentMarker(with: place, coordinates: marker.position)
                showInfoView(for: establishmentMarker)
                return true
            }
        } else {
            if let place = self.itemList.objects.first(where: { ($0.coordinates?.latitude ?? 0) == marker.position.latitude && ($0.coordinates?.longitude ?? 0) == marker.position.longitude }) {
                let establishmentMarker = EstablishmentMarker(with: place, coordinates: marker.position)
                    showInfoView(for: establishmentMarker)
                return true
            }
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print(mapView.camera.zoom)
        if abs(currentZoom - mapView.camera.zoom) > 0.3 {
            loadMoreIfNeededAndUpdate()
            self.currentZoom = mapView.camera.zoom
        }
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if !isInfoViewHidden {
            hideInfoView()
        }
    }
    
}

fileprivate extension PlaceMapViewController {
    
    struct Constants {
        static let defaulZoom: Float = 18
        static let animationDuration: TimeInterval = TimeInterval(0.6)
        static let dumpingValue: CGFloat = CGFloat(0.8)
        static let spingVelocity: CGFloat = CGFloat(1)        
        static let kMapStyle = "[" +
            "  {" +
            "    \"featureType\": \"poi.business\"," +
            "    \"elementType\": \"all\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"transit\"," +
            "    \"elementType\": \"labels.icon\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }" +
        "]"
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
