//
//  PhotosCollectionViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/8/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class PhotosCollectionViewController: UICollectionViewController {
    
    var photos = [Avatar]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    var placeholderView: TableViewPlaceholderView {
        return TableViewPlaceholderView(frame: view.bounds, with: .noPhotoItems)
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            collectionView?.collectionViewLayout = flowLayout
            collectionView?.reloadData()
        }
    }

}

extension PhotosCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 2, left: 0, bottom: 2, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sideWidth = (collectionView.bounds.width - (collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing) / 2
        return CGSize(width: sideWidth, height: sideWidth)
    }
    
}

extension PhotosCollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePhotoCollectionViewCell", for: indexPath) as! ProfilePhotoCollectionViewCell
        cell.photoImageView.setImage(with: photos[indexPath.item], placeholder: #imageLiteral(resourceName: "no-image"), isThumb: false, using: .gray, placeholderSettings: nil, completion: nil)
        cell.tintColor = .black
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionView.backgroundView = photos.count == 0 ? placeholderView : nil
        return photos.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = photos[indexPath.row]
        let previeImageView: ImagePreviewViewController = UIStoryboard(.Chats).instantiateViewController()
        previeImageView.attachment = photo
        show(previeImageView, sender: self)
    }
    
}
