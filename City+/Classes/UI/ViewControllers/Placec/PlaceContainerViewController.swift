//
//  PlaceContainerViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/3/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import CoreLocation

final class PlaceContainerViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var listContainer: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var typeSegmentedControll: UISegmentedControl!
    
    var mapViewController: PlaceMapViewController?
    var listViewController: PlaceListTableViewController?
        
    var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                itemList.queryParams["name"] = searchText as AnyObject
                itemList.queryParams.removeValue(forKey: "lat")
                itemList.queryParams.removeValue(forKey: "lng")
                itemList.queryParams.removeValue(forKey: "radius")
            } else {
                itemList.queryParams.removeValue(forKey: "name")
            }
        }
    }
    
    var disposeBag = DisposeBag()
    
    var currentType: ViewType = .map {
        didSet {
            setupContainers()
        }
    }
    
    lazy var itemList: LevelPaginatedListModel<Place> = {
        let list = LevelPaginatedListModel<Place>.init(path: Place.Constants.placeRoute)
        list.queryParams["is_fake"] = false as AnyObject
        list.onNoInternetConnection = noInternetConnectionHandler
        list.onRequestFailure = requestFailureHandler
        return list
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                self.searchText = text.element ?? ""
                self.reloadItemListAndUpdate(with: nil)
            }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchText = self.searchBar.text ?? ""
                self.reloadItemListAndUpdate(with: nil)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchText = ""
                self.searchBar.text = nil
                if let location = LocationManager.shared.currentLocation {
                    self.itemList.queryParams["lat"] = location.coordinate.latitude as AnyObject
                    self.itemList.queryParams["lng"] = location.coordinate.longitude as AnyObject
                    self.itemList.queryParams["radius"] = 20000 as AnyObject
                    self.reloadItemListAndUpdate()
                }
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
    }

    func loadMoreIfNeededAndUpdate() {
        itemList.loadMore { (success) in
            if success {
                self.mapViewController?.updateMap(with: self.itemList.newObjects ?? [])
//                self.listViewController?.updateDataSource(with: self.itemList.newObjects ?? [], in: self.itemList)
                self.itemList.newObjects = nil
            }
        }
    }
    
    func locationDetected(_ location: CLLocation?) {
        if let location = location {
            self.itemList.queryParams["lat"] = location.coordinate.latitude as AnyObject
            self.itemList.queryParams["lng"] = location.coordinate.longitude as AnyObject
            self.itemList.queryParams["radius"] = 7000 as AnyObject
            self.reloadItemListAndUpdate()
        } else {
            self.itemList.queryParams.removeValue(forKey: "lat")
            self.itemList.queryParams.removeValue(forKey: "lng")
            self.itemList.queryParams.removeValue(forKey: "radius")
        }
        self.reloadItemListAndUpdate()
    }
    
    func reloadItemListAndUpdate(with completion: NetworkModelCompletion? = nil) {
        itemList.newObjects = nil
//        itemList.pageToken = nil
        itemList.load { (success) in
            completion?(success)
            if success {
//                self.listViewController?.places = []
                self.mapViewController?.places = []
                self.mapViewController?.updateMap(with: self.itemList.newObjects ?? [], isReload: true)
//                self.listViewController?.updateDataSource(with: self.itemList.newObjects ?? [], in: self.itemList)
                self.itemList.newObjects = nil
            }
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let map = segue.destination as? PlaceMapViewController {
            mapViewController = map
            map.containerViewController = self
        }
        
        if let list = segue.destination as? PlaceListTableViewController {
            listViewController = list
            list.containerViewController = self
        }
    }
}

fileprivate extension PlaceContainerViewController {
    
    @IBAction func categoryDidChange(_ sender: UISegmentedControl) {
        currentType = sender.selectedSegmentIndex == 0 ? .map : .list
    }
    
    func setupContainers() {
        listContainer.isHidden = currentType == .map
        mapContainer.isHidden = currentType == .list
    }
    
}

extension PlaceContainerViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if (searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            searchText = searchBar.text ?? ""
            reloadItemListAndUpdate(with: nil)
        }
    }
    
}

extension PlaceContainerViewController {
    
    enum ViewType {
        case map
        case list
    }
    
}
