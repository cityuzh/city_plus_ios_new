//
//  CategoryTouristPlacesTableViewController.swift
//  CityPlus
//
//  Created by Ozzy on 02.09.2020.
//  Copyright © 2020 Ivan Grab. All rights reserved.
//

import UIKit

class CategoryTouristPlacesTableViewController: UITableViewController {
    
    //MARK: - Properties
    
    fileprivate var dataSource: [Place] = []
        
    var placeholderMode: TableViewPlaceholderView.PlaceholderMode = .loading {
        didSet {
            placeholderView.currentType = placeholderMode
            placeholderView.setNeedsDisplay()
        }
    }
    
    lazy var placeholderView: TableViewPlaceholderView = {
        return TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode)
    }()
    
    lazy var itemList: PaginatedListModel<Place> = {
        let list = PaginatedListModel<Place>.init(path: Place.Constants.placeRoute)
        list.onRequestFailure = requestFailureHandler
        list.requestRowCount = 10
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
        
    var category: PlaceCategory? {
        didSet {
            if let id = category?.identifier {
                itemList.queryParams["category"] = id as AnyObject
            } else {
                itemList.queryParams.removeValue(forKey: "category")
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupNavigationTitle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadContent()
    }
    
    func loadContent() {
        self.placeholderMode = .loading
        itemList.load { (success) in
          self.placeholderMode = .noDataItems
          self.dataSource = self.itemList.objects
          self.tableView.reloadData()
        }
    }
    
    func setupNavigationTitle(){
        navigationItem.title = category?.name
    }
    
    // MARK: - UITableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TouristPlacesTableViewCell.cellIdentifier, for: indexPath) as! TouristPlacesTableViewCell
        cell.setUp(with: itemList[indexPath.item])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailCatalog: PlaceDetailContainerTableViewController = UIStoryboard(.Place).instantiateViewController()
           detailCatalog.place = itemList[indexPath.row]
        detailCatalog.isTouristPlace = true
        self.show(detailCatalog, sender: self)
    }
 
    // MARK: - UITableViewDelegate

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 239
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= itemList.count - 3 {
            loadMoreData()
        }
    }
    
}

extension CategoryTouristPlacesTableViewController: RefreshableViewControllerProtocol {
     
     func endRefreshingWithCompletion(completion: () -> Void) {
         self.refreshControl?.endRefreshing()
         self.tableView.reloadData()
     }
     
     func updateUI() {
         guard let changes = itemList.lastChanges else { return }
         
         itemList.lastChanges = nil
         guard !changes.moves.isEmpty else {
             tableView.reloadData()
             return
         }
         
         tableView.beginUpdates()
         
         if !changes.deletedIndexPaths.isEmpty {
             var section: Int
             for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                 section = indexPath.section
                 var deletedIndexPath = [IndexPath]()
                 var rowsInSection =  tableView.numberOfRows(inSection: section)
                 for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                     if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                         rowsInSection -= 1
                         tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                         deletedIndexPath.append(secondIndexPath as IndexPath)
                         
                         if rowsInSection == 0 {
                             tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                         }
                     } else {
                         tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                         deletedIndexPath.append(secondIndexPath as IndexPath)
                     }
                 }
             }
             tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
         }
         
         if !changes.insertedIndexPaths.isEmpty {
             if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                 let sectionForInsert = NSMutableIndexSet()
                 for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                     if indexPath.section > lastVisibleCell.section {
                         if !sectionForInsert.contains(indexPath.section) {
                             sectionForInsert.add(indexPath.section)
                         }
                     }
                 }
                 tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
             }
             tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
         }
         
         if !changes.updatedIndexPaths.isEmpty {
             tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
         }
         
         tableView.endUpdates()
     }
     
}
