//
//  TouristPlacesCatalogTableViewController.swift
//  CityPlus
//
//  Created by Macbook Air 13 on 8/26/20.
//  Copyright © 2020 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TouristPlacesTableViewController: BaseTableViewController {
  
    //MARK: - Properties
    
    lazy var itemList: PaginatedListModel<Place> = {
        let list = PaginatedListModel<Place>.init(path: Place.Constants.touristPlaceRoute)
        list.onRequestFailure = requestFailureHandler
        list.requestRowCount = 10
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    

    fileprivate var dataSource: [[Place]] = []

    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshContent(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
           self.placeholderMode = .loading
           itemList.load { (success) in
               self.placeholderMode = .noDataItems
               let filteredWithPosition = self.itemList.objects
            var dataSource = [[Place]]()
               var group = [Place]()
               if let firstItem = self.itemList.objects.first, var lastCategory = firstItem.category?.name {
                   var index = 0
                   for item in filteredWithPosition {
                       if lastCategory == item.category?.name {
                           group.append(item)
                       } else {
                           dataSource.append(group)
                           group = [item]
                           lastCategory = item.category?.name ?? ""
                       }
                       if index == filteredWithPosition.count - 1 {
                           dataSource.append(group)
                       }
                       index += 1
                   }
               }
               self.dataSource = dataSource
               sender?.endRefreshing()
               self.tableView.reloadData()
           }
       }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = dataSource.count == 0 ? placeholderView : nil
        return dataSource.count == 0 ? 0 : dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CatalogTouristPlacesTableViewCell.cellIdentifier, for: indexPath) as! CatalogTouristPlacesTableViewCell
        cell.setUp(with: dataSource[indexPath.row])
        cell.selectionHandler = { (place) in
            let detailPlaceVC: PlaceDetailContainerTableViewController = UIStoryboard(.Place).instantiateViewController()
            detailPlaceVC.place = place
            detailPlaceVC.isTouristPlace = true
            self.show(detailPlaceVC, sender: self)
        }
        cell.categoryHandler = { (category) in
            let myCatalogs: CategoryTouristPlacesTableViewController = UIStoryboard(.TouristPlaces).instantiateViewController()
            myCatalogs.category = category
            self.show(myCatalogs, sender: self)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 250.0
   }

   override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       if indexPath.row >= dataSource.count - 3 {
           loadMoreData()
       }
   }
}

extension TouristPlacesTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        let filteredWithPosition = self.itemList.objects
        var newDataSource = [[Place]]()
        var group = [Place]()
        if let firstItem = self.itemList.objects.first, var lastCategory =  firstItem.category?.name {
            var index = 0
            for item in filteredWithPosition {
                if lastCategory == item.category?.name {
                    group.append(item)
                } else {
                    newDataSource.append(group)
                    group = [item]
                    lastCategory = item.category?.name ?? ""
                }
                if index == filteredWithPosition.count - 1 {
                    newDataSource.append(group)
                }
                index += 1
            }
        }
                
        if newDataSource.count > self.dataSource.count {
            var indexPathes = [IndexPath]()
            let start = self.dataSource.count
            let end = newDataSource.count
            for i in start ..< end  {
                self.dataSource.append(newDataSource[i])
                indexPathes.append(IndexPath(row: i, section: 0))
            }
            self.tableView.insertRows(at: indexPathes, with: .automatic)
        }
    }
    
}
