//
//  PlaceEventsTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/4/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Nuke
import UIKit
import RxSwift
import RxCocoa

final class PlaceEventsTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    var disposeBag = DisposeBag()
    
    //MARK: - Properties
    var place: Place! {
        didSet {
            if place != nil {
                itemList.path = String(format: Event.Constants.eventRoutePathFormat, place.identifier ?? 0)
                navigationItem.title = place.name
                refreshContent(nil)
            }
        }
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - Properties
    var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                itemList.queryParams["name"] = searchText as AnyObject
            } else {
                itemList.queryParams.removeValue(forKey: "name")
            }
        }
    }
    
    lazy var itemList: PaginatedListModel<Event> = {
        let list = PaginatedListModel<Event>(path: String(format: Event.Constants.eventRoutePathFormat, place != nil ? place!.identifier! : 0))
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !searchText.isEmpty {
            searchBar.text = searchText
        }
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                self.searchText = text.element ?? ""
                self.refreshContent(nil)
            }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchText = self.searchBar.text ?? ""
                self.searchBar.resignFirstResponder()
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchText = ""
                self.searchBar.text = nil
                self.searchBar.resignFirstResponder()
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
        refreshContent(nil)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        self.placeholderMode = .loading
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
        }
    }
    
    func openPlaceEvent(for event: Event) {
        place.get { (success) in
            event.place = self.place
            if success {
                let eventDetailVC: EventContainerViewController = UIStoryboard(.Events).instantiateViewController()
                eventDetailVC.event = event
                self.show(eventDetailVC, sender: self)
            }
        }
    }
    
    //MARK: - UITableViewDelegate / UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 310
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = itemList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewEventTableViewCell.classForCoder()), for: indexPath) as! NewEventTableViewCell
//        cell.photoImageView.sd_setImage(with: URL(string: event.image?.urlPath ?? ""), placeholderImage: #imageLiteral(resourceName: "no-image.png"), options: [.lowPriority, .allowInvalidSSLCertificates, .delayPlaceholder]) { (image, error, cacheType, url) in
//
//        }
        if let url = URL(string: event.image?.urlPath ?? "") {
            let options = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "no-image.png"), transition: nil, failureImage: nil, failureImageTransition: nil, contentModes: nil)
            Nuke.loadImage(with: url, options: options, into: cell.photoImageView, progress: nil, completion: nil)
            
        }
        cell.setup(with: event)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = itemList[indexPath.row]
        event.place = place
        let eventDetailVC: EventContainerViewController = UIStoryboard(.Events).instantiateViewController()
        eventDetailVC.event = event
        show(eventDetailVC, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CreateEventTableViewController {
            let preEnevt = Event()
            preEnevt.place = place
            preEnevt.address = place.address
            destination.event = preEnevt
        }
    }
    
}

extension PlaceEventsTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}
