//
//  MyEventsTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/11/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit


final class MyEventsTableViewController: BaseTableViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var eventSegmentedControl: UISegmentedControl!

    //MARK: - Properties
    
    lazy var itemList: PaginatedListModel<AttendeeEvent> = {
       let list = PaginatedListModel<AttendeeEvent>.init(path: AttendeeEvent.Constants.myEventsPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    lazy var myItemList: PaginatedListModel<Event> = {
        let list = PaginatedListModel<Event>.init(path: Event.Constants.myEventsPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    var currentListType: ListType = .attendee
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshContent(nil)
        myItemList.load()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
    }

    override func refreshContent(_ sender: UIRefreshControl?) {
        if currentListType == .attendee {
            itemList.load { (success) in
                if success {
                    self.placeholderMode = .noDataItems
                }
                sender?.endRefreshing()
                self.tableView.reloadData()
            }
        } else {
            myItemList.load { (success) in
                if success {
                    self.placeholderMode = .noDataItems
                }
                sender?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - UITableViewDelegate / UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentListType == .attendee {
            tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        } else {
            tableView.backgroundView = myItemList.count == 0 ? placeholderView : nil
        }
        return currentListType == .attendee ? itemList.count : myItemList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = currentListType == .attendee ? itemList[indexPath.row].event! : myItemList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EventTableViewCell.classForCoder()), for: indexPath) as! EventTableViewCell
        cell.setUp(with: event)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if currentListType == .attendee {
            if (itemList.count - indexPath.row) <= (itemList.requestRowCount / 4) && itemList.state == .canLoadMore {
                itemList.loadMore { (success) -> Void in
                    if success {
                        self.updateUI(for: self.itemList)
                    }
                }
            }
        } else {
            if (myItemList.count - indexPath.row) <= (myItemList.requestRowCount / 4) && myItemList.state == .canLoadMore {
                myItemList.loadMore { (success) -> Void in
                    if success {
                        self.updateUI(for: self.myItemList)
                    }
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        func showDetail(for event: Event) {
            let eventDetailVC: EventContainerViewController = UIStoryboard(.Events).instantiateViewController()
            eventDetailVC.event = event
            show(eventDetailVC, sender: self)
        }
        
        if currentListType == .my {
            showDetail(for: myItemList[indexPath.row])
        } else {
            if let event = itemList[indexPath.row].event {
                showDetail(for: event)
            }
        }
    }
    
}

fileprivate extension MyEventsTableViewController {
    
    @IBAction func eventTypeChanged(_ sender: UISegmentedControl) {
        if let type = ListType(rawValue: sender.selectedSegmentIndex) {
            currentListType = type
            tableView.reloadData()
        }
    }
    
    func updateUI(for myList: PaginatedListModel<Event>) {
        guard let changes = myList.lastChanges else { return }
        
        myList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
    func updateUI(for itemList: PaginatedListModel<AttendeeEvent>) {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}

extension MyEventsTableViewController {
    
    enum ListType: Int {
        case attendee = 0
        case my
    }
    
}
