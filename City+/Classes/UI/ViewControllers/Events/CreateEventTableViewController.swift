//
//  CreateEventTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/16/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import GooglePlaces
import MobileCoreServices

final class CreateEventTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var about: UITextView!
    @IBOutlet weak var startDate: UITextField!
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var placeName: UITextField!
    
    @IBOutlet var textFields: [UITextField]!
    
    @IBOutlet weak var photoButton: UIButton! {
        didSet {
            photoButton.layer.cornerRadius = photoButton.bounds.height / 2
        }
    }
    @IBOutlet weak var removePhotoButton: UIButton! {
        didSet {
            removePhotoButton.layer.cornerRadius = removePhotoButton.bounds.height / 2
        }
    }
    
    @IBOutlet weak var saveBarItem: UIBarButtonItem!
    
    //MARK: - Properties
    
    var textViewHeight: CGFloat = 0.0
    var currentAction: CRUDAction = .create
    var isChanged = false
    var isImageChanged = false
    
    var event = Event()
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.minimumDate = Date()
        picker.datePickerMode = .dateAndTime
        picker.addTarget(self, action: #selector(dateDidChange(_:)), for: .valueChanged)
        return picker
    }()
    
    lazy var birthdayAccessory: UIView = {
        let accessory = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        accessory.backgroundColor = UIColor.groupTableViewBackground
        
        let cancelButton = UIButton(type: .roundedRect)
        cancelButton.setTitle(NSLocalizedString("Відмінити", comment: ""), for: .normal)
        cancelButton.setTitleColor(UIColor.darkGray, for: .normal)
        cancelButton.contentHorizontalAlignment = .left
        cancelButton.addTarget(self, action: #selector(dateDidCancel(_:)), for: .touchUpInside)
        
        let saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle(NSLocalizedString("Добре", comment: ""), for: .normal)
        saveButton.setTitleColor(UIColor.darkGray, for: .normal)
        saveButton.contentHorizontalAlignment = .right
        saveButton.addTarget(self, action: #selector(dateDidSet(_:)), for: .touchUpInside)
        
        accessory.addSubview(saveButton)
        accessory.addSubview(cancelButton)
        
        let views = ["save": saveButton, "cancel": cancelButton]
        let metrics = ["inset": 15]
        var constraints = [NSLayoutConstraint]()
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-inset-[cancel]-[save]-inset-|", options: [], metrics: metrics, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[cancel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[save]-|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        return accessory
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker = UIImagePickerController()
        imagePicker?.allowsEditing = true
        imagePicker?.mediaTypes = [kUTTypeImage as String]
        
        setupGradientedNavigationBar()
        
        fillUI(with: event)
        event.onRequestFailure = requestFailureHandler
        
        startDate.inputView = datePicker
        startDate.inputAccessoryView = birthdayAccessory
        
        endDate.inputView = datePicker
        endDate.inputAccessoryView = birthdayAccessory
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fillUI(with: event)
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath {
        case IndexPath(row: 4, section:0): return about.contentSize.height + 40
        case IndexPath(row: 0, section:1): return currentAction == .update ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat.leastNonzeroMagnitude : super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5 {
            let placeList: SelectPlaceTableViewController = UIStoryboard(.Events).instantiateViewController()
            placeList.selectionHandler = { (selectedPlace, isNew) in
                if isNew {
                    self.event.newPlace = selectedPlace.name
                } else {
                    self.event.place = selectedPlace
                    if self.event.address == nil {
                        self.event.address = selectedPlace.address
                    }
                }
                self.placeName.text = selectedPlace.name
            }
            show(placeList, sender: self)
        }
    }
    
    //MARK: - IBActions
    
    @objc func dateDidChange(_ sender: UIDatePicker) {
        if startDate.isFirstResponder {
            startDate.text = Date.shortDayDateTimeFormater.string(from: sender.date)
            event.startDate = datePicker.date
        } else {
            endDate.text = Date.shortDayDateTimeFormater.string(from: sender.date)
            event.endDate = datePicker.date
        }
    }
    
    @objc func dateDidSet(_ sender: UIButton) {
        isChanged = true
        if startDate.isFirstResponder {
            startDate.resignFirstResponder()
            startDate.text = Date.shortDayDateTimeFormater.string(from: datePicker.date)
            event.startDate = datePicker.date
        } else {
            endDate.resignFirstResponder()
            endDate.text = Date.shortDayDateTimeFormater.string(from: datePicker.date)
            event.endDate = datePicker.date
        }
    }
    
    @objc func dateDidCancel(_ sender: UIButton) {
        if startDate.isFirstResponder {
            startDate.resignFirstResponder()
            if let date = event.startDate {
                startDate.text = Date.shortDayDateTimeFormater.string(from: date)
            }
        } else {
            endDate.resignFirstResponder()
            if let date = event.endDate {
                endDate.text = Date.shortDayDateTimeFormater.string(from: date)
            }
        }
    }
    
    @IBAction func saveTapped(_ sender: UIBarButtonItem) {
        
        func succesfylUpdate() {
            setupControlls()
        }
        
        event.name = name.text
        event.about = about.text
        event.onRequestFailure = requestFailureHandler
        
        do {
            try event.validate()
            if currentAction == .create {
                present(activityVC, animated: true, completion: nil)
                event.create { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                }
            } else {
                if isChanged {
                    present(activityVC, animated: true, completion: nil)
                    isImageChanged = true
                    if event.image?.image == nil {
                        event.image?.image = imageView.image
                    }
                    event.update(with: { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                self.navigationController?.popViewController(animated: true)
                            }
                        })
                    }, isImageChanged: isImageChanged)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } catch {
            let error = error as! ValueValidationError
            ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
        }
        
    }
    
    @IBAction func delereTapped(_ sender: UIButton) {
        let deleteAction = UIAlertAction(title: NSLocalizedString("Так", comment: ""), style: .destructive) { (action) in
            self.present(self.activityVC, animated: true, completion:nil)
            self.event.delete(with: { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                })
            })
        }
        ShowAlert.showAlertContrller(with: .alert, title: "City+", message: NSLocalizedString("Ви дійсно бажаєте видалити подію?", comment: ""), actions: [deleteAction], in: self)
    }
    
    @IBAction func removePhotoAction(_ sender: UIButton) {
        event.image = nil
        imageView.image = #imageLiteral(resourceName: "no-image")
    }
    
    @IBAction func addPhotoAction(_ sender: UIButton) {
        startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: nil)
                }
            })
        }, onCancelAction: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {UICollectionViewController
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func menuWillOpen() {
        super.menuWillOpen()
        about.resignFirstResponder()
        textFields.forEach({ $0.resignFirstResponder() })
    }

}

//MARK: - Private

extension CreateEventTableViewController {
    
    enum CRUDAction {
        case create
        case update
    }
    
    func fillUI(with event: Event) {
        
        navigationItem.title = currentAction == .create ? NSLocalizedString("Нова подія", comment: "") : NSLocalizedString("Змінити подію", comment: "")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        name.text = event.name
        address.text = event.address
        if event.address == nil {
            address.text = event.place?.address
        }
        placeName.text = event.place?.name ?? event.newPlace
        imageView.setImage(with: event.image, placeholder: #imageLiteral(resourceName: "create_ivent_placeholde"), using: .gray, placeholderSettings: nil, completion: nil)
        startDate.text = event.startDate != nil ? Date.shortDayDateTimeFormater.string(from: event.startDate!) : nil
        endDate.text = event.endDate != nil ? Date.shortDayDateTimeFormater.string(from: event.endDate!) : nil
        
        
        if let aboutString = event.about {
            about.text = aboutString
            about.sizeToFit()
            about.textColor = UIColor.black
        }
        setupControlls()
    }
    
    func setupControlls() {
        
        removePhotoButton.isHidden = event.image == nil
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
}

//MARK: - UITextFieldDelegate

extension CreateEventTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == address {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            autocompleteController.tintColor = .appTintColor
            UINavigationBar.appearance().tintColor = .appTintColor
            autocompleteController.navigationController?.navigationBar.tintColor = .appTintColor
            present(autocompleteController, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        isChanged = true
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == name {
            event.name = name.text
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let newHeight = about.contentSize.height + 40
        if textViewHeight != newHeight {
            textViewHeight = newHeight
            UIView.setAnimationsEnabled(false)
            tableView.beginUpdates()
            tableView.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        isChanged = true
        return (textView.text + text).count <= Configuration.TextBounds.profileDescriptionMaxCount
    }
    
}

//MARK: - GMSAutocompleteViewControllerDelegate
extension CreateEventTableViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        isChanged = true
        address.text = place.formattedAddress
        event.address = place.formattedAddress
        event.coordinates = place.coordinate
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}


//MARK: - PikingImage

extension CreateEventTableViewController: PickingImage {
    
    func pickingDidCancel() {
        
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        isImageChanged = true
        if let newImage = thumbnail {
            isChanged = true
            let imageAvatar = Avatar()
            imageAvatar.image = newImage
            event.image = imageAvatar
            imageView.setImage(with: imageAvatar, placeholder: #imageLiteral(resourceName: "create_ivent_placeholde"), using: .gray, placeholderSettings: nil, completion: nil)
        }
    }
    
}

//MARK: - UITextViewDelegate

extension CreateEventTableViewController: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == about {
            event.about = about.text
        }
    }
    
}
