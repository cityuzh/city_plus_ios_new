//
//  EventTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/21/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class EventTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - Properties
    var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                itemList.queryParams["name"] = searchText as AnyObject
            } else {
                itemList.queryParams.removeValue(forKey: "name")
            }
        }
    }
    
    lazy var itemList: PaginatedListModel<Place> = {
        let list = PaginatedListModel<Place>.init(path: Place.Constants.placeRoute)
        list.queryParams["events_count"] = 1 as AnyObject
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
        
        LocationManager.shared.updateLocationWith(coordinateCompletionHandler: { (currentLocation) in
            if let location = currentLocation {
                self.itemList.queryParams["lat"] = location.coordinate.latitude as AnyObject
                self.itemList.queryParams["lng"] = location.coordinate.longitude as AnyObject
                self.itemList.queryParams["radius"] = 20000 as AnyObject
            } else {
                self.itemList.queryParams.removeValue(forKey: "lat")
                self.itemList.queryParams.removeValue(forKey: "lng")
                self.itemList.queryParams.removeValue(forKey: "radius")
            }
            LocationManager.shared.stopLocationUpdates()
            self.refreshContent(nil)
        }, authStatusCompletionHandler: nil)
        
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                self.searchText = text.element ?? ""
                self.refreshContent(nil)
            }.disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchText = self.searchBar.text ?? ""
                self.searchBar.resignFirstResponder()
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchText = ""
                self.searchBar.text = nil
                self.searchBar.resignFirstResponder()
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
    }
    
    func openDetailEvent(with eventID: Int, placeID: Int) {
        let place = Place()
        place.identifier = placeID
        
        let event = Event()
        event.identifier = eventID
        
        let placeEventsVC: PlaceEventsTableViewController = UIStoryboard(.Events).instantiateViewController()
        placeEventsVC.place = place
        self.show(placeEventsVC, sender: self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {            
            placeEventsVC.openPlaceEvent(for: event)
        }
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        self.placeholderMode = .loading
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
        }
    }
    
    //MARK: - UITableViewDelegate / UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PlaceTableViewCell.classForCoder()), for: indexPath) as! PlaceTableViewCell
        cell.setUp(with: itemList[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let place = itemList[indexPath.row]
        let placeEventsVC: PlaceEventsTableViewController = UIStoryboard(.Events).instantiateViewController()
        placeEventsVC.searchText = searchText
        placeEventsVC.place = place        
        show(placeEventsVC, sender: self)
    }

    override func menuWillOpen() {
        super.menuWillOpen()
        if isViewLoaded {
            searchBar.resignFirstResponder()
        }
    }    
    
}

extension EventTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}
