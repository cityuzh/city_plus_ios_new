//
//  EventInfoTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import MapKit

final class EventInfoTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var starEndDayTime: UILabel!
        
    @IBOutlet weak var noNotificationView: UIView!
    @IBOutlet weak var hourNotificationView: UIView!
    @IBOutlet weak var dayNotificationView: UIView!
    @IBOutlet weak var customNotificationView: UIView!
    
    @IBOutlet weak var maybeView: UIView!
    @IBOutlet weak var goingView: UIView!
    @IBOutlet weak var notGoingView: UIView!
        
    @IBOutlet weak var noNotificationTitle: UILabel!
    @IBOutlet weak var hourNotificationTitle: UILabel!
    @IBOutlet weak var dayNotificationTitle: UILabel!
    @IBOutlet weak var customNotificationTitle: UILabel!
    
    @IBOutlet weak var maybeTitle: UILabel!
    @IBOutlet weak var goingTitle: UILabel!
    @IBOutlet weak var notGoingTitle: UILabel!
    
    @IBOutlet weak var attendeesUserCountLable: UILabel!
    @IBOutlet weak var attendeesUsersCollectionView: UICollectionView!
    @IBOutlet weak var attendeesUsersCollectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeRatingView: StarsRatingView!
    @IBOutlet weak var placeMapView: GMSMapView!
    
    //MARK: - Properties
    
    var event: Event! {
        didSet {
            if event != nil {
                self.fillUI(with: self.event)
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillUI(with: event)
    }
    
    //MARK: - UITableViewDelegate/UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 2 && (event.myAttendeeStatus != nil && event.myAttendeeStatus != .notGoing) ? 20 : CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return section == 2 && (event.myAttendeeStatus != nil && event.myAttendeeStatus != .notGoing) ? super.tableView(tableView, viewForHeaderInSection: section) : nil
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath {
        case IndexPath(row: 1, section: 0), IndexPath(row: 0, section: 4): return UITableView.automaticDimension
        case IndexPath(row: 0, section: 5): return event.place?.coordinates != nil ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        case IndexPath(row: 0, section: 3): return (event.myAttendeeStatus != nil && event.myAttendeeStatus != .notGoing) ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        default: return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 4 {
            openMapForPlace(for: self.event.place)
        }
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: self)
        
    }
    
}

fileprivate extension EventInfoTableViewController {
    
    @IBAction func attendeeStatusChanged(_ sender: UIButton) {
        
        func changeStatus(to status: Event.AttendeeStatus) {
            present(activityVC, animated: true, completion: nil)
            event.changeAttendeStatus(to: status) { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.event.get(with: { (success) in
                            self.fillUI(with: self.event)
                        })
                    }
                })
            }
        }
        
        if let newStatus = Event.AttendeeStatus(rawValue: sender.tag) {
            if let current = event.myAttendeeStatus {
                guard current != newStatus else { return }
                changeStatus(to: newStatus)
            } else {
                changeStatus(to: newStatus)
            }
        }
        
    }
    
    @IBAction func goingNotificationChanged(_ sender: UIButton) {
        
        func changeNotification(to status: Event.AttendeeNotification) {
            present(activityVC, animated: true, completion: nil)
            event.changeNotificationAttende(to: status) { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.event.get(with: { (success) in
                            self.fillUI(with: self.event)
                        })
                    }
                })
            }
        }
        
        if let newStatus = Event.AttendeeNotification(rawValue: sender.tag) {
            if let current = event.myAttendeeNotification {
                guard current != newStatus else { return }
                changeNotification(to: newStatus)
            } else {
                changeNotification(to: newStatus)
            }
        }
        
    }
    
    @IBAction func addCustomCallendarEvent(_ sender: UIButton) {
        let eventStore : EKEventStore = EKEventStore()
        eventStore.requestAccess(to: .event) { (granted, error) in
            if !granted {
                ShowAlert.showAlertForTwoActions(controller: self, firstAction: {
                    UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                }, firstActionTitle: "OK", title: NSLocalizedString("City+", comment: ""), message: NSLocalizedString("Ви повинні дозволити \"City+\" вносити дані у Ваш календар!", comment: ""))
            }
            let event = EKEvent(eventStore: eventStore)
            event.startDate = self.event.startDate ?? Date()
            event.endDate = self.event.endDate ?? Date()
            event.title = self.event.name
            event.notes = self.event.about
            event.location = self.event.address
            event.calendar = eventStore.defaultCalendarForNewEvents
                        
            DispatchQueue.main.async {
                let eventViewController = EKEventEditViewController()
                eventViewController.event = event
                eventViewController.eventStore = eventStore
                eventViewController.editViewDelegate = self
                eventViewController.delegate = self
                UINavigationBar.appearance().tintColor = .appTintColor
                self.present(eventViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func openPlaceDetail(_ sender: UITapGestureRecognizer) {
        let placeDetailVC: PlaceDetailContainerTableViewController = UIStoryboard(.Place).instantiateViewController()
        placeDetailVC.place = self.event.place
        show(placeDetailVC, sender: self)
    }
    
    func openMapForPlace(for place: Place?) {
        guard let coordinates = place?.coordinates else { return }
        let regionDistance:CLLocationDistance = 10000
        let regionSpan = MKCoordinateRegion.init(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = place?.name
        mapItem.openInMaps(launchOptions: options)
    }
    
    func fillUI(with event: Event) {
        guard isViewLoaded else { return }
        name.text = event.name
        about.text = event.about
        
        address.text = event.address
        
        if let myAttendee = event.myAttendeeStatus {
            updateButtons(for: myAttendee)
        }
        
        if let notificationSend = event.myAttendeeNotification {
            updateNotificationButtons(for: notificationSend)
        }
        
        var startDate = ""
        var startDateTime = ""
        if let firstDate = event.startDate {
            startDate = Date.monthDayNameDateFormater.string(from:  firstDate).capitalized
            startDateTime =  Date.longDayDateTimeFormater.string(from: firstDate).capitalized
        }
        if let secondDate = event.endDate {
            startDate += " - " + Date.monthDayNameDateFormater.string(from: secondDate).capitalized
            startDateTime += " - " + Date.longDayDateTimeFormater.string(from: secondDate).capitalized
        }
        
        starEndDayTime.text = startDateTime
        
        attendeesUserCountLable.text = ("\(event.totalAttendes)" + NSLocalizedString(" Відвідають або зацікавлені", comment: "зацікавлені в події"))
        attendeesUsersCollectionView.reloadData()
        attendeesUsersCollectionViewHeight.constant = event.attendees.count == 0 ? 0 : 50.0
        
        if let place = event.place, let coordinates = place.coordinates  {
            let newCamera = GMSCameraPosition.camera(withTarget: coordinates, zoom: 14)
            placeMapView.camera = newCamera
            
            let establishminMarker = EstablishmentMarker(with:place, coordinates: coordinates)
            establishminMarker.map = placeMapView
            
            placeNameLabel.text = place.name
            placeRatingView.rating = NSNumber(value: place.rating)
        }
        
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet([2]), with: .none)
        tableView.endUpdates()
    }
    
    func updateButtons(for status: Event.AttendeeStatus) {
        notGoingView.backgroundColor = status == .notGoing ? UIColor(hex: 0x3B4DC4) : UIColor(hex: 0xB0B8E8)
        goingView.backgroundColor = status == .going ? UIColor(hex: 0x3B4DC4) : UIColor(hex: 0xB0B8E8)
        maybeView.backgroundColor = status == .maybe ? UIColor(hex: 0x3B4DC4) : UIColor(hex: 0xB0B8E8)
    }
    
    func updateNotificationButtons(for status: Event.AttendeeNotification) {
        noNotificationView.backgroundColor = status == .notSend ? UIColor(hex: 0xB0B8E8) : UIColor(hex: 0xF3F4FB)
        hourNotificationView.backgroundColor = status == .oneHourBefore ? UIColor(hex: 0xB0B8E8) : UIColor(hex: 0xF3F4FB)
        dayNotificationView.backgroundColor = status == .oneDayBefore ? UIColor(hex: 0xB0B8E8) : UIColor(hex: 0xF3F4FB)
        
        noNotificationTitle.textColor = status == .notSend ? .white : UIColor(hex: 0x4d4d4d)
        hourNotificationTitle.textColor = status == .oneHourBefore ? .white : UIColor(hex: 0x4d4d4d)
        dayNotificationTitle.textColor = status == .oneDayBefore ? .white : UIColor(hex: 0x4d4d4d)
    }
    
}

extension EventInfoTableViewController: EKEventEditViewDelegate {
    
    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension EventInfoTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return event.attendees.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AttendeeUserCollectionViewCell.classForCoder()), for: indexPath) as! AttendeeUserCollectionViewCell
        cell.setUp(with: event.attendees[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let user = event.attendees[indexPath.row]
        let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
        profileVC.user = user
        profileVC.showMenuButton = false
        show(profileVC, sender: self)
    }
    
}
