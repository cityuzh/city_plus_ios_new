//
//  EventCommentsTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class EventCommentsTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    //MARK: - Properties
        
    var event: Event!
    lazy var itemList: PaginatedListModel<Comment> = {
        let list = PaginatedListModel<Comment>.init(path: String(format: Comment.Constants.eventCommentsListPathFormat, event.identifier ?? 0))
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placeholderView = TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode, customImage: #imageLiteral(resourceName: "img_empty state_no discussion.png"), customTitle: NSLocalizedString("Обговорення відсутні", comment: ""), customSubTitle: NSLocalizedString("Розпочніть обговорення!", comment: ""))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshContent(nil)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        itemList.load { (success) in
            sender?.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    func send(comment: Comment, attachment: UIImage?) {
        comment.isSending = true
        itemList.objects.insert(comment, at: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        tableView.endUpdates()
        comment.post(for: event) { (success) in
            if success {
                comment.isSending = false
                self.tableView.beginUpdates()
                self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self.tableView.endUpdates()
            } else {
                self.itemList.objects.remove(at: 0)
                self.tableView.beginUpdates()
                self.tableView.deleteRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self.tableView.endUpdates()
            }
        }
    }
    
    //MARK: - UITableViewDelegate / UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        placeholderMode = .noDataItems
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let comment = itemList[indexPath.row]
        let identifier = comment.attachment?.urlPath == nil ? String(describing: CommentTableViewCell.classForCoder()) : "Attached" + String(describing: CommentTableViewCell.classForCoder())
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CommentTableViewCell
        cell.setUp(with: comment)
        cell.profileHandler = { (sender) in
            if let index = tableView.indexPath(for: sender)?.row {
                let commentUser = self.itemList[index].sender
                let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                profileVC.user = commentUser
                profileVC.showMenuButton = false
                self.show(profileVC, sender: self)
            }
        }
        cell.attachmentHandler = { (sender) in
            if let index = tableView.indexPath(for: sender)?.row {
                let attachment = self.itemList[index].attachment
                let imagePreviewVC: ImagePreviewViewController = UIStoryboard(.Chats).instantiateViewController()
                imagePreviewVC.attachment = attachment
                self.show(imagePreviewVC, sender: self)
            }
        }
        cell.moreHandler = { (sender, actionCell) in
            if let parent = self.parent as? EventContainerViewController {
                parent.canBecome = false
                parent.inputBar.resignFirstResponder()
                parent.resignFirstResponder()
            }
            let deleteAction = UIAlertAction(title: NSLocalizedString("Видалити", comment: ""), style: .destructive, handler: { (action) in
                if let actionIndex = tableView.indexPath(for: actionCell) {
                    let actionComment = self.itemList.objects[actionIndex.row]
                    actionCell.alpha = 0.5
                    actionCell.isUserInteractionEnabled = false
                    actionComment.delete(for: self.event, with: { (success) in
                        actionCell.alpha = 1
                        actionCell.isUserInteractionEnabled = true
                        if success {
                            self.itemList.objects.remove(at: actionIndex.row)
                            tableView.beginUpdates()
                            tableView.deleteRows(at: [actionIndex], with: .automatic)
                            tableView.endUpdates()
                        }
                        if let parent = self.parent as? EventContainerViewController {
                            parent.canBecome = true
                            parent.becomeFirstResponder()
                        }
                    })
                }
            })
            let cancelAction = UIAlertAction(title: NSLocalizedString("Відмінити", comment: ""), style: .cancel, handler: nil)
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        cell.alpha = comment.isSending ? 0.5 : 1
        cell.isUserInteractionEnabled = comment.isSending ? false : true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
    }
    
}

extension EventCommentsTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        refreshControl?.endRefreshing()
        tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.moves.isEmpty {
            for move in changes.moves {
                tableView.moveRow(at: move.fromIndexPath, to: move.toIndexPath)
            }
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}

fileprivate extension EventCommentsTableViewController {
    
    
}
