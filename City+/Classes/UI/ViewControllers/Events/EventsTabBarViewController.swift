//
//  EventsTabBarViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 11/13/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class EventsTabBarViewController: UITabBarController {
    
    // MARK: - Properties
    
    var sessionManager: SessionManager?
    
    @IBOutlet weak var menuBarButtonItem: UIBarButtonItem!
    
    var currentNavigationController: UINavigationController?
    lazy var activityVC: ActivityViewController = {
        return ActivityViewController(viewController: self, sourceVeiw: nil)
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        
        setupGradientedNavigationBar()
        selectedIndex = Tabs.Date.rawValue
        tabBar.items?[0].image = #imageLiteral(resourceName: "ic_events.png").tint(with: UIColor(hex: 0xB0B8E8)).withRenderingMode(.alwaysOriginal)
        tabBar.items?[1].image = #imageLiteral(resourceName: "ic_pin").tint(with: UIColor(hex: 0xB0B8E8)).withRenderingMode(.alwaysOriginal)
        tabBar.unselectedItemTintColor = UIColor(hex: 0xB0B8E8)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        menuWillClose()
    }
    
}

extension EventsTabBarViewController: UITabBarControllerDelegate {
    
    
    
}

// MARK: - Public

extension EventsTabBarViewController {
    
    @IBAction func openMenu(_ sender: UIBarButtonItem) {
        AppDelegate.shared.mainContainerController?.openMenu()
    }
    
    func setNavigationTitle(with title: String) {
        navigationItem.title = title
    }
    
    func bottomNavigationController(_ bottomNavigationController: UINavigationController?, willShow viewController: UIViewController) {
        if let viewControllers = bottomNavigationController?.viewControllers {
            if  viewControllers.count >= 2 {
                navigationItem.leftBarButtonItems = []
                currentNavigationController = bottomNavigationController
            } else {
                navigationItem.leftBarButtonItems = nil
                currentNavigationController = nil
            }
        }
    }
    
    func removeCustomButtomIfNeeded(for bottomNavigationController: UINavigationController?) {
        if let viewControllers = bottomNavigationController?.viewControllers, viewControllers.count - 1 == 1 {
            navigationItem.leftBarButtonItems = nil
            currentNavigationController = nil
        }
    }
    
}

// MARK: - Private

private extension EventsTabBarViewController {
    
    @IBAction func customBackButtonTapped(_ sender: UIBarButtonItem) {
        currentNavigationController?.popViewController(animated: true)
    }
    
}

extension EventsTabBarViewController: MenuAppereance {
    
    func menuWillClose() {
        let button = UIButton()
        button.addTarget(self, action: #selector(openMenu(_:)), for: .touchUpInside)
        if let user = AppDelegate.shared.sessionManager.storedUser, UserModel.networkUser(with: user).isHasChanged {
            button.setImage(#imageLiteral(resourceName: "menuActive"), for: .normal)
        } else {
            button.setImage(#imageLiteral(resourceName: "burger_menu"), for: .normal)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        for vc in viewControllers ?? [] {
            if let nVC = vc as? UINavigationController {
                for vcn in nVC.viewControllers {
                    if let appereance = vcn as? MenuAppereance {
                        appereance.menuWillClose()
                    }
                }
            } else {
                if let appereance = vc as? MenuAppereance {
                    appereance.menuWillClose()
                }
            }
        }
    }
    
    func menuWillOpen() {
        for vc in viewControllers ?? [] {
            if let nVC = vc as? UINavigationController {
                for vcn in nVC.viewControllers {
                    if let appereance = vcn as? MenuAppereance {
                        appereance.menuWillOpen()
                    }
                }
            } else {
                if let appereance = vc as? MenuAppereance {
                    appereance.menuWillOpen()
                }
            }
        }
    }
    
}

extension EventsTabBarViewController {
    
    enum Tabs: Int {
        case Date = 0
        case List = 1
    }
    
}


