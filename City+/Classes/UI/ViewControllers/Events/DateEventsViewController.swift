//
//  DateEventsViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 11/6/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Nuke
import UIKit
import CoreImage

final class DateEventsViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var dateFilterContainer: UIView!
    @IBOutlet weak var dateFilterCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var lastOfsett: CGFloat = 0
    var isHidden: Bool {
        return dateFilterContainer.frame.origin.y == -dateFilterContainer.frame.height
    }
    var isNeedToShow: Bool = true
    var isHidding: Bool {
        return dateFilterContainer.frame.origin.y < 0 && dateFilterContainer.frame.origin.y > -dateFilterContainer.frame.height
    }
    
    var placeholderMode: TableViewPlaceholderView.PlaceholderMode = .loading {
        didSet {
            placeholderView.currentType = placeholderMode
            placeholderView.setNeedsDisplay()
        }
    }
    lazy var placeholderView: TableViewPlaceholderView = {
        return TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode)
    }()
    
    lazy var itemList: GroupedPaginatedListModel<Event> = {
        let list = GroupedPaginatedListModel<Event>.init(path: Event.Constants.dateEventPath)
        list.onRequestFailure = requestFailureHandler
        list.isReversed = true
        return list
    }()
    
    var selectedDate: Int? = Calendar.current.dateComponents([.day], from: Date()).day {
        didSet {
            var todatDateComponents = Calendar.current.dateComponents([.day, .weekday, .month, .year, .hour], from: Date())
            todatDateComponents.day = selectedDate
            
            let cellDate = Calendar.current.date(from: todatDateComponents)
            
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "YYYY-MM-dd"
            
            if let date = cellDate {
                if selectedDate != ((Calendar.current.dateComponents([.day], from: Date()).day ?? 0) + 7) {
                    itemList.queryParams["events_from"] = dateFormat.string(from: date) as AnyObject
                    itemList.queryParams["events_to"] = dateFormat.string(from: date) as AnyObject
                } else {
                    itemList.queryParams["events_from"] = dateFormat.string(from: date) as AnyObject
                    itemList.queryParams.removeValue(forKey: "events_to")
                }
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedDate = Calendar.current.dateComponents([.day], from: Date()).day
        for view in tableView.subviews {
            if let scroll = view as? UIScrollView {
                scroll.delegate = self
            }
        }
        loadAndReload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
        tableView.contentInset = !isHidden ? UIEdgeInsets(top: dateFilterContainer.bounds.height, left: 0, bottom: tableView.contentInset.bottom, right: 0) : UIEdgeInsets.zero
    }
    
    func loadAndReload() {
        self.placeholderMode = .loading
        itemList.load { (success) in
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
            self.isNeedToShow = false
        }
    }

}

//MARK - UICollectionViewDelegate / DataSource

extension DateEventsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var dateComponents = Calendar.current.dateComponents([.day, .weekday, .month, .year, .hour], from: Date())
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DateCollectionViewCell.classForCoder()), for: indexPath) as! DateCollectionViewCell
        dateComponents.day = dateComponents.day! + indexPath.item
        let cellDate = Calendar.current.date(from: dateComponents)
        if let date = cellDate, indexPath.item != 7 {
            cell.setup(with: date)
        } else {
            cell.setupWeekCell()
        }
        cell.backgroundColor = selectedDate != dateComponents.day ? UIColor.newLightBlueColor : UIColor.newAppTintColor
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var dateComponents = Calendar.current.dateComponents([.day, .weekday, .month, .year, .hour], from: Date())
        dateComponents.day = dateComponents.day! + indexPath.item
        selectedDate = dateComponents.day
        
        let cellRect = collectionView.collectionViewLayout.layoutAttributesForItem(at: indexPath)
        let frameHeight = (cellRect?.frame.origin.x ?? 0.0) - collectionView.contentOffset.x + (cellRect?.frame.width ?? 0)
        if frameHeight >= collectionView.bounds.width / 2 {
            let nextIndexPath = IndexPath(item: min(collectionView.numberOfItems(inSection: indexPath.section) - 1, indexPath.item + 1), section: indexPath.section)
            let nextCell = collectionView.collectionViewLayout.layoutAttributesForItem(at: nextIndexPath)
            if ((nextCell?.frame.origin.x ?? 0) - collectionView.contentOffset.x + (nextCell?.frame.width ?? 0)) > collectionView.bounds.width {
                collectionView.scrollToItem(at: nextIndexPath, at: .right, animated: true)
            }
        } else {
            let reviousIndexPath = IndexPath(item: max(0, indexPath.item - 1), section: indexPath.section)
            let previousCell = collectionView.collectionViewLayout.layoutAttributesForItem(at: reviousIndexPath)
            if (previousCell?.frame.origin.x ?? 0) < collectionView.bounds.origin.x {
                collectionView.scrollToItem(at: reviousIndexPath, at: .left, animated: true)
            }
        }
        
        collectionView.reloadData()
        itemList.objects.removeAll()
        itemList.groupedObjects.removeAll()
        tableView.reloadData()
        loadAndReload()
    }
    
}

extension DateEventsViewController: GroupedRefreshableViewControllerProtocol {
    
    var refreshControl: UIRefreshControl? {
        get {
            return UIRefreshControl()
        }
        set { }
    }
    
    
    func updateUI() {
        guard let _ = itemList.lastChanges else { return }
        itemList.lastChanges = nil
        tableView.reloadData()
    }
    
}

extension DateEventsViewController: UIScrollViewDelegate {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let actualOffset = scrollView.contentOffset.y + scrollView.contentInset.top
        if abs(actualOffset) >= scrollView.contentSize.height / 4, !isHidden, !isHidding, !isNeedToShow, lastOfsett < actualOffset {
            UIView.animate(withDuration: 0.3) {
                self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                self.dateFilterContainer.transform = CGAffineTransform(translationX: 0, y: -self.dateFilterContainer.bounds.height)
            }
        }
        lastOfsett = actualOffset
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if velocity.y <= -1, isHidden, !isHidding {
            isNeedToShow = true
            UIView.animate(withDuration: 0.3, animations: {
                self.tableView.contentInset = UIEdgeInsets(top: self.dateFilterContainer.bounds.height, left: 0, bottom: 0, right: 0)
                self.dateFilterContainer.transform = CGAffineTransform.identity
            }) { (completed) in
                if completed {
                    self.isNeedToShow = false
                }
            }
        }
    }
    
}

//MARK - UITableViewDataSource / UITableViewDelegate

extension DateEventsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.backgroundView = itemList.sectionCount == 0 ? placeholderView : nil
        return itemList.sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.rowsCountForSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = self.itemList[indexPath]
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewEventTableViewCell.classForCoder()), for: indexPath) as! NewEventTableViewCell
        
        if let url = URL(string: event.image?.urlPath ?? "") {
            let options = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "no-image.png"), transition: nil, failureImage: nil, failureImageTransition: nil, contentModes: nil)
            Nuke.loadImage(with: url, options: options, into: cell.photoImageView, progress: nil, completion: nil)
        }
        cell.setup(with: event)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = itemList[indexPath]
        let eventDetailVC: EventContainerViewController = UIStoryboard(.Events).instantiateViewController()
        eventDetailVC.event = event
        show(eventDetailVC, sender: self)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return itemList.headerTitleForSection(section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let title = self.tableView(tableView, titleForHeaderInSection: section) else {
            return nil
        }
        let height = self.tableView(tableView, heightForHeaderInSection: section)
        let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.bounds.width, height: height)))
        view.backgroundColor = .white
        let label = UILabel()
        label.text = title
        label.font = UIFont.monteserratBoldFont(ofSize: 16)
        label.textColor = UIColor(hex: 0x4D4D4D)
        label.frame = CGRect(origin: CGPoint(x: 15, y: 0), size: view.frame.size)
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(itemList.count - 1)
    }
    
}
