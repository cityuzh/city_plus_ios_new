//
//  EventContainerViewController.swift
//  City+
//
//  Created by Ivan Grab on 4/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import MobileCoreServices
import ALTextInputBar
import CoreGraphics

final class EventContainerViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bluredView: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var selectionIndicatorView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var year: UILabel!
    
    lazy var inputBar: ALTextInputBar = {
        let bar = ALTextInputBar()
        bar.showTextViewBorder = true
        bar.textViewCornerRadius = 15
        bar.textViewBorderColor = UIColor(hex: 0xB0B8E8)
        bar.backgroundColor = .white
        bar.defaultHeight = 50
        bar.delegate = self
        bar.horizontalSpacing = 0
        bar.rightView = rightBarView
//        bar.leftView = leftBarView
        bar.textView.placeholder = NSLocalizedString("Запитайте", comment: "")
        return bar
    }()
    
    lazy var leftBarView: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.addTarget(self, action: #selector(attachmendAction(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "clip"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "clipActive"), for: .selected)
        return button
    }()
    
    lazy var rightBarView: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.addTarget(self, action: #selector(sendAction(_:)), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "send").tint(with: UIColor(hex:0xB0B8E8)), for: .normal)
        return button
    }()
    
    var keyboardHeight: CGFloat = 50
    var lastInputHeight: CGFloat = 50
    
    
    //MARK: - Properties
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    var commentTableViewController: EventCommentsTableViewController?
    
    override var inputAccessoryView: UIView? {
        return canBecome ? inputBar : nil
    }
    
    var canBecome = false
    override var canBecomeFirstResponder: Bool {
        return canBecome
    }
    
    //MARK: - Properties
    
    var event: Event!
    var temComment: Comment?
    var attachment: UIImage?
    
    fileprivate var isCanChangeCategory = false
    var currentCategory: InfoCategory = .info
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage.setImage(with: event.image, placeholder: #imageLiteral(resourceName: "no-image"), using: .white, placeholderSettings: nil, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let date = event.startDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM"
            month.text = formatter.string(from: date)
            formatter.dateFormat = "d"
            day.text = formatter.string(from: date)
            formatter.dateFormat = "YYYY"
            year.text = formatter.string(from: date)
        }
        
        event.get { (success) in
            if success {
                for child in self.children {
                    if let info = child as? EventInfoTableViewController {
                        info.event = self.event
                    }
                }
                self.backgroundImage.setImage(with: self.event.image, placeholder: #imageLiteral(resourceName: "no-image"), using: .white, placeholderSettings: nil, completion: nil)
            }
        }
        
        setupClearNavigationBar()
        if !event.isMy {
            navigationItem.rightBarButtonItem = nil
            navigationItem.rightBarButtonItems = []
        }
        selectCurrentButton(for: currentCategory)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        preSetups()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destination = segue.destination as? EventInfoTableViewController {
            destination.event = event
        }
        if let destination = segue.destination as? EventCommentsTableViewController {
            destination.event = event
            commentTableViewController = destination
        }
        if let destination = segue.destination as? CreateEventTableViewController {
            destination.event = event
            destination.currentAction = .update
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        canBecome = false
        inputBar.resignFirstResponder()
        resignFirstResponder()
    }
    
    override func menuWillClose() {
        super.menuWillClose()
        if currentCategory == .comments {
            canBecome = true
            becomeFirstResponder()
        }
    }
    
}

// MARK: - UITextViewDelegate

extension EventContainerViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText: NSString = NSString(string: textView.text).replacingCharacters(in: range, with: text) as NSString
        let maxSize = CGSize(width: textView.frame.size.width - 16, height: CGFloat(MAXFLOAT))
        let textRect = newText.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15)], context: nil)
        for constraint in inputAccessoryView?.superview?.constraints ?? [] {
            if constraint.firstAttribute == .height{
                constraint.constant = CGFloat.minimum(20 + CGFloat.maximum(30, textRect.size.height + 10), Constants.maxInputHeight)
            }
        }
        return true
    }
    
}

extension EventContainerViewController: ALTextInputBarDelegate {
    
    func inputBarDidChangeHeight(height: CGFloat) {
        if lastInputHeight != height {
            DispatchQueue.main.async {
                self.lastInputHeight = height
            }
        }
    }
    
}

extension EventContainerViewController {
    
    enum InfoCategory: Int {
        case info = 0
        case comments
    }
 
    struct Constants {
        static let maxInputHeight: CGFloat = 100
        static let estimatedRowHeight: CGFloat = 140
    }
}

//MARK: - Input Methods
fileprivate extension EventContainerViewController {

    @IBAction func attachmendAction(_ sender: UIButton) {
        inputBar.resignFirstResponder()
        if let _ = attachment {
            // delet or change action
            let actionScheetVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let changeAction = UIAlertAction(title: NSLocalizedString("Змінити", comment: ""), style: .default) { (action) in
                self.startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
                    ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: nil)
                        }
                    })
                }, onCancelAction: nil)
            }
            let deleteAction = UIAlertAction(title: NSLocalizedString("Видалити", comment: ""), style: .destructive) { (action) in
                self.attachment = nil
                self.leftBarView.isSelected = false
            }
            let cancelAction = UIAlertAction(title: NSLocalizedString("Відмінити", comment: ""), style: .cancel, handler: nil)
            actionScheetVC.addAction(changeAction)
            actionScheetVC.addAction(deleteAction)
            actionScheetVC.addAction(cancelAction)
            present(actionScheetVC, animated: true, completion: nil)
        } else {
            startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    }
                })
            }, onCancelAction: nil)
        }
    }
    
    @IBAction func dublicate(_ sender: UIBarButtonItem) {
        let createEventVC: CreateEventTableViewController = UIStoryboard(.Events).instantiateViewController()
        if event.image?.image == nil {
            event.image?.image = backgroundImage.image
        }
        createEventVC.event = event
        createEventVC.currentAction = .create
        show(createEventVC, sender: self)
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        temComment = Comment()
        temComment?.sender = UserModel.networkUser(with: AppDelegate.shared.sessionManager.storedUser!)
        temComment?.isSending = true
        if !inputBar.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty  {
            temComment?.text = inputBar.text
        } else if attachment == nil {
            temComment = nil
            return
        }
        
        if let comment = temComment, let controller = commentTableViewController {
            controller.send(comment: comment, attachment: attachment)
            temComment = nil
            inputBar.text.removeAll()
            updateInputHeight(for: 0)
        }
        
    }
    
}

extension EventContainerViewController: PickingImage {
    
    func pickingDidCancel() {
        
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        if let image = thumbnail {
            attachment = image
            leftBarView.isSelected = true
        }
    }
    
}

fileprivate extension EventContainerViewController {
    
    @IBAction func categoryButtonTapper(_ sender: UIButton) {
        if let category = InfoCategory(rawValue: sender.tag) {
            scroll(to: category)
        }
    }
    
    func subscribeOnNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        
    }
    
    func unsubscribeFromNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - KeyboardAppearNotifications
    @objc func keyboardDidShow(_ notification: NSNotification) {
        if let height  = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
            keyboardHeight = height
        }
    }
    
    @objc func keyboardDidHide(_ notification: NSNotification) {
        if let _  = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
            keyboardHeight = 50
        }
    }
    
    func updateInputHeight(for textHeight: CGFloat) {
        for constraint in inputAccessoryView?.superview?.constraints ?? [] {
            if constraint.firstAttribute == .height{
                constraint.constant = CGFloat.minimum(20 + CGFloat.maximum(30, textHeight + 10), Constants.maxInputHeight)
            }
        }
    }
    
    func preSetups() {
        let gradiend = CAGradientLayer()
        gradiend.frame = bluredView.bounds
        gradiend.colors = [UIColor.clear.cgColor, UIColor.black.withAlphaComponent(0.6).cgColor]
        guard bluredView.layer.sublayers?.first(where: {$0.isKind(of: CAGradientLayer.classForCoder() )}) == nil else {
            return
        }
        bluredView.backgroundColor = .clear
        bluredView.layer.addSublayer(gradiend)
    }
    
    func selectCurrentButton(for category: InfoCategory) {
        
        infoButton.titleLabel?.font = UIFont.monteserratRegularFont(ofSize: 14)
        infoButton.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .normal)
        commentButton.titleLabel?.font = UIFont.monteserratRegularFont(ofSize: 14)
        commentButton.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .normal)
        
        switch category {
        case .info:
            infoButton.titleLabel?.font = UIFont.monteserratBoldFont(ofSize: 14)
            infoButton.setTitleColor(.white, for: .normal)
            UIView.animate(withDuration: 0.3) {
                self.selectionIndicatorView.transform = CGAffineTransform.identity
            }
        case .comments:
            commentButton.titleLabel?.font = UIFont.monteserratBoldFont(ofSize: 14)
            commentButton.setTitleColor(.white, for: .normal)
            UIView.animate(withDuration: 0.3) {
                let scale = CGAffineTransform(scaleX: self.selectionIndicatorView.bounds.width / self.commentButton.bounds.width, y: 1)
                let translate = CGAffineTransform(translationX: self.commentButton.frame.origin.x, y: 0)
                
                self.selectionIndicatorView.transform = scale.concatenating(translate)
            }
        }
    }
    
    func scroll(to category: InfoCategory, canChange: Bool = true) {
        var offset = CGPoint.zero
        switch category {
        case .comments:
            canBecome = true
            becomeFirstResponder()
            updateInputHeight(for: 0)
            offset = CGPoint(x: scrollView.bounds.width, y: 0)
        case .info:
            canBecome = false
            inputBar.resignFirstResponder()
            resignFirstResponder()
        }
        isCanChangeCategory = canChange
        scrollView.setContentOffset(offset, animated: true)
        selectCurrentButton(for: category)
        currentCategory = category
    }
    
}
