//
//  FilterUserTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/23/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class FilterUserTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var manButton: UIButton!
    @IBOutlet weak var womanButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    
    @IBOutlet weak var interestsField: UITextField!
    
    @IBOutlet weak var fromAgeField: UITextField!
    @IBOutlet weak var toAgeField: UITextField!
    
    var filter: UserFilter? {
        return (self.parent as? FilterUserContainerViewController)?.filter
    }
    
    //MARK: - Lifecycle
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let filter = filter {
            updateStates(for: filter)
        }
    }

    func updateStates(for filter: UserFilter) {
        
        fromAgeField.text = nil
        toAgeField.text = nil
        interestsField.text = nil
        
        switch filter.gender {
        case .male?: selectButton(manButton)
        case .female?: selectButton(womanButton)
        case .noDefine?: selectButton(otherButton)
        default: break
        }
        
        if let lowerAge = filter.lowerAge {
            fromAgeField.text = "\(lowerAge)"
        }
        if let upperAge = filter.upperAge {
            toAgeField.text = "\(upperAge)"
        }
        if let interest = filter.interest {
            interestsField.text = interest
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        interestsField.resignFirstResponder()
        toAgeField.resignFirstResponder()
        fromAgeField.resignFirstResponder()
        
    }

    
}

extension FilterUserTableViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case interestsField:
            filter?.interest = (textField.text ?? "") + string
        case fromAgeField:
            if let int = ((fromAgeField.text ?? "") + string).toNumber {
                filter?.lowerAge = int.intValue
            } else {
                filter?.lowerAge = nil
                return string == ""
            }
        case toAgeField:
            if let int = ((toAgeField.text ?? "") + string).toNumber {
                filter?.upperAge = int.intValue
                return true
            } else {
                filter?.upperAge = nil
                return string == ""
            }
        default: break
        }
        return true
    }
    
}

fileprivate extension FilterUserTableViewController {
    
    @IBAction func sexTapped(_ sender: UIButton) {
        clearButtonState()
        selectButton(sender)
        switch sender.tag {
        case 0: filter?.gender = .male
        case 1: filter?.gender = .female
        case 2: filter?.gender = .noDefine
        default: break
        }
    }
    
    func clearButtonState() {
        manButton.setTitleColor(UIColor.appTintColor, for: .normal)
        womanButton.setTitleColor(UIColor.appTintColor, for: .normal)
        otherButton.setTitleColor(UIColor.appTintColor, for: .normal)
        
        manButton.backgroundColor = .white
        womanButton.backgroundColor = .white
        otherButton.backgroundColor = .white
    }
    
    func selectButton(_ button: UIButton) {
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.appTintColor
    }
    
}

final class UserFilter {
    
    var lastName: String?
    var firstName: String?
    
    var lowerAge: Int?
    var upperAge: Int?
    
    var interest: String?
    
    var gender: UserModel.Gender?
    
    var isHaveValue: Bool {
        return !(lastName ?? "").isEmpty ||
        !(firstName ?? "").isEmpty ||
            lowerAge != nil ||
            upperAge != nil ||
            !(interest ?? "").isEmpty ||
        gender != nil
    }
}
