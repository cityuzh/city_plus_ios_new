//
//  FilterUserContainerViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/23/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class FilterUserContainerViewController: BaseViewController {

    var filter: UserFilter!
    
    var endSetupHandler: ((_ filter: UserFilter)->())?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func menuWillOpen() {
        super.menuWillOpen()
        if let child = children.first(where: { $0.isKind(of: FilterUserTableViewController.classForCoder()) }) as? FilterUserTableViewController {
            child.menuWillOpen()
        }
    }
    
}

fileprivate extension FilterUserContainerViewController {
    
    @IBAction func setTapped(_ sender: UIButton) {
        if let lower = filter.lowerAge, let upper = filter.upperAge {
            if lower > upper {
                ShowAlert.showInfoAlert(in: self, message: "Верхня межа віку повинна бути більшою від нижньої!")
            } else {
                endSetupHandler?(filter)
                dismiss(animated: true, completion: nil)
            }
        } else {
            endSetupHandler?(filter)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func resetTapped(_ sender: UIButton) {
        filter = UserFilter()
        if let child = children.first(where: { $0.isKind(of: FilterUserTableViewController.classForCoder()) }) as? FilterUserTableViewController {
            child.updateStates(for: filter)
        }
        endSetupHandler?(filter)
        dismiss(animated: true, completion: nil)
    }
    
}
