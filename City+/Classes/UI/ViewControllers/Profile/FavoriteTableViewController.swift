//
//  FavoriteTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/25/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class FavoriteTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var categoryContainer: UIView!
    @IBOutlet weak var favoriteCategory: UISegmentedControl!
    
    //MARK: - Properties
    
    lazy var placeList: PaginatedListModel<Place> = {
       let list = PaginatedListModel<Place>.init(path: Place.Constants.placeFavoriteListPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    lazy var vacancyList: PaginatedListModel<Vacancy> = {
        let list = PaginatedListModel<Vacancy>.init(path: Vacancy.Constants.vacancyFavoriteListPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    lazy var resumeList: PaginatedListModel<Resume> = {
        let list = PaginatedListModel<Resume>.init(path: Resume.Constants.resumeFavoriteListPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    var dataSourse: [Any] = []
    
    var currentCategory: FavoriteCategory = .place
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadAllLists()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
        refreshContent(nil)
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        switch currentCategory {
        case .place:
            placeList.load(completion: { (success) in
                sender?.endRefreshing()
                self.dataSourse = self.placeList.objects
                self.tableView.reloadData()
            })
        case .vacancy:
            vacancyList.load(completion: { (success) in
                sender?.endRefreshing()
                self.dataSourse = self.vacancyList.objects
                self.tableView.reloadData()
            })
        case .resume:
            resumeList.load(completion: { (success) in
                sender?.endRefreshing()
                self.dataSourse = self.resumeList.objects
                self.tableView.reloadData()
            })
        }
    }
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return categoryContainer
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        placeholderMode = .noDataItems
        tableView.backgroundView = dataSourse.count == 0 ? placeholderView : nil
        return dataSourse.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return currentCategory == .place ? 120.0 : UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch currentCategory {
        case .place:
            if (placeList.count - indexPath.row) <= (placeList.requestRowCount / 4) && placeList.state == .canLoadMore {
                placeList.loadMore(completion: { (success) in
                    if success {
                        self.dataSourse = self.vacancyList.objects
                        self.tableView.reloadData()
                    }
                })
            }
        case .vacancy:
            if (vacancyList.count - indexPath.row) <= (vacancyList.requestRowCount / 4) && vacancyList.state == .canLoadMore {
                vacancyList.loadMore(completion: { (success) in
                    if success {
                        self.dataSourse = self.vacancyList.objects
                        self.tableView.reloadData()
                    }
                })
            }
        case .resume:
            if (resumeList.count - indexPath.row) <= (resumeList.requestRowCount / 4) && resumeList.state == .canLoadMore {
                resumeList.loadMore(completion: { (success) in
                    if success {
                        self.dataSourse = self.resumeList.objects
                        self.tableView.reloadData()
                    }
                })
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch currentCategory {
        case .place:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PlaceTableViewCell.classForCoder()), for: indexPath) as! PlaceTableViewCell
            if let place = dataSourse[indexPath.row] as? Place {
                cell.setUp(with: place)
                cell.callHandler = { (sender) in
                    if let phone = URL(string: "tel://" + (place.phones.first != nil ? (place.phones.first!.hasPrefix("38") ? "+" + place.phones.first! : place.phones.first)! : "").replacingOccurrences(of: " ", with: "")) {
                        UIApplication.shared.open(phone, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
                cell.siteHandler = { (sender) in
                    if let url = URL(string: place.site ?? "") {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
            return cell
        case .vacancy:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: VacancyTableViewCell.classForCoder()), for: indexPath) as! VacancyTableViewCell
            if let vacancy = dataSourse[indexPath.row] as? Vacancy {
                cell.setUp(with: vacancy)
            }
            return cell
        case .resume:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ResumeTableViewCell.classForCoder()), for: indexPath) as! ResumeTableViewCell
            if let resume = dataSourse[indexPath.row] as? Resume {
                cell.setUp(with: resume)
                cell.profileHandler = { (cell) in
                    if let cellIndexPath = tableView.indexPath(for: cell) {
                        let item = self.resumeList[cellIndexPath.row]
                        let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
                        profileVC.user = item.owner
                        profileVC.showMenuButton = false
                        self.show(profileVC, sender: self)
                    }
                }
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch currentCategory {
        case .place:
            if let place = self.dataSourse[indexPath.row] as? Place {
                let placeDetail: PlaceDetailContainerTableViewController = UIStoryboard(.Place).instantiateViewController()
                placeDetail.place = place
                show(placeDetail, sender: self)
            }
        case .vacancy:
            if let vacancy = self.dataSourse[indexPath.row] as? Vacancy {
                let vacancyDetail: JobDetailTableViewController = UIStoryboard(.Job).instantiateViewController()
                vacancyDetail.vacancy = vacancy
                show(vacancyDetail, sender: self)
            }
        case .resume:
            if let resume = self.dataSourse[indexPath.row] as? Resume {
                let resumeDetail: ResumeDetailTableViewController = UIStoryboard(.Job).instantiateViewController()
                resumeDetail.resume = resume
                show(resumeDetail, sender: self)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
 
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let action = UITableViewRowAction(style: .destructive, title: NSLocalizedString("Видалити", comment: "")) { (action, editedIndexPath) in
            switch self.currentCategory {
            case .place:
                if let place = self.dataSourse[editedIndexPath.row] as? Place {
                    self.present(self.activityVC, animated: true, completion: nil)
                    place.removeFromFavorite(with: { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                self.dataSourse.remove(at: editedIndexPath.row)
                                self.placeList.objects.remove(at: editedIndexPath.row)
                                self.tableView.beginUpdates()
                                self.tableView.deleteRows(at: [editedIndexPath], with: .automatic)
                                self.tableView.endUpdates()
                            }
                        })
                    })
                }
            case .vacancy:
                if let vacancy = self.dataSourse[editedIndexPath.row] as? Vacancy {
                    self.present(self.activityVC, animated: true, completion: nil)
                    vacancy.removeFromFavorite(with: { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                self.dataSourse.remove(at: editedIndexPath.row)
                                self.vacancyList.objects.remove(at: editedIndexPath.row)
                                self.tableView.beginUpdates()
                                self.tableView.deleteRows(at: [editedIndexPath], with: .automatic)
                                self.tableView.endUpdates()
                            }
                        })
                    })
                }
            case .resume:
                if let resume = self.dataSourse[editedIndexPath.row] as? Resume {
                    self.present(self.activityVC, animated: true, completion: nil)
                    resume.removeFromFavorite(with: { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                self.dataSourse.remove(at: editedIndexPath.row)
                                self.resumeList.objects.remove(at: editedIndexPath.row)
                                self.tableView.beginUpdates()
                                self.tableView.deleteRows(at: [editedIndexPath], with: .automatic)
                                self.tableView.endUpdates()
                            }
                        })
                    })
                }
            }
        }
        return [action]
    }
    
}

fileprivate extension FavoriteTableViewController {
    
    @IBAction func changeCategory(_ sender: UISegmentedControl) {
        if let category = FavoriteCategory(rawValue: sender.selectedSegmentIndex) {
            currentCategory = category
            switch category {
            case .place:
                self.dataSourse = self.placeList.objects
            case .vacancy:
                self.dataSourse = self.vacancyList.objects
            case .resume:
                self.dataSourse = self.resumeList.objects
            }
            self.tableView.reloadData()

        }
    }
    
    func loadAllLists() {
        vacancyList.load()
        resumeList.load()
    }
    
}

extension FavoriteTableViewController {
    
    enum FavoriteCategory: Int {
        case place = 0
        case vacancy
        case resume
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
