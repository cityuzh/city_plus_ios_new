//
//  ProfileTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 2/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import MobileCoreServices

final class ProfileTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var photosPageControl: UIPageControl!
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var status: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var about: UITextView!
    @IBOutlet weak var interests: UITextView!
    @IBOutlet weak var birthday: UITextField!
    
    @IBOutlet var textViews: [UITextView]!
    @IBOutlet var textFields: [UITextField]!
    
    @IBOutlet weak var infoLable: UILabel!
    
    @IBOutlet weak var cancelRequestButton: UIButton! {
        didSet {
            cancelRequestButton.layer.cornerRadius = cancelRequestButton.bounds.height / 2
        }
    }
    @IBOutlet weak var acceptRequestButton: UIButton! {
        didSet {
            acceptRequestButton.layer.cornerRadius = acceptRequestButton.bounds.height / 2
            acceptRequestButton.layer.borderColor = UIColor.appTintColor.cgColor
            acceptRequestButton.layer.borderWidth = 1.0
        }
    }
    
    @IBOutlet weak var friendButton: UIButton! {
        didSet {
            friendButton.layer.cornerRadius = friendButton.bounds.height / 2
        }
    }
    @IBOutlet weak var chatButton: UIButton! {
        didSet {
            chatButton.layer.cornerRadius = chatButton.bounds.height / 2
            chatButton.layer.borderColor = UIColor.appTintColor.cgColor
            chatButton.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var blockButton: UIButton! {
        didSet {
            blockButton.layer.cornerRadius = blockButton.bounds.height / 2
            blockButton.layer.borderColor = UIColor.red.cgColor
            blockButton.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var photoButton: UIButton! {
        didSet {
            photoButton.layer.cornerRadius = photoButton.bounds.height / 2
        }
    }
    @IBOutlet weak var makeAvatarButton: UIButton! {
        didSet {
            makeAvatarButton.layer.cornerRadius = makeAvatarButton.bounds.height / 2
        }
    }
    @IBOutlet weak var removePhotoButton: UIButton! {
        didSet {
            removePhotoButton.layer.cornerRadius = removePhotoButton.bounds.height / 2
        }
    }
    
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    @IBOutlet weak var privateSwitch: UISwitch!
    
    @IBOutlet weak var saveBarItem: UIBarButtonItem!
    @IBOutlet weak var menuBarItem: UIBarButtonItem!
    @IBOutlet weak var skipBarItem: UIBarButtonItem!
    @IBOutlet weak var editBarItem: UIBarButtonItem!
    
    //MARK: - Properties
    
    var mediaTypes: [String] {
        return [kUTTypeImage as String]
    }
    
    var currentAction: ProfileAction = .profile    
    
    var user: UserModel? = UserModel.networkUser(with: AppDelegate.shared.sessionManager.storedUser!)
    
    var isAllowEditing = false
    
    lazy var datePicker: UIDatePicker = {
       let picker = UIDatePicker()
        picker.maximumDate = Date()
        picker.datePickerMode = .date
        picker.date = Date(timeIntervalSince1970: 946760046)
        picker.addTarget(self, action: #selector(birthdayDidChange(_:)), for: .valueChanged)
        return picker
    }()
    
    lazy var birthdayAccessory: UIView = {
        let accessory = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        accessory.backgroundColor = UIColor.groupTableViewBackground

        let cancelButton = UIButton(type: .roundedRect)
        cancelButton.setTitle(NSLocalizedString("Відмінити", comment: ""), for: .normal)
        cancelButton.setTitleColor(UIColor.darkGray, for: .normal)
        cancelButton.contentHorizontalAlignment = .left
        cancelButton.addTarget(self, action: #selector(birthdayDidCancel(_:)), for: .touchUpInside)

        let saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle(NSLocalizedString("Добре", comment: ""), for: .normal)
        saveButton.setTitleColor(UIColor.darkGray, for: .normal)
        saveButton.contentHorizontalAlignment = .right
        saveButton.addTarget(self, action: #selector(birthdayDidSet(_:)), for: .touchUpInside)

        accessory.addSubview(saveButton)
        accessory.addSubview(cancelButton)

        let views = ["save": saveButton, "cancel": cancelButton]
        let metrics = ["inset": 15]
        var constraints = [NSLayoutConstraint]()

        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false

        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-inset-[cancel]-[save]-inset-|", options: [], metrics: metrics, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[cancel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[save]-|", options: [], metrics: nil, views: views)

        NSLayoutConstraint.activate(constraints)

        return accessory
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker = UIImagePickerController()
        imagePicker?.allowsEditing = true
        imagePicker?.mediaTypes = [kUTTypeImage as String]
        
        setupGradientedNavigationBar()
        let flowLayout = photosCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        photosCollectionView.collectionViewLayout = flowLayout
        
        if currentAction == .registration {
            isAllowEditing = true
        }
        
        if let user = user {
            fillUI(with: user)
        }
        
        refreshContent(nil)
        
        for subview in photosCollectionView.subviews {
            if let scrollView = subview as? UIScrollView {
                scrollView.delegate = self
            }
        }
        
        birthday.inputView = datePicker
        birthday.inputAccessoryView = birthdayAccessory
        
        tableView.contentInset = UIEdgeInsets.init(top: -2, left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets.init(top: -2, left: 0, bottom: 0, right: 0)
        
        if (!(user?.isMe ?? false) && currentAction == .profile) || !showMenuButton {
            navigationItem.leftBarButtonItem = nil
            navigationItem.leftBarButtonItems = []
        }
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        user?.getProfile(with: { (success) in
            sender?.endRefreshing()
            if success {
                if let user = self.user {
                    self.fillUI(with: user)
                }
            }
        })
    }
    

    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let user = user else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
        switch indexPath {
        case IndexPath(row: 1, section:0): return (user.isMe && currentAction == .profile) || currentAction == .registration ? 0 : 60
        case IndexPath(row: 2, section:0): return (user.isMe && currentAction == .profile) || !user.hasOutcomingRequest && !user.hasIncomingRequest ? 0 : 60
        case IndexPath(row: 3, section:0): return (user.isMe && currentAction == .profile) || (!user.isBlockedMe && !user.isBlockedByMe) || currentAction == .registration ? 0 : 60
        case IndexPath(row: 4, section:0): return user.isMe && currentAction == .profile ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        case IndexPath(row: 5, section:0): return user.isMe && currentAction == .profile ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        case IndexPath(row: 6, section:0): return user.isMe && currentAction == .profile ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        case IndexPath(row: 7, section:0): return user.isMe && currentAction == .profile ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        case IndexPath(row: 8, section:0): return user.isMe && currentAction == .profile ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        case IndexPath(row: 7, section:1): return about.contentSize.height + 40
        case IndexPath(row: 8, section:1): return interests.contentSize.height + 40
        case IndexPath(row: 5, section:1): return user.isMe ? 0 : 0
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }    
    
    //MARK: - IBActions
    
    @objc func birthdayDidChange(_ sender: UIDatePicker) {
        birthday.text = Date.shortDateFormater.string(from: sender.date)
    }
    
    @objc func birthdayDidSet(_ sender: UIButton) {
        birthday.resignFirstResponder()
        birthday.text = Date.shortDateFormater.string(from: datePicker.date)
        user?.birthday = datePicker.date
    }
    
    @objc func birthdayDidCancel(_ sender: UIButton) {
        birthday.resignFirstResponder()
        if let birthdayDate = user?.birthday {
            birthday.text = Date.shortDateFormater.string(from: birthdayDate)
        }
    }
    
    @IBAction func editTapped(_ sender: UIBarButtonItem) {
        isAllowEditing = true
        setupControlls()
        navigationItem.rightBarButtonItems = [saveBarItem]
        navigationItem.leftBarButtonItems = [skipBarItem]
    }
    
    @IBAction func saveTapped(_ sender: UIBarButtonItem) {

        [email, firstName, lastName, birthday, interests, status, about].forEach({ $0?.resignFirstResponder() })
        
        func succesfylUpdate() {
            isAllowEditing = false
            setupControlls()
        }
        
        user?.firstName = firstName.text
        user?.lastName = lastName.text
        user?.status = status.text
        user?.additionalInfo = about.text
        user?.interests = interests.text
        user?.email = email.text
        do {
            try user?.validate()
            if currentAction == .profile {
                present(activityVC, animated: true, completion: nil)
                sessionManager.updateMyProfile(with: user!, completion: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            succesfylUpdate()
                        }
                    })
                })
            } else {
                present(activityVC, animated: true, completion: nil)
                sessionManager.updateMyProfile(with: user!, completion: { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            succesfylUpdate()
                            UIWindow.replaceStoryboard(name: .Main)
                        }
                    })
                })
            }
        } catch {
            let error = error as! ValueValidationError
            ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
        }
        
    }
    
    @IBAction func cancelTapped(_ sender: UIBarButtonItem) {
        isAllowEditing = false
        navigationItem.leftBarButtonItems = []
        setupControlls()
    }
    
    @IBAction func makeAvatarTapped(_ sender: UIButton) {
        let confirmAction = UIAlertAction(title: NSLocalizedString("Так", comment: ""), style: .default) { (action) in
            if let imageForRemoving = self.user?.photos[self.photosPageControl.currentPage] {
                self.present(self.activityVC, animated: true, completion: nil)
                imageForRemoving.makeAvatar(with: { (success) in
                    self.sessionManager.getMyProfile()
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            imageForRemoving.isAvatar = true
                            sender.isHidden = true
                            self.photosCollectionView.reloadData()
                        }
                    })
                })
            }
        }
        ShowAlert.showAlertContrller(with: .alert, title: "City+", message:  NSLocalizedString("Зробити це фото аватаром вашого профілю?", comment: ""), actions: [confirmAction], in: self)
    }
    
    @IBAction func friendAction(_ sender: UIButton) {
        user?.onRequestFailure = requestFailureHandler
        if user?.isFriend ?? false {
            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Ви дійсно бажаєте видалити користувача з друзів?", comment: ""), okHandler: { (action) in
                self.present(self.activityVC, animated: true, completion: nil)
                self.user?.deleteFriend(with: { (success) in
                    if success {
                        self.activityVC.dismiss(animated: true, completion: {
                            self.refreshContent(nil)
                        })
                    }
                })
            })
        } else {
            self.present(self.activityVC, animated: true, completion: nil)
            self.user?.sendFriendRequest(with: { (success) in
                if success {
                    self.activityVC.dismiss(animated: true, completion: {
                        self.refreshContent(nil)
                    })
                }
            })
        }
    }
    
    @IBAction func acceptRequestAction(_ sender: UIButton) {
        guard let user = user else { return }
        user.onRequestFailure = requestFailureHandler
        present(activityVC, animated: true, completion: nil)
        user.acceptFriendRequest { (success) in
            if success {
                self.activityVC.dismiss(animated: true, completion: {
                    self.refreshContent(nil)
                })
            }
        }
    }
    
    @IBAction func rejectRequestAction(_ sender: UIButton) {
        guard let user = user else { return }
        user.onRequestFailure = requestFailureHandler
        present(activityVC, animated: true, completion: nil)
        if user.hasOutcomingRequest {
            user.cancelFriendRequest { (success) in
                if success {
                    self.activityVC.dismiss(animated: true, completion: {
                        self.refreshContent(nil)
                    })
                }
            }
        } else {
            user.rejectFriendRequest { (success) in
                if success {
                    self.activityVC.dismiss(animated: true, completion: {
                        self.refreshContent(nil)
                    })
                }
            }
        }
    }
    
    @IBAction func chatAction(_ sender: UIButton) {
        let newChat = Chat()
        newChat.user = user
        newChat.onRequestFailure = requestFailureHandler
        present(activityVC, animated: true, completion: nil)
        newChat.create(for: user?.identifier ?? 0, with: { (success) in
            if success {
                self.activityVC.dismiss(animated: true, completion: {
                    let chatVC: ChatRoomTableViewController = UIStoryboard(.Chats).instantiateViewController()
                    chatVC.chat = newChat
                    self.show(chatVC, sender: self)
                })
            }
        })
    }
    
    @IBAction func removePhotoAction(_ sender: UIButton) {
        if let imageForRemoving = user?.photos[photosPageControl.currentPage] {
            present(activityVC, animated: true, completion: nil)
            imageForRemoving.deletePhoto(with: { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.user?.photos.remove(at: self.photosPageControl.currentPage)
                        self.photosCollectionView.reloadData()
                        self.setupControlls()
                    }
                })
            })
        }
    }
    
    @IBAction func addPhotoAction(_ sender: UIButton) {
        startPicking(isCameraOnly: false, isAllowEditing: true, deniedAccess: {
            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Для користування камерою, спочатку дайте дозвіл у налаштуваннях! Відкрити налаштування?", comment: ""), okHandler: { (action) in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: nil)
                }
            })
        }, onCancelAction: nil)
    }
    
    @IBAction func blockAction(_ sender: UIButton) {
        present(activityVC, animated: true, completion: nil)
        user?.onRequestFailure = requestFailureHandler
        if user?.isBlockedByMe ?? false {
            user?.deleteFromBlackList(with: { (success) in
                if success {
                    self.user?.isBlockedByMe = false
                    self.activityVC.dismiss(animated: true, completion: {
                        self.fillUI(with: self.user!)
                    })
                }
            })
        } else {
            user?.addToBlackList(with: { (success) in
                if success {
                    self.activityVC.dismiss(animated: true, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            })
        }
    }
    
    @IBAction func privateStateChanged(_ sender: UISwitch) {
        sender.isEnabled = false
        user?.changePrivateState(to: sender.isOn, with: { (success) in
            if !success {
                sender.isOn = !sender.isOn
            }
            sender.isEnabled = true
        })
    }
    
    @IBAction func genderChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: user?.gender = .male
        case 1: user?.gender = .female
        default: user?.gender = .noDefine
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ForumTableViewController {
            destination.currentList = .my
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        textViews.forEach({ $0.resignFirstResponder() })
        textFields.forEach({ $0.resignFirstResponder() })
    }

}

//MARK: - Private

extension ProfileTableViewController {
    
    enum ProfileAction {
        case registration
        case profile
    }
    
    func fillUI(with user: UserModel) {
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        photosCollectionView.reloadData()
        
        var color = UIColor.appTintColor
        var image = #imageLiteral(resourceName: "add_friends")
        if user.isFriend {
            color = .red
            image = #imageLiteral(resourceName: "delete_user_ic")
        }
        
        friendButton.isHidden = user.hasIncomingRequest || user.hasOutcomingRequest
        acceptRequestButton.isHidden = user.hasOutcomingRequest
        var titleBlock = NSLocalizedString("Блокувати", comment: "")
        if user.isBlockedByMe {
            titleBlock = NSLocalizedString("Розблокувати", comment: "")
        }
        blockButton.setTitle(titleBlock, for: .normal)
        
        var alert = NSLocalizedString("Ви заблоковані цим користувачем! Тому немаєте можливості комунікувати!", comment: "")
        if user.isBlockedByMe {
            alert = NSLocalizedString("Ви добавили цього користувача в чорний список! Для звязку з ним спочатку розблокуйте!", comment: "")
        }
        infoLable.text = alert
        
        friendButton.setImage(image, for: .normal)
        friendButton.backgroundColor = color
        
        firstName.text = user.firstName
        lastName.text = user.lastName
        birthday.text = user.birthday != nil ? Date.shortDateFormater.string(from: user.birthday!) : nil
        
        email.text = user.email
        
        switch user.gender {
        case .male: genderSegmentedControl.selectedSegmentIndex = 0
        case .female: genderSegmentedControl.selectedSegmentIndex = 1
        default: genderSegmentedControl.selectedSegmentIndex = 2
        }
        
        privateSwitch.isOn = user.isPrivate
        status.text = user.status
        
        if let aboutString = user.additionalInfo {
            about.text = aboutString
            about.sizeToFit()
            about.textColor = UIColor.black
        }
        
        if let interestString = user.interests {
            interests.text = interestString
            interests.sizeToFit()
            interests.textColor = UIColor.black
        }
        
        setupControlls()
    }
    
    func setupControlls() {
        
        navigationItem.rightBarButtonItems = (user?.isMe ?? false) ? isAllowEditing ? [saveBarItem] : [editBarItem] : []
        
        if currentAction == .registration {
            navigationItem.leftBarButtonItems = [UIBarButtonItem()]
            navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        } else {
            if (!(user?.isMe ?? false) && currentAction == .profile) || !showMenuButton {
                navigationItem.leftBarButtonItem = nil
                navigationItem.leftBarButtonItems = []
            } else {
                navigationItem.leftBarButtonItems = [menuBarItem]
            }
        }
        
        genderSegmentedControl.isUserInteractionEnabled = isAllowEditing
        photoButton.isHidden = !isAllowEditing
        removePhotoButton.isHidden = (user?.photos.count ?? 0) == 0  || !isAllowEditing
        
        if isAllowEditing {
            refreshControl = nil
        } else {
            refreshControl = UIRefreshControl()
            refreshControl?.addTarget(self, action: #selector(refreshContent(_:)), for: .primaryActionTriggered)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        let blocked = user?.isBlockedMe ?? false || user?.isBlockedByMe ?? false
        chatButton.isEnabled = !blocked
        friendButton.isEnabled = !blocked
        chatButton.alpha = blocked ? 0.5 : 1.0
        friendButton.alpha = blocked ? 0.5 : 1.0
    }
    
}

//MARK: - UITextFieldDelegate

extension ProfileTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return isAllowEditing
    }
    
    func textViewDidChange(_ textView: UITextView) {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return (textView.text + text).count <= Configuration.TextBounds.profileDescriptionMaxCount
    }
    
}

//MARK: - PikingImage

extension ProfileTableViewController: PickingImage {
    
    func pickingDidCancel() {
        
    }
    
    func didSelectMedia(with image: Attachment?, thumbnail: UIImage?) {
        if let newImage = thumbnail {
            present(activityVC, animated: true, completion: nil)
            Avatar.uploadAvatar(newImage, completion: { (success, uploadedAvatar) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        if let newAvatar = uploadedAvatar {
                            if let first = self.user!.photos.first, first.identifier == nil {
                                self.user?.photos.remove(at: 0)
                                self.user?.photos.append(newAvatar)
                            } else {
                                self.user?.photos.append(newAvatar)
                            }
                            self.photosCollectionView.reloadData()
                            self.photosCollectionView.scrollToItem(at: IndexPath(item: self.user!.photos.count - 1, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
                            self.setupControlls()
                        }
                    }
                })
            })
        }
    }
    
}

//MARK: - UITextViewDelegate

extension ProfileTableViewController: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return isAllowEditing
    }
    
}


extension ProfileTableViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photosPageControl.numberOfPages = (user?.photos.count ?? 0) > 0 ? user!.photos.count : 1
        return (user?.photos.count ?? 0) > 0 ? user!.photos.count : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ProfilePhotoCollectionViewCell.classForCoder()), for: indexPath) as! ProfilePhotoCollectionViewCell
        cell.setUp(with: (user?.photos.count ?? 0) > 0 ? user!.photos[indexPath.row] : nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView.isKind(of: UICollectionView.classForCoder()) {
            removePhotoButton.isHidden = true
            makeAvatarButton.isHidden = true
        }
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.isKind(of: UICollectionView.classForCoder()) {
            removePhotoButton.isHidden = !isAllowEditing
            makeAvatarButton.isHidden = user?.photos[photosPageControl.currentPage].isAvatar ?? true || !(user?.isMe ?? false)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isKind(of: UICollectionView.classForCoder()) {
            let width = scrollView.bounds.width
            let currentOffset = scrollView.contentOffset.x
            let fullPart: Int = Int(currentOffset / width)

            if currentOffset / width - CGFloat(fullPart) > 0.5 {
                photosPageControl.currentPage = fullPart + 1
            } else {
                photosPageControl.currentPage = fullPart
            }
        }
    }
    
}

