//
//  FindUserTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/22/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class FindUserTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    //MARK: - Properties
    
    var listType: ListType = .users
    var cellsType: CellType = .friends
    
    var startChatHandler: ((_ sender: FindUserTableViewController, _ user: UserModel)->())?
    
    lazy var itemList: PaginatedListModel<UserModel> = {
        let list = PaginatedListModel<UserModel>(path: listType == .users ? UserModel.Constants.usersListPath : UserModel.Constants.friendsListPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                itemList.queryParams["full_name"] = searchText as AnyObject
            } else {
                itemList.queryParams.removeValue(forKey: "full_name")
                if listType == .users && cellsType != .chats {
                    self.placeholderMode = .enterSearchText
                }
            }
        }
    }
    
    var disposeBag = DisposeBag()
    
    var currentFilter = UserFilter()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if listType != .users && cellsType != .chats {
            navigationItem.titleView = segmentedControl
            navigationItem.rightBarButtonItem = nil
            navigationItem.rightBarButtonItems = []
        } else if cellsType == .chats {
            navigationItem.rightBarButtonItem = nil
            navigationItem.rightBarButtonItems = []
        } else {
            self.placeholderMode = .enterSearchText
        }
        setuRXComponents()
        updatePlaceholder(for: listType)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
        navigationItem.title = listType == .users ? NSLocalizedString("Знайти Людину", comment: "") : NSLocalizedString("Друзі", comment: "")
    }
    
    override func refreshContent(_ sender: UIRefreshControl?) {
        self.placeholderMode = .loading
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
        }
    }
    
    func showRequests() {
        if listType != .users {
            navigationItem.titleView = segmentedControl
        }
        itemList.path = UserModel.Constants.friendRequestsListPath
        segmentedControl.selectedSegmentIndex = 1
        refreshContent(nil)
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        requestMoreDataIfNeededForItemAtIndex(index: indexPath.row)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userModel = itemList[indexPath.row]
        if listType == .requests || listType == .myRequests {
            let identifier = FriendRequestTableViewCell.cellIdentifier
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FriendRequestTableViewCell
            cell.setUp(with: userModel, isMy: listType == .myRequests)
            cell.cancelHandler = { (button, actionCell) in
                guard let actionIndexPath = tableView.indexPath(for: actionCell) else { return }
                let request = self.itemList[actionIndexPath.row]
                self.present(self.activityVC, animated: true, completion: nil)
                request.onRequestFailure = self.requestFailureHandler
                actionCell.isUserInteractionEnabled = false
                actionCell.alpha = 0.5
                if self.listType == .myRequests {
                    request.cancelFriendRequest(with: { (success) in
                        actionCell.isUserInteractionEnabled = true
                        actionCell.alpha = 1.0
                        if success {
                            self.activityVC.dismiss(animated: true, completion: {
                                _ = self.itemList.removeAtIndex(index: actionIndexPath.row)
                                tableView.beginUpdates()
                                tableView.deleteRows(at: [actionIndexPath], with: .automatic)
                                tableView.endUpdates()
                            })
                        }
                    })
                } else {
                    request.rejectFriendRequest(with: { (success) in
                        actionCell.isUserInteractionEnabled = true
                        actionCell.alpha = 1.0
                        if success {
                            self.activityVC.dismiss(animated: true, completion: {
                                _ = self.itemList.removeAtIndex(index: actionIndexPath.row)
                                tableView.beginUpdates()
                                tableView.deleteRows(at: [actionIndexPath], with: .automatic)
                                tableView.endUpdates()
                            })
                        }
                    })
                }
            }
            cell.acceptHandler = { (button, actionCell) in
                guard let actionIndexPath = tableView.indexPath(for: actionCell) else { return }
                let request = self.itemList[actionIndexPath.row]
                self.present(self.activityVC, animated: true, completion: nil)
                request.onRequestFailure = self.requestFailureHandler
                actionCell.isUserInteractionEnabled = false
                actionCell.alpha = 0.5
                request.acceptFriendRequest(with: { (success) in
                    actionCell.isUserInteractionEnabled = true
                    actionCell.alpha = 1.0
                    if success {
                        self.activityVC.dismiss(animated: true, completion: {
                            _ = self.itemList.removeAtIndex(index: actionIndexPath.row)
                            tableView.beginUpdates()
                            tableView.deleteRows(at: [actionIndexPath], with: .automatic)
                            tableView.endUpdates()
                        })
                    }
                })
            }
            return cell
        } else {
            var identifier = UserTableViewCell.cellIdentifier
            if cellsType == .chats {
                identifier = "CHAT" + UserTableViewCell.cellIdentifier
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! UserTableViewCell
            cell.setUp(with: userModel)
            cell.friendHandler = { (sender) in
                if userModel.isFriend {
                    ShowAlert.showInfoAlertWithCancel(in: self, message: NSLocalizedString("Ви дійсно бажаєте видалити користувача з друзів?", comment: ""), okHandler: { (action) in
                        self.present(self.activityVC, animated: true, completion: nil)
                        userModel.deleteFriend(with: { (success) in
                            self.activityVC.dismiss(animated: true, completion: {
                                if success {
                                    userModel.isFriend = false
                                    sender.isSelected = false
                                    if self.listType == .friends {
                                        self.itemList.objects.remove(at: indexPath.row)
                                        self.tableView.beginUpdates()
                                        self.tableView.deleteRows(at: [indexPath], with: .automatic)
                                        self.tableView.endUpdates()
                                    }
                                }
                            })
                        })
                    })
                } else {
                    self.present(self.activityVC, animated: true, completion: nil)
                    userModel.sendFriendRequest(with: { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                sender.isEnabled = false
                                sender.alpha = 0.5
                            }
                        })
                    })
                }
            }
            cell.chatHandler = { (sender) in
                if let cellIndexPath = tableView.indexPath(for: sender) {
                    let user = self.itemList[cellIndexPath.row]
                    self.present(self.activityVC, animated: true, completion: nil)
                    let newChat = Chat()
                    newChat.user = user
                    newChat.create(for: user.identifier ?? 0, with: { (success) in
                        self.activityVC.dismiss(animated: true, completion: {
                            if success {
                                let chatVC: ChatRoomTableViewController = UIStoryboard(.Chats).instantiateViewController()
                                chatVC.chat = newChat
                                self.show(chatVC, sender: self)
                            }
                        })
                    })
                }
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = itemList[indexPath.row]
        if cellsType == .friends {
            let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
            profileVC.user = user
            show(profileVC, sender: self)
        } else {
            startChatHandler?(self, user)
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: self)
        if let destination = segue.destination as? FilterUserContainerViewController {
            destination.filter = currentFilter
            destination.endSetupHandler = { (filter) in
                self.currentFilter = filter
                if filter.gender != nil {
                    self.itemList.queryParams["sex"] = filter.gender!.rawValue as AnyObject
                } else {
                    self.itemList.queryParams.removeValue(forKey: "sex")
                }
                
                if let from = filter.upperAge {
                    self.itemList.queryParams["age_lt"] = from as AnyObject
                } else {
                    self.itemList.queryParams.removeValue(forKey: "age_lt")
                }
                
                if let to = filter.lowerAge {
                    self.itemList.queryParams["age_gt"] = to as AnyObject
                } else {
                    self.itemList.queryParams.removeValue(forKey: "age_gt")
                }
                
                if let interests = filter.interest {
                    self.itemList.queryParams["interests"] = interests as AnyObject
                } else {
                    self.itemList.queryParams.removeValue(forKey: "interests")
                }
                self.refreshContent(nil)
            }
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        searchBar.resignFirstResponder()
    }
    
}

fileprivate extension FindUserTableViewController {
    
    @IBAction func segmenteChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            listType = .friends
            itemList.path = UserModel.Constants.friendsListPath
        } else if sender.selectedSegmentIndex == 2 {
            listType = .myRequests
            itemList.path = UserModel.Constants.myRequestsListPath
        } else {
            listType = .requests
            itemList.path = UserModel.Constants.friendRequestsListPath
        }
        updatePlaceholder(for: listType)
        refreshContent(nil)
        itemList.lastTask?.cancel()
        itemList.objects.removeAll()
        self.tableView.reloadData()
    }
    
    func setuRXComponents() {
        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.milliseconds(Int(Configuration.Constants.searchTextDelay * 1000)), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { (text) in
                if !(self.searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    self.searchText = text.element ?? ""
                } else {
                    self.searchText = ""
                }
                self.refreshContent(nil)
            }.disposed(by: disposeBag)
        searchBar.rx.searchButtonClicked
            .bind(onNext: {
                self.searchBar.endEditing(true)
                if !(self.searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    self.searchText = self.searchBar.text ?? ""
                } else {
                    self.searchText = ""
                }
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .bind(onNext: {
                self.searchBar.endEditing(true)
                self.searchBar.text?.removeAll()
                self.searchText = ""
                self.itemList.queryParams.removeValue(forKey: "title")
                self.itemList.objects.removeAll()
                self.tableView.reloadData()
                self.refreshContent(nil)
            })
            .disposed(by: disposeBag)
    }
    
    func updatePlaceholder(for listType: ListType) {
        var mainMessage = ""
        var subMessage = ""
        switch listType {
        case .friends:
            mainMessage = NSLocalizedString("Друзі відсутні", comment: "")
            subMessage = NSLocalizedString("Знайдіть друзів в меню «Пошук людей»", comment: "")
        case .requests:
            mainMessage = NSLocalizedString("Заявки відсутні", comment: "")
            subMessage = NSLocalizedString("Ваші друзі ще вас не знайшли в додатку, тож додайте їх самі!", comment: "")
        case .myRequests:
            mainMessage = NSLocalizedString("Заявки відсутні", comment: "")
            subMessage = NSLocalizedString("Знайдіть друзів та додайте", comment: "")
        case .users:
            mainMessage = NSLocalizedString("Користувачі відсутні", comment: "")
            subMessage = NSLocalizedString("Змініть дані користувача в пошуку", comment: "")
        }
        setuRXComponents()
        placeholderView.removeFromSuperview()
        
        placeholderView = TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode, customImage: #imageLiteral(resourceName: "img_empty state_no friends"), customTitle: mainMessage, customSubTitle: subMessage)
    }
    
}

extension FindUserTableViewController: RefreshableViewControllerProtocol {
    
    func endRefreshingWithCompletion(completion: () -> Void) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    func updateUI() {
        guard let changes = itemList.lastChanges else { return }
        
        itemList.lastChanges = nil
        guard !changes.moves.isEmpty else {
            tableView.reloadData()
            return
        }
        
        tableView.beginUpdates()
        
        if !changes.deletedIndexPaths.isEmpty {
            var section: Int
            for (_, indexPath) in changes.deletedIndexPaths.enumerated() {
                section = indexPath.section
                var deletedIndexPath = [IndexPath]()
                var rowsInSection =  tableView.numberOfRows(inSection: section)
                for (_, secondIndexPath) in changes.deletedIndexPaths.enumerated() {
                    if section == secondIndexPath.section && !deletedIndexPath.contains(secondIndexPath as IndexPath) {
                        rowsInSection -= 1
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                        
                        if rowsInSection == 0 {
                            tableView.deleteSections(IndexSet(integer: section), with: .automatic)
                        }
                    } else {
                        tableView.deleteRows(at: [secondIndexPath as IndexPath], with: .automatic)
                        deletedIndexPath.append(secondIndexPath as IndexPath)
                    }
                }
            }
            tableView.deleteRows(at: changes.deletedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.insertedIndexPaths.isEmpty {
            if let lastVisibleCell = tableView.indexPathsForVisibleRows?.last {
                let sectionForInsert = NSMutableIndexSet()
                for (_, indexPath) in changes.insertedIndexPaths.enumerated() {
                    if indexPath.section > lastVisibleCell.section {
                        if !sectionForInsert.contains(indexPath.section) {
                            sectionForInsert.add(indexPath.section)
                        }
                    }
                }
                tableView.insertSections(sectionForInsert as IndexSet, with: .automatic)
            }
            tableView.insertRows(at: changes.insertedIndexPaths as [IndexPath], with: .automatic)
        }
        
        if !changes.updatedIndexPaths.isEmpty {
            tableView.reloadRows(at: changes.updatedIndexPaths as [IndexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
}

extension FindUserTableViewController {
    
    enum ListType {
        case users
        case friends
        case requests
        case myRequests
    }
    
    enum CellType {
        case friends
        case chats
    }
    
}
