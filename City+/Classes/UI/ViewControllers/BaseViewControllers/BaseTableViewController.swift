//
//  BaseTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 2/13/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
    
    
    // MARK: - Properties
    
    lazy var alphaView: UIView = {
        let view = UIView()
        view.frame = self.view.bounds
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    lazy var activityVC: ActivityViewController = {
        return ActivityViewController(viewController: self, sourceVeiw: nil)
    }()
    
    var placeholderMode: TableViewPlaceholderView.PlaceholderMode = .loading {
        didSet {
            placeholderView.currentType = placeholderMode
            placeholderView.setNeedsDisplay()
        }
    }
    lazy var placeholderView: TableViewPlaceholderView = {
        return TableViewPlaceholderView(frame: tableView.bounds, with: placeholderMode)
    }()
    
    var sessionManager: SessionManager {
        return AppDelegate.shared.sessionManager
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var showMenuButton = true
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeOnNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupHandlers()
        menuWillClose()
    }
    
    deinit {
        unsubscribeOnNotifications()
    }
    
    func setupHandlers() {
        
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
    }
    
    @IBAction func openMenu(_ sender: UIBarButtonItem?) {
        AppDelegate.shared.mainContainerController?.openMenu()
    }
    
    // MARK: - Autorefreshable
    
    @IBAction func refreshContent(_ sender: UIRefreshControl?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            sender?.endRefreshing()
        }
    }
    
    func menuWillClose() {
        if showMenuButton {
            let button = UIButton()
            button.addTarget(self, action: #selector(openMenu(_:)), for: .touchUpInside)
            if let user = sessionManager.storedUser, UserModel.networkUser(with: user).isHasChanged {
                button.setImage(#imageLiteral(resourceName: "menuActive"), for: .normal)
            } else {
                button.setImage(#imageLiteral(resourceName: "burger_menu"), for: .normal)
            }
            if let viewControllers = navigationController?.viewControllers {
                if  viewControllers.count >= 2 || !sessionManager.isLoggedIn {
                    navigationItem.leftBarButtonItem = nil
                } else {
                    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)                    
                }
            }
        }
    }
    
    func menuWillOpen() {
        
    }
    
}

// MARK: - Public

extension BaseTableViewController {
    
    // MARK: - AlphaView supporting
    
    func showAlphaView() {
        if let rootVC = AppDelegate.shared.window?.rootViewController {
            rootVC.view.addSubview(alphaView)
            alphaView.layer.zPosition = 100
        }
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.alphaView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }
    }
    
    func hideAlphaView() {
        UIView.animate(withDuration: 0.3, animations: {  [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.alphaView.backgroundColor = UIColor.clear
        }) { [weak self] (finished) in
            guard let strongSelf = self else { return }
            strongSelf.alphaView.removeFromSuperview()
        }
    }
    
    
   
}

extension BaseTableViewController: MenuAppereance {
    
    
    
}

// MARK: - Private

private extension BaseTableViewController {
    
    func subscribeOnNotifications() {
        
        
    }
    
    func unsubscribeOnNotifications() {
        
        
    }
    
}
