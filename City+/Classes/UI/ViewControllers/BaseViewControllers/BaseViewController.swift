//
//  BaseViewController.swift
//  City+
//
//  Created by Ivan Grab on 2/13/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit


class BaseViewController: UIViewController {
    
    // MARK: - Properties
    
    lazy var alphaView: UIView = {
        let view = UIView()
        view.frame = self.view.bounds
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    var sessionManager: SessionManager {
        return AppDelegate.shared.sessionManager
    }
    var imageProvider: ImageProvider?
    
    lazy var activityVC: ActivityViewController = {
        return ActivityViewController(viewController: self, sourceVeiw: nil)
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var showMenuButton = true
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sessionManager.onRequestFailure = self.requestFailureHandler
        subscribeOnNotifications()
        menuWillClose()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    deinit {
        unsubscribeOnNotifications()
    }
    
    func showAlphaView() {
        if let rootVC = AppDelegate.shared.window?.rootViewController {
            rootVC.view.addSubview(alphaView)
            alphaView.layer.zPosition = 100
        }
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.alphaView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }
    }
    
    func hideAlphaView() {
        UIView.animate(withDuration: 0.3, animations: {  [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.alphaView.backgroundColor = UIColor.clear
        }) { [weak self] (finished) in
            guard let strongSelf = self else { return }
            strongSelf.alphaView.removeFromSuperview()
        }
    }
    
    @IBAction func openMenu(_ sender: UIBarButtonItem) {
        AppDelegate.shared.mainContainerController?.openMenu()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
    }
    
    @objc func menuWillClose() {
        if showMenuButton {
            let button = UIButton()
            button.addTarget(self, action: #selector(openMenu(_:)), for: .touchUpInside)
            if let user = sessionManager.storedUser, UserModel.networkUser(with: user).isHasChanged {
                button.setImage(#imageLiteral(resourceName: "menuActive"), for: .normal)
            } else {
                button.setImage(#imageLiteral(resourceName: "burger_menu"), for: .normal)
            }
            if let viewControllers = navigationController?.viewControllers {
                if  viewControllers.count >= 2 {
                    navigationItem.leftBarButtonItem = nil
                } else {
                    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
                    
                }
            }
        }
    }
    
    @objc func menuWillOpen() {
        
    }
    
}

extension BaseViewController: MenuAppereance {
    
    
    
}

// MARK: - Private

private extension BaseViewController {
    
    func subscribeOnNotifications() {
        
    }
    
    func unsubscribeOnNotifications() {
        
    }
    
}

// MARK: - CustomizableUI


