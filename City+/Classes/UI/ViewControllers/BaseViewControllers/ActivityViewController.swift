//
//  ActivityViewController.swift
//  City+
//
//  Created by Ivan Grab on 2/13/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

struct SettingsConstants {
    static let dismisButtonApearTimeInterval: Double = 7
}

class ActivityViewController: UIViewController {
    
    
    //MARK: - Constanst
    
    
    
    //MARK: - Calculating Properties
    
    
    
    //MARK: - Properties
    
    fileprivate var activity: UIActivityIndicatorView! = UIActivityIndicatorView(style: .whiteLarge)
    fileprivate var sourceView: UIView? = nil
    fileprivate var sourceViewFrame: CGRect = CGRect.zero
    fileprivate var background: CAShapeLayer = CAShapeLayer()
    fileprivate var sourceViewController: UIViewController? {
        didSet {self.setNeedsStatusBarAppearanceUpdate()}
    }
    fileprivate var dismisstTimer: Timer?
    fileprivate var dismissButton: UIButton?
    fileprivate var timer: Timer?
    //MARK: - Public Static
    
    static func showForViewController(controller: UIViewController?, sourceView: UIView?) -> ActivityViewController {
        let activityViewControler = ActivityViewController(viewController: controller, source: sourceView, withDismissButton: true)
        
        return activityViewControler
    }
    
    static func activityViewController(sourceView: UIView?) -> ActivityViewController {
        let activityViewController = ActivityViewController(viewController: nil, sourceVeiw: sourceView)
        return activityViewController
    }
    
    static func activityViewController(controller: UIViewController?, sourceView: UIView?, withDismissButton: Bool) -> ActivityViewController {
        let activityViewControoler = ActivityViewController(viewController: controller, source: sourceView, withDismissButton: withDismissButton)
        
        return activityViewControoler
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.addSublayer(background)
        background.fillRule = CAShapeLayerFillRule.evenOdd
        background.fillColor = UIColor.black.withAlphaComponent(0.6).cgColor
        self.view.addSubview(activity)
        activity.startAnimating()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.timer =  Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ActivityViewController.updatePathIfNeeded), userInfo: nil, repeats: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let button = dismissButton {
            self.view.addSubview(button)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let button = dismissButton {
            if let _ = button.superview {
                button.removeFromSuperview()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.timer?.invalidate()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    
    convenience init(viewController: UIViewController?, sourceVeiw source: UIView? ) {
        self.init()
        self.sourceView = source
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.0)
        self.transitioningDelegate = self
        self.sourceViewController = viewController
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve

        dismisstTimer = Timer.scheduledTimer(timeInterval: SettingsConstants.dismisButtonApearTimeInterval, target: self, selector: #selector(ActivityViewController.addDismissButton), userInfo: nil, repeats: false)

//        NSRunLoop.mainRunLoop().performSelector("selector", target: self, argument: nil, order: 0, modes: [NSRunLoopCommonModes])
    }
    
    convenience init(viewController: UIViewController?, source: UIView?, withDismissButton: Bool) {
        self.init()
        
        self.sourceView = source
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.0)
        self.transitioningDelegate = self
        self.sourceViewController = viewController
        self.modalPresentationStyle = .overCurrentContext
        
        if withDismissButton {
            addDismissButton()
        }
        
//        NSRunLoop.mainRunLoop().performSelector("selector", target: self, argument: nil, order: 0, modes: [NSRunLoopCommonModes])
    }
    
//    func selector() {
//        print("runLoop")
//    }
    
    @objc func addDismissButton() {
        dismisstTimer?.invalidate()
        
        self.dismissButton = UIButton(frame: CGRect(x: 30, y:  20, width: 60, height: 40))
        self.dismissButton?.setTitle("Cancel", for: .normal)
        self.dismissButton?.contentHorizontalAlignment = .center
        self.dismissButton?.setTitleColor(UIColor.white, for: .normal)
        self.dismissButton?.addTarget(self, action: #selector(ActivityViewController.dismisButtonTap), for: .touchUpInside)
        self.view.addSubview(self.dismissButton!)
    }
    
    @objc func dismisButtonTap(_ sender: UIButton?) {
        if let button = sender {
            button.removeFromSuperview()
        }
        self.dismiss(animated: true) { () -> Void in
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updatePathIfNeeded()
        
    }
    
    @objc func updatePathIfNeeded() {
        let path = UIBezierPath(rect: self.view.bounds)
        
        if let source = sourceView {
            let layerFrame = self.view.convert(source.layer.presentation()!.frame, from: source.superview!)
            if layerFrame.equalTo(sourceViewFrame) {
                return
            }
            sourceViewFrame = layerFrame
            path.usesEvenOddFillRule = true
            var subPath: UIBezierPath
            let layer = source.layer
            
            if layer.cornerRadius != 0 {
                subPath = UIBezierPath(roundedRect: layerFrame, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: layer.cornerRadius, height: layer.cornerRadius))
                path.append(subPath)
            } else {
                subPath = UIBezierPath(rect: layer.bounds)
            }
        }
        let anim = CABasicAnimation(keyPath: "path")
        anim.fromValue = self.background.path
        anim.toValue = path.cgPath
        anim.duration = 0.3
        self.background.add(anim, forKey: "path")
        self.background.path = path.cgPath
        self.activity.layer.position = self.activityViewPosition()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
//        if sourceViewController¿  {
//            return sourceViewController!.preferredStatusBarStyle()
//        }
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        if let sourseVC = sourceViewController {
            return sourseVC.prefersStatusBarHidden
        }
        return true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        if let sourseVC = sourceViewController {
            return sourseVC.preferredStatusBarUpdateAnimation
        }
        return .slide
    }
    
    //MARK: - Custom Accessors
    
    
    
    //MARK: - IBActions
    
    
    
    //MARK: - Public
    
    
    
    //MARK: - Private
    
    private func activityViewPosition() -> CGPoint {
        var position = self.view.center
        if let view = sourceView {
            var sourcePosition = self.presentingViewController?.view.convert(self.sourceView!.center, from: view.superview)
            sourcePosition = self.view.convert(sourcePosition!, from: self.presentingViewController?.view)
            position = sourcePosition!
        }
        return position
    }
    
}

extension ActivityViewController: UIViewControllerTransitioningDelegate {
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentAnimator()
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }
}

internal class PresentAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let destController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as! ActivityViewController
        
        let containerView = transitionContext.containerView
        let destView = transitionContext.view(forKey: UITransitionContextViewKey.to)
        
        containerView.addSubview(destView!)
        
        let duration = self.transitionDuration(using: transitionContext)
        
        let fillColorAnimation = CABasicAnimation(keyPath: "fillColor")
        fillColorAnimation.fromValue = UIColor.clear.cgColor
        fillColorAnimation.toValue = UIColor(white: 0, alpha: 0.8).cgColor
        fillColorAnimation.duration = duration*2
        
        destController.background.add(fillColorAnimation, forKey: "fillColor")
        
        destController.activity.bounds = CGRect.zero
        
        
        
        let activityScale = CABasicAnimation(keyPath: "transform.scale")
        activityScale.fromValue = 0
        activityScale.toValue = 1
        activityScale.duration = duration/2
        

//        if destController.dismissButton¿ {
//            destController.dismissButton?.layer.addAnimation(opacityAnimation, forKey: "opacity")
//        }
        
        destController.activity.layer.add(activityScale, forKey: "scale")

        UIView.animate(withDuration: duration, animations: { () -> Void in
            destView?.backgroundColor = UIColor(white: 0, alpha: 0.3)
            destController.background.fillColor = UIColor(white: 0, alpha: 0.8).cgColor
            }) { (compleeted: Bool) -> Void in
                transitionContext.completeTransition(true)
        }
        
    }
    
    func animationEnded(transitionCompleted: Bool) {
        
    }
}

internal class DismissAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let sourceController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as! ActivityViewController
        

        let duration = self.transitionDuration(using: transitionContext)

        let fillColorAnimation = CABasicAnimation(keyPath: "fillColor")
        fillColorAnimation.fromValue = UIColor(white: 0, alpha: 0.8).cgColor
        fillColorAnimation.toValue = UIColor.clear.cgColor
        fillColorAnimation.duration = duration
        sourceController.background.add(fillColorAnimation, forKey: "fillColor")
        
        
        let shapeOpacity = CABasicAnimation(keyPath: "opacity")
        shapeOpacity.fromValue = 1
        shapeOpacity.toValue = 1
        shapeOpacity.duration = duration
        sourceController.background.add(shapeOpacity, forKey: "opacity")
        sourceController.background.opacity = 0
        
        
        let animationDelegate = BasicAnimationDelegate()
        
        let activityScale = CABasicAnimation(keyPath: "transform.scale")
        activityScale.fromValue = 1
        activityScale.toValue = 0
        activityScale.duration = duration/2
        activityScale.delegate = animationDelegate as? CAAnimationDelegate
        
        sourceController.activity.layer.opacity = 0
        
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = 1
        opacityAnimation.toValue = 1
        opacityAnimation.duration = activityScale.duration
        
        if let button = sourceController.dismissButton {
           button.layer.add(opacityAnimation, forKey: "opacity")
        }
        
        sourceController.activity.layer.add(activityScale, forKey: "scale")
        sourceController.activity.layer.add(opacityAnimation, forKey: "opacity")
        
        
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            sourceController.view.backgroundColor = UIColor(white: 0, alpha: 0)
            }) { (compleeted: Bool) -> Void in
                transitionContext.completeTransition(true)
                
        }
    }
    
    func animationEnded(transitionCompleted: Bool) {
        
    }
}
