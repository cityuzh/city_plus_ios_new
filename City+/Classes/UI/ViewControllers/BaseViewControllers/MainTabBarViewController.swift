//
//  MainTabBarViewController.swift
//  City+
//
//  Created by Ivan Grab on 2/13/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    
    // MARK: - Properties
    
    var sessionManager: SessionManager?
    
    @IBOutlet weak var menuBarButtonItem: UIBarButtonItem!
    
    
    var currentNavigationController: UINavigationController?
    lazy var activityVC: ActivityViewController = {
        return ActivityViewController(viewController: self, sourceVeiw: nil)
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        setupGradientedNavigationBar()
        selectedIndex = Tabs.List.rawValue
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        menuWillClose()
    }
    
    func showCategoryFiler() {
        let filerVC: PlaceFilterContainerViewController = UIStoryboard(.Place).instantiateViewController()
        filerVC.filter = PlaceDataSource.shared.categories
        filerVC.modalPresentationStyle = .overCurrentContext
        filerVC.endSetupHandler = { (filter) in
            if filter != PlaceDataSource.shared.categories {
                PlaceDataSource.shared.itemList.objects.removeAll()
                PlaceDataSource.shared.categories = filter
                for controller in self.viewControllers ?? [] {
                    if let selected = self.selectedViewController, controller == selected {
                        if let nVC = controller as? UINavigationController, let mapVC = nVC.viewControllers.first as? PlaceMapViewController {
                            mapVC.reloadItemListAndUpdate()
                        } else if let nVC = controller as? UINavigationController, let listVC = nVC.viewControllers.first as? PlaceListTableViewController {
                            listVC.tableView.reloadData()
                            listVC.refreshContent(nil)
                        }
                    }
                }
            }
        }
        present(filerVC, animated: true, completion: nil)
    }
    
}

extension MainTabBarViewController: UITabBarControllerDelegate {
    
    
    
}

// MARK: - Public

extension MainTabBarViewController {
    
    @IBAction func openMenu(_ sender: UIBarButtonItem) {
        AppDelegate.shared.mainContainerController?.openMenu()
    }
    
    func setNavigationTitle(with title: String) {
        navigationItem.title = title
    }
    
    func bottomNavigationController(_ bottomNavigationController: UINavigationController?, willShow viewController: UIViewController) {
        if let viewControllers = bottomNavigationController?.viewControllers {
            if  viewControllers.count >= 2 {
                navigationItem.leftBarButtonItems = []
                currentNavigationController = bottomNavigationController
            } else {
                navigationItem.leftBarButtonItems = nil
                currentNavigationController = nil
            }
        }
    }
    
    func removeCustomButtomIfNeeded(for bottomNavigationController: UINavigationController?) {
        if let viewControllers = bottomNavigationController?.viewControllers, viewControllers.count - 1 == 1 {
            navigationItem.leftBarButtonItems = nil
            currentNavigationController = nil
        }
    }
    
}

// MARK: - Private

private extension MainTabBarViewController {
    
//    func setupTabBarItems() {
//        let itemSelectedView = UIView(frame: CGRect(x: tabBar.frame.origin.x, y: tabBar.frame.origin.y, width: tabBar.frame.width / 3, height: 56))
//        let imageView = UIImageView(frame: CGRect(x: itemSelectedView.frame.origin.x, y: itemSelectedView.frame.height - 6, width: tabBar.frame.width / 3 , height: 6))
//        imageView.backgroundColor = UIColor.appTintColor
//        itemSelectedView.addSubview(imageView)
//        tabBar.selectionIndicatorImage = makeImage(from: itemSelectedView)
//    }
    
    func makeImage(from view: UIView) -> UIImage {
        UIGraphicsBeginImageContext(view.bounds.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    @IBAction func customBackButtonTapped(_ sender: UIBarButtonItem) {
        currentNavigationController?.popViewController(animated: true)
    }
    
}

extension MainTabBarViewController: MenuAppereance {
    
    func menuWillClose() {
        let button = UIButton()
        button.addTarget(self, action: #selector(openMenu(_:)), for: .touchUpInside)
        if let user = AppDelegate.shared.sessionManager.storedUser, UserModel.networkUser(with: user).isHasChanged {
            button.setImage(#imageLiteral(resourceName: "collection_place"), for: .normal)
        } else {
            button.setImage(#imageLiteral(resourceName: "collection_place"), for: .normal)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        for vc in viewControllers ?? [] {
            if let nVC = vc as? UINavigationController {
                for vcn in nVC.viewControllers {
                    if let appereance = vcn as? MenuAppereance {
                        appereance.menuWillClose()
                    }
                }
            } else {
                if let appereance = vc as? MenuAppereance {
                    appereance.menuWillClose()
                }
            }
        }
    }
    
    func menuWillOpen() {
        for vc in viewControllers ?? [] {
            if let nVC = vc as? UINavigationController {
                for vcn in nVC.viewControllers {
                    if let appereance = vcn as? MenuAppereance {
                        appereance.menuWillOpen()
                    }
                }
            } else {
                if let appereance = vc as? MenuAppereance {
                    appereance.menuWillOpen()
                }
            }
        }
    }
    
}

extension MainTabBarViewController {
    
    enum Tabs: Int {
        case Placec = 0
        case List
    }
    
}


