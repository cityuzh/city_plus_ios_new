//
//  MainContainerViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/3/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class MainContainerViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var backMenuContainer: UIView!
    @IBOutlet weak var contentContainer: UIView!
    var dismissContainer: UIView?
    
    
    @IBOutlet weak var menuTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var panGesture: UIPanGestureRecognizer!
    
    //MARK: - Properties
    var menuViewController: MenuTableViewController?
    var childViewController: UIViewController?
    
    fileprivate var menuGradient: CAGradientLayer?
    
    fileprivate var isAnimating = false
    fileprivate var isMenuOpened = false
    var isMenuOpen: Bool {
        return menuTrailingConstraint.constant > 0
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.mainContainerController = self
        
        let closeTapGesture = UITapGestureRecognizer(target: self, action: #selector(closeDidTap(_:)))
        dismissContainer = UIView(frame: view.bounds)
        dismissContainer?.backgroundColor = .clear
        dismissContainer?.isUserInteractionEnabled = true
        dismissContainer?.addGestureRecognizer(closeTapGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isMenuOpened {
            openMenu(true)
            isMenuOpened = true
        }
    }
    
    @objc func closeDidTap(_ sender: UITapGestureRecognizer) {
        closeMenu()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? MenuTableViewController {
            menuViewController = destination
        }
    }
}


extension MainContainerViewController {
    
    func showChat(for id: Int) {
        if let chatNVC = UIStoryboard(.Chats).instantiateInitialViewController() as? UINavigationController {
            setNewChild(chatNVC)
            if let chatListVC = chatNVC.viewControllers.first as? DialogsTableViewController {
                chatListVC.openChat(with: id)
            }
        }
    }
    
    func showFriendRequest(for user: UserModel) {
        if let usersNVC = UIStoryboard(.Profile).instantiateViewController(withIdentifier: "NavigationFindUserTableViewController") as? UINavigationController {
            setNewChild(usersNVC)
            if let usersListVC = usersNVC.viewControllers.first as? FindUserTableViewController {
                usersListVC.listType = .requests
                usersListVC.showRequests()
            }
        }
    }
    
    func showTopicDetail(for topic: Topic) {
        if let forumNVC = UIStoryboard(.Forum).instantiateViewController(withIdentifier: "ForumListNavigationController") as? UINavigationController {
            setNewChild(forumNVC)
            if let forumListVC = forumNVC.viewControllers.first as? ForumTableViewController {
                forumListVC.showTopic(topic)
            }
        }
    }
    
    func showEventDetail(for eventID: Int, in placeID: Int) {
        if let eventsNVC = UIStoryboard(.Events).instantiateViewController(withIdentifier: "EventTableViewControllerNavigation") as? UINavigationController {
            setNewChild(eventsNVC)
            if let eventListVC = eventsNVC.viewControllers.first as? EventTableViewController {
                eventListVC.openDetailEvent(with: eventID, placeID: placeID)
            }
        }
    }
    
    func updateChats() {
        for child in children {
            if let childNVC = child as? UINavigationController {
                if let root = childNVC.viewControllers.first as? DialogsTableViewController {
                    root.refreshContent(nil)
                }
            }
        }
    }
    
    func openMenu(_ animated: Bool = true) {
        isAnimating = animated
        if let appereance = childViewController as? MenuAppereance {
            appereance.menuWillOpen()
        }
        if let nVC = childViewController as? UINavigationController {
            for vc in nVC.viewControllers {
                if let appereance = vc as? MenuAppereance {
                    appereance.menuWillOpen()
                }
                for childChilds in vc.children {
                    if let appereance = nVC.viewControllers.last as? MenuAppereance {
                        appereance.menuWillOpen()
                    }
                    if let appereance = childChilds as? MenuAppereance {
                        appereance.menuWillOpen()
                    }
                    if let presented = childChilds.presentedViewController as? MenuAppereance {
                        presented.menuWillOpen()
                    }
                }                
            }
            if let presented = nVC.presentedViewController as? MenuAppereance {
                presented.menuWillOpen()
            }
        }
        if let tabVC = childViewController as? UITabBarController {
            for vc in tabVC.viewControllers ?? [] {
                if let nVC = vc as? UINavigationController {
                    for vc in nVC.viewControllers {
                        if let appereance = vc as? MenuAppereance {
                            appereance.menuWillOpen()
                        }
                    }
                }
                if let appereance = vc as? MenuAppereance {
                    appereance.menuWillOpen()
                }
            }
        }
        menuViewController?.menuWillOpen()
        moveMenu(isOpen: true, animated: animated)
        if let view = dismissContainer {
            view.frame = view.bounds
            self.view.insertSubview(view, belowSubview: backMenuContainer)
        } else {
            let closeTapGesture = UITapGestureRecognizer(target: self, action: #selector(closeDidTap(_:)))
            dismissContainer = UIView(frame: view.bounds)
            dismissContainer?.backgroundColor = .clear
            dismissContainer?.isUserInteractionEnabled = true
            dismissContainer?.addGestureRecognizer(closeTapGesture)
            self.view.insertSubview(dismissContainer!, belowSubview: backMenuContainer)
        }
    }
    
    func closeMenu(_ animated: Bool = true) {
        isAnimating = animated
        if let appereance = childViewController as? MenuAppereance {
            appereance.menuWillClose()
        }
        if let nVC = childViewController as? UINavigationController {
            if let appereance = nVC.viewControllers.last as? MenuAppereance {
                appereance.menuWillClose()
            }
            for childChilds in nVC.viewControllers.last?.children ?? [] {
                if let appereance = nVC.viewControllers.last as? MenuAppereance {
                    appereance.menuWillClose()
                }
                if let appereance = childChilds as? MenuAppereance {
                    appereance.menuWillClose()
                }
                if let presented = childChilds.presentedViewController as? MenuAppereance {
                    presented.menuWillClose()
                }
            }
        }
        menuViewController?.menuWillClose()
        moveMenu(isOpen: false, animated: animated)
        dismissContainer?.removeFromSuperview()
    }
    
    func didOpenMenuItem(_ itemIndex: Int) {
        switch itemIndex {
        case 0:
            // Profile
            if let profileNVC = UIStoryboard(.Profile).instantiateInitialViewController() {
                setNewChild(profileNVC)
            }
        case 1:
            // Forum
            if let forumNVC = UIStoryboard(.Forum).instantiateInitialViewController() as? UINavigationController {
                setNewChild(forumNVC)
            }
        case 2:
            // Events
            if let eventNTVC = UIStoryboard(.Events).instantiateInitialViewController() as? UINavigationController {
                setNewChild(eventNTVC)
            }
        case 3:
            // City-Online
            let cityOnlineTVC: MainTabBarViewController = UIStoryboard(.CityOnline).instantiateViewController()
            setNewChild(cityOnlineTVC)
        case 4:
            // Tourist Places
            if let touristPlacesVC = UIStoryboard(.TouristPlaces).instantiateInitialViewController() as? UINavigationController{
                setNewChild(touristPlacesVC)
            }
        case 5:
            // Discount
            if let discountNTVC = UIStoryboard(.Discount).instantiateInitialViewController() as? UINavigationController {
                setNewChild(discountNTVC)
            }
        case 6:
            // Vacancy
            if let resumeNVC = UIStoryboard(.Job).instantiateViewController(withIdentifier: "ResumeListNavigationController") as? UINavigationController {
                setNewChild(resumeNVC)
            }
        case 7:
            // Resume
            if let jobNVC = UIStoryboard(.Job).instantiateInitialViewController() as? UINavigationController {
                setNewChild(jobNVC)
            }
        case 8:
            // Chats
            if let chatNVC = UIStoryboard(.Chats).instantiateInitialViewController() as? UINavigationController {
                setNewChild(chatNVC)
            }
        case 9:
            //Friends
            if let findUsersNVC = UIStoryboard(.Profile).instantiateViewController(withIdentifier: "NavigationFindUserTableViewController") as? UINavigationController {
                if let friendListVC = findUsersNVC.viewControllers.first as? FindUserTableViewController {
                    friendListVC.listType = .friends
                }
                setNewChild(findUsersNVC)
            }
        case 10:
            //Find Users
            if let findUsersNVC = UIStoryboard(.Profile).instantiateViewController(withIdentifier: "NavigationFindUserTableViewController") as? UINavigationController {
                setNewChild(findUsersNVC)
            }
        case 11:
            // Settings
            if let settingsNVC = UIStoryboard(.Main).instantiateViewController(withIdentifier: "SettingsNVC") as? UINavigationController {
                setNewChild(settingsNVC)
            }
        default: break
        }
    }
    
}


//MARK: - Private

fileprivate extension MainContainerViewController {
    
    struct Constants {
        static let menuAnimationInterval = TimeInterval(0.3)
    }
    
    func moveMenu(isOpen: Bool, animated: Bool = true) {
        let endConstant = isOpen ?  backMenuContainer.bounds.width : 0
        if animated {
            self.menuTrailingConstraint.constant = endConstant
            UIView.animate(withDuration: Constants.menuAnimationInterval, delay: 0, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
                if finished {
                    self.panGesture.isEnabled = isOpen
                    self.isAnimating = !finished
                }
            })
        } else {
            menuTrailingConstraint.constant = endConstant
            view.layoutIfNeeded()
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func panAction(_ sender: UIPanGestureRecognizer) {
        let velocity =  sender.velocity(in: self.view)
        if isMenuOpen && velocity.x < -30 {
            isAnimating = true
            closeMenu()
        }
    }
 
    @IBAction func screenPanAction(_ sender: UIScreenEdgePanGestureRecognizer) {
        if sender.state == .began && !isAnimating {
            isAnimating = true
            openMenu()
        }
    }
    
    func setNewChild(_ viewController: UIViewController) {
        if let oldVC = children.first(where: { !$0.isKind(of: MenuTableViewController.classForCoder()) }) {
            oldVC.view.removeFromSuperview()
            oldVC.willMove(toParent: nil)
            oldVC.removeFromParent()
            oldVC.didMove(toParent: nil)
        }
        
        viewController.willMove(toParent: self)
        viewController.view.frame = contentContainer.bounds
        contentContainer.addSubview(viewController.view)
        addChild(viewController)
        viewController.didMove(toParent: self)
        childViewController = viewController
    }
    
}
