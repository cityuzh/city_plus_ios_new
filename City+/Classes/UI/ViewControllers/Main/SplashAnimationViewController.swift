//
//  SplashAnimationViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/24/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

final class SplashAnimationViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak fileprivate var oval1: UIView!
    @IBOutlet weak fileprivate var oval2: UIView!
    @IBOutlet weak fileprivate var oval3: UIView!
    @IBOutlet weak fileprivate var logo: UIImageView!
    
    //MARK: - Properties
    
    var completionHandler: ((_ vc: SplashAnimationViewController)->())?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTransform()
        setupClearNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        oval1.layer.cornerRadius = oval1.bounds.height/2
        oval2.layer.cornerRadius = oval2.bounds.height/2
        oval3.layer.cornerRadius = oval3.bounds.width/2
        
        animate()
    }
    
}

fileprivate extension SplashAnimationViewController {
    
    func setupTransform() {
        oval1.transform = oval1.transform.scaledBy(x: 1/oval1.bounds.width, y: 1/oval1.bounds.height)
        oval2.transform = oval2.transform.scaledBy(x: 1/oval2.bounds.width, y: 1/oval2.bounds.height)
        oval3.transform = oval3.transform.scaledBy(x: 1/oval3.bounds.width, y: 1/oval3.bounds.height)
        logo.transform = logo.transform.scaledBy(x: 1/logo.bounds.width, y: 1/logo.bounds.height)
    }
    
    func animate() {
        animateFirstOval()
        self.animateSecondOval()
        self.animateThirdOval()
    }
    
    func animateFirstOval() {
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.oval1.transform = .identity
        }) { (finished) in
            if finished {
                
            }
        }
    }
    
    func animateSecondOval() {
        UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.oval2.transform = .identity
        }) { (finished) in
            if finished {
                
            }
        }
    }
    
    func animateThirdOval() {
        UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.oval3.transform = .identity
            self.logo.transform = .identity
        }) { (finished) in
            if finished {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.fadeAnimation()
                })
            }
        }
    }
    
    func fadeAnimation() {
        UIView.animate(withDuration: 0.33, animations: {
            self.oval1.alpha = 0.0
            self.oval2.alpha = 0.0
            self.oval3.alpha = 0.0
        }) { (finished) in
            if finished {
                self.completionHandler?(self)
            }
        }
    }
    
}
