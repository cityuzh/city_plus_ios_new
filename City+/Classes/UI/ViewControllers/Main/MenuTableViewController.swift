//
//  MenuTableViewController.swift
//  City+
//
//  Created by Ivan Grab on 3/3/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Nuke
import UIKit

protocol MenuAppereance: class {
    func menuWillOpen()
    func menuWillClose()
}

final class MenuTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    fileprivate var selectedIndex: Int = 1 {
        willSet {
            if let oldCell = tableView.cellForRow(at: IndexPath(row: selectedIndex, section: 0)) {
                oldCell.imageView?.image = oldCell.imageView?.image?.tint(with: UIColor.newLightBlueColor)
            }
            if let newCell = tableView.cellForRow(at: IndexPath(row: newValue, section: 0)) {
                newCell.imageView?.image = newCell.imageView?.image?.tint(with: UIColor.newAppTintColor)
            }
        }
    }
    
    //MARK: - Lifecycle
    
    lazy var user: UserModel? = {
        if let user = AppDelegate.shared.sessionManager.storedUser {
            return UserModel.networkUser(with: user)
        }
        return nil
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(userUpdated(_:)), name: NSNotification.Name.LocalStoredUserUpdated, object: nil)
        if let user = user {
            setupUI(with: user)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 8:
            cell.imageView?.isHighlighted = (user?.chatBadgeCout ?? 0) > 0
            cell.textLabel?.text = NSLocalizedString("Чати", comment: "") + ((user?.chatBadgeCout ?? 0) > 0 ? " +\(user?.chatBadgeCout ?? 0)" : "")
        case 1:
            cell.imageView?.isHighlighted = (user?.forumBadgeCout ?? 0) > 0
            cell.textLabel?.text = NSLocalizedString("Обговорення", comment: "") + ((user?.forumBadgeCout ?? 0) > 0 ? " +\(user?.forumBadgeCout ?? 0)" : "")
        case 9:
            cell.imageView?.isHighlighted = (user?.requestBadgeCout ?? 0) > 0
            cell.textLabel?.text = NSLocalizedString("Друзі", comment: "") + ((user?.requestBadgeCout ?? 0) > 0 ? " +\(user?.requestBadgeCout ?? 0)" : "")
        default: break
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 12:
            var height = UIScreen.main.bounds.height - 44
            if UIDevice().userInterfaceIdiom == .phone, UIScreen.main.nativeBounds.height >= 1792 {
                if #available(iOS 11.0, *) {
                    tableView.contentInsetAdjustmentBehavior = .never
                }
                tableView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
            }
            for i in 0..<12 {
                height -= super.tableView(tableView, heightForRowAt: IndexPath(row: i, section: 0))
            }
            return max(height, 60)
        default: return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AppDelegate.shared.mainContainerController?.closeMenu()
        switch indexPath.row {
        case 12:
            sessionManager.logOut(completion: { (success) in
                UIWindow.replaceStoryboard(name: .Authentication)
            })
        default:
            AppDelegate.shared.mainContainerController?.didOpenMenuItem(indexPath.row)
            selectedIndex = indexPath.row
        }
    }
    
    override func menuWillOpen() {
        super.menuWillOpen()
        user?.getProfile()
    }
    
    override func menuWillClose() {
        super.menuWillClose()
    }

}

extension MenuTableViewController {
    
}

fileprivate extension MenuTableViewController {
    
    func setupUI(with userModel: UserModel) {
        let option = ImageLoadingOptions(placeholder: #imageLiteral(resourceName: "profilePlaceholder"), transition: .fadeIn(duration: 0.3), failureImage: #imageLiteral(resourceName: "profilePlaceholder"), failureImageTransition: .fadeIn(duration: 0.3), contentModes: nil)
        if let urlString = userModel.avatar?.urlPath, let url = URL(string: urlString) {
            Nuke.loadImage(with: url, options: option, into: profileImage, progress: nil) { (imageResponse, error) in
                if error != nil {
                    self.profileImage.setImageWith(userModel.fullName, color: UIColor.lightGray, circular: true)
                }
            }
        }
        profileName.text = userModel.fullName
    }
    
    @objc func userUpdated(_ notification: NSNotification) {
        DispatchQueue.main.async {
            if let updatedUser = AppDelegate.shared.sessionManager.storedUser {
                self.user = UserModel.networkUser(with: updatedUser)
                DispatchQueue.main.async {
                    self.setupUI(with: UserModel.networkUser(with: updatedUser))
                    self.tableView.beginUpdates()
                    self.tableView.reloadRows(at: [IndexPath(row: 7, section: 0), IndexPath(row: 1, section: 0), IndexPath(row: 8, section: 0)], with: .none)
                    self.tableView.endUpdates()
                }
            }
        }
    }
    
}
