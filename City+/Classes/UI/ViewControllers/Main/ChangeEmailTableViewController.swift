//
//  ChangeEmailTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 4/20/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

final class ChangeEmailTableViewController: BaseTableViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var codeField: UITextField!
    
    @IBOutlet weak var barButton: UIBarButtonItem!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGradientedNavigationBar()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        sessionManager.onRequestFailure = requestFailureHandler
    }
    
    fileprivate var action: ConfirmAction = .sms {
        didSet {
            emailField.isEnabled = action == .sms
            emailField.alpha = action == .sms ? 1 : 0.5
            barButton.title = action == .sms ? NSLocalizedString("Далі", comment: "") : NSLocalizedString("Зберегти", comment: "")
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return action == .code ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if action == .code && section == 1 {
            return NSLocalizedString("КОД", comment: "")
        } else if section == 0 {
            return super.tableView(tableView, titleForHeaderInSection: section)
        }
        return nil
    }
    
}

extension ChangeEmailTableViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if action == .sms {
            barButton.isEnabled = ((emailField.text ?? "") + string).trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        } else {
            barButton.isEnabled = ((codeField.text ?? "") + string).trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        }
        return true
    }
    
}

extension ChangeEmailTableViewController {
    
    enum ConfirmAction {
        case sms
        case code
    }
    
    @IBAction func next(_ sender: UIBarButtonItem) {
        sessionManager.onNoInternetConnection = noInternetConnectionHandler
        sessionManager.onRequestFailure = requestFailureHandler
        if action == .sms {
            do {
                try EmailValidator().validate(emailField.text ?? "")
                present(activityVC, animated: true, completion: nil)
                sessionManager.sendEmailCode(to: (emailField.text ?? ""), completionHandler: { (success) in
                    if success {
                        self.activityVC.dismiss(animated: true, completion: {
                            ShowAlert.showInfoAlert(in: self, message: String(format: NSLocalizedString("Код надіслано на: %@", comment: ""), self.emailField.text!))
                            self.action = .code
                        })
                    }
                })
            } catch {
                let error = error as! ValueValidationError
                ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            }
        } else {
            if let code = codeField.text {
                present(activityVC, animated: true, completion: nil)
                sessionManager.changeEmail(with: (emailField.text ?? ""), code: code) { (success) in
                    if success {
                        self.activityVC.dismiss(animated: true, completion: {
                            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Email змінено!", comment: ""), okHandler: { (action) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        })
                    }
                }
            } else {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Код не введено!", comment: ""))
            }
        }
    }
    
}
