//
//  ChangePhoneTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/29/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreTelephony

final class ChangePhoneTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var phoneCodeField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var codeField: UITextField!
    
    @IBOutlet weak var barButton: UIBarButtonItem!
    
    var countries = [Country]() {
        didSet {
            countryPicker.reloadAllComponents()
        }
    }
    lazy var countryPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    
    lazy var countryAccessory: UIView = {
        let accessory = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        accessory.backgroundColor = UIColor.groupTableViewBackground
        
        let cancelButton = UIButton(type: .roundedRect)
        cancelButton.setTitle(NSLocalizedString("Відмінити", comment: ""), for: .normal)
        cancelButton.setTitleColor(UIColor.darkGray, for: .normal)
        cancelButton.contentHorizontalAlignment = .left
        cancelButton.addTarget(self, action: #selector(cancelTapped(_:)), for: .touchUpInside)
        
        let saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle(NSLocalizedString("Вибрати", comment: ""), for: .normal)
        saveButton.setTitleColor(UIColor.darkGray, for: .normal)
        saveButton.contentHorizontalAlignment = .right
        saveButton.addTarget(self, action: #selector(setTapped(_:)), for: .touchUpInside)
        
        accessory.addSubview(saveButton)
        accessory.addSubview(cancelButton)
        
        let views = ["save": saveButton, "cancel": cancelButton]
        let metrics = ["inset": 15]
        var constraints = [NSLayoutConstraint]()
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-inset-[cancel]-[save]-inset-|", options: [], metrics: metrics, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[cancel]-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[save]-|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        return accessory
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGradientedNavigationBar()
        
        Country.getCountrie { (countries) in
            self.countries = countries
            let networkInfo = CTTelephonyNetworkInfo()
            
            if let carrier = networkInfo.subscriberCellularProvider {
                if let code = carrier.mobileCountryCode {
                    if let country = self.countries.first(where: { $0.callingCodes.contains(code) }), let index = self.countries.firstIndex(where: {$0.name == country.name }) {
                        self.phoneCodeField.text = "+" + country.callingCodes.first!
                        self.countryPicker.selectRow(index, inComponent: 0, animated: false)
                    }
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        sessionManager.onRequestFailure = requestFailureHandler
    }

    fileprivate var action: ConfirmAction = .sms {
        didSet {
            phoneCodeField.isEnabled = action == .sms
            phoneCodeField.alpha = action == .sms ? 1 : 0.5
            phoneField.isEnabled = action == .sms
            phoneField.alpha = action == .sms ? 1 : 0.5
            barButton.title = action == .sms ? NSLocalizedString("Далі", comment: "") : NSLocalizedString("Зберегти", comment: "")
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return action == .code ? super.tableView(tableView, heightForRowAt: indexPath) : 0
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if action == .code && section == 1 {
            return NSLocalizedString("СМС КОД", comment: "")
        } else if section == 0 {
            return super.tableView(tableView, titleForHeaderInSection: section)
        }
        return nil
    }
    
    @objc func cancelTapped(_ sender: UIButton) {
        phoneCodeField.resignFirstResponder()
    }
    
    @objc func setTapped(_ sender: UIButton) {
        phoneCodeField.resignFirstResponder()
        phoneField.becomeFirstResponder()
    }

}

extension ChangePhoneTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == phoneCodeField {
            textField.inputAccessoryView = countryAccessory
            textField.inputView = countryPicker
        } else {
            textField.inputAccessoryView = nil
            textField.inputView = nil
        }
        textField.reloadInputViews()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if action == .sms {
            barButton.isEnabled = ((phoneField.text ?? "") + string).trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        } else {
            barButton.isEnabled = ((codeField.text ?? "") + string).trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        }        
        return true
    }
    
}

extension ChangePhoneTableViewController {
    
    enum ConfirmAction {
        case sms
        case code
    }
    
    @IBAction func next(_ sender: UIBarButtonItem) {
        sessionManager.onNoInternetConnection = noInternetConnectionHandler
        sessionManager.onRequestFailure = requestFailureHandler
        if action == .sms {
            do {
                try PhoneValidator().validate((phoneCodeField.text ?? "") + (phoneField.text ?? ""))
                present(activityVC, animated: true, completion: nil)
                sessionManager.sendSMS(to: ((phoneCodeField.text ?? "") + (phoneField.text ?? "")), completionHandler: { (success) in
                    if success {
                        self.activityVC.dismiss(animated: true, completion: {
                            ShowAlert.showInfoAlert(in: self, message: String(format: NSLocalizedString("СМС Код надіслано на: %@", comment: ""), self.phoneField.text!))
                            self.action = .code
                        })
                    }
                })
            } catch {
                let error = error as! ValueValidationError
                ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            }
        } else {
            if let code = codeField.text {
                present(activityVC, animated: true, completion: nil)
                sessionManager.changePhone(with: ((phoneCodeField.text ?? "") + (phoneField.text ?? "")), code: code) { (success) in
                    if success {
                        self.activityVC.dismiss(animated: true, completion: {
                            ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("Номер телефону змінено!", comment: ""), okHandler: { (action) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        })
                    }
                }
            } else {
                ShowAlert.showInfoAlert(in: self, message: NSLocalizedString("СМС Код не введено!", comment: ""))
            }
        }
    }
    
}

extension ChangePhoneTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(countries[row].name)" + " (\(countries[row].nativeName)): +" + countries[row].callingCodes.first!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        phoneCodeField.text = "+" + countries[row].callingCodes.first!
    }
    
}
