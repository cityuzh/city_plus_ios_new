//
//  SettingsTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/28/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Messages
import MessageUI

final class SettingsTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var allNotificationsSwitch: UISwitch!
    @IBOutlet weak var messagesSwitch: UISwitch!
    @IBOutlet weak var friendsSwitch: UISwitch!
    @IBOutlet weak var topicsSwitch: UISwitch!
    
    @IBOutlet weak var footerView: UIView!
    
    //MARK: - Lifecycle
    
    var isSendingRequest = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGradientedNavigationBar()
        updateStates()        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        sessionManager.onRequestFailure = requestFailureHandler
        sessionManager.onNoInternetConnection = { (request) in
            self.showNoInternetConnectionAlert()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let freeSpace = max(30, (tableView.bounds.height - tableView.contentSize.height))
        var rect = footerView.frame
        rect = CGRect(origin: footerView.frame.origin, size: CGSize(width: footerView.bounds.width, height: freeSpace))
        footerView.frame = rect
    }
    
    func updateStates() {
        sessionManager.getMyProfile { (success) in
            DispatchQueue.main.async {
                if let user = self.sessionManager.storedUser {
                    self.allNotificationsSwitch.isOn = !UserDefaults.standard.bool(forKey: "AllNotificationKeyDisabled")
                    
                    if !self.allNotificationsSwitch.isOn {
                        self.messagesSwitch.isOn = false
                        self.friendsSwitch.isOn = false
                        self.topicsSwitch.isOn = false
                    } else {
                        self.messagesSwitch.isOn = user.isPushOnMessage
                        self.friendsSwitch.isOn = user.isPushOnRequest
                        self.topicsSwitch.isOn = user.isPushOnTopic
                    }
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 && indexPath.section == 0 {
            return AppDelegate.shared.sessionManager.storedUser?.isCanChangePassword ?? true ? super.tableView(tableView, heightForRowAt: indexPath) : 0.0
        }
        if indexPath.row == 0 && indexPath.section == 0 {
            return AppDelegate.shared.sessionManager.storedUser?.authenticationType == .phone ? super.tableView(tableView, heightForRowAt: indexPath) : 1 / UIScreen.main.scale
        }
        if indexPath.row == 1 && indexPath.section == 0 {
            return AppDelegate.shared.sessionManager.storedUser?.authenticationType == .email ? super.tableView(tableView, heightForRowAt: indexPath) : 0.0
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let user = AppDelegate.shared.sessionManager.storedUser {
            if indexPath.row == 0, indexPath.section == 0, user.authenticationType == .email  {
                cell.separatorInset = .zero
            }
            
            if indexPath.row == 1, indexPath.section == 0, !user.isCanChangePassword {
                cell.separatorInset = .zero
            }
        }
    }

}

fileprivate extension SettingsTableViewController {
    
    @IBAction func allChanged(_ sender: UISwitch) {
        guard !isSendingRequest else { return }
        sessionManager.onRequestFailure = requestFailureHandler
        if sender.isOn {
            allNotificationsSwitch.isEnabled = false
            allNotificationsSwitch.setOn(false, animated: true)
            if let token = sessionManager.pushToken {
                self.allNotificationsSwitch.isEnabled = false
                isSendingRequest = true
                sessionManager.setDeviceToken(with: token) { (success) in
                    self.isSendingRequest = false
                    self.allNotificationsSwitch.isEnabled = true
                    if success {
                        UserDefaults.standard.set(false, forKey: "AllNotificationKeyDisabled")
                        UserDefaults.standard.synchronize()
                        self.allNotificationsSwitch.setOn(true, animated: true)
                    }
                    self.updateStates()
                }
            }
        } else {
            allNotificationsSwitch.isEnabled = false
            allNotificationsSwitch.setOn(true, animated: true)
            isSendingRequest = true
            sessionManager.deleteDeviceToken { (success) in
                self.isSendingRequest = false
                self.allNotificationsSwitch.isEnabled = true
                if success {
                    UserDefaults.standard.set(true, forKey: "AllNotificationKeyDisabled")
                    UserDefaults.standard.synchronize()
                    
                    self.allNotificationsSwitch.setOn(false, animated: true)
                }
                self.updateStates()
            }
        }
    }
    
    @IBAction func messageNotificationChanged(_ sender: UISwitch) {
        guard !isSendingRequest else { return }
        sessionManager.onRequestFailure = requestFailureHandler
        if sender.isOn {
            messagesSwitch.isEnabled = false
            messagesSwitch.setOn(false, animated: true)
            self.messagesSwitch.isEnabled = false
            isSendingRequest = true
            sessionManager.changeMessagePush(with: true) { (success) in
                self.isSendingRequest = false
                self.messagesSwitch.isEnabled = true
                if success {
                    self.sessionManager.storedUser?.isPushOnMessage = true;
                    self.messagesSwitch.setOn(true, animated: true);
                }
            }
        } else {
            messagesSwitch.isEnabled = false
            messagesSwitch.setOn(true, animated: true)
            isSendingRequest = true
            sessionManager.changeMessagePush(with: false) { (success) in
                self.isSendingRequest = false
                self.messagesSwitch.isEnabled = true
                if success {
                    self.sessionManager.storedUser?.isPushOnMessage = false;
                    self.messagesSwitch.setOn(false, animated: true)
                }
            }
        }
    }
    
    @IBAction func friendNotificationChanged(_ sender: UISwitch) {
        guard !isSendingRequest else { return }
        sessionManager.onRequestFailure = requestFailureHandler
        if sender.isOn {
            friendsSwitch.isEnabled = false
            friendsSwitch.setOn(false, animated: true)
            self.friendsSwitch.isEnabled = false
            isSendingRequest = true
            sessionManager.changeRequestPush(with: true) { (success) in
                self.isSendingRequest = false
                self.friendsSwitch.isEnabled = true
                if success {
                    self.sessionManager.storedUser?.isPushOnRequest = true;
                    self.friendsSwitch.setOn(true, animated: true)
                }
            }
        } else {
            friendsSwitch.isEnabled = false
            friendsSwitch.setOn(true, animated: true)
            isSendingRequest = true
            sessionManager.changeRequestPush(with: false) { (success) in
                self.isSendingRequest = false
                self.friendsSwitch.isEnabled = true
                if success {
                    self.sessionManager.storedUser?.isPushOnRequest = false;
                    self.friendsSwitch.setOn(false, animated: true)
                }
            }
        }
    }
    
    @IBAction func topicNotificationChanged(_ sender: UISwitch) {
        guard !isSendingRequest else { return }
        sessionManager.onRequestFailure = requestFailureHandler
        if sender.isOn {
            topicsSwitch.isEnabled = false
            topicsSwitch.setOn(false, animated: true)
            self.topicsSwitch.isEnabled = false
            isSendingRequest = true
            sessionManager.changeTopicPush(with: true) { (success) in
                self.isSendingRequest = false
                self.topicsSwitch.isEnabled = true
                if success {
                    self.sessionManager.storedUser?.isPushOnTopic = true;
                    self.topicsSwitch.setOn(true, animated: true)
                }
            }
        } else {
            topicsSwitch.isEnabled = false
            topicsSwitch.setOn(true, animated: true)
            isSendingRequest = true
            sessionManager.changeTopicPush(with: false) { (success) in
                self.isSendingRequest = false
                self.topicsSwitch.isEnabled = true
                if success {
                    self.sessionManager.storedUser?.isPushOnTopic = false;
                    self.topicsSwitch.setOn(false, animated: true)
                }
            }
        }
    }
    
    @IBAction func contactUs(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let emails = ["cityplus.uzh@gmail.com"]
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(emails)
            mail.setSubject("City+ iOS")
            present(mail, animated: true)
        } else {
            ShowAlert.showInfoAlert(in: self, message: "Спочатку налаштуйте поштовий клієнт!")
        }
    }
    
}

extension SettingsTableViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
