//
//  ChangePasswordTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/28/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class ChangePasswordTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmNewPassword: UITextField!
    
    @IBOutlet weak var saveBarButton: UIBarButtonItem!
    
    //MARK: - Properties
    
    var registrationModel = RegisterUserFormModel()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGradientedNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        oldPassword.becomeFirstResponder()
    }

}

extension ChangePasswordTableViewController {
    
    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        registrationModel.password = newPassword.text ?? ""
        registrationModel.confirmPassword = confirmNewPassword.text ?? ""
        
        do {
            try registrationModel.validate()
            sessionManager.onRequestFailure = requestFailureHandler
            sessionManager.onNoInternetConnection = { (request) in
                self.showNoInternetConnectionAlert()
            }
            present(activityVC, animated: true, completion: nil)
            sessionManager.changePassword(with: registrationModel, oldPassword: oldPassword.text ?? "") { (success) in
                if success {
                    self.activityVC.dismiss(animated: true, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        } catch {
            let error = error as! RegisterUserFormError
            ShowAlert.showInfoAlert(in: self, message: error.localizedDescription)
            return
        }
    }
    
}

extension ChangePasswordTableViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        saveBarButton.isEnabled = !(oldPassword.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && !(newPassword.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && !(confirmNewPassword.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        return true
    }
    
}
