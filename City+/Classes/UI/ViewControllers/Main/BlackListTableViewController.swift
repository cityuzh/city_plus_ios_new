//
//  BlackListTableViewController.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class BlackListTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var editItem: UIBarButtonItem!
    @IBOutlet fileprivate weak var saveItem: UIBarButtonItem!
    
    //MARK: - Properties
    
    lazy var itemList: PaginatedListModel<UserModel> = {
        let list = PaginatedListModel<UserModel>(path: UserModel.Constants.blackListPath)
        list.onRequestFailure = requestFailureHandler
        list.onNoInternetConnection = noInternetConnectionHandler
        return list
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupGradientedNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshContent(nil)
    }
    
    // MARK: - Table view data source

    override func refreshContent(_ sender: UIRefreshControl?) {
        itemList.load { (success) in
            sender?.endRefreshing()
            if success {
                self.placeholderMode = .noDataItems
            }
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = itemList.count == 0 ? placeholderView : nil
        return itemList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userModel = itemList[indexPath.row]
        let identifier = UserTableViewCell.cellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! UserTableViewCell
        cell.setUp(with: userModel)
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let user = itemList[indexPath.row]
        if editingStyle == .delete {
            present(activityVC, animated: true, completion: nil)
            user.onRequestFailure = requestFailureHandler
            user.deleteFromBlackList { (success) in
                if success {
                    self.activityVC.dismiss(animated: true, completion: {
                        self.itemList.objects.remove(at: indexPath.row)
                        tableView.beginUpdates()
                        tableView.deleteRows(at: [indexPath], with: .automatic)
                        tableView.endUpdates()
                    })
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Видалити") { (action, actionIndexPath) in
            let user = self.itemList[actionIndexPath.row]
            self.present(self.activityVC, animated: true, completion: nil)
            user.onRequestFailure = self.requestFailureHandler
            user.deleteFromBlackList { (success) in
                if success {
                    self.activityVC.dismiss(animated: true, completion: {
                        self.itemList.objects.remove(at: indexPath.row)
                        tableView.beginUpdates()
                        tableView.deleteRows(at: [actionIndexPath], with: .automatic)
                        tableView.endUpdates()
                    })
                }
            }
        }
        return [deleteAction]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = itemList[indexPath.row]
        let profileVC: ProfileTableViewController = UIStoryboard(.Profile).instantiateViewController()
        profileVC.user = user
        profileVC.showMenuButton = false
        show(profileVC, sender: self)
    }

}

fileprivate extension BlackListTableViewController {
    
    @IBAction func edit(_ sender: UIBarButtonItem) {
        if sender == editItem {
            self.tableView.setEditing(true, animated: true)
            navigationItem.rightBarButtonItem = saveItem
        } else {
            navigationItem.rightBarButtonItem = editItem
            self.tableView.setEditing(false, animated: true)
        }
    }
    
}
