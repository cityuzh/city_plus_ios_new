//
//  DiscountTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 1/16/19.
//  Copyright © 2019 Ivan Grab. All rights reserved.
//

import UIKit

final class DiscountTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var shadowContainer: UIView!
    @IBOutlet weak var discountImageView: UIImageView!
    
    @IBOutlet fileprivate weak var discountTitle: UILabel!
    @IBOutlet fileprivate weak var discountAmount: UILabel!
    @IBOutlet fileprivate weak var originalPrice: UILabel!
    @IBOutlet fileprivate weak var lineView: UIView!
    @IBOutlet fileprivate weak var currentPrice: UILabel!
    @IBOutlet fileprivate weak var discountDate: UILabel!
    
    //MARK: - Properties
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func setUp(with discount: Discount) {
        discountTitle.text = discount.name
        discountAmount.text = " -\(discount.persentDiscount)% "
        originalPrice.text = "\(discount.oldPrice) ₴"
        currentPrice.text = "\(discount.newPrice) ₴"
        
        discountAmount.isHidden = discount.persentDiscount == 0
        lineView.isHidden = discount.oldPrice == 0
        originalPrice.isHidden = discount.oldPrice == 0
        currentPrice.isHidden = discount.newPrice == 0
        
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "dd MMM"
        
        discountDate.text = "\(formatter.string(from: discount.startDate ?? Date())) - \(formatter.string(from: discount.endDate ?? Date()))"
    }

}
