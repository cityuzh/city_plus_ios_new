//
//  CommentTableViewCell.swift
//  City+
//
//  Created by Ivan Grab on 4/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class CommentTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var commentMessage: UILabel!
    @IBOutlet weak var attachedImage: UIImageView?
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var pubDate: UILabel!
    
    @IBOutlet weak var moreButton: UIButton!
    
    var profileHandler: ((_ sender: CommentTableViewCell)->())?
    var attachmentHandler: ((_ sender: CommentTableViewCell)->())?
    var moreHandler: ((_ sender: UIButton, _ inCell: CommentTableViewCell)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        userImage.isUserInteractionEnabled = true
        userImage.addGestureRecognizer(imageGesture)
        
        let nameGesture = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        userName.isUserInteractionEnabled = true
        userName.addGestureRecognizer(nameGesture)
        
        let commentImageGesture = UITapGestureRecognizer(target: self, action: #selector(attachmenTapped(_:)))
        attachedImage?.isUserInteractionEnabled = true
        attachedImage?.addGestureRecognizer(commentImageGesture)
        
        let pubDateGesture = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        pubDate.isUserInteractionEnabled = true
        pubDate.addGestureRecognizer(pubDateGesture)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        attachedImage?.image = nil
        userImage.image = nil
        
    }

    func setUp(with comment: Comment) {
        moreButton.isHidden = !comment.isMy
        commentMessage.text = comment.text
        userName.text = comment.sender?.fullName
        pubDate.text = comment.createdAt?.getElapsedInterval()
        attachedImage?.setImage(with: comment.attachment, placeholder: #imageLiteral(resourceName: "no-image"), using: .gray, placeholderSettings: nil, completion: nil)
        userImage.setImage(with: comment.sender?.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: { (success) in
            if !success {
                self.userImage.setImageWith(comment.sender?.fullName ?? "", color: UIColor.lightGray, circular: true)
            }
        })
        
        
    }

}

fileprivate extension CommentTableViewCell {
    
    @IBAction func moreAction(_ sender: UIButton) {
        moreHandler?(sender, self)
    }
    
    @objc func profileTapped(_ sender: UITapGestureRecognizer) {
        profileHandler?(self)
    }
    
    @objc func attachmenTapped(_ sender: UITapGestureRecognizer) {
        attachmentHandler?(self)
    }
}
