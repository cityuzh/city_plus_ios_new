//
//  MessageTableViewCell.swift
//  City+
//
//  Created by Ivan Grab on 5/31/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class MessageTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var userImage: UIImageView? {
        didSet {
            userImage?.roundedCorners(on: .allCorners, with: (userImage?.bounds.height ?? 0) / 2)
        }
    }
    @IBOutlet weak var userName: UILabel?
    @IBOutlet weak var elapsedDate: UILabel?
    @IBOutlet weak var readLabel: UILabel?
    @IBOutlet weak var previewImage: UIImageView?
    @IBOutlet weak var playImage: UIImageView?
    @IBOutlet weak var messageTextView: UITextView? {
        didSet {
            messageTextView?.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
            messageTextView?.textContainerInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    @IBOutlet weak var failedButton: UIButton?
    
    @IBOutlet weak var readLabelHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var previewHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var previewWidthConstraint: NSLayoutConstraint?
    @IBOutlet weak var newContaineHeightConstraint: NSLayoutConstraint?
    
    //MARK: - Properties
    
    var errorHandler: ((_ sender: MessageTableViewCell)->())?
    var profileHandler: ((_ sender: MessageTableViewCell)->())?
    var previewHandler: ((_ sender: MessageTableViewCell)->())?
    var menuTouchHandler: ((_ sender: MessageTableViewCell, _ isShowMenuImmediatly: Bool)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        messageTextView?.delegate = self
        let profileTapGesture = UITapGestureRecognizer(target: self, action: #selector(userTapped(_:)))
        userImage?.addGestureRecognizer(profileTapGesture)
        userImage?.isUserInteractionEnabled = true
        
        let previewTapGesture = UITapGestureRecognizer(target: self, action: #selector(previewTapped(_:)))
        previewImage?.addGestureRecognizer(previewTapGesture)
        previewImage?.isUserInteractionEnabled = true
        
        let longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTapTapped(_:)))
        self.previewImage?.addGestureRecognizer(longTapGesture)
        self.previewImage?.isUserInteractionEnabled = true
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        previewImage?.image = nil
        newContaineHeightConstraint?.constant = 0
        readLabelHeightConstraint?.constant = 0
    }


    func setUp(with message: Message, isLastMessage: Bool, isNew: Bool, textColor: UIColor, in chat: Chat) {
        userName?.text = message.user?.fullName
        userImage?.setImage(with: message.user?.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: { (success) in
            if !success {
                self.userImage?.setImageWith(message.user?.fullName ?? "", color: UIColor.lightGray, circular: true)
            }
        })
        
        elapsedDate?.textColor = textColor.withAlphaComponent(0.5)
        userName?.textColor = textColor.withAlphaComponent(0.75)
        messageTextView?.textColor = textColor
        readLabel?.textColor = textColor.withAlphaComponent(0.5)
        
        if let date = message.date {
            if date.timeIntervalSinceNow > 24 * 60 * 60 {
                elapsedDate?.text = date.formatedPubDate()
            } else {
                elapsedDate?.text = date.getElapsedInterval()
            }
        }
        
        switch message.messageType {
        case .image:
            if let imageSize = message.contentSize {
                let ratio = imageSize.width / imageSize.height
                if imageSize.width > imageSize.height {
                    self.previewWidthConstraint?.constant = self.bounds.width - 110
                    self.previewHeightConstraint?.constant = (self.previewWidthConstraint?.constant ?? 0.0) * 1/ratio
                } else if imageSize.width == imageSize.height {
                    self.previewHeightConstraint?.constant = 3/2 * self.bounds.width
                    self.previewWidthConstraint?.constant = 3/2 * self.bounds.width
                } else {
                    self.previewWidthConstraint?.constant = self.bounds.width / 2
                    self.previewHeightConstraint?.constant = self.bounds.width / 2 * 1/ratio
                }
            }
            previewImage?.setImage(with: message.content, placeholder: #imageLiteral(resourceName: "placePlaceholder"), using: .gray, placeholderSettings: (.center, nil), completion: { (success) in
                if success {
                    if let image = self.previewImage?.image {
                        message.content?.image = image
                    }
                }
            })
        case .video:
            if let imageSize = message.contentSize {
                let ratio = imageSize.width / imageSize.height
                if imageSize.width > imageSize.height {
                    self.previewWidthConstraint?.constant = self.bounds.width - 110
                    self.previewHeightConstraint?.constant = (self.previewWidthConstraint?.constant ?? 0.0) * 1/ratio
                } else if imageSize.width == imageSize.height {
                    self.previewHeightConstraint?.constant = 3/2 * self.bounds.width
                    self.previewWidthConstraint?.constant = 3/2 * self.bounds.width
                } else {
                    self.previewWidthConstraint?.constant = self.bounds.width / 2
                    self.previewHeightConstraint?.constant = self.bounds.width / 2 * 1/ratio
                }
            }
            previewImage?.setImage(with: message.content?.preview, placeholder: #imageLiteral(resourceName: "placePlaceholder"), using: .gray, placeholderSettings: (.center, nil), completion: { (success) in
                if success {
                    message.content?.preview?.image = self.previewImage?.image
                }
            })
        case .text:
            messageTextView?.text = message.text
        default: break
        }
        
        if isLastMessage &&  message.isRead && message.isMy && chat.isReadStatusEnabled {
            readLabelHeightConstraint?.constant = 15
        } else {
            readLabelHeightConstraint?.constant = 0
        }
        
        newContaineHeightConstraint?.constant = isNew ? 44 : 0
        failedButton?.isHidden = !message.isFailed
                
    }
    
}

extension MessageTableViewCell: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.superview != nil {
            if !UIMenuController.shared.isMenuVisible {
                 menuTouchHandler?(self, false)
            }
        }
    }

}

fileprivate extension MessageTableViewCell {
    
    @IBAction func errorButtonTapped(_ sender: UIButton) {
        errorHandler?(self)
    }
 
    @objc func userTapped(_ sender: UITapGestureRecognizer) {
        profileHandler?(self)
    }
    
    @objc func previewTapped(_ sender: UITapGestureRecognizer) {
        previewHandler?(self)
    }
    
    @objc func longTapTapped(_ sender: UITapGestureRecognizer) {
        menuTouchHandler?(self, true)
    }
        
}
