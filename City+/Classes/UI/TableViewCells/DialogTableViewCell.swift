//
//  DialogTableViewCell.swift
//  City+
//
//  Created by Ivan Grab on 5/31/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class DialogTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView! {
        didSet {
            userImage.roundedCorners(on: .allCorners, with: userImage.bounds.height / 2)
        }
    }

    @IBOutlet weak var elapsedDate: UILabel!
    @IBOutlet weak var lastMessage: UILabel!
    
    @IBOutlet weak var unreadCount: UILabel!
    @IBOutlet weak var unreadCountContainer: UIView!  {
        didSet {
            unreadCountContainer.roundedCorners(on: .allCorners, with: unreadCountContainer.bounds.height / 2)
        }
    }
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        unreadCountContainer.isHidden = true
        userImage.image = #imageLiteral(resourceName: "profilePlaceholder")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        unreadCountContainer.roundedCorners(on: .allCorners, with: unreadCountContainer.bounds.height / 2)
        userImage.roundedCorners(on: .allCorners, with: userImage.bounds.height / 2)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        unreadCountContainer.isHidden = true
        userImage.image = #imageLiteral(resourceName: "profilePlaceholder")
    }
    
    func setUp(with chat: Chat) {
        
        userName.text = chat.user?.fullName
        userImage.setImage(with: chat.user?.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: { (success) in
            if !success {
                self.userImage?.setImageWith(chat.user?.fullName ?? "", color: UIColor.lightGray, circular: true)
            }
        })
        lastMessage.text = chat.lastMessage
        elapsedDate.text = chat.lastDate?.getElapsedInterval()
        unreadCountContainer.isHidden = chat.unreadCount == 0
        unreadCount.text = String(chat.unreadCount)
        unreadCount.layoutIfNeeded()
        unreadCountContainer.layoutIfNeeded()
        unreadCountContainer.roundedCorners(on: .allCorners, with: unreadCountContainer.bounds.height / 2)
        userImage.roundedCorners(on: .allCorners, with: userImage.bounds.height / 2)
    }

}
