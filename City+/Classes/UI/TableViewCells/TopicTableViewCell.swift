//
//  TopicTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/12/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class TopicTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var attachmentImageView: UIImageView?
    @IBOutlet fileprivate weak var userName: UILabel!
    @IBOutlet fileprivate weak var pubDate: UILabel!
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var contenteLabel: UILabel?
    @IBOutlet fileprivate weak var contentTextView: UITextView?
    
    @IBOutlet fileprivate weak var commentCount: UILabel?
    @IBOutlet fileprivate weak var likesCount: UILabel?
    
    @IBOutlet fileprivate weak var likeButton: UIButton?
    @IBOutlet fileprivate weak var likeImge: UIImageView?
    
    @IBOutlet weak var moreImageView: UIView?
    @IBOutlet weak var imageCountLabel: UILabel?
    
    @IBOutlet fileprivate weak var likesLabel: UILabel?
    @IBOutlet fileprivate weak var likesLabelHeight: NSLayoutConstraint?
    
    
    var likersHandler: ((_ cell: TopicTableViewCell)->())?
    var likeHandler: ((_ cell: TopicTableViewCell, _ label: UILabel, _ sender: UIButton, _ likeImage: UIImageView)->())?
    var profileHandler: ((_ cell: TopicTableViewCell)->())?
    var topic: Topic!
    
    //MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(touchProfile(_:)))
        userImageView.isUserInteractionEnabled = true
        userImageView.addGestureRecognizer(tapGesture)
        
        let tapLikeGesture = UITapGestureRecognizer(target: self, action: #selector(touchLikers(_:)))
        likesLabel?.isUserInteractionEnabled = true
        likesLabel?.addGestureRecognizer(tapLikeGesture)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImageView.image = #imageLiteral(resourceName: "profilePlaceholder.png")
        attachmentImageView?.image = #imageLiteral(resourceName: "no-image.png")
    }

    func setUp(with topic: Topic) {
        self.topic = topic
        userName.text = topic.user?.fullName
        
        pubDate.text = topic.createdAt?.formatedPubDate()
        
        titleLabel.text = topic.title
        contenteLabel?.text = topic.topicContent
        contentTextView?.text = topic.topicContent
        
        commentCount?.text = "\(topic.commentCount)"
        likesCount?.text = "\(topic.likesCount)"
        likeImge?.isHighlighted = topic.isLikedByMe
        
        imageCountLabel?.text = "1/\(topic.attchments.count)"
        
        if let lastLiker = topic.lastLiked {
            var text = ""
            if topic.likesCount == 1 {
                text = "Уподобав \(lastLiker.fullName) і 1 інших"
            } else if topic.likesCount > 1 {
                text = "Уподобали \(lastLiker.fullName) і \(topic.likesCount - 1) інших"
            }
            likesLabel?.text = text
            likesLabelHeight?.constant = 20
        } else {
            likesLabel?.text = ""
            likesLabelHeight?.constant = 0
        }
    }

}

fileprivate extension TopicTableViewCell {
    
    @IBAction func likeTapped(_ sender: UIButton) {
        likeHandler?(self, likesCount!, sender, likeImge!)
    }
    
    @objc func touchProfile(_ sender: UITapGestureRecognizer) {
        profileHandler?(self)
    }
    
    @objc func touchLikers(_ sender: UITapGestureRecognizer) {
        likersHandler?(self)
    }
    
}
