//
//  EventTableViewCell.swift
//  City+
//
//  Created by Ivan Grab on 3/22/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var placeImage: UIImageView! {
        didSet {
            placeImage.roundedCorners(on: .allCorners, with: 15.0)
        }
    }
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var attendeesLabel: UILabel!
    
    
    //MARK: - Lifecycle
    
    func setUp(with event: Event) {
        placeImage.setImage(with: event.image, placeholder: #imageLiteral(resourceName: "placePlaceholder").tint(with: UIColor.appTintColor), using: .gray, placeholderSettings: (.scaleAspectFit, nil), completion: nil)
        placeName.text = event.name
        startDateLabel.text = Date.shortDayDateTimeFormater.string(from: event.startDate ?? Date())
        addressLabel.text = event.address
        attendeesLabel.text = "\(event.totalAttendes)"
    }
    
}
