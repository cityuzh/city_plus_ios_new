//
//  TopicLikeInfoTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/16/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class TopicLikeInfoTableViewCell: UITableViewCell {

    //MARK: - Properties
    
    @IBOutlet fileprivate weak var likeButton: UIButton!
    @IBOutlet fileprivate weak var likesLabel: UILabel!
    @IBOutlet fileprivate weak var likesLabelHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var likeCount: UILabel!
    @IBOutlet fileprivate weak var commentCount: UILabel!
    @IBOutlet fileprivate weak var likesImage: UIImageView!
    
    //MARK: - Properties
    
    var likersHandler: ((_ cell: TopicLikeInfoTableViewCell)->())?
    var likeHandler: ((_ cell: TopicLikeInfoTableViewCell, _ label: UILabel, _ sender: UIButton, _ likeImage: UIImageView)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapLikeGesture = UITapGestureRecognizer(target: self, action: #selector(touchLikers(_:)))
        likesLabel.isUserInteractionEnabled = true
        likesLabel.addGestureRecognizer(tapLikeGesture)
    }

    func setUp(with topic: Topic) {
        likeCount.text = "\(topic.likesCount)"
        commentCount.text = "\(topic.commentCount)"
        likesImage.isHighlighted = topic.isLikedByMe
        
        if let lastLiker = topic.lastLiked {
            var text = ""
            if topic.likesCount == 1 {
                text = "Уподобав \(lastLiker.fullName) і 1 інших"
            } else if topic.likesCount > 1 {
                text = "Уподобали \(lastLiker.fullName) і \(topic.likesCount - 1) інших"
            }
            likesLabel.text = text
            likesLabelHeight.constant = 20
        } else {
            likesLabel.text = ""
            likesLabelHeight.constant = 0
        }
    }
    
}

fileprivate extension TopicLikeInfoTableViewCell {
    
    @IBAction func like(_ sender: UIButton) {
        likeHandler?(self, likeCount, sender, likesImage)
    }
    
    @objc func touchLikers(_ sender: UITapGestureRecognizer) {
        likersHandler?(self)
    }
    
}
