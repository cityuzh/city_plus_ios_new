//
//  TopicCommentTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/16/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class TopicCommentTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var userImage: UIImageView!
    @IBOutlet fileprivate weak var userName: UILabel!
    @IBOutlet fileprivate weak var pubDate: UILabel!
    
    @IBOutlet fileprivate weak var likesLabel: UILabel!
    
    @IBOutlet fileprivate weak var textMessage: UITextView!    
    @IBOutlet fileprivate weak var attachmentImage: UIImageView?
    
    @IBOutlet fileprivate weak var likeCount: UILabel!
    @IBOutlet fileprivate weak var moreButton: UIButton!
    @IBOutlet fileprivate weak var replyButton: UIButton!
    @IBOutlet fileprivate weak var replyImage: UIImageView!
    @IBOutlet fileprivate weak var likeButton: UIButton!
    @IBOutlet fileprivate weak var likeImage: UIImageView!
    
    var likeHandler: ((_ cell: TopicCommentTableViewCell, _ label: UILabel, _ sender: UIButton, _ likeImage: UIImageView)->())?
    var likersHandler: ((_ cell: TopicCommentTableViewCell)->())?
    var moreHandler: ((_ cell: TopicCommentTableViewCell)->())?
    var replyHandler: ((_ cell: TopicCommentTableViewCell)->())?
    var attachmentHandler: ((_ cell: TopicCommentTableViewCell)->())?
    var profileHandler: ((_ cell: TopicCommentTableViewCell)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(touchProfile(_:)))
        userImage.isUserInteractionEnabled = true
        userImage.addGestureRecognizer(tapGesture)
        
        let tapAttachmentGesture = UITapGestureRecognizer(target: self, action: #selector(touchAttachment(_:)))
        attachmentImage?.isUserInteractionEnabled = true
        attachmentImage?.addGestureRecognizer(tapAttachmentGesture)
        
        let tapLikeGesture = UITapGestureRecognizer(target: self, action: #selector(touchLikers(_:)))
        likesLabel.isUserInteractionEnabled = true
        likesLabel.addGestureRecognizer(tapLikeGesture)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        attachmentImage?.image = nil
        userImage.image = nil
        
    }
    
    func setUp(with comment: TopicComment) {
        userName.text = comment.sender?.fullName
        userImage.setImage(with: comment.sender?.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: { (success) in
            if !success {
                self.userImage?.setImageWith(comment.sender?.fullName ?? "", color: UIColor.lightGray, circular: true)
            }
        })
        pubDate.text = comment.createdAt?.formatedPubDate()
        
        if let reply = comment.replyUser {
            let attributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.appTintColor]
            let defaultAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.black]
            let attributedMessage = NSMutableAttributedString(string: comment.text ?? "", attributes: defaultAttributes)
            attributedMessage.addAttributes(attributes, range: NSString(string: comment.text ?? "").range(of: reply.fullName))
            textMessage.attributedText = attributedMessage
        } else {
            let defaultAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.black]
            let attributedMessage = NSMutableAttributedString(string: comment.text ?? "", attributes: defaultAttributes)
            textMessage.attributedText = attributedMessage
        }
        
        moreButton.isHidden = !comment.isMy
        replyButton.isHidden = comment.isMy
        replyImage.isHidden = comment.isMy
        
        likeCount?.text = "\(comment.likesCount)"
        likeImage.isHighlighted = comment.isLikedByMe
        
        if let attachImgV = attachmentImage {
            attachImgV.setImage(with: comment.attachment, placeholder: #imageLiteral(resourceName: "no-image"), using: .gray, placeholderSettings: nil, completion: nil)
        }
        
        if let lastLiker = comment.lastLiked {
            var text = ""
            if comment.likesCount == 1 {
                text = "Уподобав \(lastLiker.fullName)"
            } else if comment.likesCount > 1 {
                text = "Уподобали \(lastLiker.fullName) і \(comment.likesCount - 1) інших"
            }
            likesLabel.text = text
        } else {
            likesLabel.text = ""
        }
    }
}

fileprivate extension TopicCommentTableViewCell {
    
    @IBAction func like(_ sender: UIButton) {
        likeHandler?(self, likeCount, sender, likeImage)
    }
    
    @IBAction func more(_ sender: UIButton) {
        moreHandler?(self)
    }
    
    @IBAction func reply(_ sender: UIButton) {
        replyHandler?(self)
    }
    
    @objc func touchProfile(_ sender: UITapGestureRecognizer) {
        profileHandler?(self)
    }
    
    @objc func touchAttachment(_ sender: UITapGestureRecognizer) {
        attachmentHandler?(self)
    }
    
    @objc func touchLikers(_ sender: UITapGestureRecognizer) {
        likersHandler?(self)
    }
}
