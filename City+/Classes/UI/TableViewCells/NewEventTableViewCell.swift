//
//  NewEventTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 11/6/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class NewEventTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var gradienContainer: UIView!
    @IBOutlet weak var photoImageView: UIImageView! {
        didSet {
            photoImageView.layer.cornerRadius = 15
            photoImageView.layer.borderColor = UIColor.lightGray.cgColor
            photoImageView.layer.borderWidth = 1.0 / UIScreen.main.scale
        }
    }
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel?
    @IBOutlet weak var attendeesLabel: UILabel!
    @IBOutlet weak var attendeesHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.image = nil
    }

    func setup(with event: Event) {
        titleLabel.text = event.name
        addressLabel?.text = event.place?.name
        var dateMessage = ""
        if let start = event.startDate {
            if let endDate = event.endDate {
                if Calendar.current.isDate(start, inSameDayAs: endDate) {
                    dateMessage = Date.shortDayDateTimeFormater.string(from: event.startDate ?? Date())
                    let timeFormatter = DateFormatter()
                    timeFormatter.timeStyle = .short
                    dateMessage += " - \(timeFormatter.string(from: endDate))"
                } else {
                    dateMessage = Date.shortDayDateTimeFormater.string(from: start) + " - " + Date.shortDayDateTimeFormater.string(from: endDate)
                }
            } else {
                 Date.shortDayDateTimeFormater.string(from: event.startDate ?? Date())
            }
        }
        dateLabel.text = dateMessage
        attendeesHeightConstraint.constant = event.attendees.count > 0 ? 30 : 0
        attendeesLabel.isHidden = event.attendees.count == 0
        
        var message = ""
        message = ("\(event.totalAttendes)" + NSLocalizedString(" відвідають або зацікавлені", comment: "зацікавлені в події"))
        attendeesLabel.text = message
    }
}
