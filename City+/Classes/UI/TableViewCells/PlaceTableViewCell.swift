//
//  PlaceTableViewCell.swift
//  City+
//
//  Created by Ivan Grab on 3/7/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class PlaceTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var placeImage: UIImageView! {
        didSet {
            placeImage.roundedCorners(on: .allCorners, with: 8.0)
        }
    }
    @IBOutlet weak var placeName: UILabel!
    
    @IBOutlet weak var workHoursContainer: UIView?
    @IBOutlet weak var placeWorkHours: UILabel?
    
    @IBOutlet weak var eventsCount: UILabel?
    @IBOutlet weak var addressLabel: UILabel?
    
    @IBOutlet weak var siteButton: UIButton?
    @IBOutlet weak var callButton: UIButton?
    @IBOutlet weak var favoriteButton: UIButton?
    
    @IBOutlet weak var ratingView: StarsRatingView?
    
    var callHandler: ((_ sender: UIButton)->())?
    var siteHandler: ((_ sender: UIButton)->())?
    var favoriteHandler: ((_ sender: UIButton)->())?
    
    
    //MARK: - Lifecycle
    
    func setUp(with place: Place) {
        placeImage.setImage(with: place.photos.first, placeholder: #imageLiteral(resourceName: "no-image"), isThumb: true, using: .gray, placeholderSettings: nil, completion: nil)
        
        placeName.text = place.name
        placeWorkHours?.text = place.schedules.first?.formatedWorkHours ?? "даних немає"
        placeWorkHours?.setNeedsDisplay()
        
        ratingView?.rating = NSNumber(value: place.rating)
        
        var frame = workHoursContainer?.frame ?? .zero
        frame.size.width = placeWorkHours?.bounds.width ?? 0 + 10
        workHoursContainer?.frame = frame
        workHoursContainer?.setNeedsDisplay()
        workHoursContainer?.layer.setNeedsDisplay()
        workHoursContainer?.layer.cornerRadius = (workHoursContainer?.bounds.height ?? 2) / 2
        
        
        favoriteButton?.setImage(#imageLiteral(resourceName: "favoritesUnselectedButton"), for: .normal)
        favoriteButton?.setImage(#imageLiteral(resourceName: "favoriteSelectedButton"), for: .selected)
        
        callButton?.setImage(#imageLiteral(resourceName: "call"), for: .normal)
        
        siteButton?.setImage(#imageLiteral(resourceName: "globe"), for: .normal)

        favoriteButton?.isSelected = place.isFavorite
        siteButton?.isEnabled = place.site != nil
        callButton?.isEnabled = place.phones.count > 0
        
        siteButton?.alpha = place.site != nil ? 1 : 0.5
        callButton?.alpha = place.phones.count > 0 ? 1 : 0.5
        eventsCount?.text = place.eventsCount == 1 ? "\(place.eventsCount) подія" : [2,3,4,102,103,104,202,203,204].contains(place.eventsCount) ? "\(place.eventsCount) події" : "\(place.eventsCount) подій"
        addressLabel?.text = place.address
    }
    
}

fileprivate extension PlaceTableViewCell {
    
    @IBAction func call(_ sender: UIButton) {
        callHandler?(sender)
    }
    
    @IBAction func site(_ sender: UIButton) {
        siteHandler?(sender)
    }
    
    @IBAction func favorite(_ sender: UIButton) {
        favoriteHandler?(sender)
    }
    
    
}
