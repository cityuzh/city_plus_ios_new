//
//  CatalogTouristPlacesTableViewCell.swift
//  CityPlus
//
//  Created by Macbook Air 13 on 8/28/20.
//  Copyright © 2020 Ivan Grab. All rights reserved.
//

import UIKit

class CatalogTouristPlacesTableViewCell: UITableViewCell {

    //MARK: - IBOutlets

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    @IBOutlet weak var categoryName: UILabel!
    
    //MARK: - Properties

    
    var selectionHandler: ((_ catalog: Place)->())?
    var categoryHandler: ((_ category: PlaceCategory?)->())?
    
    fileprivate var catalogs: [Place] = []

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(with catalogs: [Place]) {
          let name = catalogs.first?.category?.name ?? ""
          categoryName.text = name
          self.catalogs = catalogs
          collectionView.reloadData()
      }

    @IBAction func categoryTapped(_ sender: UIButton) {
        categoryHandler?(catalogs.first?.category)
    }
}

    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate 

extension CatalogTouristPlacesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catalogs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TouristCatalogCollectionViewCell.cellIdentifier, for: indexPath) as! TouristCatalogCollectionViewCell
        cell.setUp(with: catalogs[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let catalog = catalogs[indexPath.item]
        selectionHandler?(catalog)
    }
    
}


