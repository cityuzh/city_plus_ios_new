//
//  ResumeTableViewCell.swift
//  City+
//
//  Created by Ivan Grab on 5/4/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class ResumeTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    
    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var salary: UILabel!
    @IBOutlet weak var resumeTitle: UILabel!
    @IBOutlet weak var workTypeTitle: UILabel!
    @IBOutlet weak var ownerName: UILabel!
    @IBOutlet weak var elapsedTime: UILabel!
    @IBOutlet weak var categoryTitle: UILabel!
    
    @IBOutlet weak var premiumImage: UIImageView!
    
    @IBOutlet weak var favouriteButton: UIButton?
    @IBOutlet weak var conteinerView: UIView!
    
    static var identitfier: String {
        return String(describing: self.init().classForCoder)
    }
    
    var profileHandler: ((_ sender: ResumeTableViewCell)->())?
    var favouriteHandler: ((_ sender: ResumeTableViewCell)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
        ownerName.addGestureRecognizer(gesture)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    @objc func tap(_ sender: UITapGestureRecognizer) {
        profileHandler?(self)
    }
    
    func setUp(with resume: Resume) {
        
        resumeTitle.text = resume.title
        resumeTitle.textColor = resume.isRead ? UIColor.readedJobTitleTintColor : .jobTitleTintColor
//        conteinerView.backgroundColor = resume.isRead ? .white : UIColor.jobTitleTintColor.withAlphaComponent(0.10)
        
        about.text = resume.about
                
        salary.isHidden = resume.salary == nil
        if let salaryNumber = resume.salary {
            salary.text = String(salaryNumber) + " грн"
        }                
        
        elapsedTime.text = resume.createdAt?.getElapsedInterval()
        premiumImage.isHidden = !resume.isPremium
        
        ownerName.text = resume.owner?.fullName
        workTypeTitle.text = resume.workType.title
        categoryTitle.text = resume.category?.name
        
        var image = #imageLiteral(resourceName: "favoritesUnselectedButton")
        if resume.isFavorite {
            image = #imageLiteral(resourceName: "favoriteSelectedButton")
        }
        favouriteButton?.setImage(image, for: .normal)
    }
    
}

fileprivate extension ResumeTableViewCell {
    
    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        favouriteHandler?(self)
    }
    
}
