//
//  VacancyTableViewCell.swift
//  City+
//
//  Created by Ivan Grab on 4/26/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class VacancyTableViewCell: UITableViewCell {

    //MARK: - IBOutlets

    
    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var salary: UILabel!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var elapsedTime: UILabel!
    
    @IBOutlet weak var favouriteButton: UIButton?
    
    @IBOutlet weak var premiumImage: UIImageView!
    @IBOutlet weak var conteinerView: UIView!
    
    static var identitfier: String {
        return String(describing: self.init().classForCoder)
    }
    
    var favouriteHandler: ((_ sender: VacancyTableViewCell)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUp(with vacancy: Vacancy) {

        jobTitle.text = vacancy.title
        jobTitle.textColor = vacancy.isRead ? .readedJobTitleTintColor : .jobTitleTintColor
        
//        conteinerView.backgroundColor = vacancy.isRead ? .white : UIColor.jobTitleTintColor.withAlphaComponent(0.10)
        
        companyName.text = vacancy.companyName
        about.text = vacancy.about
        
        salary.isHidden = vacancy.salary == nil
        if let salaryNumber = vacancy.salary {
            salary.text = String(salaryNumber) + " грн"
        }
        
        elapsedTime.text = vacancy.createdAt?.getElapsedInterval()
        premiumImage.isHidden = !vacancy.isPremium
        
        var image = #imageLiteral(resourceName: "favoritesUnselectedButton")
        if vacancy.isFavorite {
            image = #imageLiteral(resourceName: "favoriteSelectedButton")
        }
        favouriteButton?.setImage(image, for: .normal)
    }
    
    
}

fileprivate extension VacancyTableViewCell {
    
    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        favouriteHandler?(self)
    }
    
}
