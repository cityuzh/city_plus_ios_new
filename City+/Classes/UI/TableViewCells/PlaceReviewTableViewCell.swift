//
//  PlaceReviewTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/15/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class PlaceReviewTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var userImage: UIImageView!
    @IBOutlet fileprivate weak var ratingView: StarsRatingView!
    @IBOutlet fileprivate weak var attachmentImage: UIImageView!
    @IBOutlet fileprivate weak var attachmentImageHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var userName: UILabel!
    @IBOutlet fileprivate weak var createdDate: UILabel!
    @IBOutlet fileprivate weak var reviewComment: UILabel!
    
    var attachmentHandler: ((_ cell: PlaceReviewTableViewCell)->())?
    
    //MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapAttachmentGesture = UITapGestureRecognizer(target: self, action: #selector(touchAttachment(_:)))
        attachmentImage?.isUserInteractionEnabled = true
        attachmentImage?.addGestureRecognizer(tapAttachmentGesture)
        changeFont()
    }
    
    func setUp(with review: PlaceReview) {
        userName.text = review.user?.fullName
        ratingView.rating = NSNumber(value: review.rating)
        if let attach = review.attchment {
            attachmentImage.setImage(with: attach, placeholder: #imageLiteral(resourceName: "no-image"), using: .gray, placeholderSettings: nil, completion: nil)
            attachmentImageHeight.constant = 250
        } else {
            attachmentImageHeight.constant = 0
        }
        userImage.setImage(with: review.user?.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: { (success) in
            if !success {
                self.userImage?.setImageWith(review.user?.fullName ?? "", color: UIColor.lightGray, circular: true)
            }
        })
        createdDate.text = review.createdAt?.formatedPubDate()
        reviewComment.text = review.text
    }
    
    @objc func touchAttachment(_ sender: UITapGestureRecognizer) {
        attachmentHandler?(self)
    }
    
    private func changeFont(){
        userName.font = UIFont.monteserratRegularFont(ofSize: 17.0)
        createdDate.font = UIFont.monteserratRegularFont(ofSize: 14.0)
        reviewComment.font = UIFont.monteserratRegularFont(ofSize: 17.0)
        
    }
}
