//
//  MyTouristPlacesTableViewCell.swift
//  CityPlus
//
//  Created by Ozzy on 02.09.2020.
//  Copyright © 2020 Ivan Grab. All rights reserved.
//

import UIKit

class TouristPlacesTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var touristPlaceImageView: UIImageView!
    @IBOutlet weak var touristPlaceName: UILabel!
    
    func setUp(with model: Place) {
        touristPlaceImageView.setImage(with: model.photos.first, placeholder: #imageLiteral(resourceName: "no-image"), isThumb: false, using: .gray, placeholderSettings: nil) { (success) in
            
        }
        touristPlaceName.text = model.name
        touristPlaceName.font = UIFont.monteserratMediumFont(ofSize: 17)
    }
}
