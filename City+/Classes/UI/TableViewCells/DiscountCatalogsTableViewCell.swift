//
//  DiscountCatalogsTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 12/25/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class DiscountCatalogsTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var catalogsCollectionView: UICollectionView!
    @IBOutlet weak var categoryName: UILabel!
    
    //MARK: - Properties
    
    var selectionHandler: ((_ catalog: DiscountCatalog)->())?
    var categoryHandler: ((_ category: PlaceCategory?)->())?
    
    fileprivate var catalogs: [DiscountCatalog] = []
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUp(with catalogs: [DiscountCatalog]) {
        let name = catalogs.first?.category?.name ?? ""
        categoryName.text = name
        self.catalogs = catalogs
        catalogsCollectionView.reloadData()
    }

}

fileprivate extension DiscountCatalogsTableViewCell {
    
    @IBAction func categoryTapped(_ sender: UIButton) {
        categoryHandler?(catalogs.first?.category)
    }
    
}

extension DiscountCatalogsTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catalogs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CatalogCollectionViewCell.cellIdentifier, for: indexPath) as! CatalogCollectionViewCell
        cell.setUp(with: catalogs[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let catalog = catalogs[indexPath.item]
        selectionHandler?(catalog)
    }
    
}

