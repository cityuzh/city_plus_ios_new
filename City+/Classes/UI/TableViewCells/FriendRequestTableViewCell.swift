//
//  FriendRequestTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/14/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class FriendRequestTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelMyButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    //MARK: - Properties
    
    var cancelHandler: ((_ button: UIButton, _ cell: FriendRequestTableViewCell)->())?
    var acceptHandler: ((_ button: UIButton, _ cell: FriendRequestTableViewCell)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImage.image = nil
        
    }
    
    func setUp(with user: UserModel, isMy: Bool) {
        userImage.setImage(with: user.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: { (success) in
            if !success {
                self.userImage?.setImageWith(user.fullName, color: UIColor.lightGray, circular: true)
            }
        })
        userName.text = user.fullName
        acceptButton.isHidden = isMy
        cancelButton.isHidden = isMy
        cancelMyButton.isHidden = !isMy                        
    }
    
}

fileprivate extension FriendRequestTableViewCell {
    
    @IBAction func cancelAction(_ sender: UIButton) {
        cancelHandler?(sender, self)
    }
    
    @IBAction func acceptAction(_ sender: UIButton) {
        acceptHandler?(sender, self)
    }
}
