//
//  UserTableViewCell.swift
//  City+
//
//  Created by Ivan Grab on 3/22/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var phone: UILabel?
    
    @IBOutlet weak var chatButton: UIButton?
    @IBOutlet weak var friendActionButton: UIButton?
    
    //MARK: - Properties
    
    var chatHandler: ((_ sender: UserTableViewCell)->())?
    var friendHandler: ((_ button: UIButton)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImage.image = nil
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        userImage.layer.cornerRadius = userImage.layer.bounds.width / 2
    }

    func setUp(with user: UserModel) {
        userImage.setImage(with: user.avatar, placeholder: #imageLiteral(resourceName: "profilePlaceholder"), using: .gray, placeholderSettings: nil, completion: { (success) in
            if !success {
                self.userImage?.setImageWith(user.fullName, color: UIColor.lightGray, circular: true)
            }
        })
        userName.text = user.fullName
        phone?.text = user.phone

        friendActionButton?.isSelected = user.isFriend
        friendActionButton?.isEnabled =  !user.hasIncomingRequest && !user.hasOutcomingRequest && !user.isBlockedMe && !user.isBlockedByMe
        friendActionButton?.alpha = user.hasIncomingRequest || user.hasOutcomingRequest ? 0.5 : 1.0
        chatButton?.isEnabled = !user.isBlockedMe && !user.isBlockedByMe
    }

}

fileprivate extension UserTableViewCell {
    
    @IBAction func friendAction(_ sender: UIButton) {
        friendHandler?(sender)
    }
    
    @IBAction func chatAction(_ sender: UIButton) {
        chatHandler?(self)
    }
}
