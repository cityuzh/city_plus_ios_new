//
//  TopicPhotosTableViewCell.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/16/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class TopicPhotosTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var photoCollectionView: UICollectionView!
    @IBOutlet fileprivate weak var photosPageControl: UIPageControl!
    @IBOutlet fileprivate weak var currentImageIndex: UILabel!
    @IBOutlet fileprivate weak var currentImageView: UIView!
    
    fileprivate var images: [Avatar] = []
    
    var photoTappedHandler: ((_ imageIndex: Int)->())?
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let flowLayout = photoCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        photoCollectionView.collectionViewLayout = flowLayout
        
        photoCollectionView.dataSource = self
        photoCollectionView.delegate = self
    }

    func setUp(with topic: Topic) {
        images = topic.attchments
        currentImageIndex.text = "1/\(images.count)"
        currentImageView.isHidden = images.count == 0
        photoCollectionView.reloadData()
    }
    
}

extension TopicPhotosTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photosPageControl.numberOfPages = max(images.count, 1)
        return max(images.count, 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ProfilePhotoCollectionViewCell.classForCoder()), for: indexPath) as! ProfilePhotoCollectionViewCell
        cell.setUp(with: images.count > 0 ? images[indexPath.row] : nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        photoTappedHandler?(indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isKind(of: UICollectionView.classForCoder()) {
            let width = scrollView.bounds.width
            let currentOffset = scrollView.contentOffset.x
            let fullPart: Int = Int(currentOffset / width)
            
            if currentOffset / width - CGFloat(fullPart) > 0.5 {
                photosPageControl.currentPage = fullPart + 1
            } else {
                photosPageControl.currentPage = fullPart
            }
            currentImageIndex.text = "\(photosPageControl.currentPage + 1)/\(photosPageControl.numberOfPages)"
        }
    }
    
}

