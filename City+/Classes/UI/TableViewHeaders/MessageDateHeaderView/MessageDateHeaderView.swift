//
//  MessageDateHeaderView.swift
//  City+
//
//  Created by Ivan Grab on 5/31/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class MessageDateHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var leftSeparator: UIView!
    @IBOutlet weak var rightSeparator: UIView!
    
    var textColor: UIColor! {
        didSet {
            dateLabel.textColor = textColor
            leftSeparator.backgroundColor = textColor
            rightSeparator.backgroundColor = textColor
        }
    }

}
