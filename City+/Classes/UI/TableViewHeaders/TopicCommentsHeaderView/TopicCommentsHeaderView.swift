//
//  TopicCommentsHeaderView.swift
//  CityPlus
//
//  Created by Ivan Grab on 7/24/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class TopicCommentsHeaderView: UITableViewHeaderFooterView {

    var tapHandler: (()->())?
    
}

fileprivate extension TopicCommentsHeaderView {
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        tapHandler?()
    }
    
}
