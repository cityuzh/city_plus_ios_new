//
//  ChatSettings.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/20/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class ChatSettings: NSObject {
    
    var currentBackgroundItem = BackgroundItem(id: "0")
    var currentTextColorItem = TextColorItem(id: "0")

    override init() {
        
    }
    
    init?(coder aDecoder: NSCoder) {
        if let backgroundID = aDecoder.decodeObject(forKey: Keys.background) as? String {
            currentBackgroundItem = BackgroundItem(id: backgroundID)
        }
        if let textColorID = aDecoder.decodeObject(forKey: Keys.textColor) as? String {
            currentTextColorItem = TextColorItem(id: textColorID)
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(currentBackgroundItem.id, forKey: Keys.background)
        aCoder.encode(currentTextColorItem.id, forKey: Keys.textColor)
    }
}

extension ChatSettings {
    
    static func loadOrInitSaved() -> ChatSettings {
        guard let storedData = UserDefaults.standard.object(forKey: "ChatSettings") as? Data, let settinsArray = NSKeyedUnarchiver.unarchiveObject(with: storedData) as? [ChatSettings], let settings = settinsArray.first else {
            return ChatSettings()
        }
        return settings
    }
    
    static func saveSettings(_ settings: ChatSettings) {
        let data = NSKeyedArchiver.archivedData(withRootObject: [settings])
        UserDefaults.standard.set(data, forKey: "ChatSettings")
        UserDefaults.standard.synchronize()
    }
    
}

extension ChatSettings: NSCoding {}

fileprivate extension ChatSettings {
    
    struct Keys {
        static let background = "Chat_Background"
        static let textColor = "Chat_Text_Color"
    }
    
}
