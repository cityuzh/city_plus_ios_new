//
//  WheterManager.swift
//  CityPlus
//
//  Created by Ivan Grab on 8/15/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class WheterManager {

    static let shared = WheterManager()
 
    var dayTemperature: Int = 0
    var nightTemperature: Int = 0
    
    fileprivate lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration)
    }()
    
    func loadWhether(with completion: @escaping ((_ success: Bool)->())) {
        getWhetehrForUzhhorod(with: completion)
    }
    
}

fileprivate extension WheterManager {
    
    struct Constants {
        static let baseURL = "https://api.openweathermap.org/data/2.5"
        static let apiKey = "044ced4b42ce75a7d86fed4758da79c8"
        static let cityName = "Uzhhorod"
        static let countryCode = "UA"
    }
 
    func getWhetehrForUzhhorod(with completion: @escaping ((_ success: Bool)->())) {
        getCurrentWheter(for: Constants.countryCode, with: Constants.cityName, completion: completion)
    }
    
    func getCurrentWheter(for country: String, with cityName: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=Uzhhorod%2CUA&units=metric&appid=044ced4b42ce75a7d86fed4758da79c8") else {
            completion(false)
            return
        }
//        let cityQueryItem = URLQueryItem(name: "g", value: "\(cityName),\(country)")
//        let apiQueryItem = URLQueryItem(name: "appid", value: Constants.apiKey)
//        let metricQueryItem = URLQueryItem(name: "units", value: "metric")
//        let whetherURL = url.appendingPathComponent("forecast?\(cityQueryItem)&\(metricQueryItem)&\(apiQueryItem)")
//
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let jsonObj = json as? [String:Any], let listArray = jsonObj["list"] as? [[String : Any]] {
                    if let firstNight = listArray.first(where: { (whether) -> Bool in
                        if let timeInterval = whether["dt"] as? Double {
                            let date = Date(timeIntervalSince1970: timeInterval)
                            let hours = Calendar.current.component(.hour, from: date)
                            return hours >= 3 && hours <= 5
                        }
                        return false
                    }), let wheter = firstNight["main"] as? [String : Any], let temp = wheter["temp"] as? Double {
                        self.nightTemperature = Int(temp)
                    }
                    if let firstDay = listArray.first(where: { (whether) -> Bool in
                        if let timeInterval = whether["dt"] as? Double {
                            let date = Date(timeIntervalSince1970: timeInterval)
                            let hours = Calendar.current.component(.hour, from: date)
                            return hours >= 12 && hours <= 14
                        }
                        return false
                    }), let wheter = firstDay["main"] as? [String : Any], let temp = wheter["temp"] as? Double {
                        self.dayTemperature = Int(temp)
                    }
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
            if let error = error {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
        task.resume()
    }
    
}

