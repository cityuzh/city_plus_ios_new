//
//  PlaceDataSource.swift
//  CityPlus
//
//  Created by Ivan Grab on 6/8/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class PlaceDataSource: NSObject {

    static let shared = PlaceDataSource()
    
    lazy var itemList: LevelPaginatedListModel<Place> = {
        let list = LevelPaginatedListModel<Place>.init(path: Place.Constants.placeRoute)
        list.queryParams["is_fake"] = false as AnyObject
        return list
    }()
    
    var categories = [PlaceCategory]() {
        didSet {
            if categories.count > 0 {
                itemList.queryParams["category"] = categories.map({ $0.identifier ?? 0}) as AnyObject
            } else {
                itemList.queryParams.removeValue(forKey: "category")
            }
        }
    }
    
    var searchText: String = "" {
        didSet {
            if !searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                itemList.queryParams["name"] = searchText as AnyObject
                itemList.queryParams.removeValue(forKey: "lat")
                itemList.queryParams.removeValue(forKey: "lng")
                itemList.queryParams.removeValue(forKey: "radius")
            } else {
                itemList.queryParams.removeValue(forKey: "name")
            }
        }
    }
    
}
