//
//  ShowAlert.swift
//  City+
//
//  Created by Ivan Grab on 12.02.18.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

class ShowAlert: NSObject {
    
    class func showAlertContrller(with style: UIAlertController.Style, title: String?, message: String?, actions: [UIAlertAction], in controller: UIViewController, completion: (()->Void)? = nil, onCancelAction: (()->())? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        for action in actions {
            alertController.addAction(action)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Відмінити", comment: ""), style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
            DispatchQueue.main.async {
                onCancelAction?()
            }
        }
        alertController.addAction(cancelAction)
        controller.present(alertController, animated: true, completion: completion)
    }
    
    class func showForgotPasswordAlert(controller: UITableViewController, okActionHandler: @escaping ((_ email: String)->())) {
        let alertController = UIAlertController(title: NSLocalizedString("Mot de passe oublié?", comment: "")
            , message: NSLocalizedString("Entrer votre adresse mail enregistrée pour votre compte.", comment: ""), preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = NSLocalizedString("E-mail:", comment: "")
                 textField.keyboardType = .emailAddress
        }
        let okAction = UIAlertAction(title: NSLocalizedString("Envoyer", comment: ""), style: .default, handler: { (action) -> Void in
            if let email = alertController.textFields?.first?.text {
                okActionHandler(email)
            }
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Annuler", comment: ""), style: .cancel, handler: { (action) -> Void in
              alertController.dismiss(animated: true, completion: nil)
        })
 alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        controller.present(alertController, animated: true, completion: nil)

    }
    
    class func showAlertForOneAction(controller: UITableViewController? = nil, firstAction: @escaping (() -> Void), firstActionTitle: String, title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let editAction = UIAlertAction(title: firstActionTitle, style: .default, handler: { (action) -> Void in
            firstAction()
        })
        alertController.addAction(editAction)
        controller?.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlertForTwoActions(controller: UIViewController? = nil, firstAction: (()->Void)? = nil, firstActionTitle: String, title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let editAction = UIAlertAction(title: firstActionTitle, style: .default, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
            firstAction!()
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        })
     
        alertController.addAction(editAction)
           alertController.addAction(cancelAction)
        DispatchQueue.main.async {
          controller?.present(alertController, animated: true, completion: nil)
        }
       
    }
    
    class func showErrorAlert(in controller: UIViewController, error: RESTClient.ErrorType) {
        let alertController = UIAlertController(title: ((Bundle.main.infoDictionary?["CFBundleDisplayName"] ?? "City+" ) as! String), message: error.description, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(okAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    class func showInfoAlertWithCancel(in controller: UIViewController, message: String, okHandler: ((_ action: UIAlertAction)->())? = nil) {
        let alertController = UIAlertController(title: ((Bundle.main.infoDictionary?["CFBundleDisplayName"] ?? "City+" ) as! String), message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Відмінити", comment: ""), style: .cancel, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        })
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
            okHandler?(action)
        })
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    class func showInfoAlert(in controller: UIViewController, message: String, okHandler: ((_ action: UIAlertAction)->())? = nil) {
        let alertController = UIAlertController(title: ((Bundle.main.infoDictionary?["CFBundleDisplayName"] ?? "City+" ) as! String), message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
            okHandler?(action)
        })
        alertController.addAction(okAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    class func showInfoAlertWithOKAction(in controller: UIViewController, message: String, title: String? = "City+") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(okAction)
        controller.present(alertController, animated: true, completion: nil)
    }

    
}
