//
//  EmailValidator.swift
//  City+
//
//  Created by Ivan Grab on 10/10/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

final class EmailValidator: Validator {

    override func validate(_ value: Any?) throws {
        try super.validate(value)
        
        guard let value = value as? String, !value.isEmpty else {
            throw ValueValidationError.invalid("Email обов'язковий!")
        }
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        guard NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: value) else {
            throw ValueValidationError.invalid("Email некоректний.")
        }
    }
    
}
