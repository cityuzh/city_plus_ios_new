//
//  PhoneValidator.swift
//  City+
//
//  Created by Ivan Grab on 2/15/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class PhoneValidator: Validator {

    override func validate(_ value: Any?) throws {
        try super.validate(value)
        
        guard let value = value as? String, !value.isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Телефон є обов'язковим!", comment: ""))
        }
        
        guard value.count >= 11 && value.count <= 15
            else { throw ValueValidationError.invalid(NSLocalizedString("Номер телефону некоректний, наприклад: +380000000000", comment: "")) }
    }
    
}
