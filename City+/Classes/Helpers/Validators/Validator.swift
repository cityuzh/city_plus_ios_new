//
//  Validator.swift
//  City+
//
//  Created by Ivan Grab on 12.02.18.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

class Validator: NSObject {
  
    func validate(_ value: Any?) throws { }

}

enum ValueValidationError: Error {
    
    case invalid(String)
    case required(String)
    
    var localizedDescription: String {
        switch self {
        case .invalid(let message):
            return message
        case .required(let message):
            return message
        }
    }
    
}

