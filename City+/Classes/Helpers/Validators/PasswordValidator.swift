//
//  PasswordValidator.swift
//  City+
//
//  Created by Ivan Grab on 10/10/17.
//  Copyright © 2017 Ivan Grab. All rights reserved.
//

import UIKit

final class PasswordValidator: Validator {

    override func validate(_ value: Any?) throws {
        try super.validate(value)
        
        guard let value = value as? String, !value.isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Пароль є обов'язковим!", comment: ""))
        }
        
        let regex = "^(?=.*\\w).{4,20}$"
        guard NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: value)
            else { throw ValueValidationError.invalid(NSLocalizedString("Пароль повинен складатися з 4 до 20 символів.", comment: "")) }
    }
    
}
